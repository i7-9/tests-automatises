<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>L-TableauVide</name>
   <tag></tag>
   <elementGuidId>2c29560c-67b0-4dc8-bcef-27b3e05ab854</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'm6a7dfd2f_tbod_tempty_tcell_statictext-lb' and @innerHTML = 'Aucune ligne à afficher.']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>m6a7dfd2f_tbod_tempty_tcell_statictext-lb</value>
   </webElementProperties>
</WebElementEntity>
