<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>T-Type DS</name>
   <tag></tag>
   <elementGuidId>041f1e97-7b77-4d69-b79a-61ea520ea64e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'mc22229f9-tb' and @aria-required = 'true']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mc22229f9-tb</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
</WebElementEntity>
