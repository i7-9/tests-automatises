<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MEP07BD</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>1</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8b1cd0b2-d712-4d5f-845c-b8358fb1f72d</testSuiteGuid>
   <testCaseLink>
      <guid>0c4b726c-84c8-4f7a-9222-fe41fca4c868</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>''</defaultValue>
         <description> Permet d'indiquer le no du journal à tester</description>
         <id>2fc089f1-5f0a-473c-a111-89b6eb362d62</id>
         <masked>false</masked>
         <name>vNoJournal</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/SE.R.DSY.CT.MEP07/ValidationJournauxBD</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
