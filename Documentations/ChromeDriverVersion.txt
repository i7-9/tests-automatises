Shutdown ‘Katalon Studio’
Using any browser navigate to: https://chromedriver.storage.googleapis.com/index.html 316
Wait for the page to load
Click the “Name” tab to sort Chromedriver releases
Download the latest version for your OS
Extract & copy Chromedriver.exe to the following KATALON folder:
…\configuration\resources\drivers\chromedriver_win32
Restart ‘Katalon Studio’
Execute any test case through Chrome