import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

int nbTaches
ArrayList<String> arrayTaches = new ArrayList<String>()
ArrayList<String> arrayDurees = new ArrayList<String>()
ArrayList<ArrayList<String>> array = new ArrayList<ArrayList<String>>()

CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()

//Sauvegarde les numeros d'activités
CustomKeywords.'maximo.InterventionKeys.accederAIntervention'()

//Sélectionne l'intervention voulue
CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('A-SuiviInterventions/T-Intervention', 5, null)
CustomKeywords.'maximo.GeneralKeys.setTextElement'('A-SuiviInterventions/T-Intervention', '796772', null)
CustomKeywords.'maximo.GeneralKeys.sendKeys'('A-SuiviInterventions/T-Intervention', [Keys.ENTER], null)
CustomKeywords.'maximo.GeneralKeys.waitForElementValue'('A-SuiviInterventions/L-PremiereInt', 'value', '796772', 5, null)
CustomKeywords.'maximo.GeneralKeys.click'('A-SuiviInterventions/L-PremiereInt', null)

//Navigue à l'onglet Plan
CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('A-SuiviInterventions/P-Int/Tabs/Tab Plans', 10, null)
CustomKeywords.'maximo.GeneralKeys.click'("A-SuiviInterventions/P-Int/Tabs/Tab Plans", null)

//Sauvegarde les données de l'intervention
nbTaches = CustomKeywords.'maximo.InterventionKeys.getNbTaches'()
array = CustomKeywords.'maximo.InterventionKeys.sauvegardeActivites'(nbTaches)
CustomKeywords.'maximo.InterventionKeys.resetTaches'()
arrayTaches = array.get(0)
arrayDurees = array.get(1)

//Navigue à Visual Scheduler
CustomKeywords.'maximo.VisualScheduler.accederAVS'()
//CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()

//Navigue à l'onglet Plan Travail de la première intervention
CustomKeywords.'maximo.GeneralKeys.click'("A-VisualScheduler/P-Liste/L-PremiereIntervention", null)
CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'("A-VisualScheduler/Tabs/Tab-PlanTravail", 20, null)
CustomKeywords.'maximo.GeneralKeys.click'("A-VisualScheduler/Tabs/Tab-PlanTravail", null)
CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'("A-VisualScheduler/P-Plan/NonPlanifie/Img-Filtre", 10, null)

//Assigne les tâches de l'intervention
CustomKeywords.'maximo.VisualScheduler.assigneTaches'(arrayTaches, arrayDurees)