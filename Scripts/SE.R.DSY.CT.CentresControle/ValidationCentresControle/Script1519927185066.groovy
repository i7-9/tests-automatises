import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.testdata.InternalData as InternalData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import org.openqa.selenium.WebElement as WebElement
import org.junit.After
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.Keys as Keys
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import com.kms.katalon.core.webui.driver.DriverFactory
import java.util.List


//Definition des variables

List<WebElement> portlets
WebElement currentPortletElement
WebElement currentPortletLabel
InternalData currentFileCC
String directory = 'Data Files/CentresControle/'

CustomKeywords.'maximo.GeneralKeys.openBrowser'('ACC')
CustomKeywords.'maximo.GeneralKeys.connexion'(GlobalVariable.username)

currentFileCC = findTestData(directory + sFileRef)

CustomKeywords.'maximo.CentresControleKeys.clicRefreshCentreControle'()
CustomKeywords.'maximo.CentresControleKeys.confirmAlertRefreshCentreControle'()

//Recupere l'ensemble des portlets dans un Array de WebElement
int cptPortlet = 0
boolean end = false

'Debut de la boucle de validation des portlets'
while (!end) {
	
	//Against StaleElementReferenceException
	portlets = CustomKeywords.'maximo.GeneralKeys.findElementsBy'(By.className("portletbox"))
	currentPortletElement = portlets.get(cptPortlet)
	currentPortletLabel = currentPortletElement.findElement(By.tagName("label"))
	
	//Parcourir les portlets et comparaison avec le currentFileCC du profil utilisateur
	int cptFileRow = 1
	boolean identique = false
	while (!identique && (cptFileRow <= currentFileCC.getRowNumbers())) {
		println ("Portlet courant : "+ currentPortletLabel.getText() + "comparaison : "+currentFileCC.getValue('Portlets', cptFileRow).toString())		
		//Condition permettant de valider si le portlet actuel existant de le currentFileCC du profil utilisateur
		if(currentPortletLabel.getText() == currentFileCC.getValue('Portlets', cptFileRow).toString()){
			WebUI.executeJavaScript("arguments[0].style.color = 'green'; arguments[0].style.fontSize = 'x-large'", Arrays.asList(currentPortletLabel))
			identique = true
			
			switch (currentPortletLabel.getText()) {
				case "Insertion rapide":
					validateInsertionRapide(currentFileCC)
					break
				case 'Applications favorites':
					validateApplicationsFavorites(currentFileCC)
					break
				case 'Rapports favoris':
					validateRapportsFavoris(currentFileCC)
					break
			}
		}
		cptFileRow++
	}
	
	if(!identique){
		CustomKeywords.'maximo.GeneralKeys.logError'(
			"Le portlet : " + currentPortletLabel.getText() + " n'a pas ete trouve. \n"
			+"Solutions : \n"
			+"1. mettre a jour le centre de demarrage."
			+"2. le portlet est en trop. \n" 
			+"3. le Data File du profil n ' est pas a jour \n"
			+" Profil utilisateur : "+ sFileRef)
	}
		
	cptPortlet++
	
	if(cptPortlet == portlets.size()){
		end = true
	}
}

CustomKeywords.'maximo.GeneralKeys.deconnexion'()

/**
 * Fonction permettant de valider les liens d'insertions rapides
 * @return
 */
def validateInsertionRapide(def TestData datasFile){
	'Validation du contenu du portlet : insertion rapide'
	int cptFileElementRow = 1

	try {
		while ((datasFile.getValue('IR', cptFileElementRow).toString() != '') && (cptFileElementRow < datasFile.getRowNumbers())) {
			//Verifie la pr?sence du lien sur la page
			if(CustomKeywords.'maximo.GeneralKeys.verifyElementPresent'('P-Accueil/CentreControle/InsertionsRapides/'
				 + datasFile.getObjectValue('IR', cptFileElementRow), 2, FailureHandling.OPTIONAL)) {
				CustomKeywords.'maximo.CentresControleKeys.clicInsertionRapide'(datasFile.getObjectValue("IR", cptFileElementRow))
				CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()
				CustomKeywords.'maximo.GeneralKeys.clicBoutonHome'()
				CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()
			} else {
				CustomKeywords.'maximo.GeneralKeys.logError'(("L'insertion rapide : " + datasFile.getObjectValue('IR', cptFileElementRow)) + "n'est pas pr?sente. Valider la configuration du centre de contr?le ou des droits d'acces du profil utilisateur")
			}
			cptFileElementRow++
		}
		
	} catch (Exception e) {
		CustomKeywords.'maximo.GeneralKeys.logError'("Erreur lors de la validation des insertions rapides" + e.printStackTrace())
	}
}

/**
 * Fonction permettant de valider les applications favorites
 * @return
 */
def validateApplicationsFavorites(def TestData datasFile){
	'Validation du contenu du portlet : applications favorites'
	int cptFileElementRow = 1

	try {
		while ((datasFile.getValue('AF', cptFileElementRow).toString() != '') && (cptFileElementRow < datasFile.getRowNumbers())) {
			 //Verifie la pr?sence du lien sur la page
			 if (CustomKeywords.'maximo.GeneralKeys.verifyElementPresent'('P-Accueil/CentreControle/ApplicationsFavorites/'
				 + datasFile.getObjectValue('AF', cptFileElementRow), 3, FailureHandling.OPTIONAL)) {
				 CustomKeywords.'maximo.CentresControleKeys.clicApplicationFavorite'(datasFile.getObjectValue("AF", cptFileElementRow))
				 CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()
				 CustomKeywords.'maximo.GeneralKeys.clicBoutonHome'()
				 CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()
			 } else {
				 CustomKeywords.'maximo.GeneralKeys.logError'("L'application favorites : " + datasFile.getObjectValue('AF', cptFileElementRow) + "n'est pas presente. Valider la configuration du centre de contr?le ou des droits d'acces du profil utilisateur")
			 }
			 cptFileElementRow++
		 }
		
	} catch (Exception e) {
		CustomKeywords.'maximo.GeneralKeys.logError'("Erreur lors de la validation des applications favorites" + e.printStackTrace())
	}
}

/**
 * Fonction permettant de valider les rapports favoris
 * @return
 */
def validateRapportsFavoris(def TestData datasFile){
	'Validation du contenu du portlet : applications favorites'
	/*
	int cptFileElementRow = 1

	try {
		while ((datasFile.getValue('AF', cptFileElementRow).toString() != '') && (cptFileElementRow < datasFile.getRowNumbers())) {
			 //Verifie la pr?sence du lien sur la page
			 if (WebUI.verifyElementPresent(findTestObject('P-Accueil/CentreControle/ApplicationsFavorites/'
				 + datasFile.getObjectValue('AF', cptFileElementRow)), 2, FailureHandling.OPTIONAL)) {
				 
				 WebUI.delay(2)
				 WebUI.click(findTestObject('P-Accueil/CentreControle/ApplicationsFavorites/' + datasFile.getObjectValue("AF", cptFileElementRow)))
				 
				 'Etape permettant de valider la pr?sence de la fenetre d erreur'
				 if(WebUI.verifyElementPresent(findTestObject('P-Accueil/Navbar/AllerA/Alert-Error/Alert'), 3, FailureHandling.OPTIONAL)){
					 WebUI.click(findTestObject('P-Accueil/Navbar/AllerA/Alert-Error/B-Alert-Non'))
				 }
				 
				 'Attendre le chargement de la page'
				 WebUI.waitForPageLoad(20)
				 WebUI.click(findTestObject('P-Accueil/Navbar/B-Home'))
				 
				 'Etape permettant de valider la pr?sence de la fenetre d erreur'
				 if(WebUI.verifyElementPresent(findTestObject('P-Accueil/Navbar/AllerA/Alert-Error/Alert'), 3, FailureHandling.OPTIONAL)){
					 WebUI.click(findTestObject('P-Accueil/Navbar/AllerA/Alert-Error/B-Alert-Non'))
				 }
			 } else {
				 log.logError(("L'application favorites : " + datasFile.getObjectValue('AF', cptFileElementRow)) + "n'est pas pr?sente. Valider la configuration du centre de contr?le ou des droits d'acces du profil utilisateur")
			 }
			 WebUI.delay(2)
			 cptFileElementRow++
		 }
		
	} catch (Exception e) {
		log.logError("Erreur lors de la validation des applications favorites" + e.printStackTrace())
	}*/
}