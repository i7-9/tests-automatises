import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject



import java.util.ArrayList

import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

ArrayList<String> arrayTaches = new ArrayList<String>()
ArrayList<String> arrayMainOeuvre = new ArrayList<String>()
ArrayList<String> arrayArticles = new ArrayList<String>()
ArrayList<String> arrayNoArticles = new ArrayList<String>()
ArrayList<Integer> listQteDispo = new ArrayList<>()
ArrayList<String> listeInformation = new ArrayList<String>()
String noIntervention
String sClassification
TestData DS_TP = TestDataFactory.findTestData("Data Files/Interventions/DF_Interventions")
int currentRow = 1
int maxRows = DS_TP.getRowNumbers()
boolean DAINonTRansfere = true
String nbMsg = ""
int nbArticle = 0
ArrayList<String> listNoTache = new ArrayList<String>()
CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()

//Navigation dans le menu aller à --> Intervention -> Demande de service
//Accéder à l'application d'intervention
CustomKeywords.'maximo.InterventionKeys.accederAIntervention'()

//Creer nouvelle intervention
while (currentRow <= 7) {
	DAINonTRansfere = true
	CustomKeywords.'maximo.GeneralKeys.click'('A-SuiviInterventions/BarreOutilsInt/B-Nouvelle Int', null)
	
	//Rempli les informations de l'intervention
	CustomKeywords.'maximo.InterventionKeys.remplitInfosInt'(
		DS_TP.getValue("Description", currentRow), 
		DS_TP.getValue("Type", currentRow),
		DS_TP.getValue("Mode Execution", currentRow),
		DS_TP.getValue("Atelier", currentRow),
		DS_TP.getValue("NoGO", currentRow),
		DS_TP.getValue("NoActif", currentRow),
		DS_TP.getValue("Emplacement", currentRow),
		null)
	
	noIntervention = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-No Intervention'), 'value')
	//Si l'intervention contient une GO, sauvegarde les données de la GO
	/*if(WebUI.getAttribute(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Details/T-No GO'), 'value') != "") {
		CustomKeywords.'maximo.InterventionKeys.accederAGOImbriquee'()
		sClassification = CustomKeywords.'maximo.GammesOperationsKeys.sauvegardeClassification'()
		arrayTaches = CustomKeywords.'maximo.GammesOperationsKeys.sauvegardeTaches'()
		arrayMainOeuvre = CustomKeywords.'maximo.GammesOperationsKeys.sauvegardeMainOeuvre'()
		arrayArticles = CustomKeywords.'maximo.GammesOperationsKeys.sauvegardeArticles'()
	
		//Sort de la GO imbriquée
		CustomKeywords.'maximo.GeneralKeys.sendKeys'("A-GammesOperations/P-GO/O-GammeOperations/T-No", [Keys.ALT, "r"],null)
	}*/
	
	/*//Clique sur l'onglet Plans
	CustomKeywords.'maximo.GeneralKeys.click'('A-SuiviInterventions/P-Int/Tabs/Plans', null)
	CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('A-SuiviInterventions/P-Int/Plans Tab/T-GO', 10, null)
	CustomKeywords.'maximo.InterventionKeys.verifieTaches'(arrayTaches)
	CustomKeywords.'maximo.InterventionKeys.verifieMainOeuvre'(arrayMainOeuvre)
	CustomKeywords.'maximo.InterventionKeys.verifieArticles'(arrayArticles)
	
	CustomKeywords.'maximo.GeneralKeys.click'('A-SuiviInterventions/P-Int/Tabs/Intervention', null)
	
	//Sauvegarde l'intervention
	//CustomKeywords.'maximo.GeneralKeys.clicEnregistrerObject'()
	
	//Vérifie la classification de la GO dans l'onglet Intervention
	CustomKeywords.'maximo.GeneralKeys.verifyElementValue'('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Classification', 'value', sClassification, 2, null)
	*/
	/*//Change pour l'onglet plan
	for(int i=0; !WebUI.verifyElementNotPresent(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Details/Infos Int/T-No Intervention'), 3, FailureHandling.OPTIONAL) && i<3; i++) {
		CustomKeywords.'maximo.GeneralKeys.click'('A-SuiviInterventions/P-Int/Tabs/Plans', FailureHandling.OPTIONAL)
	}
	
	*/
	
	// Si l'onglet actions correctives est présent, cocher des actions correctives
	if(WebUI.waitForElementPresent(findTestObject('A-SuiviInterventions/P-Int/Tabs/ActionsCorrectives'), 10)){
		CustomKeywords.'maximo.InterventionKeys.selectionnerActionsCorrectives'()
	}

	// Ajouter une ligne d'article
	if(DS_TP.getValue("BesoinArticleSup", currentRow) == "OUI"){
		CustomKeywords.'maximo.InterventionKeys.ajouterArticle'(DS_TP.getValue("Magasin", currentRow))
	}
	
	CustomKeywords.'maximo.InterventionKeys.allerAArticle'()
	WebUI.delay(1)
	// Création d'un liste contenant les numéro d'article présent dans le plan de travail
	//arrayNoArticles = CustomKeywords.'maximo.InterventionKeys.sauveGarderArticles'()
	
	nbArticle = CustomKeywords.'maximo.InterventionKeys.retournerNbArticles'()
	
	// Gérer le besoin en Matériel
	listQteDispo = CustomKeywords.'maximo.InterventionKeys.gestionDuBesoinEnMateriel'(
							DS_TP.getValue("QuantiteDispo", currentRow),
							DS_TP.getValue("Magasin", currentRow),
							nbArticle)
	
	WebUI.delay(1)
	CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
	nbArticle = CustomKeywords.'maximo.InterventionKeys.retournerNbArticles'()
	
	// S'il n' ya pas d'article dans le plan de travailler ajouter un
	if (nbArticle == 0 && currentRow != 1){
		CustomKeywords.'maximo.InterventionKeys.ajouterArticle'(DS_TP.getValue("Magasin", currentRow))
	}
	nbMsg = CustomKeywords.'maximo.InterventionKeys.RetournerNbMSgJournal'()
	
	// Cliquer sur l'onglet intervention
	WebUI.waitForElementPresent(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'), 8)
	WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
	
	// Pour le premier cas de test on vérifie que les msg d'erreurs sont présents lors du changement de statut 	
	if (currentRow == 2)
		CustomKeywords.'maximo.InterventionKeys.validationMSGErr'()
	
	// Entrer un compte GL
	CustomKeywords.'maximo.InterventionKeys.entrerCompteGL'(DS_TP.getValue("CompteGL", currentRow))
	
	// Pour le deuxième cas de test on vérifie que les msg d'erreurs sont présents lors du changement de statut
	if (currentRow == 2)
		CustomKeywords.'maximo.InterventionKeys.validationMSGErr'()
	try{
		// Entrer une date pour objectif de démarrage
		CustomKeywords.'maximo.InterventionKeys.envoyerDate'(DS_TP.getValue("ObjectifDemarrage", currentRow), 'Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos planification/T-ObjectifDemarrage')	
	
		// Changer le statut de l'intervention à Pret à ordonnancer
		CustomKeywords.'maximo.InterventionKeys.changerStatut'('PRET A ORDONNANCER')	
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/FenetreChargement'), 60)
		WebUI.delay(2)
		WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
		
		if(nbArticle > 0){
			// Attendre 40 secondes afin de laisser le temps à l'escalade d'envoyer une message
			WebUI.delay(50)	
		}
		
		// Retourner vers la liste des interventions et réouvrir l'intervention
		CustomKeywords.'maximo.InterventionKeys.reouvrirIntervention'()
		WebUI.delay(2)
		WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
		
		// Valider les messages présents dans le journal
		CustomKeywords.'maximo.InterventionKeys.validationJournal'(nbMsg, nbArticle)
	
		// Enregistrer le nombre de messages présents dans le journal
		nbMsg = CustomKeywords.'maximo.InterventionKeys.RetournerNbMSgJournal'()
				
		// Vérification que la quantité disponibles dans le magasin a changée apres le changement de statut
		if (listQteDispo.size()>0){
			CustomKeywords.'maximo.InterventionKeys.verificationNouvelleQteDispo'(listQteDispo, DS_TP.getValue("Magasin", currentRow))
		}
		// Verfications du statut de l'intevention ainsi que les statuts des tâches
		// Si la quantité ajoutée n,est pas disponible on vérifie que le statu est à:ATTENTMAT sinon on vérifie qu'il est à pret
		 if (DS_TP.getValue("QuantiteDispo", currentRow) == "NON"){
			 CustomKeywords.'maximo.InterventionKeys.validationDesStatuts'(nbArticle, "ATTENTMAT")
		 }else{
		 	CustomKeywords.'maximo.InterventionKeys.validationDesStatuts'(nbArticle, "PRET")
		 }
		//try{
		 boolean datteObjectifModifie = false
			// tant que la DAI n'a pas été transféréeen commande interne
			while(DAINonTRansfere){
				try{
					WebUI.comment ("nbMsg avant modification date objecti" + nbMsg)
					// Envoyer une nouvelle date de demarrage et verifier le journal
					if (DS_TP.getValue("NouveauObjectifDemarrage", currentRow) != "" && datteObjectifModifie == false){
						CustomKeywords.'maximo.InterventionKeys.envoyerDate'(DS_TP.getValue("NouveauObjectifDemarrage", currentRow), 'Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos planification/T-ObjectifDemarrage')
						//Appuyer sur sauvegarder
						CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
						WebUI.waitForPageLoad(20)
						WebUI.delay(5)
						WebUI.waitForElementClickable(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'),20)
						WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
						datteObjectifModifie = true
						// Valider les messages présents dans le journal
						CustomKeywords.'maximo.InterventionKeys.validationJournal'(nbMsg, nbArticle )
						// Enregistrer le nombre de messages présents dans le journal
						nbMsg = CustomKeywords.'maximo.InterventionKeys.RetournerNbMSgJournal'()
					}
					// Entrer une date pour début planifié
					CustomKeywords.'maximo.InterventionKeys.envoyerDate'(DS_TP.getValue("DebutPlanifie", currentRow), 'Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos planification/T-DebutPlanifie')
					CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
					WebUI.waitForPageLoad(20)
					WebUI.delay(15)
					WebUI.waitForElementClickable(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'),20)
					WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
					
					// Changer le statut à cédulé
					if (DS_TP.getValue("QuantiteDispo", currentRow) == "OUI"){
						CustomKeywords.'maximo.InterventionKeys.changerStatut'('CEDULE')
						WebUI.delay(3)
						WebUI.waitForElementNotPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK'), 5)
						WebUI.delay(2)
						WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
					}
					DAINonTRansfere = false		
								
				}catch (Exception d){
					try{
						// Si la DAI n'a pas encore étée reçue
						if(nbArticle > 0 && (WebUI.verifyTextPresent("DAI", false))){
							CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)
							WebUI.delay(200)
						}else{
							CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)
						}
					}catch (Exception e){
						// Si le système indique que l'intervention a été modifiée par un autre utilisateur						
						CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)					
					}
					// On clique sur annuler si necessaire
					if (WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule'), 5)){
						CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule',null)
					}
					// Réouvrir l'intervention
					CustomKeywords.'maximo.InterventionKeys.reouvrirIntervention'()
				}	
			}
			
			// Retourner vers la liste des interventions et réouvrir l'intervention
			CustomKeywords.'maximo.InterventionKeys.reouvrirIntervention'()
			WebUI.delay(2)
			WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
			// Valider les messages présents dans le journal apres l'envoie de la date de debut
			CustomKeywords.'maximo.InterventionKeys.validationJournal'(nbMsg, nbArticle)
			
			// Enregistrer le nombre de messages présents dans le journal
			nbMsg = CustomKeywords.'maximo.InterventionKeys.RetournerNbMSgJournal'()
			
			// Envoyer une nouvelle date pour début planifié et verifier le journal
			if (DS_TP.getValue("NouveauDebutPlanifie", currentRow) != ""){
				CustomKeywords.'maximo.InterventionKeys.envoyerDate'(DS_TP.getValue("NouveauDebutPlanifie", currentRow), 'Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos planification/T-DebutPlanifie')
				//Appuyer sur sauvegarder
				CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
				WebUI.waitForPageLoad(20)
				WebUI.delay(5)
				// Valider les messages présents dans le journal
				CustomKeywords.'maximo.InterventionKeys.validationJournal'(nbMsg, nbArticle )
				// Enregistrer le nombre de messages présents dans le journal
				nbMsg = CustomKeywords.'maximo.InterventionKeys.RetournerNbMSgJournal'()
			}
			
			listeInformation = CustomKeywords.'maximo.InterventionKeys.listeInfoRapport'(noIntervention, DS_TP.getValue("NoActif", currentRow), 
																						DS_TP.getValue("NoGO", currentRow), DS_TP.getValue("DebutPlanifie", currentRow),
																						listeInformation, CustomKeywords.'maximo.InterventionKeys.sauvgarderArticles'())

			currentRow++
	
	
	}catch (Exception e){
		// Gérer la cas ou un message d'erreur apparait indiquant qu'un item n'est pas valide
		if (WebUI.waitForElementPresent(findTestObject('Object Repository/MS-MessageSystem/B-Ok'), 15)){
			try{
				// Si le message d'erreur concerne un article invalide
				if (WebUI.verifyTextPresent("validate", false)){
					CustomKeywords.'maximo.GeneralKeys.logError'("Il ya un item dans la liste d'article qui n'est pas valide")
					CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)
					CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule',null)
					WebUI.waitForElementNotPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule'), 5)
				}
			}catch (Exception r){
			
				CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)
		
				if (WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule'), 5)){
					CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule',null)
				}
			}			
		}
	}

	//Retourner vers la liste des interventions 
	CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
	CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()
}

CustomKeywords.'maximo.InterventionKeys.imprimerListeInfoRapport'(listeInformation)

//Deconnexion
CustomKeywords.'maximo.GeneralKeys.deconnexion'()

/*if (sUseMap.toString().toUpperCase().equals("OUI") && sNoActif != "") {
 'Ajout d\'un actif à partir de la carte'
 for(int i=0; WebUI.verifyElementNotPresent(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Infos Int/L-Actif_Carte'), 5, FailureHandling.OPTIONAL) && i<3; i++) {
	 WebUI.waitForElementClickable(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Infos Int/B-Actif'), 5, FailureHandling.OPTIONAL)
	 WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Infos Int/B-Actif'), FailureHandling.OPTIONAL)
	 WebUI.waitForElementClickable(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Infos Int/L-Actif_Carte'), 5, FailureHandling.OPTIONAL)
	 WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Infos Int/L-Actif_Carte'), FailureHandling.OPTIONAL)
 }
 
 WebUI.waitForPageLoad(20)
 
 'Clique sur le bouton d\'informations'
 WebUI.waitForElementClickable(findTestObject('P-Cartographie/B-Recherche'), 15)
 for(int i=0; !WebUI.verifyElementPresent(findTestObject('P-Cartographie/T-Type Actif'), 3, FailureHandling.OPTIONAL) && i<3; i++) {
	 WebUI.click(findTestObject('P-Cartographie/B-Recherche'), FailureHandling.OPTIONAL)
	 
 }
 WebUI.delay(1)

 'Sélectionne le type d\'actif'
 WebUI.setText(findTestObject('P-Cartographie/T-Type Actif'), 'Bornes incendie')
 WebUI.setText(findTestObject('P-Cartographie/T-Field'), 'Numéro d\'actif')
 WebUI.setText(findTestObject('P-Cartographie/T-Numero Actif'), sNoActif)

 'Recherche l\'actif'
 WebUI.click(findTestObject('P-Cartographie/B-Effectue Recherche'))

 'Ajoute l\'actif aux résultats'
 WebUI.click(findTestObject('P-Cartographie/L-AddToResults'))

 'Ajoute la valeur du résultat à la DS'
 WebUI.click(findTestObject('P-Cartographie/B-Ajout Resultats'))
 WebUI.click(findTestObject('P-Cartographie/L-Test'))
 WebUI.sendKeys(findTestObject('P-Cartographie/L-Test'), Keys.chord(Keys.ESCAPE))

 'Ferme la fenêtre de réultats'
 //WebUI.click(findTestObject('P-Cartographie/B-Ferme Resultats'))
 WebUI.clickOffset(findTestObject('P-Cartographie/Tableau Resultats'), 550, 10)
}*/