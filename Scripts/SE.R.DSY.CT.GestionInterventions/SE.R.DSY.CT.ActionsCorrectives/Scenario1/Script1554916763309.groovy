import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject



import java.util.ArrayList

import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

ArrayList<String> arrayNoInterv = new ArrayList<String>()

TestData DS_TP = TestDataFactory.findTestData("Data Files/ActionsCorrectives/Scenario1")
int currentRow = 1
int maxRows = DS_TP.getRowNumbers()
boolean DAINonTRansfere = true
String nbMsg = ""
int nbArticle = 0
int tempsAttente = 40
boolean erreur
String msg
ArrayList<String> listPosNoTache = new ArrayList<String>()
ArrayList<Integer> listIdTache = new ArrayList<String>()
CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()

//Navigation dans le menu aller à --> Intervention -> Demande de service
//Accéder à l'application d'intervention
CustomKeywords.'maximo.InterventionKeys.accederAIntervention'()




//Creer nouvelle intervention
while (currentRow <= 8) {
	CustomKeywords.'maximo.InterventionKeys.accederAIntervention'()
	CustomKeywords.'maximo.GeneralKeys.click'('A-SuiviInterventions/BarreOutilsInt/B-Nouvelle Int', null)
	
	//Rempli les informations de l'intervention
	CustomKeywords.'maximo.InterventionKeys.remplitInfosInt'(
		DS_TP.getValue("Description", currentRow),
		DS_TP.getValue("Type", currentRow),
		DS_TP.getValue("Mode Execution", currentRow),
		DS_TP.getValue("Atelier", currentRow),
		DS_TP.getValue("NoGO", currentRow),
		DS_TP.getValue("NoActif", currentRow),
		DS_TP.getValue("Emplacement", currentRow),
		DS_TP.getValue("CompteGL", currentRow)
		)
	
	noIntervention = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-No Intervention'), 'value')
	arrayNoInterv.add(noIntervention)
	// Entrer un compte GL
	//CustomKeywords.'maximo.InterventionKeys.entrerCompteGL'(DS_TP.getValue("CompteGL", currentRow))
	
	if(currentRow == 1){
		listPosNoTache = CustomKeywords.'maximo.InterventionKeys.retournerListPosTache'(DS_TP.getValue("HU", currentRow))
	}
	
	CustomKeywords.'maximo.InterventionKeys.selectionnerCertainesActionsCor'(DS_TP.getValue("P1", currentRow), DS_TP.getValue("P2", currentRow), DS_TP.getValue("P3", currentRow), DS_TP.getValue("SPr", currentRow), DS_TP.getValue("HU", currentRow), listPosNoTache)
	WebUI.delay(tempsAttente)
	erreur = CustomKeywords.'maximo.InterventionKeys.validerPrioriteFonctionnaliteActif'(DS_TP.getValue("Priorite actif apres creation", currentRow), DS_TP.getValue("NoActif", currentRow), DS_TP.getValue("État opérationnel apres creation", currentRow))
	
	
	currentRow++
	
	
	
}

currentRow = 1
int i = 0
// Fermer intervention
while (currentRow <= 8) {
	CustomKeywords.'maximo.InterventionKeys.terminerIntervention'(arrayNoInterv.get(i))
	CustomKeywords.'maximo.InterventionKeys.validerPrioriteFonctionnaliteActif'(DS_TP.getValue("Priorite actif apres fermeture", currentRow), DS_TP.getValue("NoActif", currentRow), DS_TP.getValue("État opérationnel apres fermeture", currentRow))
	
	
	i++
	currentRow++
}


CustomKeywords.'maximo.GeneralKeys.deconnexion'()


