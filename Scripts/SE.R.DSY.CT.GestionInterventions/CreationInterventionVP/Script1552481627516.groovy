import java.util.ArrayList


import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.ArrayList

import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


ArrayList<String> listTacheMO = new ArrayList<String>()
ArrayList<String> listAssignementID = new ArrayList<String>()
ArrayList<String> arrayNoActivite = new ArrayList<String>()
ArrayList<String> arrayMainOeuvre = new ArrayList<String>()
ArrayList<String> arrayArticles = new ArrayList<String>()
ArrayList<String> arrayNoArticles = new ArrayList<String>()
ArrayList<Integer> listQteDispo = new ArrayList<>()
ArrayList<String> listeInformation = new ArrayList<String>()
ArrayList<String> listeCodeU = new ArrayList<String>()
String noIntervention 
String sClassification
TestData DS_TP = TestDataFactory.findTestData("Data Files/Interventions/DF_InterventionsVP")
int currentRow = 1
int maxRows = DS_TP.getRowNumbers()
boolean DAINonTRansfere = true
String nbMsg = ""
int nbArticle = 0
int nbActivite = 0
String dateDebut
String emplacement

CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()

//Creer nouvelle intervention
while (currentRow <= maxRows) {
	//Accéder à l'application d'intervention
	CustomKeywords.'maximo.InterventionKeys.accederAIntervention'()
	DAINonTRansfere = true
	CustomKeywords.'maximo.GeneralKeys.click'('A-SuiviInterventions/BarreOutilsInt/B-Nouvelle Int', null)
	
	//Rempli les informations de l'intervention
	CustomKeywords.'maximo.InterventionKeys.remplitInfosInt'(
		DS_TP.getValue("Description", currentRow), 
		DS_TP.getValue("Type", currentRow),
		DS_TP.getValue("Mode Execution", currentRow),
		DS_TP.getValue("Atelier", currentRow),
		DS_TP.getValue("NoGO", currentRow),
		DS_TP.getValue("NoActif", currentRow),
		DS_TP.getValue("Emplacement", currentRow),
		DS_TP.getValue("CompteGL", currentRow))
	
	noIntervention = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-No Intervention'), 'value')
	emplacement = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Emplacement'), 'value')
	// Si l'onglet actions correctives est présent, cocher des actions correctives
	if(WebUI.waitForElementPresent(findTestObject('A-SuiviInterventions/P-Int/Tabs/ActionsCorrectives'), 10)){
		CustomKeywords.'maximo.InterventionKeys.selectionnerActionsCorrectives'()
	}

	// Ajouter une ligne d'article
	if(DS_TP.getValue("BesoinArticleSup", currentRow) == "OUI"){
		CustomKeywords.'maximo.InterventionKeys.ajouterArticle'(DS_TP.getValue("Magasin", currentRow))
	}
	
	CustomKeywords.'maximo.InterventionKeys.allerAArticle'()
	WebUI.delay(1)
	
	nbArticle = CustomKeywords.'maximo.InterventionKeys.retournerNbArticles'()
	arrayNoActivite = CustomKeywords.'maximo.InterventionKeys.changerAtelierTaches'(DS_TP.getValue("Atelier", currentRow))
	nbActivite = arrayNoActivite.size()
	// Gérer le besoin en Matériel
	listQteDispo = CustomKeywords.'maximo.InterventionKeys.gestionDuBesoinEnMateriel'(
							DS_TP.getValue("QuantiteDispo", currentRow),
							DS_TP.getValue("Magasin", currentRow),
							nbArticle)
	
	WebUI.delay(1)
	CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
	nbArticle = CustomKeywords.'maximo.InterventionKeys.retournerNbArticles'()
	
	// S'il n' ya pas d'article dans le plan de travailler ajouter un
	if (nbArticle == 0){
		CustomKeywords.'maximo.InterventionKeys.ajouterArticle'(DS_TP.getValue("Magasin", currentRow))
	}
	nbArticle = CustomKeywords.'maximo.InterventionKeys.retournerNbArticles'()
	
	if (DS_TP.getValue("NoGO", currentRow) == ""){
		CustomKeywords.'maximo.InterventionKeys.ajouterMO'()
	}
	
	nbMsg = CustomKeywords.'maximo.InterventionKeys.RetournerNbMSgJournal'()
	
	// Cliquer sur l'onglet intervention
	WebUI.waitForElementPresent(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'), 8)
	WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
	
	
	
	try{
		// Entrer une date pour objectif de démarrage
		CustomKeywords.'maximo.InterventionKeys.envoyerDate'(DS_TP.getValue("ObjectifDemarrage", currentRow), 'Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos planification/T-ObjectifDemarrage')
	
		// Changer le statut de l'intervention à Pret à ordonnancer
		CustomKeywords.'maximo.InterventionKeys.changerStatut'('PRET A ORDONNANCER')
	
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/FenetreChargement'), 60)
		WebUI.delay(2)
		WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
		
		if(nbArticle > 0){
			// Attendre 40 secondes afin de laisser le temps à l'escalade d'envoyer une message
			WebUI.delay(55)
		}
		
		// Retourner vers la liste des interventions et réouvrir l'intervention
		CustomKeywords.'maximo.InterventionKeys.reouvrirIntervention'()
		
		// Valider les messages présents dans le journal
		CustomKeywords.'maximo.InterventionKeys.validationJournal'(nbMsg, nbArticle)
	
		// Enregistrer le nombre de messages présents dans le journal
		nbMsg = CustomKeywords.'maximo.InterventionKeys.RetournerNbMSgJournal'()
				
		// Vérification que la quantité disponibles dans le magasin a changée apres le changement de statut
		if (listQteDispo.size>0){
			CustomKeywords.'maximo.InterventionKeys.verificationNouvelleQteDispo'(listQteDispo, DS_TP.getValue("Magasin", currentRow))
		}
		// Verfications du statut de l'intevention ainsi que les statuts des tâches
		// Si la quantité ajoutée n,est pas disponible on vérifie que le statu est à:ATTENTMAT sinon on vérifie qu'il est à pret
		 if (DS_TP.getValue("QuantiteDispo", currentRow) == "NON"){
			 CustomKeywords.'maximo.InterventionKeys.validationDesStatuts'(nbArticle, "ATTENTMAT")
		 }else{
			 CustomKeywords.'maximo.InterventionKeys.validationDesStatuts'(nbArticle, "PRET")
		 }
	 
		// Aller à Visual Scheduler
		CustomKeywords.'maximo.VisualScheduler.accederAVS'()
		// Ouvrir plan de travail
		CustomKeywords.'maximo.VisualScheduler.ouvrirPlanTravail'(noIntervention)
		// Recherche l'intervention dans les intervention non ordonnancés
		CustomKeywords.'maximo.VisualScheduler.rechercheNonOrd'(noIntervention,DS_TP.getValue("NoGO", currentRow))
		// Ordonnancer l'intervention
		CustomKeywords.'maximo.VisualScheduler.ordonnancerIntervention'(arrayNoActivite, noIntervention)
		
		// Réouvrir l'intervention et vérifier le statut des tâches si l'intervention n'est pas en ATTENMAT
		if (DS_TP.getValue("QuantiteDispo", currentRow) == "OUI"){
			CustomKeywords.'maximo.InterventionKeys.ouvrirUneIntevention'(noIntervention)
			listTacheMO = CustomKeywords.'maximo.InterventionKeys.listNoTache'()
			CustomKeywords.'maximo.InterventionKeys.verifierStatutTacheMO'("CEDULE", listTacheMO)
			
			//Aller à Visual Assigment
			CustomKeywords.'maximo.VisualAssignment.accederAVS'()
			CustomKeywords.'maximo.VisualAssignment.rechercheOrd'(noIntervention,DS_TP.getValue("NoGO", currentRow))
			// Sauvgarder la list contenant les id des assignation
			listAssignementID = CustomKeywords.'maximo.VisualAssignment.retournerListAssigementID'(DS_TP.getValue("NoGO", currentRow))
			// Assigner la tâche
			CustomKeywords.'maximo.VisualAssignment.assigner'(listAssignementID)
			// Réouvrir l'intervention
			CustomKeywords.'maximo.InterventionKeys.ouvrirUneIntevention'(noIntervention)
			
			if (DS_TP.getValue("NoGO", currentRow) != ""){
				// Récupérer la date de début planifié
				dateDebut = CustomKeywords.'maximo.VisualAssignment.recupererDateDebutPlanifie'()
		
				while(DAINonTRansfere){
					try{
						// Entrer une date pour début planifié
						CustomKeywords.'maximo.VisualAssignment.envoyerDebutPLan'(dateDebut , 'Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos planification/T-DebutPlanifie')
						CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
						WebUI.waitForPageLoad(20)
						WebUI.delay(10)
						WebUI.waitForElementClickable(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'),20)
						WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
				
						// Changer le statut à cedulé
						if (DS_TP.getValue("QuantiteDispo", currentRow) == "OUI"){
							CustomKeywords.'maximo.InterventionKeys.changerStatut'('CEDULE')
							WebUI.delay(3)
							WebUI.waitForElementNotPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK'), 5)
							WebUI.delay(2)
							WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))						
						}								
						DAINonTRansfere = false	
									
					}catch (Exception d){
						try{
							// Si le message d'erreur concerne la DAI on appuie sur OK et on attend 200 secondes
							if(nbArticle > 0 && WebUI.verifyTextPresent("DAI", false)){
								CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)
								WebUI.delay(200)
							// sinon on clique juste sur OK
							}else{
								CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)
							}
						}catch (Exception e){
						
							CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)
						}
						// On clique sur annuler si necessaire
						if (WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule'), 5)){
							CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule',null)
						}
						// Réouvrir l'intervention
						CustomKeywords.'maximo.InterventionKeys.reouvrirIntervention'()
					}
				}
			}

			// Valider les messages présents dans le journal apres l'envoie de la date de debut
			CustomKeywords.'maximo.InterventionKeys.validationJournal'(nbMsg, nbArticle)
		
			// Enregistrer le nombre de messages présents dans le journal
			nbMsg = CustomKeywords.'maximo.InterventionKeys.RetournerNbMSgJournal'()
		}
		
		// Ouvrir l'intervention
		CustomKeywords.'maximo.InterventionKeys.ouvrirUneIntevention'(noIntervention)
		
		// Enregistrer la liste pour le rapport
		listeInformation = CustomKeywords.'maximo.InterventionKeys.listeInfoRapport'(noIntervention, DS_TP.getValue("NoActif", currentRow),
																					DS_TP.getValue("NoGO", currentRow), DS_TP.getValue("DebutPlanifie", currentRow),
																					listeInformation, CustomKeywords.'maximo.InterventionKeys.sauvgarderArticles'())
		if (DS_TP.getValue("QuantiteDispo", currentRow) == "OUI"){
			// Retourner la liste des codeU de la main d'oeuvre qui a été affectée
			listeCodeU = CustomKeywords.'maximo.InterventionKeys.listAffectation'()
			int nbAffectation = 0
		
			// Pour chaque main d'oeuvre, on ouvre sa session Maximo et on vérifie que la tâche et bien présente dans XPRess reporting
			while (nbAffectation < listeCodeU.size() ){
				// Ouvrir Maximo pour chaque main d'oeuvre et aller à XPRess reporting
				CustomKeywords.'maximo.GeneralKeys.reouvrirMaximo'(listeCodeU.get(nbAffectation))
				CustomKeywords.'maximo.XPRessReporting.allerAXPRessRepo'()
				// Vérifier que sa tâche est présente
				CustomKeywords.'maximo.XPRessReporting.rechercheIntervention'(emplacement)
				CustomKeywords.'maximo.XPRessReporting.validerIntervention'(arrayNoActivite, emplacement, noIntervention, DS_TP.getValue("NoGO", currentRow))
				nbAffectation++
			}
		
			//Se déconnecter et se reconnecter avec le compte administrateur
			CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/P-Accueil/Navbar/B-Logout',null)
			CustomKeywords.'maximo.GeneralKeys.connexion'()
		}
		currentRow++
		
	}catch (Exception e){
		// Gérer la cas ou un message d'erreur apparait indiquant qu'un item n'est pas valide
		if (WebUI.waitForElementPresent(findTestObject('Object Repository/MS-MessageSystem/B-Ok'), 15)){
			try{
				// Si le message d'erreur concerne un article invalide
				if (WebUI.verifyTextPresent("validate", false)){
					CustomKeywords.'maximo.GeneralKeys.logError'("Il ya un item dans la liste d'article qui n'est pas valide")
					CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)
					CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule',null)
					WebUI.waitForElementNotPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule'), 5)
				}
			}catch (Exception r){
				CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)
		
				if (WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule'), 5)){
					CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule',null)
				}				
			}			
		}
		//Retourner vers la liste des interventions
		CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
		CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()
	}
}
// Imprimer le rapport
CustomKeywords.'maximo.InterventionKeys.imprimerListeInfoRapport'(listeInformation)
//Deconnexion
CustomKeywords.'maximo.GeneralKeys.deconnexion'()
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	