import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.util.ArrayList
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

ArrayList<String> arrayNoActivite = new ArrayList<String>()
ArrayList<String> arrayMainOeuvre = new ArrayList<String>()
ArrayList<String> arrayArticles = new ArrayList<String>()
ArrayList<String> arrayNoArticles = new ArrayList<String>()
ArrayList<Integer> listQteDispo = new ArrayList<>()
ArrayList<String> listeInformation = new ArrayList<String>()
String noIntervention
String sClassification
TestData DS_TP = TestDataFactory.findTestData("Data Files/Interventions/DF_Interventions")
int currentRow = 1
int maxRows = DS_TP.getRowNumbers()
boolean DAINonTRansfere = true
String nbMsg = ""
int nbArticle = 0

CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()

//Navigation dans le menu aller à --> Intervention -> Demande de service
//Accéder à l'application d'intervention
CustomKeywords.'maximo.InterventionKeys.accederAIntervention'()

//Creer nouvelle intervention
while (currentRow <= maxRows) {
	DAINonTRansfere = true
	CustomKeywords.'maximo.GeneralKeys.click'('A-SuiviInterventions/BarreOutilsInt/B-Nouvelle Int', null)
	
	//Rempli les informations de l'intervention
	CustomKeywords.'maximo.InterventionKeys.remplitInfosInt'(
		DS_TP.getValue("Description", currentRow), 
		DS_TP.getValue("Type", currentRow),
		DS_TP.getValue("Mode Execution", currentRow),
		DS_TP.getValue("Atelier", currentRow),
		DS_TP.getValue("NoGO", currentRow),
		DS_TP.getValue("NoActif", currentRow),
		DS_TP.getValue("Emplacement", currentRow))
	noIntervention = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-No Intervention'), 'value')
	if(WebUI.waitForElementPresent(findTestObject('A-SuiviInterventions/P-Int/Tabs/ActionsCorrectives'), 10)){
		CustomKeywords.'maximo.InterventionKeys.selectionnerActionsCorrectives'()
	}

	// Ajouter une ligne d'article
	if(DS_TP.getValue("BesoinArticleSup", currentRow) == "OUI"){
		CustomKeywords.'maximo.InterventionKeys.ajouterArticle'(DS_TP.getValue("Magasin", currentRow))
	}
	
	CustomKeywords.'maximo.InterventionKeys.allerAArticle'()
	WebUI.delay(1)
	
	nbArticle = CustomKeywords.'maximo.InterventionKeys.retournerNbArticles'()
	nbArticle = CustomKeywords.'maximo.InterventionKeys.retournerNbArticles'()
	
	// Gérer le besoin en Matériel
	listQteDispo = CustomKeywords.'maximo.InterventionKeys.gestionDuBesoinEnMateriel'(
							DS_TP.getValue("QuantiteDispo", currentRow),
							DS_TP.getValue("Magasin", currentRow),
							nbArticle)
	
	WebUI.delay(1)
	CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
	nbArticle = CustomKeywords.'maximo.InterventionKeys.retournerNbArticles'()
	
	arrayNoActivite = CustomKeywords.'maximo.InterventionKeys.changerAtelierTaches'(DS_TP.getValue("Atelier", currentRow))
	nbMsg = CustomKeywords.'maximo.InterventionKeys.RetournerNbMSgJournal'()
	
	// Cliquer sur l'onglet intervention
	WebUI.waitForElementPresent(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'), 8)
	WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))

	// Entrer un compte GL
	CustomKeywords.'maximo.InterventionKeys.entrerCompteGL'(DS_TP.getValue("CompteGL", currentRow))
	
	try{
		// Entrer une date pour objectif de démarrage
		CustomKeywords.'maximo.InterventionKeys.envoyerDate'(DS_TP.getValue("ObjectifDemarrage", currentRow), 'Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos planification/T-ObjectifDemarrage')
	
	
		// Changer le statut de l'intervention à Pret à ordonnancer
		CustomKeywords.'maximo.InterventionKeys.changerStatut'('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-PretAOrdonnancer')

	
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/FenetreChargement'), 60)
		WebUI.delay(2)
		WebUI.click(findTestObject('A-SuiviInterventions/P-Int/Tabs/Intervention'))
		
		if(nbArticle > 0){
			// Attendre 50 secondes afin de laisser le temps à l'escalade d'envoyer une message
			WebUI.delay(50)
		}
		
		// Retourner vers la liste des interventions et réouvrir l'intervention
		CustomKeywords.'maximo.InterventionKeys.reouvrirIntervention'()
		
		// Valider les messages présents dans le journal
		CustomKeywords.'maximo.InterventionKeys.validationJournal'(nbMsg, nbArticle)
	
		// Enregistrer le nombre de messages présents dans le journal
		nbMsg = CustomKeywords.'maximo.InterventionKeys.RetournerNbMSgJournal'()
		
		
		// Vérification que la quantité disponibles dans le magasin a changée apres le changement de statut
		if (listQteDispo.size>0){
			CustomKeywords.'maximo.InterventionKeys.verificationNouvelleQteDispo'(listQteDispo, DS_TP.getValue("Magasin", currentRow))
		}
		// Verfications du statut de l'intevention ainsi que les statuts des tâches
		// Si la quantité ajoutée n,est pas disponible on vérifie que le statu est à:ATTENTMAT sinon on vérifie qu'il est à pret
		 if (DS_TP.getValue("QuantiteDispo", currentRow) == "NON"){
			 CustomKeywords.'maximo.InterventionKeys.validationDesStatuts'(nbArticle, "ATTENTMAT")
		 }else{
			 CustomKeywords.'maximo.InterventionKeys.validationDesStatuts'(nbArticle, "PRET")
		 }
		 
		 CustomKeywords.'maximo.VisualScheduler.accederAVS'()
		 CustomKeywords.'maximo.VisualScheduler.ouvrirPlanTravail'(noIntervention)
		 CustomKeywords.'maximo.VisualScheduler.rechercheNonOrd'(noIntervention)
		 CustomKeywords.'maximo.VisualScheduler.ordonnancerIntervention'(noIntervention, arrayNoActivite)
	
	}catch (Exception e){
	// Gérer la cas ou un message d'erreur apparait indiquant qu'un item n'est pas valide
		if (WebUI.waitForElementPresent(findTestObject('Object Repository/MS-MessageSystem/B-Ok'), 15)){
			if (WebUI.verifyTextPresent("validate", false)==true){
				CustomKeywords.'maximo.GeneralKeys.logError'("Il ya un item dans la liste d'article qui n'est pas valide")
			}
			// clique sur le bouton ok
			CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/MS-MessageSystem/B-Ok',null)
		}
		if (WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule'), 5)){
			CustomKeywords.'maximo.GeneralKeys.click'('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule',null)
			WebUI.waitForElementNotPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule'), 5)
		}
		// Réouvrir l'intervention
		CustomKeywords.'maximo.InterventionKeys.reouvrirIntervention'()
	}
	
	//Retourner vers la liste des interventions
	CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
	CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()
	

}