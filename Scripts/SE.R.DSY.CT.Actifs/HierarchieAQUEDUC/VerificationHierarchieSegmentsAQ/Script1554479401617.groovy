import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject


import java.util.ArrayList

import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable



TestData actifs_TP_Fiche = TestDataFactory.findTestData("Data Files/Actifs/LC/AQUEDUC/FicheActif/LC-Segments")
TestData actifs_TP_Spec = TestDataFactory.findTestData("Data Files/Actifs/LC/AQUEDUC/Specifications/LC-Segments")
int nbActifs = 15
int noRow
int i = 1
int noRowMax = actifs_TP_Fiche.getRowNumbers()
ArrayList<String> arrayAttributs  = new ArrayList<String>()
ArrayList<String> rapportActifsTrouv  = new ArrayList<String>()
ArrayList<String> rapportActifsNonTrouv  = new ArrayList<String>()
String TypeSegment
String noGeo
int noRowSpec
boolean present = false
CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()
CustomKeywords.'maximo.ActifsKeys.accederAActifs'()
CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()


while (i <= nbActifs){
	
	noRow = Math.floor(Math.random() * ((noRowMax-1) - 1 + 1)) + 1
	noRowSpecFonction = ((noRow - 1) * 25 + 1)+24
	noGeo = actifs_TP_Fiche.getValue(8,noRow+1)
	WebUI.comment("noGeoooo" + noGeo)
	TypeSegment = actifs_TP_Spec.getValue(7,(noRowSpecFonction+1))
	WebUI.comment("fonction" + TypeSegment)
	
	// Segements borne d'incendie
	if (TypeSegment == "BRANCHEMENT BORNE INCENDIE"){
		present = CustomKeywords.'maximo.ActifsKeys.rechercherActif'(noGeo,"segment d''aqueduc", TypeSegment, "LC-AQU-BI")
		
	// Segments entrée de service
	}else if (TypeSegment == "BRANCHEMENT SERVICE EAU" || TypeSegment == "BRANCHEMENT SERVICE EAU ET GICLEUR" || TypeSegment == "BRANCHEMENT FEU" || TypeSegment == "BRANCHEMENT GICLEUR"){
		present = CustomKeywords.'maximo.ActifsKeys.rechercherActif'(noGeo,"segment", TypeSegment, "LC-AQU-VANNE")
	
	// Segments autres
	}else{
		present = CustomKeywords.'maximo.ActifsKeys.rechercherActif'(noGeo,"segment", TypeSegment, "LC-AQU-SEG")

	}


	// cliquer sur le bouton afficher liste
	if (CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('ElementsGeneriques/B-AffichageListe', 5, FailureHandling.OPTIONAL)){
		CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
	}
	
	if(present){
		rapportActifsTrouv.add(noGeo)
	}else{
		rapportActifsNonTrouv.add(noGeo)
	
	}
	i++
}

//Imprimer rapport
CustomKeywords.'maximo.ActifsKeys.imprimerListeInfoRapport'(rapportActifsTrouv, "trouve")
CustomKeywords.'maximo.ActifsKeys.imprimerListeInfoRapport'(rapportActifsNonTrouv, "nonTrouve")
