import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject


import java.util.ArrayList

import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable



TestData actifs_TP_Fiche = TestDataFactory.findTestData("Data Files/Actifs/LC/AQUEDUC/FicheActif/LC-Vannes")
TestData actifs_TP_Spec = TestDataFactory.findTestData("Data Files/Actifs/LC/AQUEDUC/Specifications/LC-Vannes")
int nbActifs = 15
int noRow
int i = 1
int noRowMax = actifs_TP_Fiche.getRowNumbers()
ArrayList<String> arrayAttributs  = new ArrayList<String>()
ArrayList<String> rapportActifsTrouv  = new ArrayList<String>()
ArrayList<String> rapportActifsNonTrouv  = new ArrayList<String>()
String requetFiche = ""
String requetSpec
String requete
int noRowSpec

CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()
CustomKeywords.'maximo.ActifsKeys.accederAActifs'()
CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()
String [] champIndesirable = ["ASSETNUM","DESCRIPTION","DESCRIPTION_LONGDESCRIPTION","TEMPLATEID","STATUSDATE","CHANGEDATE","INSTALLDATE","VDM_COORDONNEE_SPATIALE_X","VDM_COORDONNEE_SPATIALE_Y","VDM_CATL_MODELE","VDM_STATUT_POSITION_ACTIF","VDM_STATUT_PROP","LOCATION","VDM_PRIORITE_RESTAURATION"]

while (i <= nbActifs){
	
	noRow = Math.floor(Math.random() * ((noRowMax-1) - 1 + 1)) + 1
	noRowSpec = (noRow - 1) * 47 + 1
	int j = 1
	requetSpec = ""
	//23
	// Contruire la requete pour les spécification
	while (j<=47){

		String requet = CustomKeywords.'maximo.ActifsKeys.constuireRequeteSpecActif'(actifs_TP_Spec, noRowSpec)
		if (requet != ""){
			requetSpec = requetSpec + " and assetnum in (select assetnum from assetspec where SITEID = 'TP'" + requet +")\n"
		}

		noRowSpec++
		j++
	}


	// contruire requete pour les fiches d'actifs
	requetFiche = CustomKeywords.'maximo.ActifsKeys.constuireRequeteFicheActif'(actifs_TP_Fiche, noRow,champIndesirable)
	// fusionner les deux requetes
	requete = requetFiche + requetSpec
	// Voir si l'actif est présent
	Boolean actifFound = CustomKeywords.'maximo.GeneralKeys.ouvrirClauseWhereExecuteRequete'(requete)
	
	if (actifFound == true){
		// L'actif est présent

		CustomKeywords.'maximo.GeneralKeys.logPassed'(
			"La vanne de l'aqueduc : " +
			actifs_TP_Fiche.getValue(8,noRow+1) +
			" est présent dans Maximo avec les bonnes valeurs     "+ requete)
		rapportActifsTrouv.add(actifs_TP_Fiche.getValue(8,noRow+1))
	}else{

		//L'actif n'est pas trouvé
		CustomKeywords.'maximo.GeneralKeys.logError'(
			"La vanne de l'aqueduc : " +
			actifs_TP_Fiche.getValue(8,noRow+1) +
			" n'est pas présent dans Maximo ou contient une/des erreurs     "+ requete)
		rapportActifsNonTrouv.add(actifs_TP_Fiche.getValue(8,noRow+1))
	}
	
	// cliquer sur le bouton afficher liste
	if (CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('ElementsGeneriques/B-AffichageListe', 5, FailureHandling.OPTIONAL)){
		CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
	}
	
	i++
}

//Imprimer rapport
CustomKeywords.'maximo.ActifsKeys.imprimerListeInfoRapport'(rapportActifsTrouv, "trouve")
CustomKeywords.'maximo.ActifsKeys.imprimerListeInfoRapport'(rapportActifsNonTrouv, "nonTrouve")
