import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.util.ArrayList as ArrayList
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

TestData actifs_TP_Fiche = TestDataFactory.findTestData('Data Files/Actifs/LC/EGOUT/FicheActif/LC-Accessoires')

TestData actifs_TP_Spec = TestDataFactory.findTestData('Data Files/Actifs/LC/EGOUT/Specifications/LC-Accessoires')

int nbActifs = 15

int noRow

int i = 1

int noRowMax = actifs_TP_Fiche.getRowNumbers()

ArrayList<String> arrayAttributs = new ArrayList<String>()

ArrayList<String> rapportActifsTrouv = new ArrayList<String>()

ArrayList<String> rapportActifsNonTrouv = new ArrayList<String>()

String noGeoCham

String noGeo

int noRowSpec

boolean present = false

CustomKeywords.'maximo.GeneralKeys.openBrowser'()

CustomKeywords.'maximo.GeneralKeys.connexion'()

CustomKeywords.'maximo.ActifsKeys.accederAActifs'()

CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()

while (i <= nbActifs) {
    noRow = (Math.floor(Math.random() * (((noRowMax - 1) - 1) + 1)) + 1)

    noRowSpecNoGeo = ((((noRow - 1) * 18) + 1) + 11)

    noGeo = actifs_TP_Fiche.getValue(8, noRow + 1)

    noGeoCham = actifs_TP_Spec.getValue(4, noRowSpecNoGeo + 1)

    present = CustomKeywords.'maximo.ActifsKeys.rechercherActif'(noGeo, 'accessoire d\'\'egout', noGeoCham, 'LC-EGO-ACCESS')

    // cliquer sur le bouton afficher liste
    if (CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('ElementsGeneriques/B-AffichageListe', 5, FailureHandling.OPTIONAL)) {
        CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
    }
    
    if (present) {
        rapportActifsTrouv.add(noGeo)
    } else {
        rapportActifsNonTrouv.add(noGeo)
    }
    
    i++
}

//Imprimer rapport
CustomKeywords.'maximo.ActifsKeys.imprimerListeInfoRapport'(rapportActifsTrouv, 'trouve')

CustomKeywords.'maximo.ActifsKeys.imprimerListeInfoRapport'(rapportActifsNonTrouv, 'nonTrouve')

WebUI.acceptAlert()

