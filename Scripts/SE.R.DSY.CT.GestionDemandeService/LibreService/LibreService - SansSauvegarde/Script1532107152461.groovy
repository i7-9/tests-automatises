import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.Keys as Keys

String sNoDS = "0"
TestData DS_TP = TestDataFactory.findTestData("Data Files/DemandesServices/DS_TP_")
//int maxRows = DS_TP.getRowNumbers()
int maxRows = 21
int currentRow = 1
boolean bLoop

CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'(GlobalVariable.username)

//Itère à travers des différentes lignes du fichier DS_TP
while (currentRow <= maxRows) {
	//Navigue à la page libreservice
	CustomKeywords.'maximo.DemandeServiceLSKeys.accederADemandeServiceLS'()
	
	if(CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('A-DemandeService/P-DS/B-PasDeSauvegarde', 2, FailureHandling.OPTIONAL, null)) {
		CustomKeywords.'maximo.GeneralKeys.click'('A-DemandeService/P-DS/B-PasDeSauvegarde', null)
	}
	
	//Remplit les informations de la DS
	CustomKeywords.'maximo.DemandeServiceLSKeys.remplitInfosUtilisateur'(
		DS_TP.getValue("NomPour", currentRow), 
		DS_TP.getValue("TelephonePour", currentRow), 
		DS_TP.getValue("CourrielPour", currentRow))
	
	CustomKeywords.'maximo.DemandeServiceLSKeys.remplitDetailsDS'(
		DS_TP.getValue("NoGDC", currentRow), 
		DS_TP.getValue("NoActif", currentRow), 
		DS_TP.getValue("Type", currentRow), 
		DS_TP.getValue("ModeExec", currentRow), 
		DS_TP.getValue("Atelier", currentRow), 
		DS_TP.getValue("Classification", currentRow),
		DS_TP.getValue("Emplacement", currentRow))
	
	CustomKeywords.'maximo.DemandeServiceLSKeys.remplitAdresse'(
		DS_TP.getValue("AdresseFormatee", currentRow), 
		DS_TP.getValue("Arrondissement", currentRow), 
		DS_TP.getValue("PointReference", currentRow), 
		DS_TP.getValue("Instruction", currentRow))
	
	CustomKeywords.'maximo.DemandeServiceLSKeys.remplitDescription'(
		DS_TP.getValue("Description", currentRow), 
		DS_TP.getValue("DescriptionDetaillee", currentRow))
	
	/*
	//Sauvegarde la DS Libre-Service
	WebUI.click(findTestObject("A-DemandeServiceLS/P-DS LS/B-Soumettre"))
	
	//Sauvegarde le numéro de la DS
	sNoDS = CustomKeywords.'maximo.DemandeServiceLSKeys.sauvegardeNoDS'()
	
	//Appuie sur le bouton Affiche les détails
	WebUI.click(findTestObject('A-DemandeServiceLS/P-DS LS/Pop Confirmation/B-Details'))
	
	//Vérifie les infos de la DS
	CustomKeywords.'maximo.DemandeServiceLSKeys.verifieInfosDSLS'(
		sNoDS, 
		DS_TP.getValue("Type", currentRow),
		DS_TP.getValue("ModeExec", currentRow),
		DS_TP.getValue("Classification", currentRow),
		DS_TP.getValue("NoActif", currentRow),
		DS_TP.getValue("Description", currentRow),
		DS_TP.getValue("DescriptionDetaillee", currentRow))
	
	//Navigation dans le menu aller à --> Interventions -> Demande de service'
	CustomKeywords.'maximo.DemandeServiceKeys.accederADemandeService'()
	
	//Recherche la demande de service créée
	bLoop = true
	while(bLoop) {
		try {
			WebUI.setText(findTestObject('A-DemandeService/P-DS Liste/Numero DS'), sNoDS)
			WebUI.sendKeys(findTestObject('A-DemandeService/P-DS Liste/Numero DS'), Keys.chord(Keys.ENTER))
			WebUI.waitForPageLoad(15)
			WebUI.delay(1)
			WebUI.click(findTestObject('A-DemandeService/P-DS Liste/Premiere DS'))
			bLoop = false
		} catch(Exception ex) {}
	}
	
	//Vérifie les informations de la DS'
	WebUI.verifyElementAttributeValue(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-DS disabled'), 'value', sNoDS, 2)
	CustomKeywords.'maximo.DemandeServiceKeys.verifieInfosDS'(
		DS_TP.getValue("Type", currentRow), 
		DS_TP.getValue("ModeExec", currentRow), 
		DS_TP.getValue("Classification", currentRow), 
		"NON", "NON")
		
		*/
	
	currentRow++
}
