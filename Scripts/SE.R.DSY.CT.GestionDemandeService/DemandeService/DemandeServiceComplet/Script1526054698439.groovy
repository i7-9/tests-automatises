import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

TestData DS_TP = TestDataFactory.findTestData("Data Files/DemandesServices/DF_DS_TP")
int maxRows = DS_TP.getRowNumbers()
int currentRow = 13
String lastMaster = ""

CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()
CustomKeywords.'maximo.DemandeServiceKeys.accederADemandeService'()
CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()

while (currentRow <= maxRows) {
	CustomKeywords.'maximo.GeneralKeys.logInfo'('Information sur le cas en cours  : ' +currentRow)
	//Creer nouvelle DS
	CustomKeywords.'maximo.GeneralKeys.creerNouvelElement'()
	
	CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-DS', 5, null)
	sNoDS = WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-DS'), 'value')
	
	
	CustomKeywords.'maximo.DemandeServiceKeys.remplirNoGdc'(DS_TP.getValue("NoGDC", currentRow))
	
	
	//Remplir la section Détails de la demande de service
	CustomKeywords.'maximo.DemandeServiceKeys.remplirAdresse'(DS_TP.getValue("AdresseFormatee", currentRow), DS_TP.getValue("Arrondissement", currentRow))
	
	//Remplit la section Informations utilisateur si nécessaire
	
	CustomKeywords.'maximo.DemandeServiceKeys.remplirInfosUtilisateur'(
			DS_TP.getValue("NomPour", currentRow),
			DS_TP.getValue("TelephonePour", currentRow),
			DS_TP.getValue("CourrielPour", currentRow),
			DS_TP.getValue("Type", currentRow),
			DS_TP.getValue("Responsable", currentRow))
	
	//Rempli la section de détails de la DS
	CustomKeywords.'maximo.DemandeServiceKeys.remplirDetailsDS'(
		DS_TP.getValue("Description", currentRow), 
		DS_TP.getValue("DescriptionDetaillee", currentRow), 
		DS_TP.getValue("Type", currentRow), 
		DS_TP.getValue("NoActif", currentRow), 
		DS_TP.getValue("ModeExec", currentRow), 
		DS_TP.getValue("Atelier", currentRow), 
		DS_TP.getValue("Classification", currentRow),
		DS_TP.getValue("Emplacement", currentRow),
		currentRow)
	
	
	
	//Si la DS est Master, sauver son numéro pour référence ultérieure (quand les DS non-master seront créees)
	if(DS_TP.getValue("TestDoublon?", currentRow).toUpperCase() == "OUI") {
		if(DS_TP.getValue("IsMaster?", currentRow).toUpperCase() == "OUI") {
			lastMaster = sNoDS
		}
		CustomKeywords.'maximo.DemandeServiceKeys.remplirDoublonDS'(DS_TP.getValue("IsMaster?", currentRow), lastMaster)
	}
		
	CustomKeywords.'maximo.GeneralKeys.clicEnregistrerObject'()
	
	//Sauvegarde du numéro de la DS pour référence ultérieure
	//CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-DS', 5, null)
	
	//Valider les infos de la DS
	CustomKeywords.'maximo.DemandeServiceKeys.verifierInfosDS'(
		DS_TP.getValue("Type", currentRow), 
		DS_TP.getValue("ModeExec", currentRow), 
		DS_TP.getValue("Classification", currentRow),
		DS_TP.getValue("TestDoublon?", currentRow),
		DS_TP.getValue("IsMaster?", currentRow))
	
	
	//Passe à la prochaine ligne dans le fichier excel
	currentRow++
}

	CustomKeywords.'maximo.GeneralKeys.deconnexion'()


/*
'Rempli le champ Actif avec la carte si le numero d\'actif est 2000184'
if (sUseMap.toString().toUpperCase().equals("OUI") && sNoActif != "") {
    'Ajout d\'un actif à partir de la carte'
    WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Actif'), 5, FailureHandling.OPTIONAL)
    WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Actif'))
	WebUI.delay(2)
    WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Actif_Carte'), 5, FailureHandling.OPTIONAL)
    WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Actif_Carte'))
    WebUI.waitForPageLoad(25)

    'Clique sur le bouton d\'informations'
    WebUI.waitForElementClickable(findTestObject('P-Cartographie/B-Recherche'), 15)
	
	for(int i=0; !WebUI.verifyElementPresent(findTestObject('P-Cartographie/T-Type Actif'), 5, FailureHandling.OPTIONAL) && i<3; i++) {
		WebUI.click(findTestObject('P-Cartographie/B-Recherche'), FailureHandling.OPTIONAL)
	}

    WebUI.delay(1)

    'Sélectionne le type d\'actif'
    WebUI.setText(findTestObject('P-Cartographie/T-Type Actif'), 'Bornes incendie')
    WebUI.setText(findTestObject('P-Cartographie/T-Field'), 'Numéro d\'actif')
    WebUI.setText(findTestObject('P-Cartographie/T-Numero Actif'), sNoActif)

    'Recherche l\'actif'
    WebUI.click(findTestObject('P-Cartographie/B-Effectue Recherche'))

    'Ajoute l\'actif aux résultats'
    WebUI.click(findTestObject('P-Cartographie/L-AddToResults'))

    'Ajoute la valeur du résultat à la DS'
    WebUI.click(findTestObject('P-Cartographie/B-Ajout Resultats'))

    'Ferme la fenêtre de réultats'
    WebUI.clickOffset(findTestObject('P-Cartographie/Tableau Resultats'), 555, 10)
}
**/
