import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner as WebUiDriverCleaner
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.thoughtworks.selenium.Selenium as Selenium
import org.openqa.selenium.firefox.FirefoxDriver as FirefoxDriver
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.openqa.selenium.WebDriver as WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium as WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern as Pattern
import static org.apache.commons.lang3.StringUtils.join
import org.eclipse.persistence.internal.oxm.record.json.JSONParser.value_return as value_return
import org.junit.After as After
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.StaleElementReferenceException as StaleElementReferenceException
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

'Joindre L\'url du site'
WebUI.openBrowser(GlobalVariable.site)

'Aggrandit la fen�tre (important pour la cartographie)'
WebUI.maximizeWindow()

'Utilisation d\'un compte par d�faut pour la connexion'
WebUI.comment('Connexion � l\'application avec le login ')

WebUI.setText(findTestObject('P-Login/T-Username'), GlobalVariable.username)

WebUI.setMaskedText(findTestObject('P-Login/T-Passwd'), GlobalVariable.mdp)

WebUI.click(findTestObject('P-Login/B-Login'))

'Attendre le chargement de la page apr�s le login'
WebUI.waitForPageLoad(10)

'Choisir l\'application Demande de service'
WebUI.comment('Navigation dans le menu aller � --> Intervention -> Demande de service')

WebUI.comment('Acc�der � l\'application demande de service LS')

WebUI.click(findTestObject('P-Accueil/Navbar/B-AllerA'))

WebUI.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/Interventions/M-InterventionsM'))

WebUI.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/Interventions/M-DemandeService'))

WebUI.click(findTestObject('P-Accueil/Navbar/AllerA/Interventions/M-DemandeService'))

'Attendre le chargement de la page'
WebUI.waitForPageLoad(10)

'Creer nouvelle DS'
WebUI.click(findTestObject('A-DemandeService/BarreOutils DS/B-NouvelleDS'))

WebUI.waitForPageLoad(10)

Thread.sleep(5000)

'Rempli les champs de la DS � cr�er'
while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Adresse Formatee'), 'value') != sAdresse) {
    WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Adresse Formatee'), sAdresse)
}

while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Arrondissement'), 'value') != sArrondissement) {
    WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Arrondissement'), sArrondissement)
}

while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-PointReference'), 'value') != sPointReference) {
    WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-PointReference'), sPointReference)
}

while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Instructions'), 'value') != sInstructions) {
    WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Instructions'), sInstructions)
}

'Rempli les informations du demandeur si n�cessaire'
if (sNomPour != '') {
    while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-DemandePour'), 'value') != sNomPour) {
        WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-DemandePour'), sNomPour)
    }
    
    while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-Telephone DemandePour'), 'value') != sTelephonePour) {
        WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-Telephone DemandePour'), sTelephonePour)
    }
    
    while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-Courriel DemandePour'), 'value') != sCourrielPour) {
        WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-Courriel DemandePour'), sCourrielPour)
    }
}

'Rempli le champ Description'
while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Description'), 'value') != ('[Katalon] ' + sDescription)) {
    WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Description'), 5, FailureHandling.OPTIONAL)

    WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Description'), '[Katalon] ' + sDescription)
}

'Rempli le champ Description D�taill�e si n�cessaire'
if (sDescriptionDetaillee != '') {
    WebUI.switchToFrame(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/Iframe Description'), 0)

    /*while(WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/TA-Description'), 'value') != sDescriptionDetaillee) {
		WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/TA-Description'), sDescriptionDetaillee)
	}*/
    WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/TA-Description'), sDescriptionDetaillee)

    'Switch To Default Context est n�cessaire afin de garder une valeur dans la description d�taill�e'
    WebUI.switchToDefaultContent()
}

'Rempli le champ Type'
while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Type DS'), 'value') != sType) {
    WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Type DS'), sType)
}

'Rempli le champ Actif avec la carte si le numero d\'actif est 2000184'
if (sNoActif == '2000184') {
    'Ajout d\'un actif � partir de la carte'
    WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Actif'), 5, FailureHandling.OPTIONAL)

    WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Actif'))

    WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Actif_Carte'), 5, FailureHandling.OPTIONAL)

    WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Actif_Carte'))

    WebUI.waitForPageLoad(20)

    'Clique sur le bouton d\'informations'
    WebUI.waitForElementClickable(findTestObject('P-Cartographie/B-Informations'), 10)

    WebUI.click(findTestObject('P-Cartographie/B-Informations'))

    WebUI.delay(1)

    'Clique sur l\'actif'
    WebUI.clickOffset(findTestObject('P-Cartographie/Div Cartographie'), 575, 300)

    WebUI.delay(1)

    'Navigue jusqu\'� l\'actif'
    for (int i = 0; i < 7; i++) {
        WebUI.click(findTestObject('P-Cartographie/B-NextEntity'))
    }
    
    'Ajoute l\'actif aux r�sultats'
    WebUI.click(findTestObject('P-Cartographie/L-AddToResults'))

    'D�selectionne tous les r�sultats'
    WebUI.delay(1)

    WebUI.click(findTestObject('P-Cartographie/B-Deselectionne Resultats'))

    'S�lectionne le r�sultat voulu'
    WebUI.delay(1)

    WebUI.clickOffset(findTestObject('P-Cartographie/Tableau Resultats'), 70, 210)

    'Ajoute la valeur du r�sultat � la DS'
    WebUI.click(findTestObject('P-Cartographie/B-Ajout Resultats'))

    'Ferme la fen�tre de r�ultats'
    WebUI.clickOffset(findTestObject('P-Cartographie/Tableau Resultats'), 555, 10)
} else if (sNoActif != '') {
    while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Actif'), 'value') != sNoActif) {
        WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Actif'), sNoActif)
    }
}

WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Execution'), 5, FailureHandling.OPTIONAL)

'Rempli le mode d\'ex�cution'
switch (sExec) {
    case 'PHC':
        while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Execution'), 'value', FailureHandling.OPTIONAL) == '') {
            WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Execution'), 2, FailureHandling.OPTIONAL)

            WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Execution'), FailureHandling.OPTIONAL)

            WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_PHC'), 5, FailureHandling.OPTIONAL)

            WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_PHC'), FailureHandling.OPTIONAL)

            WebUI.waitForElementNotPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_PHC'), 5, FailureHandling.OPTIONAL)
        }
        
        break
    case 'PLAN':
        while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Execution'), 'value', FailureHandling.OPTIONAL) == '') {
            WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Execution'), 2, FailureHandling.OPTIONAL)

            WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Execution'), FailureHandling.OPTIONAL)

            WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_PLAN'), 5, FailureHandling.OPTIONAL)

            WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_PLAN'), FailureHandling.OPTIONAL)

            WebUI.waitForElementNotPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_PHC'), 5, FailureHandling.OPTIONAL)
        }
        
        break
    case 'UHR':
        while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Execution'), 'value', FailureHandling.OPTIONAL) == '') {
            WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Execution'), 2, FailureHandling.OPTIONAL)

            WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Execution'), FailureHandling.OPTIONAL)

            WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_UHR'), 5, FailureHandling.OPTIONAL)

            WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_UHR'), FailureHandling.OPTIONAL)

            WebUI.waitForElementNotPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_PHC'), 5, FailureHandling.OPTIONAL)
        }
        
        break
    case 'URT':
        while (WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Execution'), 'value', FailureHandling.OPTIONAL) == '') {
            WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Execution'), 2, FailureHandling.OPTIONAL)

            WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Execution'), FailureHandling.OPTIONAL)

            WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_URT'), 5, FailureHandling.OPTIONAL)

            WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_URT'), FailureHandling.OPTIONAL)

            WebUI.waitForElementNotPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Exec_PHC'), 5, FailureHandling.OPTIONAL)
        }
        
        break
    default:
        new KeywordLogger().logError('Mode d\'ex�cution invalide')}

'Rempli le champ Atelier'
WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Atelier'))

WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Atelier_TP'), 5, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Atelier_TP'))

WebUI.waitForElementNotPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-Atelier_TP'), 5, FailureHandling.OPTIONAL)

'Sauvegarde la DS'
WebUI.waitForElementClickable(findTestObject('A-DemandeService/BarreOutils DS/B-Save'), 5, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('A-DemandeService/BarreOutils DS/B-Save'))

'S\'il y a un pop-up lors de la sauvegarde, une erreur s\'est produite. Arr�t de la s�quence de test'
WebUI.verifyElementNotPresent(findTestObject('A-DemandeService/P-DS/Erreur/ErrorCreation'), 5)

'Sauvegarde du num�ro de la DS pour r�f�rence ult�rieure'
sNoDS = WebUI.getAttribute(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-DS'), 'value')

'D�lais pour assurer la cr�ation de la DS'
Thread.sleep(10000)

'---- V�rification du Workflow ----'
switch (sType) {
    case 'MC':
        'V�rifie le statut de la DS'
        WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut'), 20, FailureHandling.OPTIONAL)

        if ((sExec != 'URT') && (sExec != 'UHR')) {
            WebUI.verifyElementAttributeValue(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut'), 'value', 'PREP', 5, FailureHandling.CONTINUE_ON_FAILURE)
        } else {
            WebUI.verifyElementAttributeValue(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut'), 'value', 'ENCRS', 5)
        }
        
        'V�rifie que l\'intervention ait bien �t� cr�e'
        WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/Tabs/Enregistrements Associes'), 5, FailureHandling.OPTIONAL)

        WebUI.click(findTestObject('A-DemandeService/P-DS/Tabs/Enregistrements Associes'))

        WebUI.verifyElementNotPresent(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/Interventions vide'), 5)

        WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'), 5, FailureHandling.OPTIONAL)

        WebUI.click(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'))

        WebUI.click(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/L-Intervention'))

        'V�rifie le statut de l\'intervention'
        if ((sExec != 'URT') && (sExec != 'UHR')) {
            WebUI.verifyElementAttributeValue(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Statut'), 'value', 'ATTAPPRO', 5)
        } else {
            WebUI.verifyElementAttributeValue(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Statut'), 'value', 'ATTEXEC', 5)
        }
        
        'V�rifie le mode d\'execution et le type'
        WebUI.verifyElementAttributeValue(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Execution'), 'value', sExec, 5)

        WebUI.verifyElementAttributeValue(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Type'), 'value', sType, 5)

        break
    case 'AME':
        'DS Statut ANALYSE, DS visible dans le CDC du planificateur'
        WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut'), 20, FailureHandling.OPTIONAL)

        WebUI.verifyElementAttributeValue(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut'), 'value', 'ANALYSE', 5)

        break
    case 'ADOC':
        WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut'), 20, FailureHandling.OPTIONAL)

        'DS Statut NOUVEAU, DS visible dans le CDC du pilote expert entretien'
        WebUI.verifyElementAttributeValue(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut'), 'value', 'NOUVEAU', 5)

        break
}

