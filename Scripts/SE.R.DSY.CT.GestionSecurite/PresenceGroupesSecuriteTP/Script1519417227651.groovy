import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.testdata.InternalData as InternalData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//R�cup�ration du dataFile (Data Files/Securite/Groupes) dans une variable pour le parcourir'
InternalData groupes = findTestData('Data Files/Securite/Groupes')
int currentObject = 1

//Acc�der � la page de connexion du site
WebUI.openBrowser(GlobalVariable.site)
//Aggrandit la fen�tre
WebUI.maximizeWindow()

//Connexion avec l'utilisateur du test en cours
CustomKeywords.'maximo.GeneralKeys.connexion'('uaissid')

//Navigation dans le menu aller � --> S�curite d'acc�s -> Groupe de s�curit�
CustomKeywords.'maximo.GeneralKeys.click'('P-Accueil/Navbar/B-AllerA', null)
CustomKeywords.'maximo.GeneralKeys.mouseOver'('P-Accueil/Navbar/AllerA/SecuriteAcces/M-SecuriteAccesM', null)
CustomKeywords.'maximo.GeneralKeys.mouseOver'('P-Accueil/Navbar/AllerA/SecuriteAcces/M-GroupeSecurite', null)
CustomKeywords.'maximo.GeneralKeys.click'('P-Accueil/Navbar/AllerA/SecuriteAcces/M-GroupeSecurite', null)

//�tape permettant de valider la pr�sence de la fen�tre d'erreur'
if(CustomKeywords.'maximo.GeneralKeys.verifyElementPresent'('P-Accueil/Navbar/AllerA/Alert-Error/Alert', 3, null)){
	CustomKeywords.'maximo.GeneralKeys.click'('P-Accueil/Navbar/AllerA/Alert-Error/B-Alert-Non', null)
}

//D�but de la validation de la pr�sence des groupes de s�curit�
while (currentObject <= groupes.data.size()) {
	//Acc�der a la clause WHERE
	CustomKeywords.'maximo.GeneralKeys.click'('A-GroupesSecurite/P-GroupesSecurite Liste/B-ZoneRecherchePlus', null)
	CustomKeywords.'maximo.GeneralKeys.mouseOver'('A-GroupesSecurite/P-GroupesSecurite Liste/B-ClauseWhere', null)
	CustomKeywords.'maximo.GeneralKeys.click'('A-GroupesSecurite/P-GroupesSecurite Liste/B-ClauseWhere', null)
	WebUI.delay(1)
	
	//Saisir la clause WHERE
	CustomKeywords.'maximo.GeneralKeys.setTextElement'('A-GroupesSecurite/P-GroupesSecurite Liste/ClauseWhere/TA-ClauseWhere', 
		"upper(groupname) = upper('" + groupes.getObjectValue("GroupeSecurite", currentObject)+"')", null)
	
	//Ex�cuter la recherche du groupe de s�curit�
	CustomKeywords.'maximo.GeneralKeys.click'('A-GroupesSecurite/P-GroupesSecurite Liste/ClauseWhere/B-Rechercher', null)
	WebUI.delay(1)
	
	//Analyse du resultat de la recherche du groupe de securite
	if(CustomKeywords.'maximo.GeneralKeys.verifyElementNotPresent'('A-GroupesSecurite/P-GroupesSecurite Liste/ClauseWhere/Alert', 1, null)){
		CustomKeywords.'maximo.GeneralKeys.click'('A-GroupesSecurite/P-GroupesSecurite Details/B-AffichageListe', null)
	}else{
		//Le groupe n'existe pas
	}
	
	//Attendre le chargement de la page
	WebUI.delay(1)
	currentObject++
}

//D�connexion de l'utilisateur
WebUI.click(findTestObject('P-Accueil/Navbar/B-Logout'))