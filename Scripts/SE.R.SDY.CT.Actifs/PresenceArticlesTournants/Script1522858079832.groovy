import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

TestData articlestournants_TP = TestDataFactory.findTestData("Data Files/ArticlesTournants/ArticlesTournants_TP_")

CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()
CustomKeywords.'maximo.ReferentielArticlesKeys.accederAReferentielArticles'()
//CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()

int maxRows = articlestournants_TP.getRowNumbers()
int currentRow = 1
String sql = 'itemnum like'

while (currentRow <= maxRows) {
	if(CustomKeywords.'maximo.GeneralKeys.ouvrirClauseWhereExecuteRequete'(sql +"'%"+articlestournants_TP.getValue("Article", currentRow)+"'"))
	{
		CustomKeywords.'maximo.ReferentielArticlesKeys.comparerNoArticle'(articlestournants_TP.getValue("Article", currentRow))
		CustomKeywords.'maximo.ReferentielArticlesKeys.comparerDescription'(articlestournants_TP.getValue("Description", currentRow))
		CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
		
	}else{
	CustomKeywords.'maximo.GeneralKeys.logError'("L'article tournant : "
		+" "+articlestournants_TP.getValue("Article", currentRow) 
		+" est introuvable!")
	}
	WebUI.delay(1)
	currentRow++
}
CustomKeywords.'maximo.GeneralKeys.deconnexion'()