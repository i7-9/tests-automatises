import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.text.SimpleDateFormat
import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testdata.TestData as TestData

CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()

TestData DS_TP = TestDataFactory.findTestData("Data Files/ApproVerificationStocks/DF_VerificationStocks")
int currentRow = 1
int maxRows = DS_TP.getRowNumbers()
// on choisi les articles ayant un position entre 0 et 250
int posMax = 250
// On choisi en tout 150 articles
int nbArticle = 150

// Aller à gestion des stocks
accerderAGestionStocks()

String clauseWhereCasier = "((itemnum in(select itemnum from  invbalances where  upper(binnum) is null or upper(binnum) like 'DEFAUT' and (location = 'CDM-DISTR' or location = 'LAC-DISTR')))) and (((location = 'CDM-DISTR' or location = 'LAC-DISTR')))"
String clauseWhereQuantite = "((itemnum in(select itemnum  from  invbalances where  invbalances.curbal > 900 and (location = 'LAC-DISTR' or location = 'LAC-DISTR')))) and (((location = 'CDM-DISTR' or location = 'LAC-DISTR')))"
// Faire une clause where pour les articles qui n'ont pas un numéro de casier valide
doClauseWhere(clauseWhereCasier, "Casier")
// Faire un clause where pour les articles qui n'ont pas une bonne quantité en stock
doClauseWhere(clauseWhereQuantite, "Quantite en stock")
// Effacer la clause where
WebUI.delay(1)
effacerClauseWhere()
	

while (currentRow <= maxRows) {
	// On choisi le magasin désiré
	WebUI.delay(1)
	choisirMagasin(DS_TP.getValue("Magasin", currentRow))
	
	for (int i = 0; i < nbArticle; i++){
		// On choisi un nombre aléatoire pour la position de l'article
		int pos = Math.floor(Math.random() * ((posMax-1) - 0 + 1)) + 0;
		boolean loop = true
		
		while (loop){
			// On enregistre la position maximale et minimale des articles présents sur la page
			String [] nbArt = WebUI.getText(findTestObject('Object Repository/A-GestionStocks/T-NbArticle')).split(" ")
			int posArtMin
			int posArtMax = Integer.parseInt(nbArt[nbArt.size()-3])
			try{
				posArtMin = Integer.parseInt(nbArt[nbArt.size()-7])
			}catch (Exception e){
				posArtMin = Integer.parseInt(nbArt[nbArt.size()-6])
			}
		
			// Si l'article desiré est visible on clique dessus
			if (pos >=(posArtMin-1) && pos < posArtMax){
				TestObject article = CustomKeywords.'maximo.GeneralKeys.convertWebElement'("m6a7dfd2f_tdrow_[C:1]_ttxt-lb[R:"+pos+"]")
				WebUI.waitForElementPresent(article,5)
				WebUI.click(article)
				loop=false
			
			}
		
			// Si la position de l'article est plus petite que la position minimale on appuie sur le bouton page precedente
			if (pos < posArtMin-1 && WebUI.getAttribute(findTestObject('Object Repository/A-GestionStocks/B-PagePrecedente'), 'ev') == 'true') {
				WebUI.click(findTestObject('Object Repository/A-GestionStocks/B-PagePrecedente'))
				WebUI.waitForPageLoad(10)
				WebUI.delay(1)
			// Si la position de l'article est plus grande ou égale à la position maximale on appuie sur le bouton page suivante
			}else if (pos >= posArtMax  && WebUI.getAttribute(findTestObject('Object Repository/A-GestionStocks/B-PageSuivante'), 'ev') == 'true'){
				WebUI.click(findTestObject('Object Repository/A-GestionStocks/B-PageSuivante'))
				WebUI.waitForPageLoad(10)
				WebUI.delay(1)
			}
		}
	
		// On verifie les valeurs présentes dans les champs casier et quantite en stock
		WebUI.delay(1)
		verificationArticle()
	}

	currentRow++
}


/**
 * Fonction qui permet de choisir le magasin désiré
 * 
 * @param magasin
 * @return
 */
def choisirMagasin(String magasin){

	WebUI.waitForElementPresent(findTestObject('Object Repository/A-GestionStocks/T-RechercheMagasin'), 5)
	// saisir le nom du magasin désiré
	WebUI.click(findTestObject('Object Repository/A-GestionStocks/T-RechercheMagasin'))
	WebUI.setText(findTestObject('Object Repository/A-GestionStocks/T-RechercheMagasin'), "=" + magasin)
	// Appuyer sur entrer pour afficher les articles
	WebUI.sendKeys(findTestObject('Object Repository/A-GestionStocks/T-RechercheMagasin'), Keys.chord(Keys.ENTER))
	WebUI.delay(1)
}


/**
 * Fonction qui permet de verifer les champs casier et quantite en stocks et valider si ces champs possède une valeur valide ou non
 * @return
 */
def verificationArticle(){
	// Enregistrer le numéro de l'article
	String noArticle = WebUI.getAttribute(findTestObject('Object Repository/A-GestionStocks/Article/T-NoArticle'), 'value')
	
	try{
		// Enregistrer la valeur présente dans les champs casier et quantit en stock
		String casier = WebUI.getAttribute(findTestObject('Object Repository/A-GestionStocks/Article/T-Casier'), 'value')
		String quantiteStock = WebUI.getAttribute(findTestObject('Object Repository/A-GestionStocks/Article/T-QuantiteStock'), 'value')
		String [] qte = quantiteStock.split(",")
		
		WebDriver driver = DriverFactory.getWebDriver()
		WebElement element
	
		// Si le champ casier contient une valeur et si cette valeur n'est pas defaut on valide et on le colorie en vert sinon on le colorie en rouge
		element = driver.findElement(By.id('ma1a38afd_tdrow_[C:1]_txt-tb[R:0]'))
		if (casier!= "" && casier != "DEFAUT"){
			CustomKeywords.'maximo.GeneralKeys.logPassed'("L'article "+noArticle+" se trouve dans un casier identifié par un nom")
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
		
		}else{
			CustomKeywords.'maximo.GeneralKeys.logError'("Le casier de l'article "+noArticle+" n'a pas de nom ou son nom est DEFAUT")
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))

		}
		// Si le champ quantite en stock contient une valeur plus petite que 9000 on valide et on le colorie en vert sinon on le colorie en rouge
		element = driver.findElement(By.id('ma1a38afd_tdrow_[C:4]_txt-tb[R:0]'))
		if (Integer.parseInt(qte[0]) < 9000){
			CustomKeywords.'maximo.GeneralKeys.logPassed'("La quantité en stock de l'article "+noArticle+" est inférieure à 9000")
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
		
		}else{
			CustomKeywords.'maximo.GeneralKeys.logError'("La quantité en stock de l'article "+noArticle+" est supérieure à 9000")
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
			
		}
		
	}catch (Exception e){
		CustomKeywords.'maximo.GeneralKeys.logError'("L'article "+noArticle+" n'a pas de champs pour la quantité en stock ou/et pour le casier")
	}
	// Cliquer sur le bouton affichage en liste
	WebUI.delay(1)
	CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
	
}


/**
 * Fonction qui permet d'aller dans la gestion des stocks
 */
def accerderAGestionStocks(){
	WebUI.waitForElementPresent(findTestObject('Object Repository/P-Accueil/Navbar/B-AllerA'), 5)
	WebUI.click(findTestObject('Object Repository/P-Accueil/Navbar/B-AllerA'))
	WebUI.waitForElementPresent(findTestObject('Object Repository/P-Accueil/Navbar/AllerA/Stock/M-StockM'), 5)
	WebUI.click(findTestObject('Object Repository/P-Accueil/Navbar/AllerA/Stock/M-StockM'))
	WebUI.waitForElementPresent(findTestObject('Object Repository/P-Accueil/Navbar/AllerA/Stock/B-GestionStocks'), 5)
	WebUI.click(findTestObject('Object Repository/P-Accueil/Navbar/AllerA/Stock/B-GestionStocks'))
}


/**
 * Fonction qui permet de faire un clause where et de voir s'il ya des articles qui n'ont pas de bonnes valleurs dans le champ casier ou/et dans le cham quantite en stock
 * 
 * @param clauseWhere représente la requête SQL
 * @param nomChamp remprésente le nom du champ soit casier soit quantie en stock
 * @return
 */
def doClauseWhere(String clauseWhere, String nomChamp){
	
	try{
		// Accéder à la clause where
		WebUI.delay(1)
		WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/B-ZoneRecherchePlus'), 5)
		WebUI.click(findTestObject('Object Repository/ElementsGeneriques/B-ZoneRecherchePlus'))
		WebUI.click(findTestObject('Object Repository/ElementsGeneriques/B-ClauseWhere'))
		WebUI.delay(1)
		// Entrer la requête SQL
		WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/TA-ClauseWhere'), 5)
		WebUI.click(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/TA-ClauseWhere'))
		WebUI.setText(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/TA-ClauseWhere'), clauseWhere)
		//Cliquer sur le bouton rechercher
		WebUI.click(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/B-Rechercher'))
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/B-Rechercher'), 5)
		WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/B-OKnotFound'), 1)
		// S'il n'y a aucun résultat un message apprait on appuie sur OK 
		WebUI.click(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/B-OKnotFound'))
		CustomKeywords.'maximo.GeneralKeys.logError'("Tous les articles possèdent la bonne valeur dans le champ"+ nomChamp)
	}catch (Exception e){
		// S'il ya présence articles à la suite de la recherche SQL on enregistre le nombre d'article présent
		String [] nbArt = WebUI.getText(findTestObject('Object Repository/A-GestionStocks/T-NbArticle')).split(" ")
		CustomKeywords.'maximo.GeneralKeys.logPassed'("Il y a "+nbArt[nbArt.size()-1]+" articles qui n'ont pas la bonne valeur dans le champ "+ nomChamp)
	}
	
}


/**
 * Fonction qui permet de supprimer toute requête présente dans le clause where
 * @return
 */
def effacerClauseWhere(){
	// Accéder à la clause where
	WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/B-ZoneRecherchePlus'), 5)
	WebUI.click(findTestObject('Object Repository/ElementsGeneriques/B-ZoneRecherchePlus'))
	WebUI.click(findTestObject('Object Repository/ElementsGeneriques/B-ClauseWhere'))
	WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/TA-ClauseWhere'), 5)
	WebUI.click(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/TA-ClauseWhere'))
	// Supprimer la requête
	WebUI.setText(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/TA-ClauseWhere'), " ")
	//Appuyer sur le bouton rechercher
	WebUI.click(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/B-Rechercher'))
}

