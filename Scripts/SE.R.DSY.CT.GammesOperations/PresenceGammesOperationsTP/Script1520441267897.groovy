import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.util.List
def array_go = TestDataFactory.findTestData("Data Files/GammesOperations/RapportGO")

List array_go_taches = new ArrayList()
CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'(GlobalVariable.username)
CustomKeywords.'maximo.GammesOperationsKeys.accederAGO'()
CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()

int currentRow = 1
String startJPnum = ""
while (currentRow <= array_go.getRowNumbers()) {
	//Recupere le code de la GO et recherche
	startJPnum = array_go.getValue("no", currentRow)
	CustomKeywords.'maximo.GeneralKeys.rechercheRapide'(startJPnum)
	
	CustomKeywords.'maximo.GeneralKeys.logWarning'("Validation de l'entête de la GO")
	//def String noGo, def String descGO, def String org, def String site, def String sDuree, def String classif, def String classifDesc, def String gP
    CustomKeywords.'maximo.GammesOperationsKeys.validerEnteteGO'(
		startJPnum, 
		array_go.getValue("Description", currentRow),
		sOrganisation,
		sSite,
		"",
		array_go.getValue("Classification", currentRow),
		array_go.getValue("Description classification", currentRow),
		"",
		sGp)
	
	CustomKeywords.'maximo.GeneralKeys.logWarning'("Parcours des tâches")
	//Recupere les tâche de la mm GO
	while (startJPnum == array_go.getValue("no", currentRow)) {
		if(array_go.getValue("No Tache", currentRow) != ""){
			println('La GO dispose de tâches : ' + array_go.getValue("No Tache", currentRow))
			array_go_taches.add(array_go.getValue("No Tache", currentRow))
		}else{
			println('La Go ne dispose de tâches ')
		}
		currentRow++
	}
	
	CustomKeywords.'maximo.GammesOperationsKeys.valideTachesGO'(array_go_taches)
	CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
	array_go_taches.clear()
}
CustomKeywords.'maximo.GeneralKeys.deconnexion'()
