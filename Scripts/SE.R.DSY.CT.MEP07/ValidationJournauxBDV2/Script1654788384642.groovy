
import com.kms.katalon.core.testcase.TestCase as TestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import internal.GlobalVariable as GlobalVariable
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.GsonBuilder
import com.kms.katalon.core.util.KeywordUtil
import org.json.JSONObject
import org.json.XML
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.concurrent.Callable
import java.util.concurrent.Executors

public class TestsCasesMep07BD {

   public TestsCasesMep07BD(){}
			 
   def transactionsJournal
   def noJournal
   def currentTxnSeqCheck
   def startAt = 4000
   def endAt = 4524
   
   def listGenAttrsAsset = [
	   "VDM_CLASSIFICATION_RESEAU","VDM_ADRESSE","VDM_INV_NO", "VDM_INV_ID", "VDM_ACTIF_ASSOCIE", "INSTALLDATE","VDM_DATE_FIN","VDM_DATE_ABANDON",
	   "VDM_ELEVATION_TERRAIN","VDM_RESPONSABLE_OPERATIONNEL", "VDM_TERRITOIRE","VDM_PROPRIETAIRE","VDM_JURIDICTION","VDM_HIERARCHIE_TRONCON",
	   "VDM_STATUT_POSITION_ACTIF", "VDM_STATUT_DESSIN_NUMERISATION","VDM_LOCALISATION", "STATUS","VDM_NO_SECTEUR_REGULATION", 
	   "VDM_NOM_RESERVOIR", "VDM_TYPE_RESEAU", "VDM_COORDONNEE_SPATIALE_X", "VDM_COORDONNEE_SPATIALE_Y", "ASSETTAG"
   ]
   
  /** EGOUT **/
  def listSEGEGAttrsAssetSpecs = [["BASSIN_VERSANT", "ALN"], ["CLASSE_DE_RESISTANCE_EG","ALN"], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DATE_REHABILITATION","DATE"], ["DECOUPAGE", "ALN"], ["DIAMETRE_H_MM", "ALN"], ["DIAMETRE_H_PO", "ALN"], ["DIAMETRE_V_MM", "ALN"], ["DIAMETRE_V_PO", "ALN"], ["FORME_CONDUITE", "ALN"], ["ID_EGO_POINT_DEBUT","ALN"], ["ID_EGO_POINT_FIN", "ALN"], ["MATERIAU_SEGMENT_EG", "ALN"], ["NO_CONDUITE_PI", "ALN"],["NO_CONTRAT", "ALN"], ["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"], ["NOM_USUEL", "ALN"], ["PENTE_FOSSE_LATERAL_1","ALN"], ["PENTE_FOSSE_LATERAL_2", "ALN"], ["PRECISION_DATE_INSTALL","ALN"], ["PROFONDEUR_RADIER_AMONT","ALN"],["PROFONDEUR_RADIER_AVAL","ALN"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["SENS_ECOULEMENT", "ALN"],["TYPE_ECOULEMENT", "ALN"],["TYPE_REHABILITATION_EG", "ALN"],["TYPE_SEGMENT_EG", "ALN"]]
  def listCHEGAttrsAssetSpecs = [["BASSIN_VERSANT", "ALN"], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "DATE"], ["FORME_CHAMBRE", "ALN"], ["LARGEUR_CHAMBRE", "NUM"],["LONGUEUR_CHAMBRE", "NUM"], ["MATERIAU_CHAMBRE_EG", "ALN"], ["NO_CHAMBRE_DEEU", "ALN"], ["NO_TRONCON_GEOBASE","ALN"], ["NOM_FICHIER_CREATION","ALN"], ["PRECISION_DATE_INSTALL","ALN"],["PROFONDEUR", "NUM"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_CHAMBRE_EG","ALN"]]
  def listACCEGAttrsAssetSpecs = [["ALTITUDE", "NUM"],["COMPARTIMENT", "ALN"],["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"],["ID_POINT", "ALN"],["ID_POLE", "ALN"],["NO_TRONCON_GEOBASE", "ALN"],["NOM_FICHIER_CREATION", "ALN"],["PRECISION_DATE_INSTALL","ALN"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"]]
  def listREGEGAttrsAssetSpecs = [["ALTITUDE", "NUM"],["BASSIN_VERSANT", "ALN"],["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"],["DECOUPAGE", "ALN"],["DIMENSION_BASE_M", "ALN"], ["DIMENSION_CHEMINEE_M","ALN"], ["DIMENSION_COUVERCLE_PO","ALN"],["ID_POINT", "ALN"],["ID_POLE", "ALN"],["MATERIAU_REGARD", "ALN"],["NO_GEOMATIQUE_CHAMBRE_LIE","ALN"],["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"], ["PRECISION_DATE_INSTALL","ALN"],["PROFONDEUR", "NUM"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_REGARD_EG", "ALN"],["VISIBLE", "ALN"]]
  def listRACEGAttrsAssetSpecs = [["ALTITUDE", "NUM"], ["BASSIN_VERSANT", "ALN"],["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"],["DECOUPAGE", "ALN"],["ID_POINT", "ALN"],["ID_POLE", "ALN"],["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"],["PRECISION_DATE_INSTALL","ALN"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"], ["TYPE_RACCORD_EG", "ALN"]]
  def listBASAttrsAssetSpecs = [["BASSIN_VERSANT", "ALN"],["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "ALN"], ["LARGEUR_BASSIN", "ALN"], ["LONGUEUR_BASSIN", "ALN"],["MATERIAU_BASSIN", "ALN"],["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"],["PRECISION_DATE_INSTALL","ALN"],["PROFONDEUR", "NUM"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_BASSIN", "ALN"]]
  def listPUIAssetSpecs = [["ALTITUDE", "NUM"], ["BASSIN_VERSANT", "ALN"],["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"],["DECOUPAGE", "ALN"], ["DIMENSION_GRILLE", "ALN"],["ID_POINT", "ALN"],["ID_POLE", "ALN"],["MATERIAU_PUISARD", "ALN"],["NO_PUISARD_REMPLACE","ALN"],["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"],["PRECISION_DATE_INSTALL","ALN"],["PROFONDEUR", "NUM"], ["PROVENANCE_DONNEE", "ALN"], ["REMARQUE_GEOMATIQUE", "ALN"], ["VISIBLE", "ALN"]]
   
   /** AQUEDUC **/
  def listRACAQAttrsAssetSpecs = [["ALTITUDE", "NUM"],["COMPARTIMENT", "ALN"],["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"],["DECOUPAGE", "ALN"],["ID_POINT", "ALN"],["ID_POLE", "ALN"], ["MATERIAU_RACCORD_AQ", "ALN"], ["NO_TRONCON_GEOBASE",  "ALN"],["NOM_FICHIER_CREATION","ALN"], ["PRECISION_DATE_INSTALL","ALN"], ["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_RACCORD_AQ", "ALN"]]
  def listACCAQAttrsAssetSpecs = [["ALTITUDE", "NUM"], ["COMPARTIMENT", "ALN"], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "ALN"], ["DIAMETRE_MM", "ALN"], ["ID_POINT", "ALN"], ["ID_POLE", "ALN"], ["NO_TRONCON_GEOBASE", "ALN"], ["NOM_FICHIER_CREATION","ALN"], ["PRECISION_DATE_INSTALL","ALN"], ["PROVENANCE_DONNEE", "ALN"], ["REMARQUE_GEOMATIQUE", "ALN"], ["TYPE_ACCESSOIRE_AQ","ALN"]]
  def listVANAttrsAssetSpecs = [["ALTITUDE", "NUM"],["COMPARTIMENT", "ALN"], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "ALN"], ["DIAMETRE_MM", "ALN"], ["FONCTION", "ALN"], ["ID_POINT", "ALN"], ["ID_POLE", "ALN"], ["MECANISME_VANNE", "ALN"], ["NO_CHAMBRE_SUR_PLAN_CLE","ALN" ], ["NO_TRONCON_GEOBASE", "ALN"], ["NOM_FICHIER_CREATION", "ALN"], ["ORIENTATION_VANNE", "ALN"], ["PRECISION_DATE_INSTALL", "ALN"], ["PROFONDEUR", "NUM"], ["PROVENANCE_DONNEE", "ALN"], ["REMARQUE_GEOMATIQUE", "ALN"], ["VISIBLE", "ALN"]]    
  def listCHAQAttrsAssetSpecs = [["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "ALN"], ["FORME_CHAMBRE", "ALN"], ["LARGEUR_CHAMBRE", "NUM"], ["LONGUEUR_CHAMBRE", "NUM"], ["MATERIAU_CHAMBRE_AQ", "ALN"], ["NO_CHAMBRE_REGULATION","ALN"], ["NO_CHAMBRE_SUR_PLAN_CLE", "ALN"], ["NO_PLAN_CONTRAT_TEC", "ALN"], ["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"], ["PROFONDEUR", "NUM"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_CHAMBRE", "ALN"]]
  def listSEGAQAttrsAssetSpecs = [["CLASSE_DE_RESISTANCE_AQ", "ALN" ], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DATE_REHABILITATION", "DATE"], ["DECOUPAGE", "ALN"], ["DIAMETRE_MM", "ALN"], ["ID_POINT_DEBUT", "ALN"], ["ID_POINT_FIN", "ALN"], ["MATERIAU_SEGMENT_AQ", "ALN"], ["NO_TRONCON_GEOBASE","ALN"], ["NOM_FICHIER_CREATION","ALN"], ["PROVENANCE_DONNEE", "ALN"], ["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_PROTECTION", "ALN"], ["TYPE_REHABILITATION_AQ","ALN"],["TYPE_SEGMENT_AQ","ALN"]]
  def listBIAssetSpecs = [["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"],["DECOUPAGE", "ALN"], ["ID_POINT", "ALN"],["ID_POLE", "ALN"],["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"],["PRECISION_DATE_INSTALL","ALN"],["PROFONDEUR", "NUM"],	   ["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"]]
  def listREGAQAssetSpecs  = [["ALTITUDE", "NUM"], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "ALN"], ["ID_POINT", "ALN"], ["ID_POLE", "ALN"], ["NO_TRONCON_GEOBASE", "ALN"], ["NOM_FICHIER_CREATION","ALN"], ["PRECISION_DATE_INSTALL","ALN"], ["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"], ["TYPE_REGARD_AQ", "ALN"]]

  def errors = new ArrayList()
  def activeLog = false
  
  DecimalFormat df = new DecimalFormat("#.000")
  
  
   public executerTestCase() {
		configurerConnexionBD()
		definirNoJournal()
		recupérerTransactionsDuJournal()
		validerTransactions()
		validerHirarchieEmplacements()
		validerActifsExistantsSansEmplacements()
		validerTournees()
		resumerErreur()
	}
	
	private definirNoJournal() {
		if(CustomKeywords.'maximo.BDKeys.recupererNoJournal'()) {
			noJournal = CustomKeywords.'maximo.BDKeys.recupererNoJournal'()
			markInfo( "Le NO de journal saisi est : "+ noJournal)
		}else {
			noJournal = recupererDernierNoJournal()
			markInfo( "Aucun NO de journal saisi, le dernier journal consommé sera récupéré : "+ noJournal)
		}
	}
	
	private recupererDernierNoJournal(){
		String sql = "\n SELECT MAX(TXNJOURNALNO) AS NO_JOURNAL" + "\n"
		sql +="FROM VDM_BDSA_ASSETTXN vba" + "\n"
		def noJournalBd = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		return noJournalBd[0].getAt("NO_JOURNAL")
	}
	
	private configurerConnexionBD() {
		CustomKeywords.'maximo.BDKeys.configureConnexionDB'()
	}
		
	private recupérerTransactionsDuJournal() {
		def statusTransactions = validerStatutTransactionsJournal()
		if(statusTransactions) {
			validationIntegritéJournal()
			String sql = "\n SELECT TXNJOURNALNO, UUID, UUID_AVANT, VDM_CLASSEACTIF, VDM_TYPEACTIF, TXNSEQUENCE, ACTION, TXN_MESSAGE, TXNFICTIVECHANGED, TXNTERRITORYCHANGED FROM VDM_BDSA_ASSETTXN WHERE TXNJOURNALNO = "+ noJournal + " ORDER BY TXNSEQUENCE"
			transactionsJournal = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
			markPassed("JOURNAL : " + noJournal + " contient "+ transactionsJournal.size() +" transactions")
		}else {
			KeywordUtil.markErrorAndStop("JOURNAL : "+ noJournal + " contient des transactions <> du statut TRAITE")
		}
	}
	
	private validerStatutTransactionsJournal() {
		String sql = "\n SELECT TXNJOURNALNO FROM VDM_BDSA_ASSETTXN WHERE TXNJOURNALNO = "+ noJournal + "\n" 
		       sql +="AND TXNSTATUS <> 'TRAITE' ORDER BY TXNSEQUENCE"
		def transactions = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		if(transactions.size() > 0) {
			return false
		}else {
			return true
		}
	}
	
	private validationIntegritéJournal() {
		markInfo("JOURNAL : "+ noJournal + " Validation de l'intégrité du journal en cours")
		validerNombreTransactions()
		validerIntegriteAgregation()
		validerIntegriteScindement()
	}
	
	private validerNombreTransactions() {
		String sql = "\n SELECT COUNT(*) AS NBTXNSBD, TXNTOTALACTIF AS NBTXNBASE FROM VDM_BDSA_ASSETTXN a "+"\n"
		sql +="WHERE a.TXNJOURNALNO = "+ noJournal + "\n"
		sql +="GROUP BY TXNTOTALACTIF"+ "\n"
		def nbTxns = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		
		if(nbTxns[0].get("NBTXNSBD") != nbTxns[0].get("NBTXNBASE")) {
			errors.add("JOURNAL : "+ noJournal + " n'est pas intègre au niveau du nombre de transactions")
			markError("JOURNAL : "+ noJournal + " n'est pas intègre au niveau du nombre de transactions")
			return false
		}else if (nbTxns.size() == 0){
			errors.add("JOURNAL : "+ noJournal + " n'existe pas dans l'envrionnement")
			markError("JOURNAL : "+ noJournal + " n'existe pas dans l'envrionnement")
			return false
		}else {
			return true
		}
	}
	
	private validerIntegriteAgregation() {
		String sql = "\n SELECT COUNT(*) AS NBINT FROM VDM_BDSA_ASSETTXN a "+"\n"
		sql +="WHERE a.TXNJOURNALNO =  "+ noJournal + "\n"
		sql +="AND a.ACTION = 'AGREGATION'"+"\n"
		sql +="AND ((SELECT COUNT(*) FROM VDM_BDSA_ASSETTXN b WHERE b.TXNJOURNALNO =  "+ noJournal + "  AND b.UUID = a.UUID AND (b.ACTION = 'AGREGATION' OR (b.ACTION = 'INSERTION' AND b.TXNTERRITORYCHANGED = 'NMXM_TO_MXM'))) < 2"+"\n"
		sql +="OR (SELECT COUNT(*) FROM VDM_BDSA_ASSETTXN c WHERE c.TXNJOURNALNO =  "+ noJournal + "  AND c.UUID = a.UUID_AVANT  AND c.ACTION = 'SUPPRESSION') <> 1)"+"\n"
		
		def transactions = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		
		if(transactions[0].get("NBINT") > 0) {
			errors.add("JOURNAL : "+ noJournal + " n'est pas intègre au niveau des agregations")
			markError("JOURNAL : "+ noJournal + " n'est pas intègre au niveau des agregations")
			return false
		}else {
			return true
		}
	}
	
	private validerIntegriteScindement() {
		String sql = "\n SELECT COUNT(*) AS NBINT FROM VDM_BDSA_ASSETTXN a "+"\n"
		sql +="WHERE a.TXNJOURNALNO = "+ noJournal + "\n"
		sql +="AND a.ACTION = 'SCINDEMENT'"+"\n"
		sql +="AND (NOT EXISTS (SELECT 1 FROM VDM_BDSA_ASSETTXN b WHERE b.TXNJOURNALNO = "+ noJournal + " AND b.ACTION = 'SCINDEMENT' AND b.UUID <> a.UUID )"+"\n"
		sql +="OR NOT EXISTS (SELECT 1 FROM VDM_BDSA_ASSETTXN c WHERE c.TXNJOURNALNO = "+ noJournal + " AND c.ACTION = 'SUPPRESSION' AND c.UUID = a.UUID_AVANT))"+"\n"
		
		def transactions = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		if(transactions[0].get("NBINT") > 0) {
			errors.add("JOURNAL : "+ noJournal + " n'est pas intègre au niveau des scindements")
			markError("JOURNAL : "+ noJournal + " n'est pas intègre au niveau des scindements")
			return false
		}else {
			return true
		}
	}
	
	private validerTransactions() {
		if(transactionsJournal.size() > 0) {
			if(startAt != null && endAt != null) {
				for(int a = startAt; a < endAt; a++) {
					println("Position : "+ a + " erreurs : "+ errors.size())
					definirAction(transactionsJournal[a])
				}
			}else {
				(transactionsJournal as ArrayList).parallelStream().forEachOrdered({txn ->
					definirAction(txn)
				})
			}
		}else {
			errors.add("Le résultat de la récupération des transactions du journal "+ noJournal + " est NULL ")
			KeywordUtil.markErrorAndStop("Le résultat de la récupération des transactions du journal "+ noJournal + " est NULL ")
		}
	}
	

	private validerSiTransactionSuperieureExistantes(uuid, txnsequence, classeActif, typeActif) {
		String sql =  "\n SELECT COUNT(*) AS NBTXN"+ "\n"
			   sql += "FROM VDM_BDSA_ASSETTXN WHERE TXNJOURNALNO = "+ noJournal  + "\n"
			   sql += "AND UUID = '"+uuid+"' AND TXNSEQUENCE > "+txnsequence + "\n"
			   sql += "ORDER BY TXNSEQUENCE"
			   
		def listeTxnSup = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		if(listeTxnSup != null || listeTxnSup.size() > 0) {
			markPassed("Transaction(s) existante(s) > " +  txnsequence + " "+ classeActif + " "+ typeActif +" au UUID "+ uuid)
		}else {
			markPassed("Aucune(s) transaction(s)  > " +  txnsequence + " au UUID "+ uuid)
		}

		return listeTxnSup
	}
	
	private peuxDisposerActifFictif(classeActif, typeActif) {
		def disposeFictif = false
		if (classeActif =="AQUEDUC" && (typeActif =="RACCORD" || typeActif=="REGARD")) {
			disposeFictif = true
		}else if (classeActif =="EGOUT" && (typeActif =="RACCORD" || typeActif=="SEGMENT" || typeActif=="REGARD")) {
			disposeFictif = true
		}
		return disposeFictif
	}
	
	private definirAction(transaction) {
		def transactionsSuperieures = validerSiTransactionSuperieureExistantes(transaction.getAt("UUID"), transaction.getAt("TXNSEQUENCE"),transaction.getAt("VDM_CLASSEACTIF"),transaction.getAt("VDM_TYPEACTIF") )
		
		if(peuxDisposerActifFictif(transaction.getAt("VDM_CLASSEACTIF"), transaction.getAt("VDM_TYPEACTIF"))){
			def jsonActif = convertXmlToGson(transaction.getAt("TXN_MESSAGE").getSubString(1, (int) transaction.getAt("TXN_MESSAGE").length()))
			if(jsonActif.get("ACTIF_FICTIF").getAsString() == "OUI" && transaction.getAt("TXNFICTIVECHANGED") == null) {
				markInfo(transaction.getAt("TXNSEQUENCE") + " ACTIF AVEC UUID "+ transaction.getAt("UUID") + " EST UN ACTIF FICTIF ")
			}else {
				validerAction(transactionsSuperieures, transaction)
			}
		}else {
			validerAction(transactionsSuperieures, transaction )
		}
	}
	
	private validerAction(transactionsSuperieures, transaction) {		
		//Valider le résultat de l'action
		switch (transaction.getAt("ACTION")) {
			case "INSERTION": 
				validerInsertion(transactionsSuperieures, transaction) 
				break
			case "AGREGATION": 
				validerAgregation(transactionsSuperieures, transaction) 
				break
			case "SCINDEMENT": 
				validerScindement(transactionsSuperieures, transaction) 
				break
			case "SUPPRESSION": 
				validerSuppression(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"), transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE")) 
				break
			default: 
				validerModificationAttributs(transactionsSuperieures, transaction) 
				break
		}
	}
	

	
	private validerInsertion(transactionsSuperieures, transaction) {
		if(transactionsSuperieures[0].get("NBTXN") == 0) {
			//Valider structure complète de l'actif
			effectuerValidationActifComplet(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXN_MESSAGE"), transaction.getAt("TXNSEQUENCE"))
		}else {
			//Valider structure partielle de l'actif
			effectuerValidationActifPartielle(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE"))
		}		
	}
	
	private validerAgregation(transactionsSuperieures, transaction) {
		markInfo( transaction.getAt("TXNSEQUENCE") + " DEBUT VALIDATION AGREGATION au UUID "+ transaction.getAt("UUID") + " UUID_AVANT " + transaction.getAt("UUID_AVANT"))
		if(transactionsSuperieures[0].get("NBTXN") == 0) {
			//Valider structure complète de l'actif
			effectuerValidationActifComplet(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXN_MESSAGE"), transaction.getAt("TXNSEQUENCE"))
		}else {
			//Valider structure partielle de l'actif
			effectuerValidationActifPartielle(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE"))
		}	
		validerSuppression(transaction.getAt("UUID_AVANT"), transaction.getAt("VDM_CLASSEACTIF"), transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE"))
	}

	private validerScindement(transactionsSuperieures, transaction) {
		markInfo( transaction.getAt("TXNSEQUENCE") + " DEBUT VALIDATION SCINDEMENT au UUID "+ transaction.getAt("UUID") + " UUID_AVANT " + transaction.getAt("UUID_AVANT"))
		if(transactionsSuperieures[0].get("NBTXN") == 0) {
			//Valider structure complète de l'actif
			effectuerValidationActifComplet(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXN_MESSAGE"), transaction.getAt("TXNSEQUENCE"))
		}else {
			//Valider structure partielle de l'actif
			effectuerValidationActifPartielle(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE"))
		}
		validerSuppression(transaction.getAt("UUID_AVANT"), transaction.getAt("VDM_CLASSEACTIF"), transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE"))
	}
	
	private validerSuppression(uuid, classeActif, typeActif, txnsequence) {
		String sql = "\n SELECT ASSETNUM FROM ASSET" + "\n"
               sql +="WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' \n" 
			   sql+= "AND STATUS = 'R' AND VDM_DATE_FIN IS NOT NULL"
			   
		def transaction = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		if(transaction.size() == 1) {
			if(activeLog) {
				markPassed(txnsequence +" SUCCES SUPPRESION: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" a été retiré avec succès avec la transaction")
			}

			return true
		}else {
			errors.add(txnsequence +" ECHEC SUPPRESSION: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +  " n'a pas été retiré avec la transaction")
			markError(txnsequence +" ECHEC SUPPRESSION: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +  " n'a pas été retiré avec la transaction")
			return false
		}
	}
	

	
	private validerModificationAttributs(transactionsSuperieures, transaction) {
		if(transactionsSuperieures[0].get("NBTXN") > 0) {
			markPassed(transaction.getAt("TXNSEQUENCE")  +" SUCCES MODIFICATION_ATTR: "+ transaction.getAt("VDM_CLASSEACTIF") + " " + transaction.getAt("VDM_TYPEACTIF") + " avec le UUID : " + transaction.getAt("UUID") +" dispose de transactions >  le contenu de cette transaction ne sera pas validé")
		}else {
			effectuerValidationActifComplet(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXN_MESSAGE"), transaction.getAt("TXNSEQUENCE"))
		}
	}
		
	private effectuerValidationActifComplet(uuid, classeActif, typeActif, xmlClob, txnsequence) {
		def jsonActif = convertXmlToGson(xmlClob.getSubString(1, (int) xmlClob.length()))
		validerTableAsset(uuid, classeActif, typeActif, txnsequence, jsonActif )
		validerTableAssetSpecs(uuid, classeActif, typeActif, txnsequence, jsonActif)	
	}
	
	private effectuerValidationActifPartielle(uuid, classeActif, typeActif, txnsequence) {
		String sql = genererSQLValidationActifPartiel(uuid, classeActif,  typeActif)
		def actif = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		
		if(actif.size() == 1) {
			markPassed(txnsequence +" SUCCES INSERTION VALIDATION PARTIELLE: "+ classeActif + " " + typeActif+ " avec le UUID : " + uuid +" validation partielle conforme")
		}else {
			errors.add(txnsequence +" ECHEC INSERTION VALIDATION PARTIELLE: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" validation partielle NON conforme")
			markError(txnsequence +" ECHEC INSERTION VALIDATION PARTIELLE: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" validation partielle NON conforme")
		}
	}
	
	private validerTableAsset(uuid, classeActif, typeActif, txnsequence, jsonActif) {
		//Valider ASSET
		String sqlAsset = sqlTableAssetActif(uuid, classeActif, typeActif, jsonActif)
		def resultatActif = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlAsset)
		if(resultatActif.size() == 1) {
			markPassed(txnsequence + " SUCCES VALIDATION TABLE ASSET: "+ classeActif+ " " + typeActif+ " avec le UUID : " + uuid +" données conformes ")
		}else {
			errors.add(txnsequence + " ECHEC VALIDATION TABLE ASSET: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" données NON conformes " + sqlAsset)
			markError(txnsequence + " ECHEC VALIDATION TABLE ASSET: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" données NON conformes ")
		}
	}
	
	private validerTableAssetSpecs(uuid, classeActif, typeActif, txnsequence, jsonActif) {
		//Valider ASSETSPEC
		String sqlSpecs = sqlTableAssetSpecsActif(uuid, classeActif, typeActif, jsonActif)
		def resultatSpecs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlSpecs)
		
		if(resultatSpecs.size() == 1) {
			markPassed(txnsequence +" SUCCES TABLE ASSETSPECS: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" données conformes")
		}else {
			errors.add(txnsequence +" ECHEC TABLE ASSETSPECS: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" données NON conformes" + sqlSpecs)
			markError(txnsequence +" ECHEC TABLE ASSETSPECS: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" données NON conformes")
		}
	}
	
	private sqlTableAssetActif(uuid, classeActif, typeActif, jsonActif) {
		
		String sqlTableAsset = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			   sqlTableAsset +="AND UUID = '"+ uuid + "'" + " \n"
			   sqlTableAsset +="AND VDM_CLASSEACTIF = '"+ classeActif + "' \n"
			   sqlTableAsset +="AND VDM_TYPEACTIF = '"+ typeActif + "' \n"
			   
			   for(int a = 0; a < listGenAttrsAsset.size(); a++) {
				   if(jsonActif.get(listGenAttrsAsset[a]) != null) {
					   if(jsonActif.get(listGenAttrsAsset[a]).getAsString() != "") {
						   if(listGenAttrsAsset[a] == "INSTALLDATE" || listGenAttrsAsset[a] =="VDM_DATE_FIN" || listGenAttrsAsset[a] == "VDM_DATE_ABANDON") {
							   sqlTableAsset +="AND TRUNC("+ listGenAttrsAsset[a]+ ") = TRUNC(to_timestamp( '"+ jsonActif.get(listGenAttrsAsset[a]).getAsString() + "' , 'YYYY-MM-DD "+ "\"T\""+ "HH24:MI:SS.ff3"+ "\"Z\""+ "')) \n"
						   }
						   else if(listGenAttrsAsset[a] == "VDM_COORDONNEE_SPATIALE_X" ||listGenAttrsAsset[a] == "VDM_COORDONNEE_SPATIALE_Y" ||listGenAttrsAsset[a] == "VDM_ELEVATION_TERRAIN") {
							   //BigDecimal bdecim = new BigDecimal(Float.parseFloat(jsonActif.get(listGenAttrsAsset[a]).getAsString()))
							   sqlTableAsset +="AND "+ listGenAttrsAsset[a]+ " = '"+ df.format(jsonActif.get(listGenAttrsAsset[a]).getAsBigDecimal()).toString().replace(".", ",") + "' \n"
						   }
						   else if (listGenAttrsAsset[a] =="VDM_ADRESSE") {
							   sqlTableAsset +="AND "+ listGenAttrsAsset[a]+ " LIKE '"+ jsonActif.get(listGenAttrsAsset[a]).getAsString().replace("'", "_") + "' \n"
						   }
						   else {
							   if(listGenAttrsAsset[a] != "VDM_ACTIF_ASSOCIE" || (listGenAttrsAsset[a] == "VDM_ACTIF_ASSOCIE" && identifierVDM_ACTIF_ASSOCIE(jsonActif.get("VDM_ACTIF_ASSOCIE").getAsString()))) {
								   sqlTableAsset +="AND "+ listGenAttrsAsset[a]+ " = '"+ jsonActif.get(listGenAttrsAsset[a]).getAsString() + "' \n"
							   }
						   }
					   }else {
						   sqlTableAsset +="AND "+ listGenAttrsAsset[a]+ " IS NULL  \n "
					   }
				   }
			   }

		return sqlTableAsset
	}
	
	private sqlTableAssetSpecsActif(uuid, classeActif, typeActif, jsonActif) {
		def listSpecs = recupererListeAssetSpecs(classeActif, typeActif)
		String sqlTableAssetSpecs = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			   sqlTableAssetSpecs +="AND UUID = '"+ uuid + "'" + " \n"
			   sqlTableAssetSpecs +="AND VDM_CLASSEACTIF = '"+ classeActif + "' \n"
			   sqlTableAssetSpecs +="AND VDM_TYPEACTIF = '"+ typeActif + "' \n"
		
	   for(int a = 0; a < listSpecs.size(); a ++) {
		   String key = listSpecs[a][0] as String
		   String keyType = listSpecs[a][1] as String

		   if(jsonActif.get(key) != null) {
			   if(jsonActif.get(key).toString() != "") {		   
				   if(keyType == "ALN") {
					   if(key != "REMARQUE_GEOMATIQUE") {
						   if(jsonActif.get(key).getAsString() == "") {
							   sqlTableAssetSpecs += " AND EXISTS(SELECT 1 FROM ASSETSPEC WHERE ASSETSPEC.ASSETNUM = ASSET.ASSETNUM AND ASSETSPEC.ASSETATTRID = '"+ key +"' AND ASSETSPEC.ALNVALUE IS NULL)"+ " \n"
						   }else {
							   sqlTableAssetSpecs += " AND EXISTS(SELECT 1 FROM ASSETSPEC WHERE ASSETSPEC.ASSETNUM = ASSET.ASSETNUM AND ASSETSPEC.ASSETATTRID = '"+ key +"' AND ASSETSPEC.ALNVALUE = '"+ jsonActif.get(key).getAsString() +"')"+ " \n"
						   }
					   }
				   }else if (keyType == "NUM") {
					   if(key == "LONGUEUR_CHAMBRE" || key == "LARGEUR_CHAMBRE" || key == "PROFONDEUR" || key == "ALTITUDE") {
						   sqlTableAssetSpecs += " AND EXISTS(SELECT 1 FROM ASSETSPEC WHERE ASSETSPEC.ASSETNUM = ASSET.ASSETNUM AND ASSETSPEC.ASSETATTRID = '"+ key +"' AND ASSETSPEC.NUMVALUE = '"+ df.format(jsonActif.get(key).getAsBigDecimal()).toString().replace(".", ",") + "')" + " \n"	   
					   }else {
						   sqlTableAssetSpecs += " AND EXISTS(SELECT 1 FROM ASSETSPEC WHERE ASSETSPEC.ASSETNUM = ASSET.ASSETNUM AND ASSETSPEC.ASSETATTRID = '"+ key +"' AND ASSETSPEC.NUMVALUE = '"+ jsonActif.get(key).getAsString().replace(".", ",") +"')" + " \n"
					   }
			
				   }else if (keyType == "DATE") {
					   if(jsonActif.get(key).getAsString() != "") {
						   sqlTableAssetSpecs += " AND EXISTS(SELECT 1 FROM ASSETSPEC WHERE ASSETSPEC.ASSETNUM = ASSET.ASSETNUM AND ASSETSPEC.ASSETATTRID = '"+ key +"' AND ASSETSPEC.ALNVALUE = '"+ jsonActif.get(key).getAsString() +"')"+ " \n"
					   }
				   }
			   }
		   }
	   }
		return sqlTableAssetSpecs
	}
	
	
	private genererSQLValidationActifPartiel(uuid, classeActif, typeActif) {
		String sqlAssetPartiel = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			   sqlAssetPartiel +="AND UUID = '"+ uuid + "'" + " \n"
			   sqlAssetPartiel +="AND VDM_CLASSEACTIF = '"+ classeActif + "' \n"
			   sqlAssetPartiel +="AND VDM_TYPEACTIF = '"+ typeActif + "' \n"
		return sqlAssetPartiel
	}
	

	private recupererListeAssetSpecs(classeActif, typeActif) {
		def list
		if(classeActif =="AQUEDUC") {
			list = recupererListeAssetSpecsAqueduc(typeActif)
		}else if(classeActif =="EGOUT") {
			list = recupererListeAssetSpecsEgout(typeActif)
		}
		return list
	}
	
	private recupererListeAssetSpecsAqueduc(typeActif) {
		def list = null
		switch (typeActif) {
			case "BI":
				list =  listBIAssetSpecs
				break
			case "SEGMENT":
				list = listSEGAQAttrsAssetSpecs
				break
			case "CHAMBRE":
				list = listCHAQAttrsAssetSpecs
				break
			case "VANNE":
				list = listVANAttrsAssetSpecs
				break
			case "REGARD":
				list = listREGAQAssetSpecs
				break
			case "ACCESSOIRE":
				list = listACCAQAttrsAssetSpecs
				break
			case "RACCORD":
				list = listRACAQAttrsAssetSpecs
				break
		}
		return list
	}
	
	private recupererListeAssetSpecsEgout(typeActif) {
		def list = null
			switch (typeActif) {
			case "PUISARD":
				list = listPUIAssetSpecs
				break
			case "SEGMENT":
				list = listSEGEGAttrsAssetSpecs
				break
			case "CHAMBRE":
				list = listCHEGAttrsAssetSpecs
				break
			case "BASSIN":
				list = listBASAttrsAssetSpecs
				break
			case "REGARD":
				list = listREGEGAttrsAssetSpecs
				break
			case "ACCESSOIRE":
				list = listACCEGAttrsAssetSpecs
				break
			case "RACCORD":
				list = listRACEGAttrsAssetSpecs
				break
		}
		return list
	}
	
	
	
	private validerPrefixeEmplacement(uuid, classeActif, typeActif, territoire, txnsequence) {
		String prefixeArrondissement = definirPrefixeArrondissement(territoire)
		String sql = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' AND LOCATION LIKE '"+prefixeArrondissement +"%'"
		def actif = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		
		if(actif.size() == 1) {
			markPassed(txnsequence + " SUCCES PREFIXE EMPLACEMENT: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" dispose du bon préfixe d'emplacement soit : "+ prefixeArrondissement)
		}else {
			errors.add(txnsequence+ " ECHEC PREFIXE EMPLACEMENT: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +  " ne dispose pas du bon préfixe d'emplacement, aurait du être : "+ prefixeArrondissement)
			markError(txnsequence+ " ECHEC PREFIXE EMPLACEMENT: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +  " ne dispose pas du bon préfixe d'emplacement, aurait du être : "+ prefixeArrondissement)
		}
	}
	
	
	private definirPrefixeArrondissement(territoire) {
		switch (territoire) {
			case "24":
				return "AC"
				break
			case "9":
				return "AJ"
				break
			case "2":
				return "CG"
				break
			case "6":
				return "BG"
				break
			case "17":
				return "LC"
				break
			case "18":
				return "LS"
				break
			case "23":
				return "MH"
				break
			case "16":
				return "MN"
				break
			case "5":
				return "OM"
				break
			case "13":
				return "FR"
				break
			case "22":
				return "PM"
				break
			case "19":
				return "PR"
				break
			case "25":
				return "RL"
				break
			case 15:
				return "LR"
				break
			case "21":
				return "SO"
				break
			case "14":
				return "LN"
				break
			case "12":
				return "VD"
				break
			case "20":
				return "VM"
				break
			case "26":
				return "VE"
				break
			default: 
				return "TERRITOIRE INTROUVABLE "+ territoire
				break
			}
	}
	

	private identifierVDM_ACTIF_ASSOCIE(valeur) {
		def vdmActifAssocie = null
		if(valeur != "" && valeur != null && valeur.length() == 36) {
			vdmActifAssocie = valeur
		}
		return vdmActifAssocie
	}
	

	
	private validerActifsExistantsSansEmplacements() {
		String sqlActifsSansEmpl = "\n SELECT UUID FROM ASSET WHERE SITEID = 'TP' "+ "\n"
		sqlActifsSansEmpl +="AND STATUS = 'E'" + " \n"
		sqlActifsSansEmpl +="AND LOCATION is null" + " \n"
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlActifsSansEmpl)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : IL N'EXISTE AUCUN ACTIFS DU SITE TP SANS EMPLACEMENTS")
		}else {
			errors.add("ECHEC : Il EXISTE DES ACTIFS DU SITE TP SANS EMPLACEMENTS VOICI LA REQUETE DE VALIDATION: "+ sqlActifsSansEmpl)
			markError(" ECHEC : Il EXISTE DES ACTIFS DU SITE TP SANS EMPLACEMENTS VOICI LA REQUETE DE VALIDATION: "+ sqlActifsSansEmpl)
		}
	}
	
	

	
	private validerHirarchieEmplacements() {
		//Emplacements
		validerStatutsEmplacements()
		validerHierarchieAQU()
		validerHierarchieEGO()
		
		validerCorrespondanceEmplacementTerritoire()
	}
	
	private validerHierarchieAQU() {
		//AQUEDUC
		validerHierarchieAQUBornesIncendie()
		validerHierarchieAQUVannesBornesIncendie()
		validerHierarchieAQUSegmentsBornesIncendie()
		validerHierarchieAQUChambres()
		validerHierarchieAQURegards()
		validerHierarchieAQUAccessoiresSansVDMA()
		validerHierarchieAQUAccessoiresAvecVDMA()
		validerHierarchieAQUVannesRéseauxSansVDMA()
		validerHierarchieAQUVannesRéseauxAvecVDMA()
		validerHierarchieAQUSegmentsRéseaux()
		validerHierarchieAQURaccords()
		validerHierarchieAQUVannesEntreesService()
		validerHierarchieAQUSegmentsEntreesService()
	}
	
	private validerHierarchieEGO() {
		//EGOUT
		validerHierarchieEGOSegments()
		validerHierarchieEGOPuisards()
		validerHierarchieEGORaccords()
		validerHierarchieEGOChambres()
		validerHierarchieEGORegards()
		validerHierarchieEGOBassins()
		validerHierarchieEGOAccessoiresSansVDMA()
		validerHierarchieEGOAccessoiresAvecVDMA()
	}
	
	private validerStatutsEmplacements() {
		String sqlStatEmpl = "\n SELECT count(*) AS NBLOC FROM LOCATIONS a " +"\n"
		sqlStatEmpl += "WHERE a.SITEID = 'TP' " +"\n"
		sqlStatEmpl += "AND a.STATUS = 'E' " +"\n"
		sqlStatEmpl += "AND EXISTS (SELECT 1 FROM  ASSET a WHERE a.SITEID = 'TP' AND a.LOCATION = a.LOCATION) " +"\n"
		sqlStatEmpl += "AND NOT EXISTS (SELECT 1 FROM ASSET c WHERE c.SITEID = 'TP' AND c.LOCATION = a.LOCATION AND c.STATUS = 'E')" +"\n"
		sqlStatEmpl += "AND NOT EXISTS (SELECT 1 from lochierarchy lh where lh.parent = a.location)" +"\n"
		
		def emplacements = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlStatEmpl)
		
		if(emplacements[0].getAt("NBLOC") == 0) {
			markPassed("SUCCES : IL N'EXISTE AUCUN EMPLACEMENT AU STATUT E AVEC ACTIFS R")
		}else {
			errors.add("ECHEC : Il EXISTE DES EMPLACEMENT AU STATUT E DISPOSANT D'ACTIFS COMPLÈTEMENT RETIRÉS: "+ sqlStatEmpl)
			markError(" ECHEC : Il EXISTE DES ACTIFS DU SITE TP SANS EMPLACEMENTS VOICI LA REQUETE DE VALIDATION: "+ sqlStatEmpl)
		}
	}
	
	private validerHierarchieAQUBornesIncendie() {
		String sqlEmplBI = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplBI += "WHERE siteid = 'TP' and status = 'E' and itemnum like 'BI%' and location not like '__-AQU-BI_%' " +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplBI)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE BORNES D'INCENDIE OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES BORNES SONT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplBI)
			markError(" ECHEC : Il EXISTE DES ACTIFS DU SITE TP SANS EMPLACEMENTS VOICI LA REQUETE DE VALIDATION: "+ sqlEmplBI)
		}
	}
	
	private validerHierarchieAQUVannesBornesIncendie() {
		String sqlEmplVI = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplVI += "WHERE siteid = 'TP' and itemnum = 'VAN000' and status = 'E' " +"\n"
		sqlEmplVI += "and exists (select 1 from assetspec where assetattrid like 'FONCTION' and alnvalue = 'BI' and assetnum = asset.assetnum) " +"\n"
		sqlEmplVI += "and exists (select 1 from lochierarchy WHERE location = asset.location and parent NOT like '__-AQU-BI_%') " +"\n"
		sqlEmplVI += "and exists (select 1 from lochierarchy WHERE location = asset.location and parent NOT like '__-AQU-BIVI') " +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplVI)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE VANNE D'ISOLEMENT DE BORNES D'INCENDIE OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES VANNES D'ISOLEMENT DE BORNES D'INCENDIE QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplVI)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES VANNES D'ISOLEMENT DE BORNES D'INCENDIE QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplVI)
		}
	}
	
	private validerHierarchieAQUSegmentsBornesIncendie() {
		String sqlEmplSEGBI = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplSEGBI += "WHERE siteid = 'TP' and itemnum like 'SEGAQ%' and status = 'E' " +"\n"
		sqlEmplSEGBI += "and exists (select 1 from assetspec where assetnum = asset.assetnum and assetattrid like 'TYPE_SEGMENT_AQ' and alnvalue = 'BI') " +"\n"
		sqlEmplSEGBI += "and location not like '__-AQU-BI_%'" +"\n"
		sqlEmplSEGBI += "and location not like '__-AQU-BISEG'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplSEGBI)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE VANNE D'ISOLEMENT DE BORNES D'INCENDIE OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES SEGMENTS DE BORNES D'INCENDIE QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplSEGBI)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES SEGMENTS DE BORNES D'INCENDIE QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplSEGBI)
		}
	}
	
	private validerHierarchieAQUChambres() {
		String sqlEmplCHAQ = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplCHAQ += "WHERE siteid = 'TP' and itemnum like 'CHAQ%' and status = 'E' " +"\n"
		sqlEmplCHAQ += "and exists (select 1 from lochierarchy WHERE location = asset.location and parent NOT like '__-AQU-VNCH')" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplCHAQ)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE CHAMBRE D'AQUEDUC OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES CHAMBRES D'AQUEDUC QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplCHAQ)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES CHAMBRES D'AQUEDUC QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplCHAQ)
		}
	}
	
	private validerHierarchieAQUVannesRéseauxSansVDMA() {
		String sqlEmplVANR = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , DESCRIPTION, LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplVANR += "WHERE siteid = 'TP' and itemnum = 'VAN000' and status = 'E' AND UUID IS NOT NULL " +"\n"
		sqlEmplVANR += "AND ( VDM_ACTIF_ASSOCIE IS NULL OR NOT EXISTS (SELECT 1 FROM ASSET a2 WHERE a2.siteid = asset.siteid AND asset.VDM_ACTIF_ASSOCIE = a2.UUID )) " +"\n"
		sqlEmplVANR += "and exists(select 1 from assetspec where assetnum = asset.assetnum and assetattrid like 'FONCTION' and alnvalue in ('INC', 'AR', 'CD', 'D','VDR', 'I', 'MP', 'RDP','RR', 'VA', 'V', 'VRP'))" +"\n"
		sqlEmplVANR += "and exists (select 1 from lochierarchy WHERE location = asset.location and parent NOT like '__-AQU-VNCH')" +"\n"
		sqlEmplVANR += "and location not like 'BE%'" +"\n"
		sqlEmplVANR += "and location not like 'CF%'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplVANR)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE VANNES RÉSEAU D'AQUEDUC SANS ACTIF ASSOCIÉ OU VDM ACTIF ASSOCIE NON EXISTANT OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES VANNES RÉSEAU D'AQUEDUC SANS ACTIF ASSOCIÉ OU VDM ACTIF ASSOCIE NON EXISTANT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplVANR)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES VANNES RÉSEAU D'AQUEDUC SANS ACTIF ASSOCIÉ OU VDM ACTIF ASSOCIE NON EXISTANT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplVANR)
		}
	}
	
	private validerHierarchieAQUVannesRéseauxAvecVDMA() {
		String sqlEmplVANR = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , DESCRIPTION, LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplVANR += "WHERE siteid = 'TP' and itemnum = 'VAN000' and status = 'E' AND VDM_ACTIF_ASSOCIE IS NOT NULL AND UUID IS NOT NULL" +"\n"
		sqlEmplVANR += "AND EXISTS (SELECT 1 FROM ASSET a2 WHERE a2.siteid = asset.siteid AND asset.VDM_ACTIF_ASSOCIE = a2.UUID ) " +"\n"
		sqlEmplVANR += "and exists(select 1 from assetspec where assetnum = asset.assetnum and assetattrid like 'FONCTION' and alnvalue in ('INC', 'AR', 'CD', 'D','VDR', 'I', 'MP', 'RDP','RR', 'VA', 'V', 'VRP'))" +"\n"
		sqlEmplVANR += "and exists (select 1 from lochierarchy WHERE location = asset.location and parent NOT like '__-AQU-CHAMBRE%')" +"\n"
		sqlEmplVANR += "and location not like 'BE%'" +"\n"
		sqlEmplVANR += "and location not like 'CF%'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplVANR)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE VANNES RÉSEAU D'AQUEDUC AVEC ACTIF ASSOCIÉ OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES VANNES RÉSEAU D'AQUEDUC AVEC ACTIF ASSOCIÉ QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplVANR)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES VANNES RÉSEAU D'AQUEDUC AVEC ACTIF ASSOCIÉ QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplVANR)
		}
	}
	
	private validerHierarchieAQUSegmentsRéseaux() {
		String sqlEmplSEGAQ = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplSEGAQ += "WHERE siteid = 'TP' and itemnum = 'SEGAQ000' and status = 'E' " +"\n"
		sqlEmplSEGAQ += "and exists( select 1 from assetspec where assetnum = asset.assetnum and assetattrid like 'TYPE_SEGMENT_AQ' and alnvalue in ('INC','A','BP','BV','D','RC','RF','R'))" +"\n"
		sqlEmplSEGAQ += "and location not like '__-AQU-SEG'" +"\n"
		sqlEmplSEGAQ += "and location not like 'BE%' and location not like 'CF%'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplSEGAQ)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE SEGMENTS DE RÉSEAU D'AQUEDUC OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES SEGMENTS DE RÉSEAU D'AQUEDUC QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplSEGAQ)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES SEGMENTS DE RÉSEAU D'AQUEDUC QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplSEGAQ)
		}
	}
	
	private validerHierarchieAQURaccords() {
		String sqlEmplRACAQ = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplRACAQ += "WHERE siteid = 'TP' and itemnum = 'RACAQ000' and status = 'E' " +"\n"
		sqlEmplRACAQ += "AND location NOT LIKE '__-AQU-RACC'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplRACAQ)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE RACCORDS D'AQUEDUC OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES RACCORDS D'AQUEDUC QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplRACAQ)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES RACCORDS D'AQUEDUC QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplRACAQ)
		}
	}

	private validerHierarchieAQURegards() {
		String sqlEmplREGAQ = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , DESCRIPTION, LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplREGAQ += "WHERE siteid = 'TP' and itemnum = 'RAGAQ000' and status = 'E' AND uuid IS NOT null" +"\n"
		sqlEmplREGAQ += "AND location NOT LIKE '__-AQU-REG'" +"\n"
		sqlEmplREGAQ += "AND location NOT LIKE '__-AQU-CHAMBRE%'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplREGAQ)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE DES REGARDS D'AQUEDUC AVEC ET ACTIF ASSOCIÉ OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES REGARDS D'AQUEDUC AVEC ET ACTIF ASSOCIÉ QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplREGAQ)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES REGARDS D'AQUEDUC AVEC ET ACTIF ASSOCIÉ QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplREGAQ)
		}
	}
	
	private validerHierarchieAQUAccessoiresSansVDMA(){
		String sqlEmplACCAQ = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , DESCRIPTION, LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplACCAQ += "WHERE siteid = 'TP' and itemnum = 'ACCAQ000' and status = 'E' AND UUID IS NOT NULL" +"\n"
		sqlEmplACCAQ += "AND NOT EXISTS (SELECT 1 FROM ASSET a2 WHERE a2.siteid = asset.siteid AND asset.VDM_ACTIF_ASSOCIE = a2.UUID )" +"\n"
		sqlEmplACCAQ += "AND exists (select 1 from lochierarchy WHERE location = asset.location and parent NOT like '__-AQU-VNCH')" +"\n"
		sqlEmplACCAQ += "AND location not like 'BE%'" +"\n"
		sqlEmplACCAQ += "AND location not like 'CF%'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplACCAQ)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE DES ACCESSOIRES D'AQUEDUC SANS ACTIF ASSOCIÉ OU AVEC ACTIF ASSOCIÉ INEXISTANT OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES ACCESSOIRES D'AQUEDUC SANS ACTIF ASSOCIÉ OU AVEC ACTIF ASSOCIÉ INEXISTANT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplACCAQ)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES ACCESSOIRES D'AQUEDUC SANS ACTIF ASSOCIÉ OU AVEC ACTIF ASSOCIÉ INEXISTANT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplACCAQ)
		}
	}
	
	private validerHierarchieAQUAccessoiresAvecVDMA(){
		String sqlEmplACCAQ = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , DESCRIPTION, LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplACCAQ += "WHERE siteid = 'TP' and itemnum = 'ACCAQ000' and status = 'E' AND VDM_ACTIF_ASSOCIE IS NOT NULL AND UUID IS NOT NULL " +"\n"
		sqlEmplACCAQ += "AND EXISTS (SELECT 1 FROM ASSET a2 WHERE a2.siteid = asset.siteid AND asset.VDM_ACTIF_ASSOCIE = a2.UUID ) " +"\n"
		sqlEmplACCAQ += "AND exists (select 1 from lochierarchy WHERE location = asset.location and parent NOT like '__-AQU-CHAMBRE%') " +"\n"
		sqlEmplACCAQ += "AND location not like 'BE%' " +"\n"
		sqlEmplACCAQ += "AND location not like 'CF%' " +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplACCAQ)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE DES ACCESSOIRES D'AQUEDUC AVEC ACTIF ASSOCIÉ OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES ACCESSOIRES D'AQUEDUC AVEC ACTIF ASSOCIÉ QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplACCAQ)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES ACCESSOIRES D'AQUEDUC AVEC ACTIF ASSOCIÉ QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplACCAQ)
		}
	}
	
	private validerHierarchieAQUVannesEntreesService() {
		String sqlEmplVANES = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplVANES += "WHERE siteid = 'TP' and itemnum = 'VAN000' and status = 'E' " +"\n"
		sqlEmplVANES += "and exists(select 1 from assetspec where assetnum = asset.assetnum and assetattrid like 'FONCTION' and alnvalue in ('SD', 'SDG', 'SF', 'SG'))" +"\n"
		sqlEmplVANES += "and exists (select 1 from lochierarchy where location = asset.location and parent NOT like '__-AQU-ES_%')" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplVANES)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE VANNES D'ENTRÉES DE SERVICE D'AQUEDUC OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES VANNES D'ENTRÉES DE SERVICE D'AQUEDUC QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplVANES)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES VANNES D'ENTRÉES DE SERVICE D'AQUEDUC QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplVANES)
		}
	}
	
	private validerHierarchieAQUSegmentsEntreesService() {
		String sqlEmplSEGES = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplSEGES += "WHERE siteid = 'TP' and itemnum = 'SEGAQ000' and status = 'E'" +"\n"
		sqlEmplSEGES += "and exists (select 1 from assetspec where assetnum = asset.assetnum and assetattrid like 'TYPE_SEGMENT_AQ' and alnvalue in ('BD','BDG','BF','BG'))" +"\n"
		sqlEmplSEGES += "and location not like '__-AQU-VANNE%' " +"\n"
		sqlEmplSEGES += "and location not like '__-AQU-ESSEG' " +"\n"
		sqlEmplSEGES += "and location not like 'BE%' " +"\n"
		sqlEmplSEGES += "and location not like 'CF%'" +"\n"
		sqlEmplSEGES += "and exists (select 1 from lochierarchy where location = asset.location and parent  not like '__-AQU-ES%')" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplSEGES)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE SEGMENTS D'ENTRÉES DE SERVICE D'AQUEDUC OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES SEGMENTS D'ENTRÉES DE SERVICE D'AQUEDUC QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplSEGES)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES SEGMENTS D'ENTRÉES DE SERVICE D'AQUEDUC QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplSEGES)
		}
	}
	
	private validerHierarchieEGOSegments() {
		String sqlEmplSEGEG = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplSEGEG += "WHERE siteid = 'TP' and itemnum = 'SEGEG000' and status = 'E' " +"\n"
		sqlEmplSEGEG += "and exists( select 1 from assetspec where assetnum = asset.assetnum and assetattrid like 'TYPE_SEGMENT_AQ' and alnvalue not in ('BD'))" +"\n"
		sqlEmplSEGEG += "and location not like '__-EGO-SEG'" +"\n"
		sqlEmplSEGEG += "and location not like 'BE%' and location not like 'CF%'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplSEGEG)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE SEGMENTS DE RÉSEAU D'EGOUT OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES SEGMENTS DE RÉSEAU D'EGOUT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplSEGEG)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES SEGMENTS DE RÉSEAU D'EGOUT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplSEGEG)
		}
	}
	
	private validerHierarchieEGOPuisards() {
		String sqlEmplPUI = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplPUI += "WHERE siteid = 'TP' and itemnum = 'PUI000' and status = 'E' " +"\n"
		sqlEmplPUI += "AND location NOT LIKE '__-EGO-PUI'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplPUI)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE DES PUISARDS OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES PUISARDS QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplPUI)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES PUISARDS QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplPUI)
		}
	}
	
	private validerHierarchieEGORaccords() {
		String sqlEmplRACEG = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplRACEG += "WHERE siteid = 'TP' and itemnum = 'RACEG000' and status = 'E' " +"\n"
		sqlEmplRACEG += "AND location NOT LIKE '__-EGO-RACC'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplRACEG)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE DES RACCORDS D'EGOUT OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES RACCORDS D'EGOUT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplRACEG)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES RACCORDS D'EGOUT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplRACEG)
		}
	}
	
	private validerHierarchieEGOChambres() {
		String sqlEmplCHEG = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplCHEG += "WHERE siteid = 'TP' and itemnum = 'CHEG000' and status = 'E' " +"\n"
		sqlEmplCHEG += "and exists (select 1 from lochierarchy WHERE location = asset.location and parent NOT like '__-EGO-CH')" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplCHEG)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE CHAMBRE D'AQUEDUC OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES CHAMBRES D'EGOUT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplCHEG)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES CHAMBRES D'EGOUT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplCHEG)
		}
	}
	
	private validerHierarchieEGORegards() {
		String sqlEmplREGEG = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , DESCRIPTION, LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplREGEG += "WHERE siteid = 'TP' and itemnum = 'RAGEG000' and status = 'E' AND uuid IS NOT null" +"\n"
		sqlEmplREGEG += "AND location NOT LIKE '__-EGO-REG'" +"\n"
		sqlEmplREGEG += "AND location NOT LIKE '__-EGO-CHAMBRE%'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplREGEG)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE DES REGARDS D'EGOUT AVEC ET ACTIF ASSOCIÉ OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES REGARDS D'EGOUT AVEC ET ACTIF ASSOCIÉ QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplREGEG)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES REGARDS D'EGOUT AVEC ET ACTIF ASSOCIÉ QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplREGEG)
		}
	}
	
	private validerHierarchieEGOBassins() {
		String sqlEmplBAS = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplBAS += "WHERE siteid = 'TP' and itemnum = 'BAS000' and status = 'E' " +"\n"
		sqlEmplBAS += "AND location NOT LIKE '__-EGO-BAS'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplBAS)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE DES BASSINS D'EGOUT OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES BASSINS D'EGOUT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplBAS)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES BASSINS D'EGOUT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplBAS)
		}
	}
	
	private validerHierarchieEGOAccessoiresSansVDMA(){
		String sqlEmplACCEG = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , DESCRIPTION, LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplACCEG += "WHERE siteid = 'TP' and itemnum = 'ACCEG000' and status = 'E' AND UUID IS NOT NULL" +"\n"
		sqlEmplACCEG += "AND NOT EXISTS (SELECT 1 FROM ASSET a2 WHERE a2.siteid = asset.siteid AND asset.VDM_ACTIF_ASSOCIE = a2.UUID )" +"\n"
		sqlEmplACCEG += "AND exists (select 1 from lochierarchy WHERE location = asset.location and parent NOT like '__-EGO-CH')" +"\n"
		sqlEmplACCEG += "AND location not like 'BE%'" +"\n"
		sqlEmplACCEG += "AND location not like 'CF%'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplACCEG)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE DES ACCESSOIRES D'EGOUT SANS ACTIF ASSOCIÉ OU AVEC ACTIF ASSOCIÉ INEXISTANT OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES ACCESSOIRES D'EGOUT SANS ACTIF ASSOCIÉ OU AVEC ACTIF ASSOCIÉ INEXISTANT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplACCEG)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES ACCESSOIRES D'EGOUT SANS ACTIF ASSOCIÉ OU AVEC ACTIF ASSOCIÉ INEXISTANT QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplACCEG)
		}
	}
	
	private validerHierarchieEGOAccessoiresAvecVDMA(){
		String sqlEmplACCEG = "\n SELECT UUID, VDM_ACTIF_ASSOCIE ,VDM_CLASSEACTIF , VDM_TYPEACTIF , DESCRIPTION, LOCATION , CHANGEBY , CHANGEDATE FROM ASSET " +"\n"
		sqlEmplACCEG += "WHERE siteid = 'TP' and itemnum = 'ACCEG000' and status = 'E' AND VDM_ACTIF_ASSOCIE IS NOT NULL AND UUID IS NOT NULL" +"\n"
		sqlEmplACCEG += "AND EXISTS (SELECT 1 FROM ASSET a2 WHERE a2.siteid = asset.siteid AND asset.VDM_ACTIF_ASSOCIE = a2.UUID)" +"\n"
		sqlEmplACCEG += "AND exists (select 1 from lochierarchy WHERE location = asset.location and parent NOT like '__-EGO-CHAMBRE%')" +"\n"
		sqlEmplACCEG += "AND location not like 'BE%'" +"\n"
		sqlEmplACCEG += "AND location not like 'CF%'" +"\n"
		
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlEmplACCEG)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : VALIDATION HIERARCHIE DES ACCESSOIRES D'EGOUT AVEC ACTIF ASSOCIÉ OK")
		}else {
			errors.add("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES ACCESSOIRES D'EGOUT AVEC ACTIF ASSOCIÉ QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplACCEG)
			markError("ECHEC : VALIDATION HIERARCHIE IL EXISTE DES ACCESSOIRES D'EGOUT AVEC ACTIF ASSOCIÉ QUI NE SONT PAS AUX BONS EMPLACEMENTS "+ sqlEmplACCEG)
		}
	}
	
	private validerCorrespondanceEmplacementTerritoire(){
		validerCorrespondanceEmplacementTerritoireLC()
		validerCorrespondanceEmplacementTerritoireMN()
		validerCorrespondanceEmplacementTerritoireLN()
		validerCorrespondanceEmplacementTerritoireVM()
	}
	
	private validerCorrespondanceEmplacementTerritoireLC(){
		String sqlLC = "\n SELECT COUNT(*) AS NB_ERROR FROM ASSET WHERE VDM_TERRITOIRE = 17 AND location NOT LIKE 'LC%' AND UUID IS NOT NULL AND VDM_TYPEACTIF NOT IN ('RACCORD', 'REGARD','SEGMENT') AND STATUS <> 'R'" +"\n"
		def emplacements = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlLC)
		if(emplacements[0].getAt("NB_ERROR") == 0) {
			markPassed("SUCCES : IL N'EXISTE AUCUN EMPLACEMENT DE LACHINE AVEC UN MAUVAIS PREXISE E AVEC ACTIFS R")
		}else {
			errors.add("ECHEC : Il EXISTE DES EMPLACEMENTS DE LACHINE AVEC LE MAUVAIS PRÉFIXE : "+ sqlLC)
			markError("ECHEC : Il EXISTE DES EMPLACEMENTS DE LACHINE AVEC LE MAUVAIS PRÉFIXE :  "+ sqlLC)
		}
	}
	
	private validerCorrespondanceEmplacementTerritoireMN(){
		String sqlMN = "\n SELECT COUNT(*) AS NB_ERROR FROM ASSET WHERE VDM_TERRITOIRE = 16 AND location NOT LIKE 'MN%' AND UUID IS NOT NULL AND VDM_TYPEACTIF NOT IN ('RACCORD', 'REGARD','SEGMENT') AND STATUS <> 'R'" +"\n"
		def emplacements = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlMN)
		if(emplacements[0].getAt("NB_ERROR") == 0) {
			markPassed("SUCCES : IL N'EXISTE AUCUN EMPLACEMENT DE MONTREAL-NORD AVEC UN MAUVAIS PREXISE E AVEC ACTIFS R")
		}else {
			errors.add("ECHEC : Il EXISTE DES EMPLACEMENTS DE MONTREAL-NORD AVEC LE MAUVAIS PRÉFIXE : "+ sqlMN)
			markError("ECHEC : Il EXISTE DES EMPLACEMENTS DE MONTREAL-NORD AVEC LE MAUVAIS PRÉFIXE :  "+ sqlMN)
		}
	}
	
	private validerCorrespondanceEmplacementTerritoireLN(){
		String sqlLN = "\n SELECT COUNT(*) AS NB_ERROR FROM ASSET WHERE VDM_TERRITOIRE = 14 AND location NOT LIKE 'LN%' AND UUID IS NOT NULL AND VDM_TYPEACTIF NOT IN ('RACCORD', 'REGARD','SEGMENT') AND STATUS <> 'R'" +"\n"
		def emplacements = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlLN)
		if(emplacements[0].getAt("NB_ERROR") == 0) {
			markPassed("SUCCES : IL N'EXISTE AUCUN EMPLACEMENT DE SAINT-LEONARD AVEC UN MAUVAIS PREXISE E AVEC ACTIFS R")
		}else {
			errors.add("ECHEC : Il EXISTE DES EMPLACEMENTS DE SAINT-LEONARD AVEC LE MAUVAIS PRÉFIXE : "+ sqlLN)
			markError("ECHEC : Il EXISTE DES EMPLACEMENTS DE SAINT-LEONARD AVEC LE MAUVAIS PRÉFIXE :  "+ sqlLN)
		}
	}
	
	private validerCorrespondanceEmplacementTerritoireVM(){
		String sqlVM = "\n SELECT COUNT(*) AS NB_ERROR FROM ASSET WHERE VDM_TERRITOIRE = 20 AND location NOT LIKE 'VM%' AND UUID IS NOT NULL AND VDM_TYPEACTIF NOT IN ('RACCORD', 'REGARD','SEGMENT') AND STATUS <> 'R'" +"\n"
		def emplacements = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlVM)
		if(emplacements[0].getAt("NB_ERROR") == 0) {
			markPassed("SUCCES : IL N'EXISTE AUCUN EMPLACEMENT DE VILLE-MARIE AVEC UN MAUVAIS PREXISE E AVEC ACTIFS R")
		}else {
			errors.add("ECHEC : Il EXISTE DES EMPLACEMENTS DE VILLE-MARIE AVEC LE MAUVAIS PRÉFIXE : "+ sqlVM)
			markError("ECHEC : Il EXISTE DES EMPLACEMENTS DE VILLE-MARIE AVEC LE MAUVAIS PRÉFIXE :  "+ sqlVM)
		}
	}
	
	
	private validerTournees() {
		String sql  = "\n SELECT ROUTE " + "\n"
		   sql += "FROM ROUTES" + "\n"
		   sql += "WHERE SITEID = 'TP' \n"
		   sql += "AND EXISTS( " + "\n"
		   sql += "		select * from ROUTE_STOP rs" + "\n"
		   sql += "		WHERE rs.route = routes.route" + "\n"
		   sql += " 	AND rs.siteid = routes.siteid" + "\n"
		   sql += "		AND EXISTS(SELECT 1 FROM ASSET a WHERE a.siteid = 'TP' and status = 'R')" + "\n"
		   sql += "	)" + "\n"
	
	 def transaction = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
	 if(transaction.size() == 0) {
		 markPassed("SUCCES : VALIDER TOURNÉES : IL N'EXISTE AUCUNE TOURNÉE AVEC ACTIFS AU STATUT R ")
	 }else {
		 errors.add("ECHEC : VALIDER TOURNÉES : Il EXISTE DES ACTIFS AU STATUT R DANS LES TOURNÉES" + sql)
		 markError("ECHEC : VALIDER TOURNÉES : Il EXISTE DES ACTIFS AU STATUT R DANS LES TOURNÉES " + sql)
	 }
}
	
	
	
	private resumerErreur() {
		if(errors.size()> 0) {
			KeywordUtil.markPassed("!!!!=========> FIN DE L EXECUTION : il y a "+errors.size()+" erreur(s)! <===============!!!! :(")
			for(int e = 0; e < errors.size();e++) {
				KeywordUtil.markWarning(errors.get(e))
			}
			
		}else {
			KeywordUtil.markPassed("Fin de l'execution sans erreurs! :)")
		}
		return null
	}
	
	private convertXmlToGson(xml) {
		//Define HashMap to store element from XML
		HashMap<String,Object> map = new HashMap();
		
		// Parse it
		def parsed = new XmlParser().parseText( xml )

		// Convert it to a Map containing a List of Maps
		def jsonObject = parsed.ATTRIBUT.collect {
			//add only selected variables to holder object(here HashMap),
			map.put(it.@nom, it.text())
		}

		//convert holder object to JSONObject directly and return as string as follows
		return new Gson().fromJson(new Gson().toJson(map) , JsonObject.class)
	}
	
	
	
	private markPassed(value) {
		if(activeLog) {
			KeywordUtil.markPassed(value)
		}
	}
	
	private markError(value) {
		if(activeLog) {
			KeywordUtil.markError(value)
		}
	}
	
	
	private markInfo(value) {
		if(activeLog) {
			KeywordUtil.logInfo(value)
		}
	}
	
}


TestsCasesMep07BD test = new TestsCasesMep07BD()
test.executerTestCase()
