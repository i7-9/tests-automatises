
import com.kms.katalon.core.testcase.TestCase as TestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import internal.GlobalVariable as GlobalVariable
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.GsonBuilder
import com.kms.katalon.core.util.KeywordUtil
import org.json.JSONObject
import org.json.XML
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.concurrent.Callable
import java.util.concurrent.Executors

public class TestsCasesMep07BD {

   public TestsCasesMep07BD(){}
			 
   def transactionsJournal
   def noJournal
   def nbThread
   
   def listGenAttrsAsset = [
	   "VDM_CLASSIFICATION_RESEAU","VDM_ADRESSE","VDM_INV_NO", "VDM_INV_ID", "VDM_ACTIF_ASSOCIE", "INSTALLDATE","VDM_DATE_FIN","VDM_DATE_ABANDON",
	   "VDM_ELEVATION_TERRAIN","VDM_RESPONSABLE_OPERATIONNEL", "VDM_TERRITOIRE","VDM_PROPRIETAIRE","VDM_JURIDICTION","VDM_HIERARCHIE_TRONCON",
	   "VDM_STATUT_POSITION_ACTIF", "VDM_STATUT_DESSIN_NUMERISATION","VDM_LOCALISATION", "STATUS","VDM_NO_SECTEUR_REGULATION", 
	   "VDM_NOM_RESERVOIR", "VDM_TYPE_RESEAU", "VDM_COORDONNEE_SPATIALE_X", "VDM_COORDONNEE_SPATIALE_Y", "ASSETTAG"
   ]
   
  /** EGOUT **/
  def listSEGEGAttrsAssetSpecs = [["BASSIN_VERSANT", "ALN"], ["CLASSE_DE_RESISTANCE_EG","ALN"], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DATE_REHABILITATION","DATE"], ["DECOUPAGE", "ALN"], ["DIAMETRE_H_MM", "ALN"], ["DIAMETRE_H_PO", "ALN"], ["DIAMETRE_V_MM", "ALN"], ["DIAMETRE_V_PO", "ALN"], ["FORME_CONDUITE", "ALN"], ["ID_EGO_POINT_DEBUT","ALN"], ["ID_EGO_POINT_FIN", "ALN"], ["MATERIAU_SEGMENT_EG", "ALN"], ["NO_CONDUITE_PI", "ALN"],["NO_CONTRAT", "ALN"], ["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"], ["NOM_USUEL", "ALN"], ["PENTE_FOSSE_LATERAL_1","ALN"], ["PENTE_FOSSE_LATERAL_2", "ALN"], ["PRECISION_DATE_INSTALL","ALN"], ["PROFONDEUR_RADIER_AMONT","ALN"],["PROFONDEUR_RADIER_AVAL","ALN"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["SENS_ECOULEMENT", "ALN"],["TYPE_ECOULEMENT", "ALN"],["TYPE_REHABILITATION_EG", "ALN"],["TYPE_SEGMENT_EG", "ALN"]]
  def listCHEGAttrsAssetSpecs = [["BASSIN_VERSANT", "ALN"], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "DATE"], ["FORME_CHAMBRE", "ALN"], ["LARGEUR_CHAMBRE", "NUM"],["LONGUEUR_CHAMBRE", "NUM"], ["MATERIAU_CHAMBRE_EG", "ALN"], ["NO_CHAMBRE_DEEU", "ALN"], ["NO_TRONCON_GEOBASE","ALN"], ["NOM_FICHIER_CREATION","ALN"], ["PRECISION_DATE_INSTALL","ALN"],["PROFONDEUR", "NUM"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_CHAMBRE_EG","ALN"]]
  def listACCEGAttrsAssetSpecs = [["ALTITUDE", "NUM"],["COMPARTIMENT", "ALN"],["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"],["ID_POINT", "ALN"],["ID_POLE", "ALN"],["NO_TRONCON_GEOBASE", "ALN"],["NOM_FICHIER_CREATION", "ALN"],["PRECISION_DATE_INSTALL","ALN"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"]]
  def listREGEGAttrsAssetSpecs = [["ALTITUDE", "NUM"],["BASSIN_VERSANT", "ALN"],["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"],["DECOUPAGE", "ALN"],["DIMENSION_BASE_M", "ALN"], ["DIMENSION_CHEMINEE_M","ALN"], ["DIMENSION_COUVERCLE_PO","ALN"],["ID_POINT", "ALN"],["ID_POLE", "ALN"],["MATERIAU_REGARD", "ALN"],["NO_GEOMATIQUE_CHAMBRE_LIE","ALN"],["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"], ["PRECISION_DATE_INSTALL","ALN"],["PROFONDEUR", "NUM"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_REGARD_EG", "ALN"],["VISIBLE", "ALN"]]
  def listRACEGAttrsAssetSpecs = [["ALTITUDE", "NUM"], ["BASSIN_VERSANT", "ALN"],["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"],["DECOUPAGE", "ALN"],["ID_POINT", "ALN"],["ID_POLE", "ALN"],["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"],["PRECISION_DATE_INSTALL","ALN"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"], ["TYPE_RACCORD_EG", "ALN"]]
  def listBASAttrsAssetSpecs = [["BASSIN_VERSANT", "ALN"],["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "ALN"], ["LARGEUR_BASSIN", "ALN"], ["LONGUEUR_BASSIN", "ALN"],["MATERIAU_BASSIN", "ALN"],["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"],["PRECISION_DATE_INSTALL","ALN"],["PROFONDEUR", "NUM"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_BASSIN", "ALN"]]
  def listPUIAssetSpecs = [["ALTITUDE", "NUM"], ["BASSIN_VERSANT", "ALN"],["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"],["DECOUPAGE", "ALN"], ["DIMENSION_GRILLE", "ALN"],["ID_POINT", "ALN"],["ID_POLE", "ALN"],["MATERIAU_PUISARD", "ALN"],["NO_PUISARD_REMPLACE","ALN"],["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"],["PRECISION_DATE_INSTALL","ALN"],["PROFONDEUR", "NUM"], ["PROVENANCE_DONNEE", "ALN"], ["REMARQUE_GEOMATIQUE", "ALN"], ["VISIBLE", "ALN"]]
   
   /** AQUEDUC **/
  def listRACAQAttrsAssetSpecs = [["ALTITUDE", "NUM"],["COMPARTIMENT", "ALN"],["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"],["DECOUPAGE", "ALN"],["ID_POINT", "ALN"],["ID_POLE", "ALN"], ["MATERIAU_RACCORD_AQ", "ALN"], ["NO_TRONCON_GEOBASE",  "ALN"],["NOM_FICHIER_CREATION","ALN"], ["PRECISION_DATE_INSTALL","ALN"], ["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_RACCORD_AQ", "ALN"]]
  def listACCAQAttrsAssetSpecs = [["ALTITUDE", "NUM"], ["COMPARTIMENT", "ALN"], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "ALN"], ["DIAMETRE_MM", "ALN"], ["ID_POINT", "ALN"], ["ID_POLE", "ALN"], ["NO_TRONCON_GEOBASE", "ALN"], ["NOM_FICHIER_CREATION","ALN"], ["PRECISION_DATE_INSTALL","ALN"], ["PROVENANCE_DONNEE", "ALN"], ["REMARQUE_GEOMATIQUE", "ALN"], ["TYPE_ACCESSOIRE_AQ","ALN"]]
  def listVANAttrsAssetSpecs = [["ALTITUDE", "NUM"],["COMPARTIMENT", "ALN"], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "ALN"], ["DIAMETRE_MM", "ALN"], ["FONCTION", "ALN"], ["ID_POINT", "ALN"], ["ID_POLE", "ALN"], ["MECANISME_VANNE", "ALN"], ["NO_CHAMBRE_SUR_PLAN_CLE","ALN" ], ["NO_TRONCON_GEOBASE", "ALN"], ["NOM_FICHIER_CREATION", "ALN"], ["ORIENTATION_VANNE", "ALN"], ["PRECISION_DATE_INSTALL", "ALN"], ["PROFONDEUR", "NUM"], ["PROVENANCE_DONNEE", "ALN"], ["REMARQUE_GEOMATIQUE", "ALN"], ["VISIBLE", "ALN"]]    
  def listCHAQAttrsAssetSpecs = [["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "ALN"], ["FORME_CHAMBRE", "ALN"], ["LARGEUR_CHAMBRE", "NUM"], ["LONGUEUR_CHAMBRE", "NUM"], ["MATERIAU_CHAMBRE_AQ", "ALN"], ["NO_CHAMBRE_REGULATION","ALN"], ["NO_CHAMBRE_SUR_PLAN_CLE", "ALN"], ["NO_PLAN_CONTRAT_TEC", "ALN"], ["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"], ["PROFONDEUR", "NUM"],["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_CHAMBRE", "ALN"]]
  def listSEGAQAttrsAssetSpecs = [["CLASSE_DE_RESISTANCE_AQ", "ALN" ], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DATE_REHABILITATION", "DATE"], ["DECOUPAGE", "ALN"], ["DIAMETRE_MM", "ALN"], ["ID_POINT_DEBUT", "ALN"], ["ID_POINT_FIN", "ALN"], ["MATERIAU_SEGMENT_AQ", "ALN"], ["NO_TRONCON_GEOBASE","ALN"], ["NOM_FICHIER_CREATION","ALN"], ["PROVENANCE_DONNEE", "ALN"], ["REMARQUE_GEOMATIQUE", "ALN"],["TYPE_PROTECTION", "ALN"], ["TYPE_REHABILITATION_AQ","ALN"],["TYPE_SEGMENT_AQ","ALN"]]
  def listBIAssetSpecs = [["DATE_CREEE", "DATE"],["DATE_MODIFIEE", "DATE"],["DECOUPAGE", "ALN"], ["ID_POINT", "ALN"],["ID_POLE", "ALN"],["NO_TRONCON_GEOBASE","ALN"],["NOM_FICHIER_CREATION","ALN"],["PRECISION_DATE_INSTALL","ALN"],["PROFONDEUR", "NUM"],	   ["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"]]
  def listREGAQAssetSpecs  = [["ALTITUDE", "NUM"], ["DATE_CREEE", "DATE"], ["DATE_MODIFIEE", "DATE"], ["DECOUPAGE", "ALN"], ["ID_POINT", "ALN"], ["ID_POLE", "ALN"], ["NO_TRONCON_GEOBASE", "ALN"], ["NOM_FICHIER_CREATION","ALN"], ["PRECISION_DATE_INSTALL","ALN"], ["PROVENANCE_DONNEE", "ALN"],["REMARQUE_GEOMATIQUE", "ALN"], ["TYPE_REGARD_AQ", "ALN"]]

  def errors = new ArrayList()
  def activeLog = false
  
  DecimalFormat df = new DecimalFormat("#.000")
  
  
   public executerTestCase() {
		configurerConnexionBD()
		definirNoJournal()
		recupérerTransactionsDuJournal()
		validerTransactions()
		validerActifsExistantsSansEmplacements()
		resumerErreur()
	}
	
	private definirNoJournal() {
		if(CustomKeywords.'maximo.BDKeys.recupererNoJournal'()) {
			noJournal = CustomKeywords.'maximo.BDKeys.recupererNoJournal'()
			markInfo( "Le NO de journal saisi est : "+ noJournal)
		}else {
			noJournal = recupererDernierNoJournal()
			markInfo( "Aucun NO de journal saisi, le dernier journal consommé sera récupéré : "+ noJournal)
		}
	}
	
	private recupererDernierNoJournal(){
		String sql = "\n SELECT MAX(TXNJOURNALNO) AS NO_JOURNAL" + "\n"
		sql +="FROM VDM_BDSA_ASSETTXN vba" + "\n"
		def noJournalBd = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		return noJournalBd[0].getAt("NO_JOURNAL")
	}
	
	private configurerConnexionBD() {
		CustomKeywords.'maximo.BDKeys.configureConnexionDB'()
	}
		
	private recupérerTransactionsDuJournal() {
		def statusTransactions = validerStatutTransactionsJournal()
		if(statusTransactions) {
			validationIntegritéJournal()
			String sql = "\n SELECT TXNJOURNALNO, UUID, UUID_AVANT, VDM_CLASSEACTIF, VDM_TYPEACTIF, TXNSEQUENCE, ACTION, TXN_MESSAGE, TXNFICTIVECHANGED, TXNTERRITORYCHANGED FROM VDM_BDSA_ASSETTXN WHERE TXNJOURNALNO = "+ noJournal + " ORDER BY TXNSEQUENCE"
			transactionsJournal = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
			markPassed("JOURNAL : " + noJournal + " contient "+ transactionsJournal.size() +" transactions")
		}else {
			KeywordUtil.markErrorAndStop("JOURNAL : "+ noJournal + " contient des transactions <> du statut TRAITE")
		}
	}
	
	private validerStatutTransactionsJournal() {
		String sql = "\n SELECT TXNJOURNALNO FROM VDM_BDSA_ASSETTXN WHERE TXNJOURNALNO = "+ noJournal + "\n" 
		       sql +="AND TXNSTATUS <> 'TRAITE' ORDER BY TXNSEQUENCE"
		def transactions = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		if(transactions.size() > 0) {
			return false
		}else {
			return true
		}
	}
	
	private validationIntegritéJournal() {
		KeywordUtil.logInfo("JOURNAL : "+ noJournal + " Validation de l'intégrité du journal en cours")
		validerNombreTransactions()
		validerIntegriteAgregation()
		validerIntegriteScindement()
	}
	
	private validerNombreTransactions() {
		String sql = "\n SELECT COUNT(*) AS NBTXNSBD, TXNTOTALACTIF AS NBTXNBASE FROM VDM_BDSA_ASSETTXN a "+"\n"
		sql +="WHERE a.TXNJOURNALNO = "+ noJournal + "\n"
		sql +="GROUP BY TXNTOTALACTIF"+ "\n"
		def nbTxns = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		
		if(nbTxns[0].get("NBTXNSBD") != nbTxns[0].get("NBTXNBASE")) {
			errors.add("JOURNAL : "+ noJournal + " n'est pas intègre au niveau du nombre de transactions")
			//KeywordUtil.markError("JOURNAL : "+ noJournal + " n'est pas intègre au niveau du nombre de transactions")
			return false
		}else if (nbTxns.size() == 0){
			errors.add("JOURNAL : "+ noJournal + " n'existe pas dans l'envrionnement")
			//KeywordUtil.markError("JOURNAL : "+ noJournal + " n'existe pas dans l'envrionnement")
			return true
		}else {
			return true
		}
	}
	
	private validerIntegriteAgregation() {
		String sql = "\n SELECT * FROM VDM_BDSA_ASSETTXN a "+"\n"
		sql +="WHERE a.TXNJOURNALNO =  "+ noJournal + "\n"
		sql +="AND a.ACTION = 'AGREGATION'"+"\n"
		sql +="AND ((SELECT COUNT(*) FROM VDM_BDSA_ASSETTXN b WHERE b.TXNJOURNALNO =  "+ noJournal + "  AND b.UUID = a.UUID AND (b.ACTION = 'AGREGATION' OR (b.ACTION = 'INSERTION' AND b.TXNTERRITORYCHANGED = 'NMXM_TO_MXM'))) < 2"+"\n"
		sql +="OR (SELECT COUNT(*) FROM VDM_BDSA_ASSETTXN c WHERE c.TXNJOURNALNO =  "+ noJournal + "  AND c.UUID = a.UUID_AVANT  AND c.ACTION = 'SUPPRESSION') <> 1)"+"\n"
		
		def transactions = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		
		if(transactions.size() > 0) {
			errors.add("JOURNAL : "+ noJournal + " n'est pas intègre au niveau des agregations")
			//KeywordUtil.markError("JOURNAL : "+ noJournal + " n'est pas intègre au niveau des agregations")
			return false
		}else {
			return true
		}
	}
	
	private validerIntegriteScindement() {
		String sql = "\n SELECT * FROM VDM_BDSA_ASSETTXN a "+"\n"
		sql +="WHERE a.TXNJOURNALNO = "+ noJournal + "\n"
		sql +="AND a.ACTION = 'SCINDEMENT'"+"\n"
		sql +="AND (NOT EXISTS (SELECT 1 FROM VDM_BDSA_ASSETTXN b WHERE b.TXNJOURNALNO = 76564 AND b.ACTION = 'SCINDEMENT' AND b.UUID <> a.UUID )"+"\n"
		sql +="OR NOT EXISTS (SELECT 1 FROM VDM_BDSA_ASSETTXN c WHERE c.TXNJOURNALNO = 76564 AND c.ACTION = 'SUPPRESSION' AND c.UUID = a.UUID_AVANT))"+"\n"
		
		def transactions = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		if(transactions.size() > 0) {
			errors.add("JOURNAL : "+ noJournal + " n'est pas intègre au niveau des scindements")
			//KeywordUtil.markError("JOURNAL : "+ noJournal + " n'est pas intègre au niveau des scindements")
			return false
		}else {
			return true
		}
	}
	
	private validerTransactions() {
		if(transactionsJournal.size() > 0) {

			//def pool = Executors.newFixedThreadPool( 4 )
				//( 1..4 ).each { more -> pool.submit {
					//println(getCursor())
				//}}
			
			if(noSeqExludedMin == null && noSeqExludedMax == null ) {
				(transactionsJournal as ArrayList).parallelStream().forEachOrdered({txn ->
					definirAction(txn)
				})
			}else {
				def cursor = 0
				def txnsq
				while( cursor <= transactionsJournal.size() ) 
				{ 
					txnsq = transactionsJournal[cursor].getAt("TXNSEQUENCE")
					
					if(txnsq < noSeqExludedMin || txnsq > noSeqExludedMax ) {
							definirAction(transactionsJournal[cursor])
					}
					cursor = cursor + 1;
				}
			}
				
		}else {
			errors.add("Le résultat de la récupération des transactions du journal "+ noJournal + " est NULL ")
			KeywordUtil.markErrorAndStop("Le résultat de la récupération des transactions du journal "+ noJournal + " est NULL ")
		}
	}
	
	private getCursor() {
		cursor= cursor + 1;
		return cursor-1;
	}
	
	
	private validerSiTransactionSuperieureExistantes(uuid, txnsequence, classeActif, typeActif) {
		String sql =  "\n SELECT TXNJOURNALNO, UUID, UUID_AVANT, VDM_CLASSEACTIF, VDM_TYPEACTIF, TXNSEQUENCE, ACTION, TXN_MESSAGE "+ "\n"
			   sql += "FROM VDM_BDSA_ASSETTXN WHERE TXNJOURNALNO = "+ noJournal  + "\n"
			   sql += "AND UUID = '"+uuid+"' AND TXNSEQUENCE > "+txnsequence + "\n"
			   sql += "ORDER BY TXNSEQUENCE"
			   
		def listeTxnSup = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		if(activeLog) {
			if(listeTxnSup != null || listeTxnSup.size() > 0) {
				markPassed("Transaction(s) existante(s) > " +  txnsequence + " "+ classeActif + " "+ typeActif +" au UUID "+ uuid)
			}else {
				markPassed("Aucune(s) transaction(s)  > " +  txnsequence + " au UUID "+ uuid)
			}
		}

		return listeTxnSup
	}
	
	private peuxDisposerActifFictif(classeActif, typeActif) {
		def disposeFictif = false
		if (classeActif =="AQUEDUC" && (typeActif =="RACCORD" || typeActif=="REGARD")) {
			disposeFictif = true
		}else if (classeActif =="EGOUT" && (typeActif =="RACCORD" || typeActif=="SEGMENT" || typeActif=="REGARD")) {
			disposeFictif = true
		}
		return disposeFictif
	}
	
	private definirAction(transaction) {
		def transactionsSuperieures = validerSiTransactionSuperieureExistantes(transaction.getAt("UUID"), transaction.getAt("TXNSEQUENCE"),transaction.getAt("VDM_CLASSEACTIF"),transaction.getAt("VDM_TYPEACTIF") )
		
		if(peuxDisposerActifFictif(transaction.getAt("VDM_CLASSEACTIF"), transaction.getAt("VDM_TYPEACTIF"))){
			def jsonActif = convertXmlToGson(transaction.getAt("TXN_MESSAGE").getSubString(1, (int) transaction.getAt("TXN_MESSAGE").length()))
			if(jsonActif.get("ACTIF_FICTIF").getAsString() == "OUI" && transaction.getAt("TXNFICTIVECHANGED") == null) {
				if(activeLog) {
					KeywordUtil.logInfo( transaction.getAt("TXNSEQUENCE") + " ACTIF AVEC UUID "+ transaction.getAt("UUID") + " EST UN ACTIF FICTIF ")
				}
				
			}else {
				validerAction(transactionsSuperieures, transaction)
			}
		}else {
			validerAction(transactionsSuperieures, transaction )
		}
	}
	
	private validerAction(transactionsSuperieures, transaction) {		
		//Valider le résultat de l'action
		switch (transaction.getAt("ACTION")) {
			case "INSERTION": 
				validerInsertion(transactionsSuperieures, transaction) 
				break
			case "AGREGATION": 
				validerAgregation(transactionsSuperieures, transaction) 
				break
			case "SCINDEMENT": 
				validerScindement(transactionsSuperieures, transaction) 
				break
			case "SUPPRESSION": 
				validerSuppression(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"), transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE")) 
				break
			default: 
				validerModificationAttributs(transactionsSuperieures, transaction) 
				break
		}
		
		//Valider hierarchie emplacement
		if(transactionsSuperieures.size() == 0  && transaction.getAt("ACTION") != "SUPPRESSION" && transaction.getAt("ACTION") != "AJUSTEMENT" && transaction.getAt("ACTION") != "REBRANCHEMENT" && transaction.getAt("TXNTERRITORYCHANGED") != "MXM_TO_NMXM" ) {
			validerHierarchieEmplacement(transaction)
		}
	}
	

	
	private validerInsertion(transactionsSuperieures, transaction) {
		if(transactionsSuperieures.size() == 0) {
			//Valider structure complète de l'actif
			effectuerValidationActifComplet(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXN_MESSAGE"), transaction.getAt("TXNSEQUENCE"))
		}else {
			//Valider structure partielle de l'actif
			effectuerValidationActifPartielle(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE"))
		}		
	}
	
	private validerAgregation(transactionsSuperieures, transaction) {
		KeywordUtil.logInfo( transaction.getAt("TXNSEQUENCE") + " DEBUT VALIDATION AGREGATION au UUID "+ transaction.getAt("UUID") + " UUID_AVANT " + transaction.getAt("UUID_AVANT"))
		if(transactionsSuperieures.size() == 0) {
			//Valider structure complète de l'actif
			effectuerValidationActifComplet(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXN_MESSAGE"), transaction.getAt("TXNSEQUENCE"))
		}else {
			//Valider structure partielle de l'actif
			effectuerValidationActifPartielle(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE"))
		}	
		validerSuppression(transaction.getAt("UUID_AVANT"), transaction.getAt("VDM_CLASSEACTIF"), transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE"))
	}

	private validerScindement(transactionsSuperieures, transaction) {
		KeywordUtil.logInfo( transaction.getAt("TXNSEQUENCE") + " DEBUT VALIDATION SCINDEMENT au UUID "+ transaction.getAt("UUID") + " UUID_AVANT " + transaction.getAt("UUID_AVANT"))
		if(transactionsSuperieures.size() == 0) {
			//Valider structure complète de l'actif
			effectuerValidationActifComplet(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXN_MESSAGE"), transaction.getAt("TXNSEQUENCE"))
		}else {
			//Valider structure partielle de l'actif
			effectuerValidationActifPartielle(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE"))
		}
		validerSuppression(transaction.getAt("UUID_AVANT"), transaction.getAt("VDM_CLASSEACTIF"), transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXNSEQUENCE"))
	}
	
	private validerSuppression(uuid, classeActif, typeActif, txnsequence) {
		String sql = "\n SELECT ASSETNUM FROM ASSET" + "\n"
               sql +="WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' \n" 
			   sql+= "AND STATUS = 'R' AND VDM_DATE_FIN IS NOT NULL"
			   
		def transaction = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		if(transaction.size() == 1) {
			if(activeLog) {
				markPassed(txnsequence +" SUCCES SUPPRESION: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" a été retiré avec succès avec la transaction")
			}
			validerActifNonExistantDansTournees(uuid, classeActif, typeActif, txnsequence)
			return true
		}else {
			errors.add(txnsequence +" ECHEC SUPPRESSION: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +  " n'a pas été retiré avec la transaction")
			//KeywordUtil.markError(txnsequence +" ECHEC SUPPRESSION: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +  " n'a pas été retiré avec la transaction")
			return false
		}
	}
	
	private validerActifNonExistantDansTournees(uuid, classeActif, typeActif, txnsequence) {
			String sql  = "\n SELECT ROUTE " + "\n"
			   sql += "FROM ROUTES" + "\n"
               sql += "WHERE SITEID = 'TP' \n"  
			   sql += "AND EXISTS( " + "\n"
			   sql += "		select * from ROUTE_STOP rs" + "\n"
			   sql += "		WHERE rs.route = routes.route" + "\n"
			   sql += " 	AND rs.siteid = routes.siteid" + "\n"
			   sql += "		AND EXISTS(SELECT 1 FROM ASSET a WHERE a.UUID = '" + uuid + "' AND rs.assetnum = a.assetnum)" + "\n"
			   sql += "	)" + "\n"
		
		 def transaction = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		 if(transaction.size() == 0) {
			 markPassed(txnsequence +" SUCCES SUPPRESSION ACTIF DANS TOURNÉE : "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" a été retiré avec succès des tournées")
			 return true
		 }else {
			 errors.add(txnsequence +" ECHEC SUPPRESSION ACTIF DANS TOURNÉE : "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +  " n'a pas été retiré correctement des tournées ")
			 //KeywordUtil.markError(txnsequence +" ECHEC SUPPRESSION: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +  " n'a pas été retiré correctement des tournées" )
			 return false
		 }
	}
	
	private validerModificationAttributs(transactionsSuperieures, transaction) {
		if(transactionsSuperieures.size() > 0) {
			markPassed(transaction.getAt("TXNSEQUENCE")  +" SUCCES MODIFICATION_ATTR: "+ transaction.getAt("VDM_CLASSEACTIF") + " " + transaction.getAt("VDM_TYPEACTIF") + " avec le UUID : " + transaction.getAt("UUID") +" dispose de transactions >  le contenu de cette transaction ne sera pas validé")
		}else {
			effectuerValidationActifComplet(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"),  transaction.getAt("VDM_TYPEACTIF"), transaction.getAt("TXN_MESSAGE"), transaction.getAt("TXNSEQUENCE"))
		}
	}
		
	private effectuerValidationActifComplet(uuid, classeActif, typeActif, xmlClob, txnsequence) {
		def jsonActif = convertXmlToGson(xmlClob.getSubString(1, (int) xmlClob.length()))
		validerTableAsset(uuid, classeActif, typeActif, txnsequence, jsonActif )
		validerTableAssetSpecs(uuid, classeActif, typeActif, txnsequence, jsonActif)	
	}
	
	private effectuerValidationActifPartielle(uuid, classeActif, typeActif, txnsequence) {
		String sql = genererSQLValidationActifPartiel(uuid, classeActif,  typeActif)
		def actif = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		
		if(actif.size() == 1) {
			markPassed(txnsequence +" SUCCES INSERTION VALIDATION PARTIELLE: "+ classeActif + " " + typeActif+ " avec le UUID : " + uuid +" validation partielle conforme")
		}else {
			errors.add(txnsequence +" ECHEC INSERTION VALIDATION PARTIELLE: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" validation partielle NON conforme")
			//KeywordUtil.markError(txnsequence +" ECHEC INSERTION VALIDATION PARTIELLE: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" validation partielle NON conforme")
		}
	}
	
	private validerTableAsset(uuid, classeActif, typeActif, txnsequence, jsonActif) {
		//Valider ASSET
		String sqlAsset = sqlTableAssetActif(uuid, classeActif, typeActif, jsonActif)
		def resultatActif = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlAsset)
		if(resultatActif.size() == 1) {
			markPassed(txnsequence + " SUCCES VALIDATION TABLE ASSET: "+ classeActif+ " " + typeActif+ " avec le UUID : " + uuid +" données conformes ")
		}else {
			errors.add(txnsequence + " ECHEC VALIDATION TABLE ASSET: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" données NON conformes " + sqlAsset)
			//KeywordUtil.markError(txnsequence + " ECHEC VALIDATION TABLE ASSET: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" données NON conformes ")
		}
	}
	
	private validerTableAssetSpecs(uuid, classeActif, typeActif, txnsequence, jsonActif) {
		//Valider ASSETSPEC
		String sqlSpecs = sqlTableAssetSpecsActif(uuid, classeActif, typeActif, jsonActif)
		def resultatSpecs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlSpecs)
		
		if(resultatSpecs.size() == 1) {
			markPassed(txnsequence +" SUCCES TABLE ASSETSPECS: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" données conformes")
		}else {
			errors.add(txnsequence +" ECHEC TABLE ASSETSPECS: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" données NON conformes" + sqlSpecs)
			//KeywordUtil.markError(txnsequence +" ECHEC TABLE ASSETSPECS: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" données NON conformes")
		}
	}
	
	private sqlTableAssetActif(uuid, classeActif, typeActif, jsonActif) {
		
		String sqlTableAsset = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			   sqlTableAsset +="AND UUID = '"+ uuid + "'" + " \n"
			   sqlTableAsset +="AND VDM_CLASSEACTIF = '"+ classeActif + "' \n"
			   sqlTableAsset +="AND VDM_TYPEACTIF = '"+ typeActif + "' \n"
			   
			   for(int a = 0; a < listGenAttrsAsset.size(); a++) {
				   if(jsonActif.get(listGenAttrsAsset[a]) != null) {
					   if(jsonActif.get(listGenAttrsAsset[a]).getAsString() != "") {
						   if(listGenAttrsAsset[a] == "INSTALLDATE" || listGenAttrsAsset[a] =="VDM_DATE_FIN" || listGenAttrsAsset[a] == "VDM_DATE_ABANDON") {
							   sqlTableAsset +="AND TRUNC("+ listGenAttrsAsset[a]+ ") = TRUNC(to_timestamp( '"+ jsonActif.get(listGenAttrsAsset[a]).getAsString() + "' , 'YYYY-MM-DD "+ "\"T\""+ "HH24:MI:SS.ff3"+ "\"Z\""+ "')) \n"
						   }
						   else if(listGenAttrsAsset[a] == "VDM_COORDONNEE_SPATIALE_X" ||listGenAttrsAsset[a] == "VDM_COORDONNEE_SPATIALE_Y" ||listGenAttrsAsset[a] == "VDM_ELEVATION_TERRAIN") {
							   //BigDecimal bdecim = new BigDecimal(Float.parseFloat(jsonActif.get(listGenAttrsAsset[a]).getAsString()))
							   sqlTableAsset +="AND "+ listGenAttrsAsset[a]+ " = '"+ df.format(jsonActif.get(listGenAttrsAsset[a]).getAsBigDecimal()).toString().replace(".", ",") + "' \n"
						   }
						   else if (listGenAttrsAsset[a] =="VDM_ADRESSE") {
							   sqlTableAsset +="AND "+ listGenAttrsAsset[a]+ " LIKE '"+ jsonActif.get(listGenAttrsAsset[a]).getAsString().replace("'", "_") + "' \n"
						   }
						   else {
							   if(listGenAttrsAsset[a] != "VDM_ACTIF_ASSOCIE" || (listGenAttrsAsset[a] == "VDM_ACTIF_ASSOCIE" && identifierVDM_ACTIF_ASSOCIE(jsonActif.get("VDM_ACTIF_ASSOCIE").getAsString()))) {
								   sqlTableAsset +="AND "+ listGenAttrsAsset[a]+ " = '"+ jsonActif.get(listGenAttrsAsset[a]).getAsString() + "' \n"
							   }
						   }
					   }else {
						   sqlTableAsset +="AND "+ listGenAttrsAsset[a]+ " IS NULL  \n "
					   }
				   }
			   }

		return sqlTableAsset
	}
	
	private sqlTableAssetSpecsActif(uuid, classeActif, typeActif, jsonActif) {
		def listSpecs = recupererListeAssetSpecs(classeActif, typeActif)
		String sqlTableAssetSpecs = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			   sqlTableAssetSpecs +="AND UUID = '"+ uuid + "'" + " \n"
			   sqlTableAssetSpecs +="AND VDM_CLASSEACTIF = '"+ classeActif + "' \n"
			   sqlTableAssetSpecs +="AND VDM_TYPEACTIF = '"+ typeActif + "' \n"
		
	   for(int a = 0; a < listSpecs.size(); a ++) {
		   String key = listSpecs[a][0] as String
		   String keyType = listSpecs[a][1] as String

		   if(jsonActif.get(key) != null) {
			   if(jsonActif.get(key).toString() != "") {		   
				   if(keyType == "ALN") {
					   if(key != "REMARQUE_GEOMATIQUE") {
						   if(jsonActif.get(key).getAsString() == "") {
							   sqlTableAssetSpecs += " AND EXISTS(SELECT 1 FROM ASSETSPEC WHERE ASSETSPEC.ASSETNUM = ASSET.ASSETNUM AND ASSETSPEC.ASSETATTRID = '"+ key +"' AND ASSETSPEC.ALNVALUE IS NULL)"+ " \n"
						   }else {
							   sqlTableAssetSpecs += " AND EXISTS(SELECT 1 FROM ASSETSPEC WHERE ASSETSPEC.ASSETNUM = ASSET.ASSETNUM AND ASSETSPEC.ASSETATTRID = '"+ key +"' AND ASSETSPEC.ALNVALUE = '"+ jsonActif.get(key).getAsString() +"')"+ " \n"
						   }
					   }
				   }else if (keyType == "NUM") {
					   if(key == "LONGUEUR_CHAMBRE" || key == "LARGEUR_CHAMBRE" || key == "PROFONDEUR" || key == "ALTITUDE") {
						   sqlTableAssetSpecs += " AND EXISTS(SELECT 1 FROM ASSETSPEC WHERE ASSETSPEC.ASSETNUM = ASSET.ASSETNUM AND ASSETSPEC.ASSETATTRID = '"+ key +"' AND ASSETSPEC.NUMVALUE = '"+ df.format(jsonActif.get(key).getAsBigDecimal()).toString().replace(".", ",") + "')" + " \n"	   
					   }else {
						   sqlTableAssetSpecs += " AND EXISTS(SELECT 1 FROM ASSETSPEC WHERE ASSETSPEC.ASSETNUM = ASSET.ASSETNUM AND ASSETSPEC.ASSETATTRID = '"+ key +"' AND ASSETSPEC.NUMVALUE = '"+ jsonActif.get(key).getAsString().replace(".", ",") +"')" + " \n"
					   }
			
				   }else if (keyType == "DATE") {
					   if(jsonActif.get(key).getAsString() != "") {
						   sqlTableAssetSpecs += " AND EXISTS(SELECT 1 FROM ASSETSPEC WHERE ASSETSPEC.ASSETNUM = ASSET.ASSETNUM AND ASSETSPEC.ASSETATTRID = '"+ key +"' AND ASSETSPEC.ALNVALUE = '"+ jsonActif.get(key).getAsString() +"')"+ " \n"
					   }
				   }
			   }
		   }
	   }
		return sqlTableAssetSpecs
	}
	
	
	private genererSQLValidationActifPartiel(uuid, classeActif, typeActif) {
		String sqlAssetPartiel = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			   sqlAssetPartiel +="AND UUID = '"+ uuid + "'" + " \n"
			   sqlAssetPartiel +="AND VDM_CLASSEACTIF = '"+ classeActif + "' \n"
			   sqlAssetPartiel +="AND VDM_TYPEACTIF = '"+ typeActif + "' \n"
		return sqlAssetPartiel
	}
	

	private recupererListeAssetSpecs(classeActif, typeActif) {
		def list
		if(classeActif =="AQUEDUC") {
			list = recupererListeAssetSpecsAqueduc(typeActif)
		}else if(classeActif =="EGOUT") {
			list = recupererListeAssetSpecsEgout(typeActif)
		}
		return list
	}
	
	private recupererListeAssetSpecsAqueduc(typeActif) {
		def list = null
		switch (typeActif) {
			case "BI":
				list =  listBIAssetSpecs
				break
			case "SEGMENT":
				list = listSEGAQAttrsAssetSpecs
				break
			case "CHAMBRE":
				list = listCHAQAttrsAssetSpecs
				break
			case "VANNE":
				list = listVANAttrsAssetSpecs
				break
			case "REGARD":
				list = listREGAQAssetSpecs
				break
			case "ACCESSOIRE":
				list = listACCAQAttrsAssetSpecs
				break
			case "RACCORD":
				list = listRACAQAttrsAssetSpecs
				break
		}
		return list
	}
	
	private recupererListeAssetSpecsEgout(typeActif) {
		def list = null
			switch (typeActif) {
			case "PUISARD":
				list = listPUIAssetSpecs
				break
			case "SEGMENT":
				list = listSEGEGAttrsAssetSpecs
				break
			case "CHAMBRE":
				list = listCHEGAttrsAssetSpecs
				break
			case "BASSIN":
				list = listBASAttrsAssetSpecs
				break
			case "REGARD":
				list = listREGEGAttrsAssetSpecs
				break
			case "ACCESSOIRE":
				list = listACCEGAttrsAssetSpecs
				break
			case "RACCORD":
				list = listRACEGAttrsAssetSpecs
				break
		}
		return list
	}
	
	
	private validerHierarchieEmplacement(transaction) {
		def jsonActif = convertXmlToGson(transaction.getAt("TXN_MESSAGE").getSubString(1, (int) transaction.getAt("TXN_MESSAGE").length()))
		
		validerPrefixeEmplacement(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"), transaction.getAt("VDM_TYPEACTIF"), jsonActif.get("VDM_TERRITOIRE").getAsString(), transaction.getAt("TXNSEQUENCE"))
		
		def sql = genererRequeteHierarchie(transaction.getAt("UUID"), transaction.getAt("VDM_CLASSEACTIF"), transaction.getAt("VDM_TYPEACTIF"), jsonActif)
		def actif = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)	
		if(actif.size() == 1) {
				markPassed(transaction.getAt("TXNSEQUENCE") + " SUCCES VALIDATION EMPLACEMENT: "+ transaction.getAt("VDM_CLASSEACTIF") + " " + transaction.getAt("VDM_TYPEACTIF") + " avec le UUID : " + transaction.getAt("UUID") +" est au bon emplacement dans la hierarchie ")
				//return true
		}else {
				//KeywordUtil.markError(transaction.getAt("TXNSEQUENCE")+ " ECHEC VALIDATION EMPLACEMENT: "+ transaction.getAt("VDM_CLASSEACTIF") + " " + transaction.getAt("VDM_TYPEACTIF") + " avec le UUID : " + transaction.getAt("UUID") +  " ne dispose pas du bon emplacement dans la hierarchie")
				//return false
		}
	}
	
	private validerPrefixeEmplacement(uuid, classeActif, typeActif, territoire, txnsequence) {
		String prefixeArrondissement = definirPrefixeArrondissement(territoire)
		String sql = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' AND LOCATION LIKE '"+prefixeArrondissement +"%'"
		def actif = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sql)
		
		if(actif.size() == 1) {
			markPassed(txnsequence + " SUCCES PREFIXE EMPLACEMENT: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +" dispose du bon préfixe d'emplacement soit : "+ prefixeArrondissement)
			//return true
		}else {
			errors.add(txnsequence+ " ECHEC PREFIXE EMPLACEMENT: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +  " ne dispose pas du bon préfixe d'emplacement, aurait du être : "+ prefixeArrondissement)
			//KeywordUtil.markError(txnsequence+ " ECHEC PREFIXE EMPLACEMENT: "+ classeActif + " " + typeActif + " avec le UUID : " + uuid +  " ne dispose pas du bon préfixe d'emplacement, aurait du être : "+ prefixeArrondissement)
			//return false
		}
	}
	
	
	private definirPrefixeArrondissement(territoire) {
		switch (territoire) {
			case "24":
				return "AC"
				break
			case "9":
				return "AJ"
				break
			case "2":
				return "CG"
				break
			case "6":
				return "BG"
				break
			case "17":
				return "LC"
				break
			case "18":
				return "LS"
				break
			case "23":
				return "MH"
				break
			case "16":
				return "MN"
				break
			case "5":
				return "OM"
				break
			case "13":
				return "FR"
				break
			case "22":
				return "PM"
				break
			case "19":
				return "PR"
				break
			case "25":
				return "RL"
				break
			case 15:
				return "LR"
				break
			case "21":
				return "SO"
				break
			case "14":
				return "LN"
				break
			case "12":
				return "VD"
				break
			case "20":
				return "VM"
				break
			case "26":
				return "VE"
				break
			default: 
				return "TERRITOIRE INTROUVABLE "+ territoire
				break
			}
	}
	
	private genererRequeteHierarchie(uuid, classeActif, typeActif, jsonActif) {
		def sql
		if(classeActif == 'AQUEDUC') {
			sql = validerHierarchieAqueduc(uuid, classeActif, typeActif, jsonActif)
		}else if (classeActif == 'EGOUT') {
			sql = validerHierarchieEgout(uuid, classeActif, typeActif, jsonActif)
		}
		return sql
	}
	
	private validerHierarchieAqueduc(uuid, classeActif, typeActif, jsonActif) {
			switch (typeActif) {
			case "BI":
				return genererRequeteEmplacementBI(uuid)
				break
			case "SEGMENT":
				return genererRequeteEmplacementSEGAQ(uuid, jsonActif)
				break
			case "CHAMBRE":
				return genererRequeteEmplacementCH(uuid)
				break
			case "VANNE":
				return genererRequeteEmplacementVAN(uuid, jsonActif)
				break
			case "REGARD":
				return genererRequeteEmplacementREGAQ(uuid,jsonActif)
				break
			case "ACCESSOIRE":
				return  genererRequeteEmplacementACCAQ(uuid,jsonActif)
				break
			case "RACCORD":
				return genererRequeteEmplacementRAC(uuid)
				break
		}
	}
	
	
	private validerHierarchieEgout(uuid, classeActif, typeActif, jsonActif) {
		switch (typeActif) {
			case "PUISARD":
				return genererRequeteEmplacementPUI(uuid)
				break
			case "SEGMENT":
				return genererRequeteEmplacementSEGEG(uuid)
				break
			case "CHAMBRE":
				return genererRequeteEmplacementCH(uuid)
				break
			case "BASSIN":
				return genererRequeteEmplacementBAS(uuid)
				break
			case "REGARD":
				return genererRequeteEmplacementREGEG(uuid,jsonActif)
				break
			case "ACCESSOIRE":
				return genererRequeteEmplacementACCEG(uuid,jsonActif)
				break
			case "RACCORD":
				return genererRequeteEmplacementRAC(uuid)
				break
		}
	}
	
	private genererRequeteEmplacementBI(uuid) {
		return "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' AND LOCATION LIKE '__-AQU-BI%'"
	}
	
	private genererRequeteEmplacementRAC(uuid) {
		return "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' AND LOCATION LIKE '__-___-RACC%'"
	}
	
	private genererRequeteEmplacementCH(uuid) {
		return "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' AND LOCATION LIKE '__-___-CH%'"
	}
	
	private genererRequeteEmplacementBAS(uuid) {
		return "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' AND LOCATION LIKE '__-EGO-BAS'"
	}
	
	private genererRequeteEmplacementPUI(uuid) {
		return "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' AND LOCATION LIKE '__-EGO-PUI'"
	}
	
	private genererRequeteEmplacementSEGEG(uuid) {
		return "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' AND LOCATION LIKE '__-EGO-SEG'"
	}
	
	private identifierVDM_ACTIF_ASSOCIE(valeur) {
		def vdmActifAssocie = null
		if(valeur != "" && valeur != null && valeur.length() == 36) {
			vdmActifAssocie = valeur
		}
		return vdmActifAssocie
	}
	
	private genererRequeteEmplacementSEGAQ(uuid, jsonActif) {
		def sql
		def typeSegment = jsonActif.get("TYPE_SEGMENT_AQ").getAsString()
		def vdmActifAssocie = identifierVDM_ACTIF_ASSOCIE(jsonActif.get("VDM_ACTIF_ASSOCIE").getAsString())
		
		if(!vdmActifAssocie){
			//PAS DE VDM_ACTIF_ASSOCIE
			if(typeSegment =="BI") {
				sql = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
				sql+= "AND LOCATION LIKE '__-AQU-BISEG'"+ "\n"
			}else if (typeSegment =='BD' || typeSegment =='BDG' || typeSegment =='BF' || typeSegment =='BG') {
					sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
					sql += "AND LOCATION LIKE '__-AQU-ESSEG'"+ "\n"
			}else {
				sql = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
				sql +="AND LOCATION LIKE '__-AQU-SEG'"+ "\n"
			}
		}else {
			//VDM_ACTIF_ASSOCIE
			String sqlAssetPartiel = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			sqlAssetPartiel +="AND UUID = '"+ vdmActifAssocie + "'" + " \n"
			def actifAssocie = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlAssetPartiel)
			if(actifAssocie) {
				sql = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'" + "\n"
				sql +="AND LOCATION = (SELECT ap.LOCATION FROM ASSET ap WHERE ap.siteid = 'TP' and ap.uuid = '"+vdmActifAssocie+"')"+ "\n"
			}else {
				if(typeSegment =="BI") {
					sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
					sql += "AND LOCATION LIKE '__-AQU-BISEG'"+ "\n"
				}
				else {
					sql = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
					sql+= "AND LOCATION LIKE '__-AQU-SEG'"+ "\n"
				}
			}
		}
		return sql
	}
	
	private genererRequeteEmplacementREGAQ(uuid, jsonActif) {
		def sql
		def vdmActifAssocie = identifierVDM_ACTIF_ASSOCIE(jsonActif.get("VDM_ACTIF_ASSOCIE").getAsString())
		
		if(!vdmActifAssocie){
			//PAS DE VDM_ACTIF_ASSOCIE
			sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
			sql +="AND LOCATION LIKE '__-AQU-REG'"+ "\n"
		}else {
			//VDM_ACTIF_ASSOCIE
			String sqlAssetPartiel = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			sqlAssetPartiel +="AND UUID = '"+ vdmActifAssocie + "'" + " \n"
			def actifAssocie = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlAssetPartiel)
			if(actifAssocie) {
				sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
				sql +="AND LOCATION = (SELECT ap.LOCATION FROM ASSET ap WHERE ap.siteid = 'TP' and ap.uuid = '"+vdmActifAssocie+"')"+ "\n"
			}else {
				sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
				sql +="AND LOCATION LIKE '__-AQU-REG'"+ "\n"
			}
		}
		return sql
	}
	
	private genererRequeteEmplacementREGEG(uuid, jsonActif) {
		def sql
		def vdmActifAssocie = identifierVDM_ACTIF_ASSOCIE(jsonActif.get("VDM_ACTIF_ASSOCIE").getAsString())
		
		if(!vdmActifAssocie){
			//PAS DE VDM_ACTIF_ASSOCIE
			sql = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
			sql+= "AND LOCATION LIKE '__-AQU-EGO'"+ "\n"
		}else {
			//VDM_ACTIF_ASSOCIE
			String sqlAssetPartiel = "SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			sqlAssetPartiel +="AND UUID = '"+ vdmActifAssocie + "'" + " \n"
			def actifAssocie = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlAssetPartiel)
			
			if(actifAssocie) {
				sql = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
				sql +="AND LOCATION = (SELECT ap.LOCATION FROM ASSET ap WHERE ap.siteid = 'TP' and ap.uuid = '"+vdmActifAssocie+"')"+ "\n"
			}else {
				sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+ "\n"
				sql += "AND LOCATION LIKE '__-EGO-REG'"+ "\n"
			}
		}
		return sql
	}
	
	
	private genererRequeteEmplacementACCEG(uuid, jsonActif) {
		def sql
		def vdmActifAssocie = identifierVDM_ACTIF_ASSOCIE(jsonActif.get("VDM_ACTIF_ASSOCIE").getAsString())
		
		if(!vdmActifAssocie){
			//PAS DE VDM_ACTIF_ASSOCIE
			sql =  "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' \n"
			sql += "AND LOCATION LIKE '__-EGO-ACC%'"+"\n"
			sql += "AND EXISTS (SELECT 1 from lochierarchy where asset.location = location and parent like '__-EGO-CH') "+"\n"
		}else {
			//VDM_ACTIF_ASSOCIE
			String sqlAssetPartiel = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			sqlAssetPartiel +="AND UUID = '"+ vdmActifAssocie + "' \n"
			
			def actifAssocie = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlAssetPartiel)
			if(actifAssocie.size() == 1) {				
				sql =  "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' \n"
				sql += "AND LOCATION LIKE '__-EGO-ACC%'"+"\n"
				sql += "AND EXISTS (SELECT 1 from lochierarchy where asset.location = location and parent = (SELECT ap.LOCATION FROM ASSET ap WHERE ap.siteid = 'TP' and ap.uuid = '"+ vdmActifAssocie +"')) "+"\n"
				
			}else {
				sql =  "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' \n"
				sql += "AND LOCATION LIKE '__-EGO-ACC%'"+"\n"
				sql += "AND EXISTS (SELECT 1 from lochierarchy where asset.location = location and parent like '__-EGO-CH') "+"\n"
			}
		}
		return sql
	}
	
	private genererRequeteEmplacementACCAQ(uuid, jsonActif) {
		def sql
		def vdmActifAssocie = identifierVDM_ACTIF_ASSOCIE(jsonActif.get("VDM_ACTIF_ASSOCIE").getAsString())
		if(!vdmActifAssocie){
			//PAS DE VDM_ACTIF_ASSOCIE
			sql =  "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' \n"
			sql += "AND LOCATION LIKE '__-AQU-ACC%'"+"\n"
			sql += "AND EXISTS (SELECT 1 from lochierarchy where asset.location = location and parent like '__-AQU-VNCH') "+"\n"
		}else {
			//VDM_ACTIF_ASSOCIE
			String sqlAssetPartiel = "SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			sqlAssetPartiel +="AND UUID = '"+ vdmActifAssocie + "' \n"
			
			def actifAssocie = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlAssetPartiel)
			if(actifAssocie.size() == 1) {
				sql =  "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' \n"
				sql += "AND LOCATION LIKE '__-AQU-ACC%'"+"\n"
				sql += "AND EXISTS (SELECT 1 from lochierarchy where asset.location = location and parent = (SELECT ap.LOCATION FROM ASSET ap WHERE ap.siteid = 'TP' and ap.uuid = '"+ vdmActifAssocie +"')) "+"\n"
				
			}else {
				sql =  "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "' \n"
				sql += "AND LOCATION LIKE '__-AQU-ACC%'"+"\n"
				sql += "AND EXISTS (SELECT 1 from lochierarchy where asset.location = location and parent like '__-AQU-VNCH') "+"\n"
			}
		}
		return sql
	}
	
	private genererRequeteEmplacementVAN(uuid, jsonActif) {
		def sql
		def fonctionVan = jsonActif.get("FONCTION").getAsString()
		def vdmActifAssocie = identifierVDM_ACTIF_ASSOCIE(jsonActif.get("VDM_ACTIF_ASSOCIE").getAsString())
		
		if(!vdmActifAssocie){
			//PAS DE VDM_ACTIF_ASSOCIE
			if(fonctionVan =="SD" || fonctionVan =="SDG" || fonctionVan =="SF" || fonctionVan =="SG" ) {
				sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+"\n"
				sql += "AND LOCATION LIKE '__-AQU-VAN%'"+"\n"
				sql += "AND EXISTS (SELECT 1 from lochierarchy where asset.location = location and parent like '__-AQU-ES%') "+"\n"
				
			}else {
				sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'" +"\n"
				sql += "AND LOCATION LIKE '__-AQU-VAN%'"+"\n"
				sql += "AND EXISTS (SELECT 1 from lochierarchy where asset.location = location and parent like '__-AQU-VNCH') "+"\n"
			}
		}else {
			//VDM_ACTIF_ASSOCIE
			String sqlAssetPartiel = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' "+ "\n"
			sqlAssetPartiel +="AND UUID = '"+ vdmActifAssocie + "' \n"
			
			def actifAssocie = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlAssetPartiel)
			if(actifAssocie.size() == 1) {
				sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'"+"\n"
				sql += "AND LOCATION LIKE '__-AQU-VAN%'"+"\n"
				sql += "AND EXISTS (SELECT 1 from lochierarchy lh where asset.location = lh.location and lh.parent in (SELECT ap.LOCATION FROM ASSET ap WHERE ap.siteid = 'TP' and ap.uuid = '"+vdmActifAssocie+"')) "+"\n"
				
			}else {
				if(fonctionVan =="BI") {
					sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'" +"\n"
					sql += "AND LOCATION LIKE '__-AQU-VAN%'"+"\n"
					sql += "AND EXISTS (SELECT 1 from lochierarchy where asset.location = location and parent like '__-AQU-BIVI') "+"\n"
				}else {
					sql  = "\n SELECT ASSETNUM FROM ASSET WHERE SITEID = 'TP' AND UUID = '"+ uuid + "'" +"\n"
					sql += "AND LOCATION LIKE '__-AQU-VAN%'"+"\n"
					sql += "AND EXISTS (SELECT 1 from lochierarchy where asset.location = location and parent like '__-AQU-VNCH') "+"\n"
				}
			}
		}
		return sql
	}
	
	
	private validerActifsExistantsSansEmplacements() {
		String sqlActifsSansEmpl = "\n SELECT UUID FROM ASSET WHERE SITEID = 'TP' "+ "\n"
		sqlActifsSansEmpl +="AND STATUS = 'E'" + " \n"
		sqlActifsSansEmpl +="AND LOCATION is null" + " \n"
		def actifs = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlActifsSansEmpl)
		
		if(actifs.size() == 0) {
			markPassed("SUCCES : IL N'EXISTE AUCUN ACTIFS DU SITE TP SANS EMPLACEMENTS")
		}else {
			errors.add("ECHEC : Il EXISTE DES ACTIFS DU SITE TP SANS EMPLACEMENTS VOICI LA REQUETE DE VALIDATION: "+ sqlActifsSansEmpl)
			//KeywordUtil.markError(" ECHEC : Il EXISTE DES ACTIFS DU SITE TP SANS EMPLACEMENTS VOICI LA REQUETE DE VALIDATION: "+ sqlActifsSansEmpl)
		}
	}
	
	
	private validerStatutsEmplacements() {
		String sqlStatEmpl = "\n SELECT count(*) AS NBLOC FROM LOCATIONS a " +"\n"
		sqlStatEmpl += "WHERE a.SITEID = 'TP' " +"\n"
		sqlStatEmpl += "AND a.STATUS = 'E' " +"\n"
		sqlStatEmpl += "AND EXISTS (SELECT 1 FROM  ASSET b WHERE b.SITEID = 'TP' AND b.LOCATION = a.LOCATION) " +"\n"
		sqlStatEmpl += "AND NOT EXISTS (SELECT 1 FROM ASSET c WHERE c.SITEID = 'TP' AND c.LOCATION = a.LOCATION AND c.STATUS = 'E')" +"\n"
		
		def emplacements = CustomKeywords.'maximo.BDKeys.executeQueryInDatabase'(sqlStatEmpl)
		
		if(emplacements[0].getAt("NBLOC") == 0) {
			markPassed("SUCCES : IL N'EXISTE AUCUN EMPLACEMENT AU STATUT E AVEC ACTIFS R")
		}else {
			errors.add("ECHEC : Il EXISTE DES EMPLACEMENT AU STATUT E DISPOSANT D'ACTIFS COMPLÈMENT RETIRÉS: "+ sqlStatEmpl)
			//KeywordUtil.markError(" ECHEC : Il EXISTE DES ACTIFS DU SITE TP SANS EMPLACEMENTS VOICI LA REQUETE DE VALIDATION: "+ sqlStatEmpl)
		}
	}
	
	private resumerErreur() {
		if(errors.size()> 0) {
			KeywordUtil.markPassed("!!!!=========> FIN DE L EXECUTION : il y a "+errors.size()+" erreur(s)! <===============!!!! :(")
			for(int e = 0; e < errors.size();e++) {
				KeywordUtil.markWarning(errors.get(e))
			}
			
		}else {
			KeywordUtil.markPassed("Fin de l'execution sans erreurs! :)")
		}
		return null
	}
	
	private convertXmlToGson(xml) {
		//Define HashMap to store element from XML
		HashMap<String,Object> map = new HashMap();
		
		// Parse it
		def parsed = new XmlParser().parseText( xml )

		// Convert it to a Map containing a List of Maps
		def jsonObject = parsed.ATTRIBUT.collect {
			//add only selected variables to holder object(here HashMap),
			map.put(it.@nom, it.text())
		}

		//convert holder object to JSONObject directly and return as string as follows
		return new Gson().fromJson(new Gson().toJson(map) , JsonObject.class)
	}
	
	
	
	private markPassed(value) {
		if(activeLog) {
			KeywordUtil.markPassed(value)
		}
	}
	
	private markError(value) {
		if(activeLog) {
			KeywordUtil.markError(value)
		}
	}
	
	
	private markInfo(value) {
		if(activeLog) {
			KeywordUtil.logInfo(value)
		}
	}
	
}


TestsCasesMep07BD test = new TestsCasesMep07BD()
test.executerTestCase()
