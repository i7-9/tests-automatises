import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.internal.JsonUtil as JsonUtil
import com.google.gson.Gson as Gson
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.util.KeywordUtil
import java.util.regex.Pattern
import java.util.regex.Matcher

//TODO: Faire une fonction qui vérifie la derniere transaction
/**
 * 
 * @author xsavoma
 *
 */
public class TestsCasesMep07 {
	// Listing des variables
	TestData Actifs_journal // Récupération du journal
	def Actif_specs//Recoit le geoJson de l'actif
	String Type_actif //Recoit le type d'actif
	String geojson //Variable temporaire pour l'extraction du geoJson
	String SQLRequest //Recoit la demande SQL à envoyer dans Maximo
	String resultat_clause //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement
	def actif_failed//Recoit le numéro d'actif, l'ID Journal, le numéro du journal et l'action fait sur l'actif
	List errors = new ArrayList ()
	boolean resultat_supression//
	def compteur //Permette de compter le nombre de chaque type d'actif
	int maxRows //Récupère le nombre d'actif total dans le journal
	int currentRow //Début du parcours du tableau
	Gson gson //Permet de récupérer le geonjson dans un objet
	String noJournal  //Store journal id
	String message=null
	String champmodifies//Champs modifiées lors de la modification d'attribut
	boolean result_error_validation//Résultat de la validation de l'erreur du tableau des erreurs enregistrée
	
	public TestsCasesMep07(){
		Actifs_journal = findTestData('Data Files/MEP-07/Journal')
		actif_failed =[][]//Recoit le numéro d'actif, l'ID Journal, le numéro du journal et l'action fait sur l'actif
		compteur = [] //Permette de compter le nombre de chaque type d'actif
		maxRows = Actifs_journal.getRowNumbers() //Récupère le nombre d'Actifs_journal total dans le journal
		currentRow = 1 //Début du parcours du tableau
		gson = new Gson() //Permet de récupérer le geonjson dans un objet
		noJournal  = Actifs_journal.getObjectValue('NO_ENVOI', 1) as String
		
	}
	
	public run() {
		WebUI.comment('Début de la validation du journal géomatique')
		connectToMaximo()
		
	}
	/**
	 * Connection à Maximo
	 * @return
	 */
	public connectToMaximo() {
		CustomKeywords.'maximo.GeneralKeys.openBrowser'()
		CustomKeywords.'maximo.GeneralKeys.connexion'()
	}
	/**
	 * Connection à la BD
	 * @return
	 */
	public connectToDB() {
		CustomKeywords.'maximo.GeneralKeys.connexionDB'() // If you want to use database connection
		String sqlTest = "select * from MX_MTL01.asset a where a.siteid='TP' and a.uuid = 'AQUEDUCBI1123'"
		CustomKeywords.'maximo.GeneralKeys.executeSqlQueryInDatabase'(sqlTest)
	}
	
	/**
	 * procédure pour obtenir le json code
	 * @return object aillant tous les actifs
	 */
	public setGeojson() {
		//On attribut le geojson à actif specs pour pouvoir comparer les specs avec l'actif actuel
		if(Actifs_journal.getObjectValue('ACTION', currentRow) != 'SUPPRESSION') {
			geojson = Actifs_journal.getObjectValue('GEOJSON_APRES', currentRow) as String
		}else {
			geojson = Actifs_journal.getObjectValue('GEOJSON_AVANT', currentRow) as String
		}
		geojson = geojson.replaceAll(Pattern.quote("\\"), Matcher.quoteReplacement("/"))
		Actif_specs = new JsonSlurper().parseText(geojson) as Map
	}
	/**
	 * Parse le json code pour le mettre en objet
	 * @return
	 */
	public parseJournal() {
		for (currentRow=1 ; currentRow <= maxRows ; currentRow++ ) {
			//par défault les résultats sont vrai donc pas d'erreur detecte
			resultat_clause = ""
			//Récupère le type d'action et le type de l'actif
			
			Type_actif = Actifs_journal.getObjectValue('NOM_TABLE', currentRow)
			
			//setGeojson()
			//On attribut le geojson à actif specs pour pouvoir comparer les specs avec l'actif actuel
			setGeojson()
			Validation_actif()
			manageErrors(currentRow, resultat_clause)
		}
	}
	
	

	/**
	 * Cette fonction s'occupe de valider les actifs
	 * @return si l'actif est désynchroniser avec Maximo on renvoi la table qui est désynchroniser aussi on renvoi null
	 */
	public Validation_actif() {
		if(CustomKeywords.'maximo.Geomatique.estArrondissementMaximo'(Actif_specs[0].features[0].properties.TERRITOIRE_R)==true) {
			
			// Si c'est une modification d'attribut
			CustomKeywords.'maximo.ActifsKeys.accederAActifs'()
			KeywordUtil.markPassed(currentRow + " --- " + Actifs_journal.getObjectValue('ACTION', currentRow)+" --- "+Type_actif)
			
			switch (Actifs_journal.getObjectValue('ACTION', currentRow)) {
				case 'INSERTION':
					if(compteur.contains(Type_actif)== true) {
						resultat_clause = CustomKeywords.'maximo.Geomatique.insertion'(Actif_specs,currentRow,Type_actif,	1)
					}
					else {
						resultat_clause = CustomKeywords.'maximo.Geomatique.insertion'(Actif_specs,currentRow,Type_actif,	0)
						compteur.add(Type_actif)
						}
					break
				case 'SUPPRESSION':
					resultat_supression = CustomKeywords.'maximo.GeneralKeys.ouvrirClauseWhereExecuteRequete'("uuid = '"+ Actifs_journal.getObjectValue('UUID', currentRow) +"' and siteid = 'TP' and ((status = 'A' and vdm_etat_operation = 'HU' and vdm_date_abandon is not null) or (status = 'R' and VDM_DATE_FIN is not null))")
					println(resultat_supression)
					if(resultat_supression == false) {resultat_clause == "ASSET"}
					else {resultat_clause == ""}
					break
				case 'MODIFICATION_ATTR':
				champmodifies = Actifs_journal.getObjectValue('CHAMPMODIFIES', currentRow)
				
					if(compteur.contains(Type_actif)== true) {
						
						resultat_clause = CustomKeywords.'maximo.Geomatique.modification_attribut'(Actif_specs,currentRow,Type_actif,	1, champmodifies)
					}
					else {
						resultat_clause = CustomKeywords.'maximo.Geomatique.modification_attribut'(Actif_specs,currentRow,Type_actif,	0, champmodifies)
						compteur.add(Type_actif)
						}
					break
				
				case 'DEPLACEMENT':
					//modification des attributs avec validation des emplacements
					//modification vdm actif associer, coordonner et emplacement
					resultat_clause = CustomKeywords.'maximo.Geomatique.deplacement'(Actif_specs,currentRow,Type_actif)
					break
				case 'REBRANCHEMENT':
					//à traiter comme ajustement
					resultat_clause = CustomKeywords.'maximo.Geomatique.rebranchement'(Actif_specs,currentRow,Type_actif)
					break
				case 'AJUSTEMENT':
					//ajustement au id point début id point fin
					resultat_clause = CustomKeywords.'maximo.Geomatique.ajustements'(Actif_specs,currentRow,Type_actif)
					break
				case 'REMPLACEMENT':
					resultat_clause = CustomKeywords.'maximo.Geomatique.Remplacement'( Actif_specs, currentRow, Type_actif)
					break
				case 'AGREGATION':
					//TODO: faire sur que ca valide qu'il y ait suppression aussi
					resultat_clause = CustomKeywords.'maximo.Geomatique.agregation'( Actif_specs, currentRow, Type_actif)
					break
				case 'INVERSION':
					//traiter comme ajustement et rebranchement
					resultat_clause = CustomKeywords.'maximo.Geomatique.inversion'(Actif_specs,currentRow,Type_actif)
					break
				case 'SCINDEMENT':
					//TODO: faire sur que ca valide qu'il y ait suppression aussi
					resultat_clause = CustomKeywords.'maximo.Geomatique.scindement'( Actif_specs, currentRow, Type_actif)
					break
			}
		}
			else {
				KeywordUtil.markPassed(noJournal + ' : arrondissement non pris en charge : ' + Actif_specs[0].features[0].properties.TERRITOIRE_R + " " + currentRow)
			}
	}
	
	
	/**
	 * Cette fonction enregistre si l'actif est désynchronisé avec Maximo
	 * @param current_actif, numéro de l'actif validé
	 * @param resultat_validation, résultat de la validation de l'actif
	 * @return
	 */
	public manageErrors(int current_actif, String resultat_validation) {
		//TODO : @mathieu.savoie : il vaut mieux utiliser un array qu'une chaine de caractère
		if(resultat_validation != null) {
				def detail_error = [current_actif,
				Actifs_journal.getObjectValue('ID_JOURNAL',current_actif),
				Actifs_journal.getObjectValue('ID_ACTIF',current_actif),
				Actifs_journal.getObjectValue('ACTION',current_actif),
				Actifs_journal.getObjectValue('NO_ENVOI',current_actif),
				Actifs_journal.getObjectValue('UUID',current_actif)]	
			    errors.add(detail_error)
				errors.get(0)[0]
		}else {
			KeywordUtil.markPassed(resultat_validation)
		}	
}
/**
 * Prend les actifs qui ont étés soulevés comme aillant des erreurs et valide s'il y a une transaction après
 * @return Les actifs qui comportes des erreurs
 */
public end() {
	CustomKeywords.'maximo.GeneralKeys.deconnexion'()
	WebUI.closeBrowser()
	message ="Les actifs désynchronisés dans Maximo sont: "
	//TODO : Le message imprime seulement 2 actif mauvais donc voir les conditions dans cette section
	//Si un actif retourne un false dans les specs ou les info arrondissements, le message d'erreur sera publié
	result_error_validation = true
	for(int current_error = 0; current_error < errors.size(); current_error ++) {
		
		if(current_error  < errors.size() - 2) {
			println('11')
			int i = errors.get(current_error)[0] + 1
			while(i <= maxRows  && result_error_validation == true) {
				println('22')
				if(errors.get(current_error)[5]== Actifs_journal.getObjectValue('UUID',i)) {
					println('33')
					result_error_validation = false
					
					message += "l'ID journal est "+ errors.get(current_error)[1]+
					"l'ID actif est "+ errors.get(current_error)[2]+
					"l'action est "+ errors.get(current_error)[3]+
					"le numéro de l'envoi est "+ errors.get(current_error)[4]+
					"le UUID de l'actif est "+ errors.get(current_error)[5]+'/n/n'
					
					
				}
				i++
		}
		}else {
			message += "l'ID journal est "+ errors.get(current_error)[1]+
			" l'ID actif est "+ errors.get(current_error)[2]+
			" l'action est "+ errors.get(current_error)[3]+
			" le numéro de l'envoi est "+ errors.get(current_error)[4]+
			" le UUID de l'actif est "+ errors.get(current_error)[5]+"/n"
		}
		
	}
	if(message =="Les actifs désynchronisés dans Maximo sont: ") {
		KeywordUtil.markPassed("Tous les actifs de ce journal sont identiques aux données dans Maximo")
	}
	else {
		KeywordUtil.markError(message)
	}
}
}


TestsCasesMep07 test = new TestsCasesMep07()
test.run()
test.setGeojson()
test.parseJournal()
test.end()
