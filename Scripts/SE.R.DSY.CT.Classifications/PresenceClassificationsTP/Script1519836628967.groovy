import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.testdata.InternalData as InternalData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Informations sur le test en cours
CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()
CustomKeywords.'maximo.ClassificationsKeys.accederAClassifications'()

//Recuperation du Fichier XLSX (Data Files/Classifications/Classifications_TP_) dans une variable pour le parcourir
TestData classifications_TP = TestDataFactory.findTestData("Data Files/Classifications/Classifications_TP_")
int currentObject = 1

//Debut de la validation de la presence des classifications
while (currentObject <= classifications_TP.getRowNumbers()) {
	
	//Nom de la classification en cours de validation
	WebUI.comment("Validation du groupe : "
		+ classifications_TP.getValue("classificationid", currentObject) 
		+ " " + classifications_TP.getValue("description", currentObject))
	
	//Accèder a la clause WHERE
	CustomKeywords.'maximo.GeneralKeys.click'('ElementsGeneriques/B-ZoneRecherchePlus', null)
	CustomKeywords.'maximo.GeneralKeys.mouseOver'('ElementsGeneriques/B-ClauseWhere', null)
	CustomKeywords.'maximo.GeneralKeys.click'('ElementsGeneriques/B-ClauseWhere', null)
	WebUI.delay(1)
	
	//Saisir la clause WHERE
	CustomKeywords.'maximo.GeneralKeys.setText'('ElementsGeneriques/ClauseWhere/TA-ClauseWhere', 
		"classstructureid = '"+ classifications_TP.getValue("classstructureid", currentObject) +"'", null)
	
	//Executer la recherche du groupe de securite
	CustomKeywords.'maximo.GeneralKeys.click'('ElementsGeneriques/ClauseWhere/B-Rechercher', null)
	WebUI.delay(1)
	
	//Analyse du resultat de la recherche du groupe de securite
	if(CustomKeywords.'maximo.GeneralKeys.verifyElementNotPresent'('ElementsGeneriques/ClauseWhere/Alert', 1, null)){
		//Le groupe existe
		CustomKeywords.'maximo.GeneralKeys.click'('ElementsGeneriques/B-AffichageListe', null)
	}else{
		//La classification n'existe pas
	}
	
	//Attendre le chargement de la page
	WebUI.delay(1)
	currentObject ++
}

//Deconnexion de l'utilisateur
WebUI.click(findTestObject('P-Accueil/Navbar/B-Logout'))





