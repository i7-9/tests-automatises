import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable
String typeReseau = "AQUEDUC"
String typeActif = "LC-Accessoires"
TestData actifs_TP = TestDataFactory.findTestData("Data Files/Actifs/LC/" + typeReseau + "/FicheActif/" + typeActif)
TestData spec_TP = TestDataFactory.findTestData("Data Files/Actifs/LC/" + typeReseau + "/Specifications/" + typeActif)
TestData modeles_TP = TestDataFactory.findTestData("Data Files/Actifs/LC/" + typeReseau + "/Modeles")
List<List<String>> allData = spec_TP.getAllData()
int currentRow
int maxRows = 0
String sRequest = ""
boolean actifFound

currentRow = 1
maxRows = actifs_TP.getRowNumbers()
nbSpecs = 1

CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()
CustomKeywords.'maximo.ActifsKeys.accederAActifs'()
CustomKeywords.'maximo.GeneralKeys.fermerFenetreIntempestive'()

while (currentRow <= maxRows) {
	specRow = 1
	
	//On effectue le test long pour le premier élément et un test rapide (par clause where) pour les autres
	sRequest = CustomKeywords.'maximo.ActifsKeys.construitRequete'(
		actifs_TP, allData, currentRow, nbSpecs, CustomKeywords.'maximo.ActifsKeys.getModelNumber'(
			typeActif, modeles_TP, actifs_TP.getValue("TEMPLATEID", currentRow)))
	
	//Si le modèle d'actif existe
	if(CustomKeywords.'maximo.ActifsKeys.getModelNumber'(
		typeActif, modeles_TP, actifs_TP.getValue("TEMPLATEID", currentRow)).toString().length() < 15) {
		actifFound = CustomKeywords.'maximo.GeneralKeys.ouvrirClauseWhereExecuteRequete'(sRequest)
		
		/*if(actifFound && currentRow == 1) {
			WebUI.waitForElementPresent(findTestObject("A-Actifs/T-Actifs/T-NoActif"), 2)
			noActif = WebUI.getAttribute(findTestObject("A-Actifs/T-Actifs/T-NoActif"), "value")
			CustomKeywords.'maximo.ActifsKeys.valideActif'(actifs_TP, currentRow)
			CustomKeywords.'maximo.ActifsKeys.valideModele'(sTypeActif, actifs_TP, currentRow)
			WebUI.click(findTestObject("A-Actifs/Tabs/Tab-Specifications"))
			WebUI.waitForElementClickable(findTestObject("A-Actifs/T-Specifications/L-Filtre"), 4)
			WebUI.click(findTestObject("A-Actifs/T-Specifications/L-Filtre"))
			
			CustomKeywords.'maximo.ActifsKeys.valideSpecs'(nbSpecs, allData, noActif)
		} else*/ 
		
		if(actifFound) {
			CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('ElementsGeneriques/B-AffichageListe', 10, FailureHandling.OPTIONAL)
			CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
		} else {
			//L'actif n'est pas trouvé
			CustomKeywords.'maximo.GeneralKeys.logError'(
				typeActif + " dans " + typeReseau + ": " +
				actifs_TP.getValue("VDM_INV_NO", currentRow) +
				" n'est pas présent dans Maximo ou contient une erreur")
		}
	} else {
		//Le modèle n'est pas trouvé
		CustomKeywords.'maximo.GeneralKeys.logError'("Le modèle de " + typeActif + " dans " + typeReseau + ": " +
				actifs_TP.getValue("VDM_INV_NO", currentRow) +
				" n'est pas présent dans Maximo ou contient une erreur")
	}
	
	currentRow++
}