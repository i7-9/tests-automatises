import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

TestData DS_TP = TestDataFactory.findTestData("Data Files/ActifsLachine/TestHierarchie/SEGAQ-ES_PROD")
int currentRow = 1
//int maxRows = DS_TP.getRowNumbers()
int maxRows = 5
String sEmplacement

CustomKeywords.'maximo.GeneralKeys.openBrowser'()
CustomKeywords.'maximo.GeneralKeys.connexion'()
CustomKeywords.'maximo.ActifsKeys.accederAActifs'()

while (currentRow <= maxRows) {
	bEnfantTrouve = false
	bLoop = true
	
	//Recherche l'actif
	CustomKeywords.'maximo.GeneralKeys.click'("ElementsGeneriques/B-RechercheAvancee", null)
	
	CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'("ElementsGeneriques/RechercheAvancee/B-Rechercher", 10, null)
	
	//Accède à VDM_ACTIF_ASSOCIE
	CustomKeywords.'maximo.GeneralKeys.setTextElement'("ElementsGeneriques/RechercheAvancee/T-NoGCAF", DS_TP.getValue("VDM_INV_ID", currentRow), null)
	CustomKeywords.'maximo.GeneralKeys.click'("ElementsGeneriques/RechercheAvancee/B-Rechercher", null)
	CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'("A-Actifs/T-Actifs/Caracteristiques/T-Emplacement", 5, null)
	
	//Accède à l'emplacement de l'actif
	sEmplacement = WebUI.getAttribute(findTestObject("A-Actifs/T-Actifs/Caracteristiques/T-Emplacement"), "value")
	WebUI.verifyMatch(WebUI.getAttribute(findTestObject("A-Actifs/T-Actifs/Caracteristiques/T-Emplacement"), "value"), "^LC-AQU-BI\\d+\$", true, FailureHandling.CONTINUE_ON_FAILURE)
	
	//Navigue à l'emplacement
	CustomKeywords.'maximo.GeneralKeys.click'("A-Actifs/T-Actifs/Caracteristiques/B-Emplacement", null)
	CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'("A-Actifs/T-Actifs/Caracteristiques/L-AllerEmplacement", 5, null)
	CustomKeywords.'maximo.GeneralKeys.click'("A-Actifs/T-Actifs/Caracteristiques/L-AllerEmplacement", null)
	CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'("A-Emplacement/Emplacement/T-Parent", 5, null)
	
	//Si le parent est LC-AQU-BI
	if(WebUI.verifyMatch(WebUI.getAttribute(findTestObject("A-Emplacement/Emplacement/T-Parent"), "value"), "^LC-AQU-BI\$", true, FailureHandling.CONTINUE_ON_FAILURE)) {
		CustomKeywords.'maximo.GeneralKeys.click'("A-Emplacement/Emplacement/B-Parent", null)
		CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'("A-Emplacement/Emplacement/L-LinkEmplacement", 2, null)
		CustomKeywords.'maximo.GeneralKeys.click'("A-Emplacement/Emplacement/L-LinkEmplacement", null)
	}
	
	WebUI.delay(2)
    
    //Vérifie si l'accessoire/chambre est dans LC-AQU
	if(WebUI.verifyMatch(WebUI.getAttribute(findTestObject("A-Emplacement/Emplacement/T-Parent"), "value"), "^LC-AQU\$", true, FailureHandling.CONTINUE_ON_FAILURE)) {
		CustomKeywords.'maximo.GeneralKeys.click'("A-Emplacement/Emplacement/B-Parent", null)
		CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'("A-Emplacement/Emplacement/L-LinkEmplacement", 2, null)
		CustomKeywords.'maximo.GeneralKeys.click'("A-Emplacement/Emplacement/L-LinkEmplacement", null)
	}
	
	CustomKeywords.'maximo.GeneralKeys.sendKeys'("A-Emplacement/Emplacement/T-Parent", Keys.chord(Keys.ALT, 'r'), null)
	
	CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
	currentRow++
}

CustomKeywords.'maximo.GeneralKeys.deconnexion'()