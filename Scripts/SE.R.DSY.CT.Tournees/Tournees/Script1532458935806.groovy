import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

TestData DS_TP = TestDataFactory.findTestData("Data Files/Tournees/InfosTournee")
int currentRow = 1
//int maxRows = DS_TP.getRowNumbers()
int maxRows = 50
CustomKeywords.'maximo.GeneralKeys.openBrowser'('ACC')
CustomKeywords.'maximo.GeneralKeys.connexion'(GlobalVariable.username)
CustomKeywords.'maximo.TourneesKeys.accederATournees'()

//Crée un nouvel objet
CustomKeywords.'maximo.GeneralKeys.clicCreationNouvelObject'()
CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('A-Tournees/T-Tournee', 5, null)

while (currentRow <= maxRows) {
	//Si on est à la 1ere colonne, remplir les infos de la tournée
	if(currentRow == 1) {
		if(DS_TP.getValue('NoTournee', 1)) {
			CustomKeywords.'maximo.GeneralKeys.setTextElement'('A-Tournees/T-Tournee', DS_TP.getValue("NoTournee", 1), null)
		}
		
		CustomKeywords.'maximo.GeneralKeys.setTextElement'('A-Tournees/T-Description_Tournee', "[Katalon] " + DS_TP.getValue('DescriptionTournee', 1), null)
		
		//Sélectionne la route
		switch(DS_TP.getValue('Route', 1)){
			case 'Intervention': 
				CustomKeywords.'maximo.GeneralKeys.check'('A-Tournees/Rb-Interventions', null)
				break
			case 'Entrees':
				CustomKeywords.'maximo.GeneralKeys.check'('A-Tournees/Rb-Entrees', null)
				break
			case 'Taches':
				CustomKeywords.'maximo.GeneralKeys.check'('A-Tournees/Rb-Taches', null)
				break
		}
	}
		
	//Crée une nouvelle ligne pour les actifs
	CustomKeywords.'maximo.GeneralKeys.click'('A-Tournees/B-Nouvel_Actif', null)
	
	//Remplit les infos de l'actif
	CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('A-Tournees/B-Actif', 2, null)
	CustomKeywords.'maximo.GeneralKeys.click'('A-Tournees/B-Actif', null)
	CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('A-Tournees/L-Search_Actif', 2, null)
	CustomKeywords.'maximo.GeneralKeys.click'('A-Tournees/L-Search_Actif', null)
	CustomKeywords.'maximo.GeneralKeys.waitForElementPresent'('A-Tournees/T-Search-Actif', 5, null)
	CustomKeywords.'maximo.GeneralKeys.setTextElement'('A-Tournees/T-Search-Actif', "=" + DS_TP.getValue('NoActif', currentRow), null)
	CustomKeywords.'maximo.GeneralKeys.sendKeys'('A-Tournees/T-Search-Actif', [Keys.ENTER], null)
	CustomKeywords.'maximo.GeneralKeys.waitForValue'('A-Tournees/L-Premier_Actif', DS_TP.getValue('NoActif', currentRow), 5, null)
	CustomKeywords.'maximo.GeneralKeys.click'('A-Tournees/L-Premier_Actif', null)
	CustomKeywords.'maximo.GeneralKeys.waitForNotPresent'('A-Tournees/L-Premier_Actif', 5, null)
	CustomKeywords.'maximo.GeneralKeys.setTextElement'('A-Tournees/T-Description_Actif', DS_TP.getValue('DescriptionActif', currentRow), null)
	
	currentRow++
}

CustomKeywords.'maximo.GeneralKeys.clicEnregistrerObject'()
CustomKeywords.'maximo.GeneralKeys.clicAfficheListe'()
CustomKeywords.'maximo.GeneralKeys.deconnexion'()

