package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class ReferentielArticlesKeys {
	private GeneralKeys gk = new GeneralKeys()

	/**
	 * Accéder a l'application Referentiel d'articles
	 */
	@Keyword
	def accederAReferentielArticles(){
		gk.click('P-Accueil/Navbar/B-AllerA', null)
		gk.mouseOver('P-Accueil/Navbar/AllerA/Stock/M-StockM', null)
		if(gk.verifyElementPresent('P-Accueil/Navbar/AllerA/Stock/M-TransactionsServices', 3, null)){
			gk.mouseOver('P-Accueil/Navbar/AllerA/Stock/M-TransactionsServices', null)
			gk.mouseOver('P-Accueil/Navbar/AllerA/Stock/M-ReferentielArticles', null)
		}else{
			gk.mouseOver('P-Accueil/Navbar/AllerA/Stock/M-ReferentielArticles', null)
		}
		gk.click('P-Accueil/Navbar/AllerA/Stock/M-ReferentielArticles', null)
	}

	/**
	 * Compare l'article dans l'objet à celui des paramètres
	 * @param noArticle
	 * @param description
	 * @param noIN
	 */
	@Keyword
	def comparerArticle(String noArticle, String description, String noIN){
		gk.valideInput('A-ReferentielArticles/P-ReferentielArticles/T-noArticle', noArticle)
		gk.valideInput('A-ReferentielArticles/P-ReferentielArticles/T-Description', description)
		gk.valideInput('A-ReferentielArticles/P-ReferentielArticles/T-noIN', noIN)
	}

	/**
	 * Valide les elements de la page article
	 * @param description
	 */
	@Keyword
	def comparerDescription(String description){
		gk.valideInput('A-ReferentielArticles/P-ReferentielArticles/T-Description', description)
	}

	/**
	 * Valide les elements de la page article
	 * @param noArticle
	 */
	@Keyword
	def comparerNoArticle(String noArticle){
		gk.valideInput('A-ReferentielArticles/P-ReferentielArticles/T-noArticle', noArticle)
	}

	/**
	 * Valide les elements de la page article
	 * @param noIN
	 */
	@Keyword
	def comparerNoSimon(String noIN){
		gk.valideInput('A-ReferentielArticles/P-ReferentielArticles/T-noIN', noIN)
	}
}
