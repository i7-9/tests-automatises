package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.Keys as Keys

public class DemandeServiceLSKeys {
	private GeneralKeys gk = new GeneralKeys()
	boolean bLoop

	/**
	 * Accède a l'application Demande de service
	 */
	@Keyword
	def accederADemandeServiceLS() {
		bLoop = true

		while(bLoop) {
			try {
				WebUI.click(findTestObject('P-Accueil/Navbar/B-AllerA'))
				WebUI.waitForElementPresent(findTestObject('P-Accueil/Navbar/AllerA/LibreService/M-LibreServiceM'), 2)
				WebUI.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/LibreService/M-LibreServiceM'))
				WebUI.waitForElementPresent(findTestObject('P-Accueil/Navbar/AllerA/LibreService/M-DemandeServiceM'), 2)
				WebUI.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/LibreService/M-DemandeServiceM'))
				WebUI.waitForElementPresent(findTestObject('P-Accueil/Navbar/AllerA/LibreService/M-CreationDS'), 2)
				WebUI.click(findTestObject('P-Accueil/Navbar/AllerA/LibreService/M-CreationDS'))
				bLoop = false
			} catch(Exception ex) {}
		}
	}
	/**
	 * Remplit les informations de la DS
	 */
	@Keyword
	def remplitInfosUtilisateur(String sNomPour, String sTelephonePour, String sCourrielPour) {
		WebUI.waitForElementPresent(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/T-DemandePour'), 5)

		//Remplit les informations du demandeur si nécessaire'
		if (sNomPour != '') {
			//Remplit le nom du demandeur
			WebUI.delay(1)
			gk.forceSetTextElement('A-DemandeServiceLS/P-DS LS/Informations DS/T-DemandePour', sNomPour)
			WebUI.delay(1)
			'Remplit le numéro de téléphone du demandeur'
			gk.forceSetTextElement('A-DemandeServiceLS/P-DS LS/Informations DS/T-Telephone DemandePour', sTelephonePour)
			'Remplit le courriel du demandeur'
			gk.forceSetTextElement('A-DemandeServiceLS/P-DS LS/Informations DS/T-Courriel DemandePour', sCourrielPour)
		}
	}

	/**
	 * Remplit les détails de la DS
	 * @param sGDC
	 * @param sActif
	 * @param sType
	 * @param sMode
	 * @param sAtelier
	 * @param sClassification
	 * @param sEmplacement
	 */
	@Keyword
	def remplitDetailsDS(String sGDC, String sActif, String sType, String sMode, String sAtelier, String sClassification, String sEmplacement) {
		bLoop = true

		while(bLoop) {

			try {
				WebUI.setText(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/T-GDC'), sGDC)

				if(!sActif.isEmpty()) {
					WebUI.click(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/B-Actif'))
					WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-RechercheActif'), 3)
					WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-RechercheActif'))

					WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Recherche'), "=" + sActif)

					WebUI.sendKeys(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Recherche'), Keys.chord(Keys.ENTER))
					WebUI.delay(2)
					WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-ResultatRecherche'))
					// appuie sur Oui pour la confirmation de l'emplacement
					if (WebUI.waitForElementPresent(findTestObject('Object Repository/MS-MessageSystem/MS-ConfirmeEmplacement/B-Oui'), 5)){
						WebUI.click(findTestObject('Object Repository/MS-MessageSystem/MS-ConfirmeEmplacement/B-Oui'))
						WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-DS'), FailureHandling.OPTIONAL)
					}


				} else if(!sEmplacement.isEmpty()) {
					WebUI.click(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/B-Emplacement'))
					WebUI.waitForElementPresent(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/L-RechercheEmplacement'), 3)
					WebUI.click(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/L-RechercheEmplacement'))
					WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Recherche'), "=" + sEmplacement)
					WebUI.sendKeys(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Recherche'), Keys.chord(Keys.ENTER))
					WebUI.delay(2)
					WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-ResultatRecherche'))
					WebUI.waitForElementNotPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-ResultatRecherche'), 5)
					// appuie sur Oui pour la confirmation de l'emplacement
					if(WebUI.waitForElementPresent(findTestObject('Object Repository/MS-MessageSystem/MS-ConfirmeEmplacement/B-Oui'), 5))
						WebUI.click(findTestObject('Object Repository/MS-MessageSystem/MS-ConfirmeEmplacement/B-Oui'))
					WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-DS'), FailureHandling.OPTIONAL)
				}

				gk.forceSetTextElement('A-DemandeServiceLS/P-DS LS/Informations DS/T-Type', sType)
				gk.forceSetTextElement('A-DemandeServiceLS/P-DS LS/Informations DS/T-Exec', sMode)
				gk.forceSetTextElement('A-DemandeServiceLS/P-DS LS/Informations DS/T-Atelier', sAtelier)

				if(sType == "AMENT") {
					gk.forceSetTextElement('A-DemandeServiceLS/P-DS LS/Classer/T-Classification', "AQUEDUC_EGOUT \\ DEMANDE_SERVICE \\ AMENT \\ " + sClassification)
				}
				bLoop = false
			} catch(Exception ex) {

				WebUI.click(findTestObject("A-DemandeServiceLS/P-DS LS/B-AnnulerSelection"), FailureHandling.OPTIONAL)
				WebUI.click(findTestObject("A-DemandeServiceLS/P-DS LS/B-AnnulerSelection_Emplacement"), FailureHandling.OPTIONAL)
			}
		}
	}

	/**
	 * Remplit l'adresse de la DS
	 * @param sAdresse
	 * @param sArrondissement
	 * @param sPointReference
	 * @param sInstructions
	 */
	@Keyword
	def remplitAdresse(String sAdresse, String sArrondissement, String sPointReference, String sInstructions) {
		bLoop = true

		while(bLoop) {
			try {
				WebUI.click(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/T-Adresse Formatee'))
				WebUI.focus(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/T-Adresse Formatee'))
				WebUI.delay(1)
				gk.forceSetTextElement('A-DemandeServiceLS/P-DS LS/Informations DS/T-Adresse Formatee', sAdresse)
				WebUI.setText(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/T-Arrondissement'), sArrondissement)
				WebUI.setText(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/T-Reference'), sPointReference)
				WebUI.setText(findTestObject('A-DemandeServiceLS/P-DS LS/Informations DS/T-Instructions'), sInstructions)
				bLoop = false
			}catch(Exception ex){}
		}
	}

	/**
	 * Remplit les descriptions de la DS
	 * @param sDescription
	 * @param sDescriptionDetaillee
	 */
	@Keyword
	def remplitDescription(String sDescription, String sDescriptionDetaillee) {
		//gk.forceSetTextElement('A-DemandeServiceLS/P-DS LS/Description/T-Description', '[Katalon] ' + sDescription)
		gk.click('A-DemandeServiceLS/P-DS LS/Description/T-Description',null)
		gk.forceSetTextElement('A-DemandeServiceLS/P-DS LS/Description/T-Description', '[Katalon] ' + sDescription)
		if(!sDescriptionDetaillee.isEmpty()) {
			//WebUI.switchToFrame(findTestObject('A-DemandeServiceLS/P-DS LS/Description/Iframe Description Detaillee'), 1)
			WebUI.setText(findTestObject('A-DemandeServiceLS/P-DS LS/Description/TA-Description Detaillee'), sDescriptionDetaillee)
			//WebUI.switchToDefaultContent()
		}
	}

	/**
	 * Sauvegarde le numéro de la DS dans le popup après création d'une intervention dans Libre-Service
	 */
	@Keyword
	def sauvegardeNoDS() {
		//Récupère le numéro de la DS créée
		WebUI.waitForElementPresent(findTestObject('A-DemandeServiceLS/P-DS LS/Pop Confirmation/L-Message Creation'), 10)
		String[] tabMessage = (WebUI.getText(findTestObject('A-DemandeServiceLS/P-DS LS/Pop Confirmation/L-Message Creation'))).split(' ')

		for(int i=0; i<tabMessage.size(); i++) {
			try {
				if(WebUI.verifyMatch(tabMessage[i], "^\\d+\$", true)) {
					return tabMessage[i]
				}
			} catch(Exception ex) {}
		}
	}

	/**
	 * Valide les informations dans la fenêtre des DS
	 * @param sNoDS
	 * @param sType
	 * @param sExec
	 * @param sClassification
	 * @param sNoActif
	 * @param sDescription
	 * @param sDescriptionDetaillee
	 */
	@Keyword
	def verifieInfosDSLS(sNoDS, sType, sExec, sClassification, sNoActif, sDescription, sDescriptionDetaillee) {
		//Vérifie les informations de la DS créée
		//Vérifie le numéro de DS
		WebUI.verifyElementAttributeValue(findTestObject('A-DemandeServiceLS/P-DS LS Details/T-NoDS'), 'value', sNoDS, 2)

		//Vérifie le statut
		if(sType == "MC") {
			gk.valideInput('A-DemandeServiceLS/P-DS LS Details/T-Statut', 'PREP', false, "value")
		} else if(sType == "AMENT") {
			if(sClassification == "ADOC" || sClassification == "SUP" || sClassification == "FORM") {
				gk.valideInput('A-DemandeServiceLS/P-DS LS Details/T-Statut', 'NOUVEAU', false, "value")
			} else if(sClassification == "AME") {
				gk.valideInput('A-DemandeServiceLS/P-DS LS Details/T-Statut', 'ANALYSE', false, "value")
			}
		}
		WebUI.delay(1)
		//Vérifie le numéro d'actif
		WebUI.verifyElementAttributeValue(findTestObject('A-DemandeServiceLS/P-DS LS Details/T-Actif'), 'value', sNoActif, 2)
		//Vérifie la description
		WebUI.verifyElementAttributeValue(findTestObject('A-DemandeServiceLS/P-DS LS Details/T-Description'), 'value', "[Katalon] " + sDescription, 2)
		
		try{
			//Vérifie la description détaillée
			if(sDescriptionDetaillee != "") {
				WebUI.switchToFrame(findTestObject('A-DemandeServiceLS/P-DS LS Details/Iframe-Description Detaillee'), 1)
				WebUI.verifyElementAttributeValue(findTestObject('A-DemandeServiceLS/P-DS LS Details/T-Description Detaillee'), 'value', sDescriptionDetaillee, 2)
				WebUI.switchToDefaultContent()
			}
		}catch (Exception e){
			gk.logError("Le champs pour la description detaillée est introuvable")
		}
	}
}
