package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DF

import java.util.List

public class GammesOperationsKeys {
	private KeywordLogger log = new KeywordLogger()
	private WebDriver driver = DF.getWebDriver()
	private GeneralKeys gk = new GeneralKeys()

	/**
	 * Acceder a l'application gamme d'operation
	 */
	@Keyword
	def accederAGO(){
		boolean bLoop = true

		while(bLoop) {
			try {
				WebUI.click(findTestObject('P-Accueil/Navbar/B-AllerA'))
				WebUI.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/Planification/M-PlanificationM'))
				WebUI.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/Planification/M-GammesOperations'))
				WebUI.click(findTestObject('P-Accueil/Navbar/AllerA/Planification/M-GammesOperations'))
				bLoop = false
			} catch(Exception ex){}
		}
	}

	/**
	 * Remplit les informations dans l'entête de la GO
	 * @param sNoGo
	 * @param sDescription
	 * @param sDuree
	 * @param sGroupeProprietaire
	 */
	@Keyword
	def remplirEnteteGO(String sNoGo, String sDescription, String sDuree, String sGroupeProprietaire){
		WebUI.setText(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/T-No'), sNoGo)
		WebUI.setText(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/T-Description'),sDescription )
		WebUI.setText(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/T-Duree'), sDuree)
		WebUI.setText(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/T-GroupeProprietaire'), sGroupeProprietaire)
	}

	/**
	 * Valide l'entete de la GO
	 * @param sNoGo
	 * @param sDescription
	 * @param sDuree
	 * @param sGroupeProprietaire
	 */
	@Keyword
	def validerEnteteGO(String noGo, String descGO, String org, String site, String duree, String classif, String classifDesc, String modeExec, String gP){
		log.logInfo("No Go : "+noGo + " Description : "+descGO)

		gk.valideInput('A-GammesOperations/P-GO/O-GammeOperations/T-No', noGo, false, "value")
		gk.valideInput('A-GammesOperations/P-GO/O-GammeOperations/T-Description', descGO, false, "value")
		valideInputEnteteGO('A-GammesOperations/P-GO/O-GammeOperations/T-Organisation', org)
		valideInputEnteteGO('A-GammesOperations/P-GO/O-GammeOperations/T-Site', site)
		valideInputEnteteGO('A-GammesOperations/P-GO/O-GammeOperations/T-ClassificationDesc', classifDesc)
		valideInputEnteteGO('A-GammesOperations/P-GO/O-GammeOperations/T-GroupeProprietaire', gP)
	}

	/**
	 * Valide les informations dans l'entête de la GO
	 * @param pathObject
	 * @param objectFileValue
	 */
	def valideInputEnteteGO(String pathObject, String objectFileValue){
		WebUI.delay(1)
		log.logInfo("valideInputEnteteGO de  : " +pathObject)
		WebElement e =	DF.getWebDriver().findElement(By.id(findTestObject(pathObject).findProperty('id').getValue()))

		//Change la couleur de la case
		if(WebUI.waitForElementAttributeValue(findTestObject(pathObject), 'value', objectFileValue, 10,FailureHandling.OPTIONAL)){
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(e))
		}else{
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(e))
			log.logError("Erreur avec la valeur de la GO : "+objectFileValue)
		}
	}

	/**
	 * Valides les tâches de la GO
	 * @param compare
	 */
	@Keyword
	def valideTachesGO(List compare){
		WebElement table = DF.getWebDriver().findElement(By.id("mc564eb3c_tbod-tbd"))
		List<WebElement> tablerow
		int nbTaches = DF.getWebDriver().findElement(By.id("mc564eb3c-lb3")).getText().split(' ')[4] as Integer

		if(compare.size() == nbTaches){
			log.logInfo("Le nombre de tâche est identique")
		}else{
			log.logError("Le nombre de tâche n'est pas identique Maximo :  "+nbTaches+" Fichier EXCEL : "+compare.size())
		}

		//Parcours et validation des taches de la GO

		/**
		 boolean endLoop = false
		 while (!endLoop) {
		 tablerow = table.findElements(By.className("tablerow"))
		 if(WebUI.verifyElementAttributeValue(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/navTable/B-Next'), "ev", "false", 30, FailureHandling.CONTINUE_ON_FAILURE)){
		 endLoop = true
		 log.logError("END LOOP")
		 }else{
		 log.logError("LOOP NEXT CLIC SUIVANT")
		 WebUI.click(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/navTable/B-Next'))
		 WebUI.delay(10)
		 }
		 log.logError(endLoop.toString())
		 }**/
	}

	/**
	 * Garde en mémoire la valeur du champ classification de la gamme d'opération et le renvoie
	 * @return classification de la GO
	 */
	@Keyword
	def sauvegardeClassification() {
		//Sauvegarde la classification
		WebUI.waitForElementPresent(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/T-Classification'), 5)
		return WebUI.getAttribute(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/T-Classification'), 'value')
	}

	/**
	 * Sauvegarde les tâches de la GO et les renvoie dans un ArrayList<String>
	 * @return arrayTaches contenant les taches de la GO
	 */
	@Keyword
	def sauvegardeTaches() {
		int i=0
		boolean bLoopTaches = true
		ArrayList<String> arrayTaches = new ArrayList<>()

		//Sauvegarde le tableau de tâches de la GO
		while(bLoopTaches) {
			try {
				//Sauve le numero de tâche
				arrayTaches.add(0+4*i, (driver.findElement(By.id("mc564eb3c_tdrow_[C:2]_txt-tb[R:"+i+"]"))).getAttribute("value"))
				//Sauve la description de la tâche
				arrayTaches.add(1+4*i, ((driver.findElement(By.id("mc564eb3c_tdrow_[C:3]-c[R:"+i+"]"))).getAttribute("innerHTML").trim().split('<'))[0])
				//Sauve la GO imbriquée
				arrayTaches.add(2+4*i, (driver.findElement(By.id("mc564eb3c_tdrow_[C:4]_txt-tb[R:"+i+"]"))).getAttribute("value"))

				//Sauve la durée de la tâche
				arrayTaches.add(3+4*i, (driver.findElement(By.id("mc564eb3c_tdrow_[C:5]_txt-tb[R:"+i+"]"))).getAttribute("value"))
				//Sauve la priorité de la tâche
				//arrayTaches.add(4+5*i, (driver.findElement(By.id("mc564eb3c_tdrow_[C:7]_txt-tb[R:"+i+"]"))).getAttribute("value"))
				i++

				//Change de page de tâches d'opération si nécessaire
				if(i%4 == 0 && WebUI.getAttribute(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/O-Taches/B-Next Page'), 'ev') == 'true') {
					//Appuie sur la flèche pour changer de page
					WebUI.click(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/O-Taches/B-Next Page'))
					WebUI.waitForPageLoad(10)
					WebUI.delay(5)
				}
			} catch(Exception e) {
				bLoopTaches = false
			}
		}

		return arrayTaches
	}

	/**
	 * Sauvegarde la main d'oeuvre planifiée dans la GO et renvoie l'information dans un array
	 * @return arrayMainOeuvre contenant 
	 */
	@Keyword
	def sauvegardeMainOeuvre() {
		int i=0;
		boolean bLoopMainOeuvre = true
		ArrayList<String> arrayMainOeuvre = new ArrayList<>()

		//Sauvegarde le tableau de main d'oeuvre de la GO
		while(bLoopMainOeuvre) {
			try{
				//Sauve le corps de métier
				arrayMainOeuvre.add(0+5*i, (driver.findElement(By.id("ma0e8b2fb_tdrow_[C:3]_txt-tb[R:"+i+"]"))).getAttribute("value"))
				//Sauve la quantité d'ouvrier
				arrayMainOeuvre.add(1+5*i, (driver.findElement(By.id("ma0e8b2fb_tdrow_[C:5]_txt-tb[R:"+i+"]"))).getAttribute("value"))
				//Sauve la durée
				arrayMainOeuvre.add(2+5*i, (driver.findElement(By.id("ma0e8b2fb_tdrow_[C:6]_txt-tb[R:"+i+"]"))).getAttribute("value"))
				//Sauve le taux horaire des employés
				arrayMainOeuvre.add(3+5*i, ((driver.findElement(By.id("ma0e8b2fb_tdrow_[C:7]-c[R:"+i+"]"))).getAttribute("innerHTML").trim().split('<'))[0])
				//Sauve le coût total
				arrayMainOeuvre.add(4+5*i, ((driver.findElement(By.id("ma0e8b2fb_tdrow_[C:8]-c[R:"+i+"]"))).getAttribute("innerHTML").trim().split('<'))[0])
				i++

				//Change de page de main d'oeuvre si nécessaire
				if(i%4 == 0 && WebUI.getAttribute(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/O-MainOeuvre/B-Next Page'), 'ev') == 'true') {
					//Appuie sur la flèche pour changer de page
					WebUI.click(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/O-MainOeuvre/B-Next Page'))
					WebUI.waitForPageLoad(10)
					WebUI.delay(5)
				}
			} catch(Exception e) {
				bLoopMainOeuvre = false
			}
		}

		return arrayMainOeuvre
	}

	/**
	 * Sauvegarde les articles utilisés dans la GO
	 * @return arrayArticles
	 */
	@Keyword
	def sauvegardeArticles() {
		int i=0;
		boolean bLoopPieces = true
		ArrayList<String> arrayArticles = new ArrayList<>()

		//Appuie sur l'onglet Articles
		WebUI.click(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/Tab Pieces'))
		WebUI.delay(10)

		//Sauvegarde le tableau de pièces de la GO
		while(bLoopPieces) {
			try{

				//Sauve le numéro de tâche associée
				arrayArticles.add(0+7*i, (driver.findElement(By.id("m27507af0_tdrow_[C:1]_txt-tb[R:"+i+"]"))).getAttribute("value"))
				//Sauve le numéro d'article
				arrayArticles.add(1+7*i, (driver.findElement(By.id("m27507af0_tdrow_[C:2]_txt-tb[R:"+i+"]"))).getAttribute("value"))
				//Sauve la description
				arrayArticles.add(2+7*i, (driver.findElement(By.id("m27507af0_tdrow_[C:3]_txt-tb[R:"+i+"]"))).getAttribute("value"))
				//Sauve le magasin
				arrayArticles.add(3+7*i, ((driver.findElement(By.id("m27507af0_tdrow_[C:4]-c[R:"+i+"]"))).getAttribute("innerHTML").trim().split('<'))[0])
				//Sauve le nombre d'article
				arrayArticles.add(4+7*i, (driver.findElement(By.id("m27507af0_tdrow_[C:5]_txt-tb[R:"+i+"]"))).getAttribute("value"))
				//Sauve le coût unitaire
				arrayArticles.add(5+7*i, ((driver.findElement(By.id("m27507af0_tdrow_[C:6]-c[R:"+i+"]"))).getAttribute("innerHTML").trim().split('<'))[0])
				//Sauve le coût total
				arrayArticles.add(6+7*i, ((driver.findElement(By.id("m27507af0_tdrow_[C:7]-c[R:"+i+"]"))).getAttribute("innerHTML").trim().split('<'))[0])
				i++

				//Change de page de pièce si nécessaire
				if(i%4 == 0 && WebUI.getAttribute(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/O-Taches/B-Next Page'), 'ev') == 'true') {
					'Appuie sur la flèche pour changer de page'
					WebUI.click(findTestObject('A-GammesOperations/P-GO/O-GammeOperations/O-Taches/B-Next Page'))
					WebUI.waitForPageLoad(10)
					WebUI.delay(5)
				}
			} catch(Exception e) {
				bLoopPieces = false
			}
		}

		return arrayArticles
	}
}
