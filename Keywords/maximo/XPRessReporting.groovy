package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.ArrayList

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.By
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement


public class XPRessReporting {
	private GeneralKeys gk = new GeneralKeys()
	private WebDriver driver = DriverFactory.getWebDriver()

	/**
	 * Fonction qui permet d'acceder à XPRess reporting
	 */
	def allerAXPRessRepo(){
		gk.click('P-Accueil/Navbar/B-AllerA', null)
		gk.click('Object Repository/P-Accueil/Navbar/AllerA/B-Intervention', null)
		gk.click('Object Repository/P-Accueil/Navbar/AllerA/B-XPRessRepo', null)
	}


	/**
	 * 
	 * @param emplacement
	 * @param listeTaches
	 * @return
	 */
	def rechercheIntervention(String emplacement){
		boolean resultat
		//Cliquer sur filtre et rechercher l'emplacement
		gk.click('Object Repository/A-XPRessReporting/B-filtre',null)
		gk.click('Object Repository/A-XPRessReporting/T-Emplacement',null)
		gk.forceSetTextElement('Object Repository/A-XPRessReporting/T-Emplacement', emplacement)
		WebUI.sendKeys(findTestObject('Object Repository/A-XPRessReporting/T-Emplacement'), Keys.chord(Keys.ENTER))
		WebUI.delay(2)
	}


	/**
	 * Fonctio qui permet de valider une intervention, si la tâche de l'intervention qui est associé à cette main d'oeuvre est présente
	 * dans l'XPRess reporting de cet utilisateur. Si c'est le cas on met de la couleur verte sinon on met de la couleur rouge
	 * 
	 * @param listeTaches, liste des tâches de l'intervention 
	 * @param emplacement, emplacement de l'actif de l'intervention
	 * @param noIntervention, numéro de l'intervention 
	 * @param noGo, numéro de la gamme d'opération de l'intervention
	 * @return
	 */
	def validerIntervention(ArrayList<String> listeTaches, String emplacement, String noIntervention, String noGo){
		boolean loop = true
		boolean resultat = false
		int i = 0
		WebElement element
		String intervention = ""
		WebElement element2 = driver.findElement(By.id("m5ead46db-lb"))
		while(loop){
			try{
				// Entregistrer le numéro de tâche
				element = driver.findElement(By.id("m5ead46db_tdrow_[C:2]-c[R:"+i+"]"))
				intervention = element.getText()
				int j = 0
				if(noGo != ""){
					// On parcourt la liste de numéro de tâches de notre intervention
					while (j < listeTaches.size() && resultat == false){
						// Si le numéro de tâche correspond à une numéro de tâche de nôtre intervention on met le resultat a true
						if(intervention == listeTaches.get(j)){
							resultat = true
							loop = false
						}
						j++
					}
				}else{
					// Si il ya une tâche qui correspond au numéro de l'intervention on met le resultat a true
					if (intervention == noIntervention && resultat == false){
						resultat = true
						loop = false
					}
				}
				// Si le resultat est a true cela veut dire que la tâche de notre intervention se trouve dans le XPRess repoting du préposé
				// On colorie en vert
				if (resultat == true){
					gk.logPassed("La bonne intervention avec l'emplacement "+emplacement+" a été trouvée" )
					WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
					WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element2))
					WebUI.delay(1)
				}
				i++
			}catch (Exception e){
				loop = false
			}
		}
		// Si la tâche de nôtre intervention ne se trouve pas dans le XPRess reporting on met de la couleur rouge
		if (resultat == false){
			gk.logError("l'intervention avec l'emplacement "+emplacement+" n'a pas été trouvée")
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element2))
			WebUI.delay(1)
		}
	}
}
