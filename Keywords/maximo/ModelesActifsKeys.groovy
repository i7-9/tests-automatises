package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DF

public class ModelesActifsKeys {
	private KeywordLogger log = new KeywordLogger()
	private GeneralKeys gk = new GeneralKeys()

	/**
	 * Acceder a l application Referentiel d'articles
	 */
	@Keyword
	def accederAModelesActifs(){
		gk.click('P-Accueil/Navbar/B-AllerA', null)
		gk.mouseOver('P-Accueil/Navbar/AllerA/Actifs/M-ActifsM', null)
		gk.mouseOver('P-Accueil/Navbar/AllerA/Actifs/M-Actifs', null)
		gk.mouseOver('P-Accueil/Navbar/AllerA/Actifs/M-ModelesActif', null)
		gk.click('P-Accueil/Navbar/AllerA/Actifs/M-ModelesActif', null)
	}

	/**
	 * Valide les elements de la page article
	 * @param compare
	 */
	@Keyword
	def comparerSimon(String noIN){
		valideInputArticle('A-ReferentielArticles/P-ReferentielArticles/T-noIN', noIN)
	}

	/**
	 * Valide l'article 
	 * @param pathObject
	 * @param compare
	 */
	def valideInputArticle(String pathObject, String compare){
		WebElement e =	DF.getWebDriver().findElement(By.id(findTestObject(pathObject).findProperty('id').getValue()))

		if(WebUI.waitForElementAttributeValue(findTestObject(pathObject), 'value', compare, 5,FailureHandling.OPTIONAL)){
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(e))
		}else{
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(e))
			log.logError("Erreur avec la valeur de l'article : "+ compare)
		}
	}
}
