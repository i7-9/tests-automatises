package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.text.SimpleDateFormat
import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.WebElement as WebElement

class InterventionKeys {
	private GeneralKeys gk = new GeneralKeys()
	private WebDriver driver = DriverFactory.getWebDriver()

	/**
	 * Navigue jusqu'à la page "Suivi des Interventions"
	 * 
	 */
	@Keyword
	def accederAIntervention() {
		gk.click('P-Accueil/Navbar/B-AllerA', null)
		gk.mouseOver('P-Accueil/Navbar/AllerA/Interventions/M-InterventionsM', null)
		gk.mouseOver('P-Accueil/Navbar/AllerA/Interventions/M-SuiviIntervention', null)
		gk.click('P-Accueil/Navbar/AllerA/Interventions/M-SuiviIntervention', null)
	}


	/**
	 * Remplit les informations de l'intervention
	 * 
	 * @param sDesc - Description de l'intervention
	 * @param sType - Type de l'intervention
	 * @param sExec - Mode d'exécution
	 * @param sAtelier - Atelier de l'intervention
	 * @param sGO - Numéro de la Gamme d'Opérations reliée
	 * @param sActif - Numéro de l'actif recherché
	 * @param sEmplacement - Emplacement de l'actif recherché
	 */
	def remplitInfosInt(String sDesc, String sType, String sExec, String sAtelier, String sGO, String sActif, String sEmplacement, String sCompteGl) {
		//Remplit le champ Description
		gk.click('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Intervention Description', null)
		gk.forceSetTextElement('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Intervention Description', sDesc)
		//Ajout d'un actif sans la carte
		selectionnerActif(sActif)
		// Remplit l'emplacement
		selectionnerEmplacement(sEmplacement)
		//Remplit le champ Type
		gk.click('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Type', null)
		gk.forceSetTextElement('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Type', sType)
		//Remplit le mode d'exécution
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Execution', null)
		//gk.setTextElement('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Execution', sExec, null)
		gk.forceSetTextElement('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Execution', sExec)
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Execution'), Keys.chord(Keys.TAB))
		//Remplit la GO
		WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Details/T-No GO'))
		gk.forceSetTextElement('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Details/T-No GO',sGO)
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Details/T-No GO'), Keys.chord(Keys.TAB))
		//Remplit l'atelier responsable
		WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Responsabilite/T-AtelierResp'))
		gk.forceSetTextElement('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Responsabilite/T-AtelierResp',sAtelier)
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Responsabilite/T-AtelierResp'), Keys.chord(Keys.TAB))

		if(sCompteGl != null){
			entrerCompteGL(sCompteGl)
		}
	}


	/**
	 * Accède à la GO reliée à l'intervention
	 * 
	 */
	def accederAGOImbriquee() {
		while(gk.verifyElementNotPresent('A-SuiviInterventions/P-Int/Int Tab/Details/L-Infos GO', 2, FailureHandling.OPTIONAL)) {
			gk.click('A-SuiviInterventions/P-Int/Int Tab/Details/B-GO', FailureHandling.OPTIONAL)
		}
		gk.click('A-SuiviInterventions/P-Int/Int Tab/Details/L-Infos GO', null)
	}



	/**
	 * Vérifie la validité des différentes tâches
	 * 
	 * @param arrayTaches - ArrayList contenant toutes les tâches à retrouver
	 */
	@Keyword
	def verifieTaches(ArrayList<String> arrayTaches) {
		int iRangeeEnfant = 0 //Compte le nombre d'enfants d'intervention vérifiés
		int iRangeeTache = 0 //Compte le nombre de tâches vérifiées
		int i=0 //Compte le nombre total d'éléments vérifiés (utilisé pour simplification)
		boolean bLoopTaches = true

		while(bLoopTaches) {
			try {
				//Vérifie si la tâche contient une GO imbriquée ou non
				if(arrayTaches.get(2+4*i) != "") {
					//La tâche contient une GO imbriquée
					//Vérifie le Récapitulatif
					gk.valideInput("mfd86f8ab_tdrow_[C:4]_txt-tb[R:"+iRangeeEnfant+"]", arrayTaches.get(1+4*iRangeeEnfant), true, "value")
					//Clique sur la flèche pour afficher les détails de la tâche
					driver.findElement(By.id('mfd86f8ab_tdrow_[C:0]_tgdet-ti[R:'+iRangeeEnfant+']_img')).click()
					WebUI.delay(1)
					//Vérifie le numéro de la GO
					gk.valideInput('A-SuiviInterventions/P-Int/Plans Tab/T-GO', arrayTaches.get(2+4*iRangeeEnfant), true, "value")
					//Vérifie la durée de l'intervention
					WebUI.delay(1)
					gk.valideInput('A-SuiviInterventions/P-Int/Plans Tab/Enfant/T-Duree', arrayTaches.get(3+4*iRangeeEnfant), true, "value")
					//Ferme les détails de la tâche
					driver.findElement(By.id('mfd86f8ab_tdrow_[C:0]_tgdet-ti[R:'+iRangeeEnfant+']_img')).click()
					iRangeeEnfant++
					//Appuie sur la flèche afin de changer de page d'intervention enfant
					if(iRangeeEnfant%6 == 0 && WebUI.getAttribute(findTestObject('A-SuiviInterventions/P-Int/Plans Tab/Enfant/B-Next Page'), 'ev') == 'true') {
						gk.click('A-SuiviInterventions/P-Int/Plans Tab/Enfant/B-Next Page', null)
						WebUI.delay(2)
					}
				} else {
					//La tâche ne contient pas de GO imbriquée
					//Vérifie la description
					gk.valideInput("mbb442a0c_tdrow_[C:3]-c[R:"+iRangeeTache+"]", arrayTaches.get(1+4*iRangeeTache), true, "value")
					//Vérifie la durée
					gk.valideInput("mbb442a0c_tdrow_[C:4]_txt-tb[R:"+iRangeeTache+"]", arrayTaches.get(3+4*iRangeeTache), true, "value")
					iRangeeTache++
					//Appuie sur la flèche afin de changer de page d'intervention enfant
					if(iRangeeTache%6 == 0 && WebUI.getAttribute(findTestObject('A-SuiviInterventions/P-Int/Plans Tab/Tache/B-Next Page'), 'ev') == 'true') {
						gk.click('A-SuiviInterventions/P-Int/Plans Tab/Tache/B-Next Page')
						WebUI.delay(2)
					}
				}
				i++
			} catch(Exception e) {
				bLoopTaches = false
			}
		}
	}


	/**
	 * Vérifie la validité de la main d'oeuvre
	 * 
	 * @param arrayMainOeuvre - ArrayList contenant toutes la main d'oeuvre à retrouver
	 */
	@Keyword
	def verifieMainOeuvre(ArrayList<String> arrayMainOeuvre) {
		int i=0;
		boolean bLoopMainOeuvre = true

		//Vérifie le nombre d'éléments dans le tableau main d'oeuvre
		/*if(arrayMainOeuvre.size()/5 == 0)
		 gk.valideInput("A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/L-NbMO", "&nbsp;0 - &nbsp;0 de 0", false, "innerHTML")
		 else if(arrayMainOeuvre.size()/5 < 6)
		 gk.valideInput("A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/L-NbMO", "&nbsp;1 - &nbsp;" + arrayMainOeuvre.size()/5 + " de " + arrayMainOeuvre.size()/5, false, "innerHTML")
		 else
		 gk.valideInput("A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/L-NbMO", "&nbsp;1 - &nbsp;6 de " + arrayMainOeuvre.size()/5, false, "innerHTML")*/
		//Vérifie le contenu du tableau de main d'oeuvre
		while(bLoopMainOeuvre) {
			try{
				//Vérifie le corps de métier
				gk.valideInput("m6c82340f_tdrow_[C:2]_txt-tb[R:"+i+"]", arrayMainOeuvre.get(0+5*i), true, "value")
				//Vérifie le nombre d'employés
				gk.valideInput("m6c82340f_tdrow_[C:3]_txt-tb[R:"+i+"]", arrayMainOeuvre.get(1+5*i), true, "value")
				//Vérifie le nombre d'heures requises
				gk.valideInput("m6c82340f_tdrow_[C:5]_txt-tb[R:"+i+"]", arrayMainOeuvre.get(2+5*i), true, "value")
				//Vérifie le taux horaire des employés
				gk.valideInput("m6c82340f_tdrow_[C:6]_txt-tb[R:"+i+"]", arrayMainOeuvre.get(3+5*i), true, "value")
				//Vérifie le coût total
				gk.valideInput("m6c82340f_tdrow_[C:7]_txt-tb[R:"+i+"]", arrayMainOeuvre.get(4+5*i), true, "value")
				i++
				//Appuie sur la flèche afin de changer de page de main d'oeuvre
				if(i%6 == 0 && WebUI.getAttribute(findTestObject('A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/B-Next Page'), 'ev') == 'true') {
					gk.click('A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/B-Next Page')
					WebUI.delay(2)
				}
			} catch(Exception e) {
				bLoopMainOeuvre = false
			}
		}
	}


	/**
	 * Vérifie la validité des articles
	 * 
	 * @param arrayArticles - ArrayList contenant tous les articles à retrouver
	 */
	@Keyword
	def verifieArticles(ArrayList<String> arrayArticles) {
		int i=0;
		boolean bLoopPieces = true

		//Clique sur l'onglet Articles
		allerAArticle()
		//Vérifie le nombre d'articles
		/*if(arrayArticles.size()/7 == 0)
		 gk.valideInput("A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/L-NbMO", "&nbsp;0 - &nbsp;0 de 0", false, "innerHTML")
		 else if(arrayArticles.size()/7 <= 6)
		 gk.valideInput("A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/L-NbMO", "&nbsp;1 - &nbsp;" + arrayArticles.size()/7 + " de " + arrayArticles.size()/7, false, "innerHTML")
		 else
		 gk.valideInput("A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/L-NbMO", "&nbsp;1 - &nbsp;6 de " + arrayArticles.size()/7, false, "innerHTML")*/

		//Vérifie le contenu du tableau de pièces
		while(bLoopPieces) {
			try{
				//Vérifie le numéro de tâche associée
				gk.valideInput("m27507af0_tdrow_[C:1]_txt-tb[R:"+i+"]", arrayArticles.get(0+5*i), true, "value")
				//Vérifie le numéro d'article
				gk.valideInput("m27507af0_tdrow_[C:2]_txt-tb[R:"+i+"]", arrayArticles.get(1+5*i), true, "value")
				//Vérifie la description
				gk.valideInput("m27507af0_tdrow_[C:3]_txt-tb[R:"+i+"]", arrayArticles.get(2+5*i), true, "value")
				//Vérifie le magasin
				gk.valideInput("m27507af0_tdrow_[C:7]_txt-tb[R:"+i+"]", arrayArticles.get(3+5*i), true, "value")
				//Vérifie le nombre d'article
				gk.valideInput("m27507af0_tdrow_[C:4]_txt-tb[R:"+i+"]", arrayArticles.get(4+5*i), true, "value")
				//Vérifie le coût unitaire
				gk.valideInput("m27507af0_tdrow_[C:5]_txt-tb[R:"+i+"]", arrayArticles.get(5+5*i), true, "value")
				//Vérifie le coût total
				gk.valideInput("m27507af0_tdrow_[C:6]_txt-tb[R:"+i+"]", arrayArticles.get(6+5*i), true, "value")
				i++
				//Appuie sur la flèche afin de changer de page de main d'oeuvre
				if(i%6 == 0 && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Suivant'), 'ev') == 'true') {
					gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Suivant')
					WebUI.delay(2)
				}
			} catch(Exception e) {
				bLoopPieces = false
			}
		}
	}


	/**
	 * Sauvegarde les activités de la tâche
	 * 
	 * @param nbTaches
	 * @return array contenant les taches et les durees des tâches
	 */
	@Keyword
	def sauvegardeActivites(int nbTaches) {
		ArrayList<String> arrayTaches = new ArrayList<String>()
		ArrayList<String> arrayDurees = new ArrayList<String>()
		ArrayList<ArrayList<String>> array = new ArrayList<ArrayList<String>>()

		for(int i=0; i<nbTaches; i++) {
			//Change de page de tâche si nécessaire
			if(i!=0 && i%6==0) {
				gk.click('A-SuiviInterventions/P-Int/Plans Tab/Tache/B-Next Page', null)
				gk.click('A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/B-NextPage', null)
			}
			//Navigue à travers des tâches
			if(arrayTaches.size() <= i) {
				//Ouvre la tâche
				WebUI.delay(2)
				driver.findElement(By.id('mbb442a0c_tdrow_[C:0]_tgdet-ti[R:'+i+']_img')).click()

				//Sauvegarde le no d'activité
				if(!arrayTaches.contains(WebUI.getAttribute(findTestObject("A-SuiviInterventions/P-Int/Plans Tab/Tache/T-NoActivite"), "value"))) {
					arrayTaches.add(WebUI.getAttribute(findTestObject("A-SuiviInterventions/P-Int/Plans Tab/Tache/T-NoActivite"), "value"))

					if(Integer.parseInt(WebUI.getAttribute(findTestObject("A-SuiviInterventions/P-Int/Plans Tab/Tache/T-DureePrevue"), "value").split(":")[1])*2>60) {
						arrayDurees.add(Integer.parseInt(WebUI.getAttribute(findTestObject("A-SuiviInterventions/P-Int/Plans Tab/Tache/T-DureePrevue"), "value").split(":")[0])*2 + ":" +
								Integer.parseInt(WebUI.getAttribute(findTestObject("A-SuiviInterventions/P-Int/Plans Tab/Tache/T-DureePrevue"), "value").split(":")[1])*2-60)
					} else {
						arrayDurees.add(Integer.parseInt(WebUI.getAttribute(findTestObject("A-SuiviInterventions/P-Int/Plans Tab/Tache/T-DureePrevue"), "value").split(":")[0])*2 + ":" +
								Integer.parseInt(WebUI.getAttribute(findTestObject("A-SuiviInterventions/P-Int/Plans Tab/Tache/T-DureePrevue"), "value").split(":")[1])*2)
					}
				}

				//Ferme la tâche
				while(WebUI.verifyElementVisible(findTestObject("A-SuiviInterventions/P-Int/Plans Tab/Tache/T-NoActivite"))) {
					try {
						driver.findElement(By.id('mbb442a0c_tdrow_[C:0]_tgdet-ti[R:'+i+']_img')).click()
					}catch(Exception ex){}
				}
			}
		}

		array.add(arrayTaches)
		array.add(arrayDurees)
		return array
	}


	/**
	 * Selectionner les actions corrective de façon aléatoire, et toujours selectionner la tâche numéro 5
	 * 
	 */
	@Keyword
	def selectionnerActionsCorrectives() {
		int i = 0
		boolean bLoopTaches = true
		int nbActionMax = 3
		gk.click('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Intervention Description', null)
		WebUI.focus(findTestObject('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Intervention Description'))
		WebUI.delay(1)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Tabs/ActionsCorrectives', null)

		// chercher le nombre d'actions correctives contenu dans la GO
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/T-NbActionsCorrectives'), 5)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/T-NbActionsCorrectives', null)
		String [] textNbAc =WebUI.getText(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/T-NbActionsCorrectives')).split(" ")
		int nbRows = Integer.parseInt(textNbAc[(textNbAc.size() - 1)])
		int  nbActions

		// CHoisir le nombre d'actions correctives à selectionner
		if (nbRows > nbActionMax){
			// nombre d'actions correctives désirées
			nbActions= Math.floor(Math.random() * ((nbActionMax-1) - 1 + 1)) + 1;
		}else {
			// nombre d'actions correctives désirées
			nbActions= Math.floor(Math.random() * ((nbRows-1) - 1 + 1)) + 1;
		}

		// Conversion d'un web element en un test object
		//TestObject actionCorrective5 = gk.convertWebElement("m4c505561_tdrow_[C:4]_checkbox-cb[R:0]_img")

		while (WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pagePrecedente'), 'ev') == 'true'){
			gk.click('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pagePrecedente',null)
			WebUI.waitForPageLoad(10)
		}
		/*// Selectionner l'action corrective numéro 5
		 WebUI.waitForElementPresent(actionCorrective5,5)
		 WebUI.click(actionCorrective5)*/

		// Position de l'action corrective dans le tableau
		int posAction = Math.floor(Math.random() * ((nbRows-1) - 1 + 1)) + 1

		// boucle qui parcourt les actions correctives
		while(i < nbActions) {
			try {

				// Si on est dans la 2e page et le checkBox a selectionné est dans la 1ere page, on retourne à la 1ere page
				if (posAction < 20 && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pagePrecedente'), 'ev') == 'true') {
					WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pagePrecedente'))
					WebUI.waitForPageLoad(10)

					// Si on est à la 1ere page et le checkBox a selectionné est dans la 2eme page, on va à la 2eme page
				}else if (posAction >= 20  && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pageSuivante'), 'ev') == 'true'){
					WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pageSuivante'))
					WebUI.waitForPageLoad(10)
				}

				// On crée un testObject en convertissant les webElement
				TestObject actionCorrective = gk.convertWebElement("m4c505561_tdrow_[C:3]_checkbox-cb[R:" +posAction+ "]_img")
				WebUI.waitForElementPresent(actionCorrective,5)

				// Si le checkBox n'est pas selectionné, on le selectionne et on modifie la position de la prochaine action corrective
				if (etatCheckBox(WebUI.getAttribute(actionCorrective,'src' ))){
					WebUI.click(actionCorrective)
					WebUI.delay(1)
					i++

				}
				// Modifier la position de la prochaine action corrective à selectionné
				posAction = Math.floor(Math.random() * ((nbRows-1) - 1 + 1)) + 1

			} catch(Exception e) {
				bLoopTaches = false
			}
		}
		// Cliquer sur sauvgarder pour enregistrer la selection des Action correctives
		gk.click('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
		WebUI.waitForPageLoad(20)
		WebUI.delay(5)
	}


	/**
	 * Fonction qui permet de sélectionner un actif et de le lier à une demande de service
	 * 
	 * @param noActif, le numéro de l'actif
	 */
	def selectionnerActif(String noActif){
		if(!noActif.isEmpty()) {
			Boolean bTrue = true

			WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Details/B-Actif'))
			WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Details/L-RechercheActif'), 3)
			WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Details/L-RechercheActif'))
			//Inscrit le numéro de l'actif
			gk.forceSetTextElement('Object Repository/FenetreSelectionnerValeur/T-Recherche', "=" + noActif)
			WebUI.sendKeys(findTestObject('Object Repository/FenetreSelectionnerValeur/T-Recherche'), Keys.chord(Keys.ENTER))
			WebUI.delay(1)
			gk.click('Object Repository/FenetreSelectionnerValeur/T-ResultatRecherche',null)
			//Attend que la fenêtre de recherche disparaîsse
			WebUI.waitForElementNotPresent(findTestObject('Object Repository/FenetreSelectionnerValeur/T-ResultatRecherche'), 5)

			// Vérification s'il ya un message d'erreur par rapport à l'emplacement
			try{
				gk.click('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Type', null)

			}catch (Exception e){
				// Appuyer sur Oui pour la confirmation de l'emplacement
				WebUI.waitForElementPresent(findTestObject('Object Repository/MS-MessageSystem/MS-ConfirmeEmplacement/B-Oui'), 5)
				WebUI.click(findTestObject('Object Repository/MS-MessageSystem/MS-ConfirmeEmplacement/B-Oui'))
			}
		}
	}


	/**
	 * Fonction qui permet de sélectionner un emplacement sur une demande de services
	 * 
	 * @param emplacement, emplacement de l'article
	 */
	def selectionnerEmplacement(String emplacement){
		if(!emplacement.isEmpty()) {
			gk.click('A-SuiviInterventions/P-Int/Int Tab/Infos Int/B-Emplacement', null)
			gk.waitForElementPresent('A-SuiviInterventions/P-Int/Int Tab/Infos Int/L-Select_Emplacement', 2, null)
			gk.click('A-SuiviInterventions/P-Int/Int Tab/Infos Int/L-Select_Emplacement', null)
			gk.setTextElement('Object Repository/FenetreSelectionnerValeur/T-Recherche', emplacement+'', null)
			gk.sendKeys('Object Repository/FenetreSelectionnerValeur/T-Recherche', [Keys.ENTER], null)
			gk.waitForElementPresent('A-SuiviInterventions/P-Int/Int Tab/Infos Int/L-Emplacement_Search_1', 2, null)
			gk.click('Object Repository/FenetreSelectionnerValeur/T-ResultatRecherche', null)
			gk.waitForNotPresent('Object Repository/FenetreSelectionnerValeur/T-ResultatRecherche', 5, null)
		}
	}


	/**
	 * Fonction qui gère le besoin en matériel, selon la quantité de matériel voulu, la quantité disponible et le retour de matériel
	 * 
	 * @param materielDispo, est ce que la quantite en stock est suffisante ou pas 
	 * @param magasin, le magasin desiré
	 * @param arrayArticles, liste contenant la liste d'articles dans plan de travail
	 * @return listQteDispo, liste contenant les quantités disponible pour chaque article
	 */
	def gestionDuBesoinEnMateriel(String materielDispo, String magasin, int nbArticle){
		ArrayList<Integer> listQteDispo = new ArrayList<>()

		// Si il ya des articles présents dans mon plan de travail
		if (nbArticle>0){
			int i = 0
			while (i < nbArticle){
				// Enregistrer la quantité disponible en magasin
				ouvrirVoirDispoArt(magasin, i)
				int quantiteDispoStock = quantiteEnStock(magasin, i)
				int qteDispo = quantiteDiso(magasin, i)
				if((quantiteDispoStock>0 && materielDispo== "OUI")||materielDispo== "NON"){
					listQteDispo.add(qteDispo)
				}
				//Cliquer sur le bouton OK
				gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/B-OK',null)
				WebUI.waitForElementNotPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/B-OK'), 5)
				// Choisir la nouvelle quantite qui sera commandée
				int nouvelleQuantite = choisirQuantiteACommande(quantiteDispoStock, materielDispo)
				WebUI.delay(3)
				if (nouvelleQuantite == 0){
					supprimerArticle(i)
				}else{
					// Modifier la quantité à commandée
					modifierQuantiteArtic(nouvelleQuantite, i)
				}
				i++
				// Cliquer sur le bouton page suivante au besoin
				WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Suivant'),5)
				if(i%6==0 && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Suivant'), 'ev') == 'true') {
					boolean loop2 = true
					while (loop2){
						try{
							driver.findElement(By.id("m900f2d81-ti7_img")).click()
							loop2 = false
						}catch (Exception e){}
					}
					WebUI.waitForPageLoad(10)
					WebUI.delay(1)
				}
			}
			// Cliquer sur sauvgarder pour enregistrer
			gk.click('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
			WebUI.waitForPageLoad(20)
		}
		return listQteDispo
	}


	/**
	 * 
	 */
	def retournerNbArticles(){
		allerAArticle()
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/T-NbRows',  null)
		// Chercher le nombre d'articles necessaire pour les tâches selectionnées
		String [] textNbRows =WebUI.getText(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/T-NbRows')).split(" ")
		int nbRows = Integer.parseInt(textNbRows[(textNbRows.size() - 1)])
		return nbRows
	}


	/**
	 * Fonction qui sauvgarde la liste des numéros d'articles de tous les articles du plan de travail et les rtourne en forme de arrayList
	 * 
	 * @return arrayArticles, la liste des articles du plan de travail
	 */
	def sauvgarderArticles(){
		int i=0;
		boolean bLoopPieces = true
		ArrayList<String> arrayArticles = new ArrayList<>()
		int nbRows = retournerNbArticles()
		// Revenir à la première page si necessaire
		while (WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Precedent'), 'ev') == 'true'){
			gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Precedent',null)
		}
		findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Precedent')

		//Sauvegarder le tableau de pièces de la GO
		while(i<=(nbRows-1)) {
			try{
				//Sauve garder le numéro de l'article
				String value = (driver.findElement(By.id("m900f2d81_tdrow_[C:2]_txt-tb[R:"+i+"]"))).getAttribute("value")
				arrayArticles.add(value)
				i++
				// Changer de page quand c'est nécessaire
				WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Suivant'),5)
				if(i%6==0 && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Suivant'), 'ev') == 'true') {
					driver.findElement(By.id("m900f2d81-ti7_img")).click()
					WebUI.waitForPageLoad(10)
					WebUI.delay(1)
				}
			} catch(Exception e) {
				bLoopPieces = false
			}
		}
		// Cliquer sur sauvgarder pour enregistrer
		gk.click('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
		WebUI.waitForPageLoad(20)
		WebUI.delay(2)
		return arrayArticles
	}


	/**
	 * Fonction qui permet d'envoyer une date soit l'objectif de démarrage et de fin ou le debut ou la fin planifié 
	 * 
	 * @param date, la date qu'on veut envoyer
	 * @param path, le champ à remplir
	 */
	def envoyerDate(String date, String path){
		gk.click('A-SuiviInterventions/P-Int/Tabs/Intervention',null)
		// Séparer la date recu en date format yyyy/MM/dd et heure hh:mm
		String date2 = date.toString()
		String [] listDate = date.split("/")
		String dateVal = listDate[0]
		String heure = listDate[1]
		String dateObjectif = dateVal + " " + heure
		// Insérer une date dans le champ Objectif date démarrage
		WebUI.waitForElementPresent(findTestObject(path), 10)
		gk.click(path,null)
		gk.forceSetTextElement(path, dateObjectif)
		WebUI.sendKeys(findTestObject(path), Keys.chord(Keys.TAB))
		WebUI.delay(3)

	}


	/**
	 * Fonction qui gère l'entrée d'un compte GL
	 * 
	 * @param compteGL, le compte GL
	 */
	def entrerCompteGL(String compteGL){
		// Entrer un numéro de compte GL
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-CompteGL'), 5)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-CompteGL', null)
		//WebUI.setText(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-CompteGL'), compteGL, FailureHandling.CONTINUE_ON_FAILURE)
		gk.forceSetTextElement('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-CompteGL', compteGL)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-CompteGL', null)
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-CompteGL'), Keys.chord(Keys.TAB))
	}


	/**
	 * Fonction qui retourne le nombre de messages présents dans la journal
	 * 
	 * @return nbMsgPresent, représent le nombre de messages présent
	 */
	def RetournerNbMSgJournal(){

		// Cliquer sur l'onglet journal
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Tabs/Journal'), 5)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Tabs/Journal', null)
		// Enregistrer le nombre de message présent dans le journal
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-Journal/JournalTravaux/T-NombreMsg'), 5)
		WebUI.delay(1)
		String [] tNbMessage = WebUI.getText(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-Journal/JournalTravaux/T-NombreMsg')).split(" ")
		String nbMsgPresent = tNbMessage[(tNbMessage.size() - 1)]
		//retourner le nombre de messages présents
		return nbMsgPresent
	}


	/**
	 * Valider le nombre de messages présents dans le journal, s'il ya le bon nombre de message, couleur verte est appliquée sinon une couleur rouge
	 * 
	 * @param nbMsgPresent le nombre de messages présents initiallement 
	 * @param nbArticle le nombre d'article présent de le plan de travail
	 */
	def validationJournal(String nbMsgPresent, int nbArticle){
		WebElement element
		String nbMsgSouhaite
		int nbMsgPres = Integer.parseInt(nbMsgPresent)

		// Cliquer sur l'onglet journal
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Tabs/Journal'), 5)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Tabs/Journal', null)
		// Si au depart il n'a aucun message dans le journal on s'attend à avoir deux message apres l'envoie de la date et
		//s'il ya deja des message on s'attend à avoir un message de plus chaque fois
		if (nbMsgPres == 0){
			nbMsgSouhaite = (nbMsgPres +2).toString()
		}else{
			nbMsgSouhaite = (nbMsgPres + 1).toString()
		}
		WebUI.delay(1)
		element = driver.findElement(By.id('m524afe2e-lb'))
		// Si l'intervention ne nécessite aucun article on vérifie qu'il n'ya aucun message dans le journal si c'est le cas on met en vert sinon en rouge
		if (nbArticle == 0 ){
			if (nbMsgPres == 0){
				gk.logPassed("Le journal contient le bon nombre de message qui est :" + nbMsgPresent)
				WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
				WebUI.delay(1)
			}else{
				gk.logError("Erreur avec le nombre de message attendu qui est: "+ 0 + " ,mais il ya: " + nbMsgPresent + " message")
				WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
				WebUI.delay(1)
			}
			// Si l'intervention nécessite des articles
		}else{
			nbMsgPresent = RetournerNbMSgJournal()
			WebUI.delay(1)
			element = driver.findElement(By.id('m524afe2e-lb'))
			// Si le nombre de message présent est correct on met l'onglet Journal des travaux en vert sinon on le met en rouge
			if (nbMsgSouhaite == nbMsgPresent){
				gk.logPassed("Le journal contient le bon nombre de message qui est :" + nbMsgSouhaite)
				WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
				WebUI.delay(1)
			}else{
				gk.logError("Erreur avec le nombre de message attendu qui est: "+ nbMsgSouhaite+ " ,mais il ya: " + nbMsgPresent + " message")
				WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
				WebUI.delay(1)
			}
		}
	}


	/**
	 * Fonction qui permet de fermer et réouvrir l'intervention afin de rafrichir les éléments du journal
	 * 
	 */
	def reouvrirIntervention(){
		// Cliquer sur l'onglet intervention et enregistrer le numéro de l'intervention
		gk.click('A-SuiviInterventions/P-Int/Tabs/Intervention', null)
		String noIntervention = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Details/T-Intervention'), "value", FailureHandling.STOP_ON_FAILURE)
		// rechercher l'intervention et appuyer sur entrer afin de réouvrir l'intervention
		gk.click('Object Repository/A-SuiviInterventions/BarreOutilsInt/RechercheIntervention',null)
		gk.forceSetTextElement('Object Repository/A-SuiviInterventions/BarreOutilsInt/RechercheIntervention', "=" + noIntervention)
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/BarreOutilsInt/RechercheIntervention'), Keys.chord(Keys.ENTER))
		WebUI.delay(1)
		// Si un message d'alerte s'affiche on appuie sur Non
		if (WebUI.waitForElementPresent(findTestObject('P-Accueil/Navbar/AllerA/Alert-Error/B-Alert-Non'), 1)){
			gk.click('P-Accueil/Navbar/AllerA/Alert-Error/B-Alert-Non',null)
		}
	}


	/**
	 * Fonction qui permet de gérer les changement de statut
	 * 
	 * @param path adresse du statut desiré
	 */
	@Keyword
	def changerStatut (String statut){
		// Cliquer sur l'icone changer statut
		WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/B-ChangerStatut'), 5)
		WebUI.delay(1)
		gk.click('Object Repository/ElementsGeneriques/BarreOutils/B-ChangerStatut',null)
		// Cliquer sur le bouton nouveau statut
		WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-NouveauStatut'), 5)
		gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-NouveauStatut',null)
		// Cliquer sur le statut désiré
		switch(statut) {
			case 'ANNULE':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-Annule',null)
				break
			case 'APPROUVE':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-Approuve',null)
				break
			case 'A REPLANIFIER':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-AReplanifier',null)
				break
			case 'EN ATTENTE DAPPROBATION':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-EnAttenteApprobation',null)
				break
			case 'EN ATTENTE DE PLANIFICATION':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-EnAttenteDePlanification',null)
				break
			case 'CEDULE':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-Cedule',null)
				break
			case 'FERME':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-Ferme',null)
				break
			case 'EN ATTENTE DE CONDITIONS':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-EnAttenteDeConditions',null)
				break
			case 'EN COURS':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-EnCours',null)
				break
			case 'PRET A ORDONNANCER':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-PretAOrdonnancer',null)
				break
			case 'TRAVAUX INTERROMPUS':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-TravauxInterrompus',null)
				break
			case 'TERMINE':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-Termine',null)
				break
			case 'EN ATTENTE DE MATERIEL':
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-EnAttenteDeMateriel',null)
				break
		}
		WebUI.delay(1)
		// Cliquer sur le bouton Ok
		WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK'), 5)
		gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK',null)
		WebUI.delay(3)

	}


	/**
	 * Validaction pour le premier cas de test les messages d'erreur lors du changement de statut sans compte GL et sans date de debut
	 * 
	 */
	def validationMSGErr(){
		// On enregistre la valeure présente dans le champ contenant le numéro GL et le champ contenant la date objectif demarrage
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-CompteGL', null)
		String GL = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-CompteGL'), 'value', FailureHandling.STOP_ON_FAILURE)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos planification/T-ObjectifDemarrage', null)
		String dateDem = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos planification/T-ObjectifDemarrage'), 'value', FailureHandling.STOP_ON_FAILURE)
		// On essaye de changer le statut de l'intervention à prêt
		changerStatut('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/T-PretAOrdonnancer')
		try {
			// On vérifie que des messages d'erreur s'affichent si le champs pour numéro GL ou/et la date sont vides, si des messages s'affichent on colorie en vert
			if (GL == '' || dateDem == '' ){
				WebUI.delay(1)
				WebUI.waitForElementNotPresent(findTestObject('Object Repository/FenetreChargement'), 40)
				WebUI.waitForElementPresent(findTestObject('Object Repository/MS-MessageSystem/MS-ChangementStatut/ManqueGL--Date/T-MS'), 5)
				WebElement element = driver.findElement(By.id('msgbox-lb'))
				WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
				WebUI.delay(1)
				WebUI.click(findTestObject('MS-MessageSystem/B-Ok'))
				// On clique sur le bouton annulé et on attend que la fenêtre disparaisse
				WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule'), 10)
				gk.click('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule',null)
				WebUI.waitForElementNotPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-Annule'), 5)
			}

		}catch(Exception ex){
			// Si les messages d'erreurs ne s'affichent pas on retourne une message d'erreur indiquant le message d'erreur qui ne s'est pas affiché
			if (GL == '' && dateDem == '' ){
				gk.logError("Un message d'erreur devait s'afficher, car il manque un compte GL et la date de demarrage")

			}else if (GL == ''){
				gk.logError("Un message d'erreur devait s'afficher, car il manque un compte GL")

			}else if (dateDem == '' ){
				gk.logError("Un message d'erreur devait s'afficher, car il manque la date de demarrage")
			}
		}
	}


	/**
	 * Vérification de l'état des checkBox 
	 * 
	 * @param src
	 * @return true si le checkBox n'est pas selectionné
	 */
	def etatCheckBox (String src){
		// On split le src pour choisir uniquement le dernier terme
		String [] listSrc = src.split("_")
		// Si le checkBox n'est pas selctionné on retourne true sinon on retourne false
		if (listSrc[(listSrc.size()-1)] == "unchecked.gif" || listSrc[(listSrc.size()-1)] == "unchecked_over.gif"){
			return true
		}else{
			return false
		}
	}


	/**
	 * Cette fonction permet de vérifier la quantité en stock pour un article en particulier selon le magasin voulu
	 * 
	 * @param magasin, le magasin qui contient l'article en question
	 * @return la quantité disponible
	 */
	def quantiteEnStock(String magasin, int i){
		// Chercher la quantité disponible
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-QuantiteEnStock'), 5)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-QuantiteEnStock',null)
		String quantiteDispo = WebUI.getText(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-QuantiteEnStock'))
		try{
			// Retourner la quantité disponible
			String[] quantite = quantiteDispo.split(",")
			return Integer.parseInt(quantite[0])
		}catch (Exception e){
			int qte = 10000
			return qte
		}
	}



	/**
	 * Cette fonction permet de vérifier la quantité disponible pour un article en particulier selon le magasin voulu
	 *
	 * @param magasin, le magasin qui contient l'article en question
	 * @return la quantité disponible
	 */
	def quantiteDiso(String magasin, int i){
		// Chercher la quantité disponible
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-QuantiteDispo'), 5)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-QuantiteDispo',null)
		String quantiteDispo = WebUI.getText(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-QuantiteDispo'))
		try{
			// Retourner la quantité disponible
			String[] quantite = quantiteDispo.split(",")
			return Integer.parseInt(quantite[0])
		}catch (Exception e){
			int qte = 10000
			return qte
		}
	}


	/**
	 * Fonction qui remplace la quantite d'article desiré, par une nouvelle quantité
	 * 
	 * @param nouvelleQuantite qui représente la nouvelle quantité voulue
	 */
	def modifierQuantiteArtic(int nouvelleQuantite, int i){
		boolean bLoop = true
		// Creer une testObject à partir de l'id
		TestObject textQuantite = gk.convertWebElement("m900f2d81_tdrow_[C:4]_txt-tb[R:"+i+"]")
		// Attendre que le champ soit modifiable
		WebUI.waitForElementPresent(textQuantite, 5)
		while(bLoop) {
			try {
				WebUI.click(textQuantite)
				bLoop = false
			}catch(Exception ex){}
		}
		// modifier la quantité qui sera commandée
		WebUI.setText(textQuantite, nouvelleQuantite.toString())
	}


	/**
	 * Fonction qui permet de supprimer une article
	 * 
	 * @param i, position du bouton supprimer de l'article
	 * 
	 * @return
	 */
	def supprimerArticle(int i){
		boolean bLoop = true
		TestObject bSupprimer = gk.convertWebElement("m900f2d81_tdrow_[C:9]_toggleimage-ti[R:"+i+"]_img")
		while(bLoop) {
			try {
				// cliquer sur supprimer
				WebUI.click(bSupprimer)
				bLoop = false
			}catch(Exception ex){}
		}
	}


	/**
	 * Fonction qui permet de faire la gestion de la commande qui sera commandée selon la quantité disponible et selon le test qu'on veut accomplir, avec ou sans attente de matériel
	 * 
	 * @param quantiteDispo représente la quantité disponible en magasin
	 * @param materielDispo	représente le désir de commander une quantité supérieure à celle en magasin ou pas
	 * @return nouvelleQuantite représente la quantité qui sera commandée
	 */
	def choisirQuantiteACommande(int quantiteDispo, String materielDispo){
		int nouvelleQuantite = 0

		// Si on veut commander un quantité qui n'est pas disponible en magasin
		if (materielDispo == "NON"){
			// Si la quantite disponible est plus petites que 0, la commande commade sera 1 sinon on commande la quantité disponible plus 1
			if (quantiteDispo < 0){
				nouvelleQuantite = 1
			}else{
				nouvelleQuantite = quantiteDispo + 1
			}
			// Si on veut commander une quantité disponible en magasin
		}else if(materielDispo == "OUI"){
			// Si la quantité disponible est plus petite ou égale à 0, la quatité commande sera 0, sinon elle sera un nombre aléatoire entre 1 et la quantité disponible
			if (quantiteDispo <= 0){
				nouvelleQuantite = 0
			}else if (quantiteDispo < 4){
				nouvelleQuantite = Math.floor(Math.random() * ((quantiteDispo-1) - 1 + 1)) + 1

			}else{
				nouvelleQuantite = Math.floor(Math.random() * ((4-1) - 1 + 1)) + 1
			}
		}
		return nouvelleQuantite
	}


	/**
	 * Fonction qui permet de changer le statut de l'intrvention en le mettant à terminer
	 * 
	 */
	def terminerIntevention(){
		// Cliquer sur le bouton terminer intervention
		WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/B-TerminerIntervention'), 5)
		WebUI.click(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/B-TerminerIntervention'))
		// Cliquer sur le bonton OK
		WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK'), 5)
		WebUI.click(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK'))
		WebUI.delay(2)
	}


	/**
	 * Fonction qui parmet d'ajouter une ligne d'article dans le plan de travail, un article qui n'est pas palinifié dans la GO
	 * 
	 */
	def ajouterArticle(String magasin){
		// Aller à la section article de l'onglet plan de travail
		allerAArticle()
		// Cliquer sur le bouton nouvelle ligne
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-NouvelleLigne'), 5)
		WebUI.delay(1)
		WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-NouvelleLigne'))
		// Afficher le menu défilant pour l'article
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/NouvelleLigneArticle/B-MenuArticle'), 5)
		WebUI.delay(1)
		WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/NouvelleLigneArticle/B-MenuArticle'))
		// Appuyer sur le bouton aller à
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/NouvelleLigneArticle/B-AllerA'), 5)
		WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/NouvelleLigneArticle/B-AllerA'))
		// Appuyer sur le bouton gestion des stocks
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/NouvelleLigneArticle/B-GestionStocks'), 5)
		WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/NouvelleLigneArticle/B-GestionStocks'))

		try{
			// Accéder à la clauseWhere et rechercher les articles qui ont du stock disponibles
			gk.click('Object Repository/ElementsGeneriques/B-ZoneRecherchePlus',null)
			gk.click('Object Repository/ElementsGeneriques/B-ClauseWhere',null)
			gk.click('Object Repository/ElementsGeneriques/ClauseWhere/TA-ClauseWhere',null)
			WebUI.setText(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/TA-ClauseWhere'), "((itemnum in(select itemnum  from  invbalances where  invbalances.curbal > 0 and (location = '"+magasin+"') ))) and ((location = '"+magasin+"'))")
			WebUI.click(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/B-Rechercher'))
			WebUI.waitForElementNotPresent(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/B-Rechercher'), 5)
			WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/B-OKnotFound'), 1)
			// S'il n'y a aucun résultat un message apprait on appuie sur OK
			WebUI.click(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/B-OKnotFound'))
			gk.logError("il n'y a aucun article possédant une quantité disponible dans le magasin")

		}catch (Exception e){
			// Choisir une position aléatoire dans la première page des articles
			int pos = Math.floor(Math.random() * ((19-1) - 0 + 1)) + 0;
			// Accéder à l'article
			TestObject article = gk.convertWebElement("m6a7dfd2f_tdrow_[C:1]_ttxt-lb[R:"+pos+"]")
			WebUI.click(article)
			WebUI.delay(1)
			// Retourner avec la valeur de l'article
			WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/B-RetournerAvecValeur'), 5)
			gk.click('Object Repository/ElementsGeneriques/B-RetournerAvecValeur',null)
			//gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/NouvelleLigneArticle/B-AfficherDetails',null)
			WebUI.delay(1)
			// Cliquer sur sauvgarder pour enregistrer
			gk.click('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
			WebUI.waitForPageLoad(20)
		}
	}


	/**
	 * Fonction qui permet d'aller dans la section article de l'onglet plan de travail
	 * 
	 */
	def allerAArticle(){
		// Cliquer sur l'onglet Plan
		gk.waitForElementPresent('A-SuiviInterventions/P-Int/Tabs/Plans', 10, null)
		gk.click('A-SuiviInterventions/P-Int/Tabs/Plans', null)
		//Clique sur l'onglet Articles
		gk.waitForElementPresent('A-SuiviInterventions/P-Int/Plans Tab/Tab Articles', 10, null)
		gk.click('A-SuiviInterventions/P-Int/Plans Tab/Tab Articles', null)
	}


	/**
	 * Fonction qui permet de vérifier si le statut de l'intervention est au bon statut, si oui elle met la case de statut en vert sinon elle le met en rouge 
	 * 
	 * @param statutAttendu représente le statut attendu pour l'intervention 
	 */
	def verifierStatutIntervention(String statutAttendu){
		WebElement element
		// Aller à l'onglet intervention et enregistrer le statut de l'intervention
		gk.click('A-SuiviInterventions/P-Int/Tabs/Intervention', FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Statut'), 5)
		String statut = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Statut'), "value")
		element = driver.findElement(By.id('md3801d08-tb'))
		// si le statut de l'intrvention correspond au statut attendu on le met en vert sinon on le met en rouge
		if (statut == statutAttendu){
			gk.logPassed("Le statut de l'intervention est correct, il est à : " + statut)
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
			WebUI.delay(1)
		}else{
			gk.logError("Le statut de l'intervention est erroné, il est à : "+statut + ". Alors qu'il doit être à : "+statutAttendu )
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
			WebUI.delay(1)
		}
	}


	/**
	 * Fonction qui parcourt la liste des articles et vérifie est ce que tous les articles appartiennent à une tâche ou non et retourne le nombe d'articles
	 * qui ne sont pas associés à une tâche
	 * 
	 * @param nbArticles représente le nombre d'articles présents dans mon plan de travail
	 * @return articlesSansTâches représente le nombre d'articles qui ne sont pas associés à une tâche
	 */
	def verifierArticleTache(int nbArticles){
		int i = 0
		int articlesSansTâches=0
		boolean etat = true

		// Aller à l'onglet article dans plan de travail
		allerAArticle()
		WebUI.delay(1)
		while (WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Precedent'), 'ev') == 'true'){
			gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Precedent',null)
			WebUI.waitForPageLoad(10)
		}
		// Pour chaque article on vérifie si le champ pour la tâche est vide ou pas, s'il est vide cela veut dire qu'il n'est pas associé à une tâche
		while(i<nbArticles){
			TestObject champTache = gk.convertWebElement("m900f2d81_tdrow_[C:1]_txt-tb[R:"+i+"]")
			String tache = WebUI.getAttribute(champTache, "value", FailureHandling.STOP_ON_FAILURE)
			if (tache == "" ){
				articlesSansTâches++
			}
			i++
			pageSuivanteArticle(i)
		}
		// Retourner le nombre d'articles qui ne sont pas associé à une tâche
		return articlesSansTâches
	}


	/**
	 * Fonction qui valide le statut des tâches dans l'onglet valeurs réelles en vérifiant s'il est égal au statut attendu, 
	 * si c'est le cas on colorie le champ statut en vert sinon on le colorie en rouge
	 * 
	 * @param statutAttendu, le statut attendu pour les tâches
	 */
	def verifierStatutDesTaches(String statutAttendu){
		boolean loop = true
		int i = 0
		WebElement element
		ArrayList<String> listTaches = listDeTachesPourAtticles()

		// Aller à l'onglet Valeurs réelles
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Tabs/ValeursReelles',null)
		WebUI.delay(1)
		while (loop){
			try{
				boolean tacheValide = false
				// Enregistrer l'element pour le champ statut
				element = driver.findElement(By.id('mfae908c1_tdrow_[C:5]_txt-tb[R:'+i+']'))
				// Créer les objets pour les champs statut et numéro de tâche
				TestObject tStatut = gk.convertWebElement("mfae908c1_tdrow_[C:5]_txt-tb[R:"+i+"]")
				TestObject tNumTache = gk.convertWebElement("mfae908c1_tdrow_[C:2]_txt-tb[R:"+i+"]")
				// Enregistrer les valeurs pour le statut et le numéro de tâche
				String statut = WebUI.getAttribute(tStatut, "value", FailureHandling.STOP_ON_FAILURE)
				String numTache = WebUI.getAttribute(tNumTache , "value", FailureHandling.STOP_ON_FAILURE)
				//Verifier si la tâche possède un article qui lui est associé
				int j = 0
				while (j<listTaches.size()){
					if(numTache == listTaches[j]){
						tacheValide = true
					}
					j++
				}
				// Si le statut de la tâche est égal au statut attendu on met du vert sinon du rouge
				if (statut == statutAttendu && tacheValide == true){
					gk.logPassed("La tâche " + numTache +" possède le bon statut qui est :" + statutAttendu)
					WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))

				}else if (tacheValide == true){
					gk.logError("La tâche " + numTache +" ne possède pas le bon statut qui est : " + statutAttendu + ", son statut actuel est à : "+statut)
					WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
				}
				i++
			}catch (Exception e){
				loop = false
			}
		}
		WebUI.delay(1)
	}


	/**
	 * Validation des statuts pour l'intevention et les tâches en fonction de la présences d'articles qui ne sont pas associés à une tâche
	 * 
	 * @param arrayArticles liste d'articles présents dans le plan de travail
	 * @param Statut Le statut attendu 
	 */
	def validationDesStatuts (int nbArticles, String Statut){

		int nbArticlesSansTache = 0

		// S'il ya des articles on vérifie s'ils sont associés à une tâche ou pas
		if (nbArticles > 0)
			nbArticlesSansTache = verifierArticleTache(nbArticles)
		// S'il n'ya pas d'articles associés à une tâche on vérifie uniquement le statut de l'intervention
		if (nbArticlesSansTache == nbArticles){
			verifierStatutIntervention(Statut)
			// Si il ya au moins un article qui est associé à l'intervention on vérifie que le statut de l'intervention ainsi que le statut des tâches
		}else if (nbArticlesSansTache > 0){
			verifierStatutIntervention(Statut)
			verifierStatutDesTaches(Statut)
			// Si tous les aticles sont associés à une tâche on vérifie uniquement le statut des tâches
		} else if (nbArticlesSansTache == 0){
			verifierStatutDesTaches(Statut)
		}
	}


	/**
	 * Fonction qui permet de vérifier les nouvelle quantités disponibles avec les anciennes quantité disponibles pour les articles qui se trouvent dans plan de travail
	 * Si la quantite disponible a diminué on colorie en vert sinon on colorie en rouge
	 *  
	 * @param qteArticleDispo, liste contenant la liste des quantite disponible avant le changmenet de statut à prêt pour les articles dans plan de travail
	 * @param magasin, le magasin possedant l'article
	 */
	def verificationNouvelleQteDispo(ArrayList<Integer> qteArticleDispo, String magasin){
		// Aller à l'onglet article dans l'oglet plan de travail
		allerAArticle()
		int i = 0
		int nouvelleQte = 0
		int ancienneQte = 0
		int qteCommande = 0
		WebElement element

		while (i < qteArticleDispo.size()){
			TestObject textQuantite = gk.convertWebElement("m900f2d81_tdrow_[C:4]_txt-tb[R:"+i+"]")
			String [] lQteCommande = WebUI.getAttribute(textQuantite, 'value').split(",")
			qteCommande = Integer.parseInt(lQteCommande[0])
			ancienneQte = qteArticleDispo[i]
			ouvrirVoirDispoArt(magasin,i)
			nouvelleQte = quantiteDiso(magasin, i)
			// Si la quantite disponible a diminué on  met le champs quantite dispo en vert sinon on la met on rouge
			WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-QuantiteDispo'), 5)
			gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-QuantiteDispo',null)
			element = driver.findElement(By.id('mcc85e169_tdrow_[C:1]-c[R:0]'))

			if ((ancienneQte-qteCommande) == nouvelleQte){
				gk.logPassed("La quantité disponible dans le magasin : "+magasin+" a diminué suite au changement de statut")
				WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
			}else{
				gk.logError("La quantité disponible dans le magasin : "+magasin+" n'a pas diminué suite au changement de statut")
				WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
			}
			WebUI.delay(1)
			//Cliquer sur le bouton OK
			gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/B-OK',null)
			WebUI.waitForElementNotPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/B-OK'), 5)
			i++
			// Changer de page quand c'est nécessaire
			pageSuivanteArticle(i)
		}
	}


	/**
	 * Fonction qui permet d'afficher la fenêtre pour voir disponibilié de l'article qui se trouve dans le plan de travail, et entrer le magasin voulu
	 * 
	 * @param magasin, magasin ou est stocké l'article
	 * @param i, la position de l'article dans la liste de plan de travail
	 * 
	 */
	def ouvrirVoirDispoArt(String magasin, int i){
		boolean bLoop = true


		// Retourner à la première page des articles
		if (i==0){
			while (WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Precedent'), 'ev') == 'true'){
				gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Precedent',null)
				WebUI.waitForPageLoad(10)
			}
		}
		// Créer un test object pour le bouton menu detail de l'article disiré
		TestObject bMenuDetail = gk.convertWebElement("m900f2d81_tdrow_[C:2]_txt-img[R:" +i+ "]")
		WebUI.focus(bMenuDetail)
		while(bLoop) {
			try {
				WebUI.click(bMenuDetail)
				bLoop = false
			}catch(Exception ex){}
		}
		// Cliquer sur le bouton voir disponibilité de l'article
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-VoirDispoArtic'), 5)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-VoirDispoArtic',null)
		//Cliquer sur le bouton filtre
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/B-Filtre'), 5)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/B-Filtre',null)
		//Rechercher le magasin désiré
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-Recherche'), 5)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-Recherche',null)
		//gk.forceSetTextElement('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-Recherche', "="+magasin)
		WebUI.setText(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-Recherche'), "=" + magasin)
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/FenetreDispoArticle/T-Recherche'), Keys.chord(Keys.ENTER))
	}


	/**
	 * Fonction qui retourne la liste de tâches qui possèdent des articles qui leurs sont attachés
	 * 
	 * @return listTaches, représente la liste de tâches
	 */
	def listDeTachesPourAtticles(){
		boolean loop = true
		int i = 0
		ArrayList<String> listTaches = new ArrayList<String>()

		// Aller à la section article de l'onglet plan de travail
		allerAArticle()
		// Revenir à la première page des articles
		while (WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Precedent'), 'ev') == 'true'){
			gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Precedent',null)
			WebUI.waitForPageLoad(10)
		}
		while (loop){
			try{
				boolean ajouter = true
				int j = 0
				WebUI.delay(1)
				// Enregistrer la valeur du champ tâche
				TestObject textTache = gk.convertWebElement("m900f2d81_tdrow_[C:1]_txt-tb[R:"+i+"]")
				String tache = WebUI.getAttribute(textTache, "value", FailureHandling.STOP_ON_FAILURE)
				// Si l'article est rattaché à une tâche, et cette tâche n'a pas été rajoutée dans la liste, on ajoute le numéro de tâches dans notre liste
				if (tache != ""){
					if(listTaches.size()==0){
						listTaches.add(tache)
					}else{
						while(j<listTaches.size()){
							if(tache == listTaches[j]){
								ajouter = false
							}
							j++
						}
						if (ajouter == true){
							listTaches.add(tache)
						}
					}
				}
				i++
				// On change de page au besoin
				pageSuivanteArticle(i)

			}catch (Exception e){
				loop = false
			}
		}
		return listTaches
	}


	/**
	 * Appuyer sur le bouton page suivante si on a parcouru tous les articles de la page courante
	 * 
	 * @param i, position de l'article courant
	 */
	def pageSuivanteArticle(int i){
		// Changer de page quand c'est nécessaire
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Suivant'),5)
		WebUI.delay(1)
		if(i%6==0 && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/B-Suivant'), 'ev') == 'true') {
			boolean loop2 = true
			while (loop2){
				try{
					driver.findElement(By.id("m900f2d81-ti7_img")).click()
					loop2 = false
				}catch (Exception e){}
			}
			// Attendre que la page se recharge
			WebUI.waitForPageLoad(10)
			WebUI.delay(1)
		}
	}


	/**
	 * Fonction qui permet de modifier le type de reservation de l'article en le mettant à HARD
	 * @return
	 */
	def reservationHard(String type, int i){
		TestObject bAfficherDetails = gk.convertWebElement("m900f2d81_tdrow_[C:0]_tgdet-ti[R:"+i+"]_img")
		if(WebUI.getAttribute(bAfficherDetails, 'ev') == 'false')
			WebUI.click(bAfficherDetails,null)
		// Cliquer sur le bouton rechercher un type de reservation
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/TypeReservation/B-RechercheTypeReservation',null)
		// Entrer le type de reservation HARD
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/TypeReservation/T-TypeReservation',null)
		gk.forceSetTextElement('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/TypeReservation/T-TypeReservation', type)
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/TypeReservation/T-TypeReservation'), Keys.chord(Keys.ENTER))
		// Cliquer sur HARD
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Articles/TypeReservation/B-Resultat',null)

	}


	/**
	 * Fonction qui retourne une liste contenant des information utiles pour le rapport final, elle contient les pramètres suivants:
	 * 
	 * @param noIntervention : numéro d'intevention
	 * @param noActif : numéro de l'actif
	 * @param noGO : numéro de la gamme d'opération
	 * @param datePlanifie : la date planifiée
	 * @param listeInformation : liste retournée contenant toutes les informations
	 * @param noArticles : liste d'articles à commander pour l'intervention
	 * @return listeInformation : liste retournée contenant toutes les informations
	 */
	def listeInfoRapport(String noIntervention, noActif, String noGO, String datePlanifie,ArrayList<String> listeInformation, ArrayList<String> noArticles){
		int i = 1
		// Ajouter les différentes infromation dans la liste d'information
		listeInformation.add("***********************************************************")
		listeInformation.add(" 		Le numéro d'intervention : 	"+noIntervention)
		listeInformation.add("		Le numéro d'actif : 		"+noActif)
		listeInformation.add("		Le numéro de GO : 			"+noGO)
		listeInformation.add("		La date planifiée: 			"+datePlanifie)
		listeInformation.add("		Listes d'articles à commander pour cette intervention :")
		// Ajouter les différents articles de l'intervention dans la liste d'information
		while (i<=noArticles.size()){
			listeInformation.add("             		     article "+i+" : "+noArticles[i-1])
			i++
		}
		return listeInformation
	}


	/**
	 * Fonction qui permet d'imprimer la liste d'informations des interventions créées dans le rapport final
	 * 
	 * @param listeInformation : la liste contenant les informations des intervention créées durant le test
	 * @return
	 */
	def imprimerListeInfoRapport(ArrayList<String> listeInformation){
		int i = 0
		// Imprimer la liste d'informations
		while(i<listeInformation.size()){
			gk.logInfo(""+listeInformation[i])
			i++
		}
	}


	/**
	 * Fonction qui permet de changer l'atelier des tâches
	 * 
	 * @param atelier, le nouveau atelier
	 * 
	 * @return arrayNoActivite, liste contenant la liste des numéros d'activités pour les tâches
	 */
	def changerAtelierTaches(String atelier){
		boolean loop = true
		int i = 0
		String noActivite
		ArrayList<String> arrayNoActivite = new ArrayList<String>()
		TestObject bAfficherDetail
		// Revenir à la première page si nécessaire
		while (WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/B-PrevPage'), 'ev') == 'true'){
			gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/B-PrevPage',null)

		}
		while (loop){
			try{
				WebUI.delay(1)
				bAfficherDetail = gk.convertWebElement("mbb442a0c_tdrow_[C:0]_tgdet-ti[R:"+i+"]_img")
				WebUI.delay(2)
				//Ouvrir les details de la tâche
				WebUI.click(bAfficherDetail)
				gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/T-Atelier',null)
				gk.forceSetTextElement('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/T-Atelier', atelier)
				WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/T-Atelier'), Keys.chord(Keys.TAB))
				gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/T-NoActivite',null)
				//Enregistrer le numéro de l'activité
				noActivite = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/T-NoActivite'), 'value')
				arrayNoActivite.add(noActivite)
				WebUI.delay(1)
				// Fermer les details de la tâche
				WebUI.click(bAfficherDetail)
				i++

				// Changer de page si necessaire
				if(i%6 == 0 && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/B-Next Page'), 'ev') == 'true') {
					gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/B-Next Page')
					WebUI.delay(2)
				}
			}catch (Exception e){
				loop = false
			}
		}
		// Cliquer sur sauvgarder pour enregistrer la selection des Action correctives
		gk.click('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
		WebUI.waitForPageLoad(20)
		WebUI.delay(5)
		return arrayNoActivite
	}


	/**
	 * Fonction qui permet d'ouvrir une intervention
	 * 
	 * @param noIntervention, numéro de l'intervention à ouvrir
	 * 
	 * @return
	 */
	def ouvrirUneIntevention(String noIntervention){
		accederAIntervention()
		gk.click('Object Repository/A-SuiviInterventions/BarreRechercheIntervention/T-RechercheIntervention',null)
		//Entrer le numéro d'actif dans la barre de recherche et appuyer sur entrer
		gk.forceSetTextElement('Object Repository/A-SuiviInterventions/BarreRechercheIntervention/T-RechercheIntervention', "=" + noIntervention)
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/BarreRechercheIntervention/T-RechercheIntervention'), Keys.chord(Keys.ENTER))
		WebUI.waitForPageLoad(10)
		//Appuyer sur le premier resultat de la recherche
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/BarreRechercheIntervention/T-ResultatRecherche'), 5)
		gk.click('Object Repository/A-SuiviInterventions/BarreRechercheIntervention/T-ResultatRecherche',null)
	}


	/**
	 * Fonction qui permet d'aller dans la section main-d'oeuvre de l'onglet plan de travail
	 *
	 */
	def allerAMainOeuvre(){
		// Cliquer sur l'onglet Plan
		gk.click('A-SuiviInterventions/P-Int/Tabs/Plans', null)
		//Clique sur l'onglet main Oeuvre
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/B-MainOeuvre', null)
	}


	/**
	 * Fonction qui permet de récupérer la liste de taches
	 *  
	 * @return arrayNoTache, liste contenant les numéros des tâches
	 */
	def listNoTache(){
		allerAMainOeuvre()
		int i = 0
		int k = 0
		ArrayList<String> arrayNoTache = new ArrayList<String>()
		boolean loop = true
		String noTache
		while (loop){
			boolean ajouter = true
			try{
				// Enregistrer le numéro de tâche
				TestObject objectNoTache = gk.convertWebElement("m5e4b62f0_tdrow_[C:1]_txt-tb[R:"+i+"]")
				noTache = WebUI.getAttribute(objectNoTache , 'value')
				if (arrayNoTache.size() == 0){
					arrayNoTache.add(noTache)
				}else{
					// Si le numéro de tâche n'est pas déja dans la liste on l'ajoute
					while (k<arrayNoTache.size()){
						if (noTache == arrayNoTache.get(k)){
							ajouter == false
						}
						k++
					}
					if (ajouter == true){
						arrayNoTache.add(noTache)
					}
				}
				i++
				// Changer de page si nécessaire
				if(i%6 == 0 && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/B-NextPage'), 'ev') == 'true') {
					gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/B-Next Page')
					WebUI.delay(2)
				}
			}catch (Exception e){
				loop = false
			}
		}
		return arrayNoTache
	}


	/**
	 * Fonction qui permet de vérifier le statut des tâches qui sont reliées à une main d'oeuvre
	 * 
	 * @param statutAttendu, le statut attendu pour la tâche
	 * @param listeTachesMO, liste des tâche qui sont reliées à une main d'oeuvre
	 * @return
	 */
	def verifierStatutTacheMO(String statutAttendu, ArrayList<String> listeTachesMO){
		boolean loop = true
		int i = 0
		// Aller à l'onglet Valeurs réelles
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Tabs/ValeursReelles',null)
		WebUI.delay(1)

		while (loop){
			try {
				int k = 0
				TestObject oNoTache = gk.convertWebElement("mfae908c1_tdrow_[C:2]_txt-tb[R:"+i+"]")
				String noTache = WebUI.getAttribute(oNoTache, 'value')
				// Pour toutes les tâches dont le numéro est présent dans ma liste de tâches reliées à une main d'oeuvre, on vérifie si le statut est égal au statut attendu
				while (k<listeTachesMO.size()){
					if (noTache == listeTachesMO.get(k)){
						TestObject oStatutTache = gk.convertWebElement("mfae908c1_tdrow_[C:5]_txt-tb[R:"+i+"]")
						String statutTache = WebUI.getAttribute(oStatutTache, 'value')
						WebElement element = driver.findElement(By.id('mfae908c1_tdrow_[C:5]_txt-tb[R:'+i+']'))

						// Si le statut de la tâche est égal au statut attendu on met du vert sinon du rouge
						if (statutTache == statutAttendu){
							gk.logPassed("La tâche " + noTache +" possède le bon statut qui est :" + statutAttendu)
							WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
						}else if (statutTache != statutAttendu){
							gk.logError("La tâche " + noTache +" ne possède pas le bon statut qui est : " + statutAttendu + ", son statut actuel est à : "+statutTache)
							WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
						}
					}
					k++
				}
				i++
			}catch (Exception e){
				loop = false
			}
		}
		WebUI.delay(1)
	}


	/**
	 * Fonction qui permet d'ajouer une main d'oeuvre
	 * @return
	 */
	def ajouterMO(){
		// Accéder à main d'oeuvre
		allerAMainOeuvre()
		// Ajouter une nouvelle main d'oeuvre
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/B-NouvelleLigne',null)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/T-CorpsMetier',null)
		gk.forceSetTextElement('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/T-CorpsMetier', "PREPAEC")
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/T-CorpsMetier'), Keys.chord(Keys.TAB))
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/T-HeuresNormales',null)
		gk.forceSetTextElement('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/T-HeuresNormales', "1:00")
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/MainOeuvre/T-HeuresNormales'), Keys.chord(Keys.TAB))
	}


	/**
	 * Fonction qui permet de selectionner les actions correctives qui correspondent au paramètres suivants
	 * 
	 * @param P1, priorite 1
	 * @param P2, priorite 2
	 * @param P3, priorite 3
	 * @param SPr, sans priorite
	 * @param HU, actif hors usage
	 * @param listNoTache, liste contenant la postion des actions correctives à selectionner 
	 * 
	 * @return
	 */
	def selectionnerCertainesActionsCor(String P1, String P2, String P3, String SPr, String HU, ArrayList<Integer> listNoTache){
		int i = 0
		String [] listSrcTriTache
		String srcTriTache = ""
		String src = "asc.gif"

		// Ajouter les priorités désirées dans une liste
		ArrayList<String> listPriorite = new ArrayList<String>()
		listPriorite.add(P1)
		listPriorite.add(P2)
		listPriorite.add(P3)
		listPriorite.add(SPr)
		// Aller à actions correctives
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Tabs/ActionsCorrectives', null)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pagePrecedente',null)
		// Revenir à la première pas si nécessaire
		while (WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pagePrecedente'), 'ev') == 'true'){
			gk.click('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pagePrecedente',null)
			WebUI.waitForPageLoad(10)
		}
		// Clique sur le bouton enregistrer
		gk.click('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
		WebUI.delay(1)
		// Si on ne veut pas d'actions correctives on clique sur oui sinon sur non et on selectionner les actions voulues
		if(SPr != ""){
			gk.click('Object Repository/P-Accueil/Navbar/AllerA/Alert-Error/B-Alert-Oui',null)
		}else{
			gk.click('P-Accueil/Navbar/AllerA/Alert-Error/B-Alert-Non',null)
			// Trier les actions selon le numéro de l'action et s'assurer que c'est en ordre croissant
			gk.click('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/B-TriTache',null)
			WebUI.delay(1)
			listSrcTriTache = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/B-TriTacheImg'), 'src').split("_")
			srcTriTache = listSrcTriTache[listSrcTriTache.size()-1]

			while (srcTriTache != src){
				gk.click('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/B-TriTache',null)
				WebUI.delay(1)
				listSrcTriTache = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/B-TriTacheImg'), 'src').split("_")
				srcTriTache = listSrcTriTache[listSrcTriTache.size()-1]
			}
		}
		WebUI.delay(1)
		int j
		//Selectionner les actions qu'on veut, en récupérant la position de notre liste de position
		while(i < 3){
			if (listPriorite.get(i) != ""){
				boolean loop = true
				if (i==0){
					j = listNoTache.get(0)
				}else if (i==1){
					j = listNoTache.get(1)
				}else if (i==2){
					j = listNoTache.get(2)
				}
				gk.click('Object Repository/A-SuiviInterventions/P-Int/Tabs/ActionsCorrectives',null)

				// Si on est dans la 2e page et le checkBox a selectionné est dans la 1ere page, on retourne à la 1ere page
				if (j < 20 && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pagePrecedente'), 'ev') == 'true') {
					WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pagePrecedente'))
					WebUI.waitForPageLoad(10)

					// Si on est à la 1ere page et le checkBox a selectionné est dans la 2eme page, on va à la 2eme page
				}else if (j >= 20  && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pageSuivante'), 'ev') == 'true'){
					WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/O-ActionsCorrectives/pageSuivante'))
					WebUI.waitForPageLoad(10)
				}
				WebUI.delay(1)
				// On crée un testObject en convertissant les webElement
				TestObject actionCorrective = gk.convertWebElement("m4c505561_tdrow_[C:3]_checkbox-cb[R:" +j+ "]_img")
				gk.clickObject(actionCorrective,null)
				WebUI.delay(1)
			}
			i++
		}
		// Cliquer sur sauvgarder pour enregistrer la selection des Action correctives
		gk.click('Object Repository/ElementsGeneriques/BarreOutils/B-Enregistrer',FailureHandling.OPTIONAL)
		WebUI.waitForPageLoad(20)
		WebUI.delay(5)
	}


	/**
	 * Fonction qui retourne la position de la tâche qui correspont au la priorite et la fonctionnalitée désirée
	 * 
	 * 
	 * @return listPosTache, le numéro de la tâche désirée
	 */
	def retournerListPosTache(String HU){
		int i = 0
		int j = 0
		String priorite
		String noTache
		int pos
		ArrayList<Integer> listPosNoTache = new ArrayList<>()
		boolean horsUsage
		String src = ""
		String[] listSrcCheckBox
		String srcCheckBox = ""
		// On ouvre le plan de travail
		gk.click('A-SuiviInterventions/P-Int/Tabs/Plans', null)
		// Choisir 3 positions de tâches
		while (j < 3){
			horsUsage = false
			// Choisir une priorité différente pour chacune des tâches
			if (j == 0){
				priorite = "1"
			}else if (j == 1){
				priorite = "2"
			}else if (j == 2){
				priorite = "3"
			}
			// Dans le cas ou la priorite est 1 et que l'actif doit être hors usage
			if (HU == "OUI" && priorite == "1"){
				horsUsage = true
				src = "cb_checked.gif"
			}
			noTache = ""
			// Tant qu'on trouve pas une tâche qui à nos critères
			while (noTache == ""){
				// On crée un testObject en convertissant les webElement
				TestObject bAfficherDetails = gk.convertWebElement("mbb442a0c_tdrow_[C:0]_tgdet-ti[R:"+i+"]_img")
				gk.clickObject(bAfficherDetails,null)
				WebUI.delay(1)
				// Si la priorite de la tâche correspond à la priorite désirée, on accède à la tâche pour vérifier si l'actif sera mis hors usage ou non
				if (priorite == WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/DatailsTache/T-Priorite'), 'value')){
					gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/DatailsTache/B-Classification',null)
					gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/DatailsTache/B-AllerAClassification',null)
					gk.click('Object Repository/A-Classifications/T-Classification',null)
					listSrcCheckBox = WebUI.getAttribute(findTestObject('Object Repository/A-Classifications/C-ArretActif'), 'src').split("/")
					srcCheckBox = listSrcCheckBox[listSrcCheckBox.size()-1]

					// Si l'etat de l'actif apres avoir choisi cet tâche cet correspond à l'état desiré on enregistre la position de la tâche, sinon on clique sur retourner
					if ((srcCheckBox == src && horsUsage == true) || (srcCheckBox != src && horsUsage == false)){
						gk.click('Object Repository/ElementsGeneriques/B-Retourner',null)
						noTache = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/DatailsTache/T-NoTache'),'value')
						pos = i
					}else{
						gk.click('Object Repository/ElementsGeneriques/B-Retourner',null)
					}
				}
				gk.clickObject(bAfficherDetails,null)
				i++

				//Appuie sur la flèche afin de changer de page de main d'oeuvre
				if(i%6 == 0 && WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/B-Next Page'), 'ev') == 'true') {
					gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/B-Next Page',null)
					WebUI.delay(2)
				}
			}
			// Ajouter la position enregistrée à ma liste
			listPosNoTache.add(pos)
			j++
		}
		return listPosNoTache
	}


	/**
	 * Fonction qui permet de valider la priorité et la fonctionnalite de l'actif
	 * 
	 * @param prioriteAttendu
	 * @param noActif
	 * @return
	 */
	def validerPrioriteFonctionnaliteActif(String prioriteAttendu, String noActif, String fonctionnalite){
		WebElement element
		// Aller à l'actif
		gk.click('P-Accueil/Navbar/B-AllerA', null)
		gk.click('P-Accueil/Navbar/AllerA/Actifs/M-ActifsM', null)
		gk.click('P-Accueil/Navbar/AllerA/Actifs/M-Actifs', null)
		// Rechercher le numéro de l'actif
		gk.click('Object Repository/A-Actifs/T-RechercheNoActif',null)
		gk.forceSetTextElement('Object Repository/A-Actifs/T-RechercheNoActif', "="+noActif)
		WebUI.sendKeys(findTestObject('Object Repository/A-Actifs/T-RechercheNoActif'), Keys.chord(Keys.ENTER))
		gk.click('Object Repository/A-Actifs/B-ResultatRecherche',null)
		// Enregistrer la priorite de l'actif
		WebUI.waitForElementPresent(findTestObject('Object Repository/A-Actifs/T-Actifs/T-Proprietaire'), 5)
		String priorite = WebUI.getAttribute(findTestObject('Object Repository/A-Actifs/T-Actifs/T-Proprietaire'), 'value')
		element = driver.findElement(By.id('m62876596-tb'))
		// Si l'actif possède la bonne priorité
		if (priorite == prioriteAttendu){
			gk.logPassed("L'actif possède la bonne priorité qui est :"+priorite)
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
			WebUI.delay(1)
			// Si l'actif ne possède pas la bonne priorité
		}else{
			gk.logError("L'actif ne possède pas la bonne priorité qui est : " + prioriteAttendu + " sa priorité est : "+priorite)
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
			WebUI.delay(1)
		}
		// Valider la fonctionallite de l'actif
		validerFonctionnaliteActif(fonctionnalite)
	}


	/**
	 * Fonction qui permet de valider la fonctionnalité de l'actif 
	 * 
	 * @param fonctionnalite, est ce que l'actif est hors usage ou fonctionnel
	 * @return erreur, si il ya une erreur retourner true sinon false
	 */
	def validerFonctionnaliteActif(String fonctionnalite){
		boolean erreur = false
		WebElement element = driver.findElement(By.id('ma34ae16b-tb'))
		String etat = WebUI.getAttribute(findTestObject('Object Repository/A-Actifs/T-Actifs/T-EtatOperationnel'), 'value')
		// Si l'état de l'actif est correct
		if ((fonctionnalite == "HU" && etat == "HORS D'USAGE") || (fonctionnalite == "F" && etat != "HORS D'USAGE")){
			gk.logPassed("L'actif possède la bon etat de service")
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
			WebUI.delay(1)
			// Si l'état de l'actif est erronné
		}else{
			erreur = true
			gk.logError("L'actif ne possède pas le bon etat de service")
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
			WebUI.delay(1)
		}
		return erreur
	}


	/**
	 * Fonction qui permet de terminer une intervention
	 * 
	 * @param noIntervention, numéro de l'intervention à fermer
	 */
	def terminerIntervention(String noIntervention){
		// Aller à l'intervention
		accederAIntervention()
		// chercher le numéro de l'intervention
		gk.click('Object Repository/A-SuiviInterventions/T-Intervention',null)
		gk.forceSetTextElement('Object Repository/A-SuiviInterventions/T-Intervention', noIntervention)
		WebUI.sendKeys(findTestObject('Object Repository/A-SuiviInterventions/T-Intervention'), Keys.chord(Keys.ENTER))
		gk.click('Object Repository/A-SuiviInterventions/L-PremiereInt',null)
		// Cliquer sur terminer
		gk.click('Object Repository/ElementsGeneriques/BarreOutils/B-TerminerIntervention',null)
		// Cliquer sur le bouton Ok
		WebUI.waitForElementPresent(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK'), 5)
		WebUI.click(findTestObject('Object Repository/ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK'))
		WebUI.delay(3)
		if(WebUI.waitForElementPresent(findTestObject('Object Repository/MS-MessageSystem/B-Fermer'), 2)){
			gk.click('Object Repository/MS-MessageSystem/B-Fermer',null)
		}
	}


	/**
	 * Fonction qui permet de retourner la list des codes U des mains d'oeuvre affectés à l'intervention
	 * 
	 * @return listCodeU, liste des codeU
	 */
	def listAffectation(){
		boolean loop = true
		int i = 0
		String codeU = ""
		ArrayList<String> listCodeU = new ArrayList<String>()
		//Aller à affectation
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Tabs/Affectations',null)

		while (loop){
			try{
				//Création du test object pour champ contenant le codeU
				TestObject tCodeU = gk.convertWebElement("m6798a95d_tdrow_[C:6]_txt-tb[R:"+i+"]")
				WebUI.click(tCodeU)
				// Enregister le codeU et l'ajouter à la liste
				codeU = WebUI.getAttribute(tCodeU, 'value')
				listCodeU.add(codeU.toLowerCase())
				i++
			}catch (Exception e){
				loop = false
			}
		}
		return listCodeU
	}


}




