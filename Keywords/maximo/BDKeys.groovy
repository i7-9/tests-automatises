package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import javax.naming.Context
import javax.naming.InitialContext
import javax.swing.Box;
import javax.swing.JFrame
import javax.swing.JLabel;
import javax.swing.JOptionPane
import javax.swing.JPanel;
import javax.swing.JTextField
import oracle.jdbc.pool.OracleDataSource
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory as DF
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import internal.GlobalVariable
import java.sql.Connection
import java.sql.*
import java.sql.DriverManager
import java.sql.SQLException
import groovy.sql.*
import groovy.sql.Sql
import oracle.jdbc.*
import oracle.jdbc.pool.*
import groovy.json.JsonSlurper
import java.sql.Driver

public class BDKeys {


	private static KeywordLogger log = new KeywordLogger()
	private GeneralKeys gk = new GeneralKeys()

	private static String connectionString = ""
	private static props = new Properties();
	private static oracleDriver = Class.forName('oracle.jdbc.OracleDriver').newInstance() as Driver
	private static Connection conn
	//private Sql sql
	private static ArrayList<String> parametresDB = new ArrayList<String>()
	private static ArrayList<String> resultasDB  = new ArrayList<String>()
	def sql

	def static getConnexion() {
		if(conn == null|| conn.isClosed()){
			conn = oracleDriver.connect(connectionString, props)
		}
		return conn
	}


	/**
	 * Set up database connection
	 * https://docs.groovy-lang.org/latest/html/api/groovy/sql/Sql.html
	 * https://www.tutorialspoint.com/groovy/groovy_database.htm
	 * Other : https://mkyong.com/jdbc/connect-to-oracle-db-via-jdbc-driver-java/
	 */
	def configureConnexionDB(){
		try {

			parametresDB.add("host (*)")
			parametresDB.add("db (*)")
			parametresDB.add("user (*)")
			parametresDB.add("pswd (*)")
			parametresDB.add("noJournal(opt)")
			resultasDB = gk.multipleInputJPanel(parametresDB)
			props.setProperty("user", resultasDB[2].trim())
			props.setProperty("password", resultasDB[3].trim())
			connectionString = "jdbc:oracle:thin:@" + resultasDB[0].trim() +":1521/"+ resultasDB[1].trim()

			log.logPassed('SUCCES : Configuration de l accès à la base de données réussie : ' + connectionString )
		}catch(Exception e) {
			log.logError('ECHEC :  de configuration de l acces à la BD')
		}
	}



	/**
	 * Allow you to execute specific query in database
	 * @param query
	 * @return
	 */
	def executeQueryInDatabase(query) {
		sql = Sql.newInstance(getConnexion())
		def results = null

		try {
			results = sql.rows(query)
		} catch (Exception e) {
			log.logFailed('ECHEC : Excécution de la requête :  \n'+ query)
		}
		
		sql.clearStatementCache()
		sql.close()
		sql.closeResources(getConnexion())

		return results;
	}

	/**
	 * Permet de récupérer le no de journal saisi lors de la configuration de la BD
	 * @return
	 */
	def recupererNoJournal() {
		return resultasDB[4].trim()
	}

}
