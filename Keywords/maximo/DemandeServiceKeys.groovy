package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.Keys as Keys

public class DemandeServiceKeys {

	private GeneralKeys gk = new GeneralKeys()
	private KeywordLogger log = new KeywordLogger()

	/**
	 * Accéder a l'application Demande de service
	 */
	@Keyword
	def accederADemandeService(){
		boolean bloop = true

		while(bloop) {
			try {
				gk.clicBoutonAllerA()
				WebUI.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/Interventions/M-InterventionsM'))
				WebUI.delay(1)
				WebUI.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/Interventions/M-DemandeService'))
				WebUI.click(findTestObject('P-Accueil/Navbar/AllerA/Interventions/M-DemandeService'))
				bloop = false
			} catch(Exception e) {}
		}
	}

	/**
	 * Valide les elements de la page article
	 * @param description
	 */
	@Keyword
	def comparerDescription(String description){
		gk.valideInput('A-ReferentielArticles/P-ReferentielArticles/T-Description', description, false, "value")
	}

	/**
	 * Verifie la valeur d'adresse formatée
	 * @param adresse
	 */
	@Keyword
	def comparerAdresseFormatee(String adresse) {
		gk.valideInput('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Adresse Formatee', adresse, false, "value")
	}

	/**
	 * Fonction qui permet de saisir le numero GDC d'une demande de service
	 * @return
	 */
	@Keyword
	def remplirNoGdc(String noGdc) {
		if(!noGdc.isEmpty()){
			WebUI.delay(1)
			WebUI.click(findTestObject("A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-NumeroGDC"))
			gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-NumeroGDC', noGdc)
		}
	}

	/**
	 * Rempli les champs "Informations d'adresse"
	 * @param adresseFormatee
	 */
	@Keyword
	def remplirAdresse(String adresseFormatee, String arrondissement) {
		boolean bLoop = true

		while(bLoop) {
			try {
				WebUI.click(findTestObject("A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Adresse Formatee"))
				gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Adresse Formatee', adresseFormatee)

				WebUI.click(findTestObject("A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Arrondissement"))
				gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Informations adresse/T-Arrondissement', arrondissement)

				bLoop = false
			}catch(Exception ex){}
		}
	}

	/**
	 * Rempli les champs de la section "Informations utilisateur"
	 * @param demandePour
	 * @param telephonePour
	 * @param courrielPour
	 */
	@Keyword
	def remplirInfosUtilisateur(String nomPour, String telephonePour, String courrielPour, String type, String responsable) {
		if(!nomPour.isEmpty()){
			boolean bLoop = true
			WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-DemandePour'), 5)
			//Remplit le champ Demandé Pour

			gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-DemandePour', nomPour)
			//Remplit le numéro de téléphone du demandeur

			gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-Telephone DemandePour', telephonePour)
			//Remplit le courriel du demandeur

			gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-Courriel DemandePour', courrielPour)

			//Remplit le nom du responsable si le type de la DS est AMENT
			while(bLoop) {
				try {
					if(!responsable.isEmpty()) {
						WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/B-Responsable'))
						WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/L-ResponsableValeur'), 2)
						WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/L-ResponsableValeur'))
						WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-RechercheResponsable'), 2)
						gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-RechercheResponsable', responsable)
						WebUI.sendKeys(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/T-RechercheResponsable'), Keys.chord(Keys.ENTER))
						WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/L-Responsable'), 2)
						WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations utilisateur/L-Responsable'))
					}
					bLoop = false
				} catch(Exception ex) {}
			}

			WebUI.delay(2)
		}
	}

	/**
	 * Rempli les champs de la section "Détails de la demande de service
	 * @param desc - Description
	 * @param descDetaillee - Description détaillée
	 * @param type - Type de DS
	 * @param actif - Actif
	 * @param exec - Mode d'exécution
	 * @param atelier - Atelier responsable
	 */
	@Keyword
	def remplirDetailsDS(String desc, String descDetaillee, String type, String actif, String exec, String atelier, String classification, String emplacement, int currentRow) {
		String newDesc
		Boolean bTrue = true
		Boolean bLoop = true

		while(bLoop) {
			try {

				//Les DS n'ont une classification que si elles sont de type AMENT
				selectionnerClassification(type, classification)

				saisirDescription(desc, descDetaillee, currentRow)

				selectionnerTypeDs(type)

				//Si le Data file de la DS contient un emplacement, sélectionner l'emplacement
				selectionnerEmplacement(emplacement)

				//Si le Data file de la DS contient un actif, sélectionner l'actif
				selectionnerActif(actif)

				selectionnerModeExecution(exec)
				selectionnerAtelierResponsable(atelier)

				bLoop = false

			} catch(Exception ex) {
				//Si jamais un crash arrive, recommence le test
				//Si le crash arrive dans lors d'une rechercher, la fenêtre de recherche doit être fermée
				WebUI.click(findTestObject("A-DemandeService/P-DS/O-DemandesServices/Informations DS/B-Annuler"), FailureHandling.OPTIONAL)
				WebUI.click(findTestObject("A-DemandeService/P-DS/O-DemandesServices/Informations DS/B-AnnuleRechercheActif"), FailureHandling.OPTIONAL)
			}
		}
	}


	/**
	 * Fonction qui permet de saisir le mode d'execution
	 * @param modeExec
	 * @return
	 */
	def selectionnerModeExecution(String modeExec){
		WebUI.delay(1)
		gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Details/T-Execution', modeExec)
		WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Execution'))
	}

	def saisirDescription(String desc, String descDetaillee, int currentRow){

		gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Details/T-Description', '[KTLN : '+currentRow+' ] ' + desc + ' ' +new Date().format('MM/dd/yyyy hh:mm'))

		if(descDetaillee != "") {
			WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/TA-DescriptionDetaillee'))
			gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Details/TA-DescriptionDetaillee', descDetaillee)

			/**
			 * WebUI.switchToFrame(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/Iframe Description'), 0)
			 * 		gk.setTextElement('A-DemandeService/P-DS/O-DemandesServices/Details/TA-Description', descDetaillee)
			 WebUI.delay(2)
			 //Switch To Default Context est nécessaire afin de garder une valeur dans la description détaillée
			 WebUI.switchToDefaultContent()
			 */


		}
	}


	/**
	 * Fonction qui permet de saisir le type de DS
	 * @param modeExec
	 * @return
	 */
	def selectionnerTypeDs(String typeDs){
		WebUI.delay(1)
		gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Details/T-Type DS', typeDs)
	}

	/**
	 * Fonction qui permet de saisir le mode d'execution
	 * @param atelier
	 * @return
	 */
	def selectionnerAtelierResponsable(String atelier){
		WebUI.delay(1)
		gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Details/T-Atelier', atelier)
	}


	/**
	 * Fonction qui permet de sélectionner un actif et de le lier à une demande de service
	 * @param noActif
	 * @return
	 */
	def selectionnerActif(String noActif){
		if(!noActif.isEmpty()) {
			Boolean bTrue = true

			WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Actif'))
			WebUI.waitForElementPresent(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Details/L-RechercheActif'), 3)
			WebUI.click(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Int Tab/Details/L-RechercheActif'))
			//Inscrit le numéro de l'actif
			gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Details/T-Recherche', "=" + noActif)
			WebUI.sendKeys(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Recherche'), Keys.chord(Keys.ENTER))
			WebUI.delay(2)
			WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-ResultatRecherche'))
			//Attend que la fenêtre de recherche disparaîsse
			WebUI.waitForElementNotPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-ResultatRecherche'), 5)
			while(bTrue) {
				try {
					WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-DS'))
					bTrue = false
				} catch(Exception e) {

				}
			}
		}
	}

	/**
	 * Fonction qui permet de sélectionner un emplacement sur une demande de services
	 * @param emplacement
	 * @return
	 */
	def selectionnerEmplacement(String emplacement){
		if(!emplacement.isEmpty()) {
			WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/B-Emplacement'))
			WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-RechercheEmplacement'), 3)
			WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-RechercheEmplacement'))
			WebUI.setText(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Recherche'), "=" + emplacement)
			WebUI.delay(1)
			WebUI.sendKeys(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/T-Recherche'), Keys.chord(Keys.ENTER))
			WebUI.delay(2)
			WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-ResultatRecherche'))
			//Attend que la fenêtre de recherche disparaîsse
			WebUI.waitForElementNotPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Details/L-ResultatRecherche'), 5)
			WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-DS'))
		}
	}


	/**
	 * Fonction qui permet de saisir la classification de la demande de service
	 * @param type
	 * @param classification
	 * @return
	 */
	def selectionnerClassification(String type, String classification){
		if(type == "AMENT") {
			gk.forceSetTextElement('A-DemandeService/P-DS/O-DemandesServices/Details/T-Classification',
					"AQUEDUC_EGOUT \\ DEMANDE_SERVICE \\ AMENT \\ " + classification)
		}
	}


	@Keyword
	def verifierInfosDS(String type, String exec, String classification, String isDoublon, String isMaster) {
		boolean bLoop = true
		//Attend que la page s'affiche
		WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut'), 20, FailureHandling.OPTIONAL)




		while(bLoop) {
			try {

				// si le type est MC on verifie qu'une DS avec le statut PREP est crée
				if(type == "MC") {
					WebUI.delay(2)
					gk.valideInput('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut', 'PREP', false, "value")
					// si le type est AMENT on verifie qu'une DS avec le statut NOUVEAU ou ANALYSE est crée
				}else if(type == "AMENT") {
					if(classification == "ADOC" || classification == "SUP" || classification == "FORM") {
						WebUI.delay(2)
						//WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut'), 5)
						gk.valideInput('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut', 'NOUVEAU', false, "value")
					} else if(classification == "AME") {
						WebUI.delay(2)
						//WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut'), 5)
						gk.valideInput('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut', 'ANALYSE', false, "value")

					}
				}

				// Appuyer sur l'onglet ENREGISTREMENTS ASSOCIÉS et vérifier si une intervention est créee
				WebUI.click(findTestObject('A-DemandeService/P-DS/Tabs/Enregistrements Associes'), FailureHandling.OPTIONAL)
				WebUI.verifyElementNotPresent(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/Interventions vide'), 5)
				if(type != "AMENT") {
					WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'), 5)
					if(!WebUI.verifyElementPresent(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'), 3)) {
						gk.logError("Une intervention aurait dû être créée")
					}else{
						//WebUI.click(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'))
						//WebUI.click(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'))
						WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'), 2)
						WebUI.click(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'))
						WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/L-Intervention'), 1)
						WebUI.click(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/L-Intervention'))
						//Vérifie le statut de l'intervention'
						gk.valideInput('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Statut', 'ATTAPPRO', false, "value")
						//Vérifie le mode d'exécution et le type
						gk.valideInput('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Execution', exec, false, "value")
						gk.valideInput('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Type', type, false, "value")

						//Annule la DS créée si elle est doublon et non master
						if(isDoublon.toUpperCase() == "OUI" && isMaster.toUpperCase() == "NON") {

							WebUI.click(findTestObject("ElementsGeneriques/BarreOutils/B-ChangerStatut"))
							WebUI.waitForElementPresent(findTestObject("ElementsGeneriques/BarreOutils/AlertChangementStatut/T-NouveauStatut"), 5)
							WebUI.click(findTestObject("ElementsGeneriques/BarreOutils/AlertChangementStatut/T-NouveauStatut"))
							WebUI.click(findTestObject("ElementsGeneriques/BarreOutils/AlertChangementStatut/T-Annule"))
							while(WebUI.verifyElementPresent(findTestObject("ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK"), 3, FailureHandling.OPTIONAL)) {
								WebUI.click(findTestObject("ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK"), FailureHandling.OPTIONAL)
							}
						}
						WebUI.click(findTestObject('ElementsGeneriques/B-Retourner'))
					}
				}
				bLoop = false
			}catch(Exception ex) {}

		}
	}
	/**
	 while(bLoop) {
	 try {
	 if(type == "MC") {
	 gk.valideInput('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut', 'PREP', false, "value")
	 }else if(type == "AMENT") {
	 if(classification == "ADOC" || classification == "SUP" || classification == "FORM") {
	 gk.valideInput('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut', 'NOUVEAU', false, "value")
	 } else if(classification == "AME") {
	 gk.valideInput('A-DemandeService/P-DS/O-DemandesServices/Informations DS/T-Statut', 'ANALYSE', false, "value")
	 }
	 }
	 WebUI.click(findTestObject('A-DemandeService/P-DS/Tabs/Enregistrements Associes'), FailureHandling.OPTIONAL)
	 //Vérifie qu'une intervention a été créee
	 WebUI.verifyElementNotPresent(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/Interventions vide'), 5)
	 //Vérifie que l'intervention a bien été créée
	 if(type != "AMENT") {
	 WebUI.waitForElementPresent(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'), 20)
	 if(!WebUI.verifyElementPresent(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'), 3)) {
	 gk.logError("Une intervention aurait dû être créée")
	 } else {
	 WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'), 2)
	 WebUI.click(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/B-Intervention'))
	 WebUI.waitForElementClickable(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/L-Intervention'), 1)
	 WebUI.click(findTestObject('A-DemandeService/P-DS/O-EnregistrementsAssocies/L-Intervention'))
	 //Vérifie le statut de l'intervention'
	 gk.valideInput('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Statut', 'ATTAPPRO', false, "value")
	 //Vérifie le mode d'exécution et le type
	 gk.valideInput('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Execution', exec, false, "value")
	 gk.valideInput('A-SuiviInterventions/P-Int/Int Tab/Infos Int/T-Type', type, false, "value")
	 //Annule la DS créée si elle est doublon et non master
	 if(isDoublon.toUpperCase() == "OUI" && isMaster.toUpperCase() == "NON") {
	 //WebUI.click(findTestObject("ElementsGeneriques/BarreOutils/B-GetPropriete")) À ajouter?
	 WebUI.click(findTestObject("ElementsGeneriques/BarreOutils/B-ChangerStatut"))
	 WebUI.waitForElementPresent(findTestObject("ElementsGeneriques/BarreOutils/AlertChangementStatut/T-NouveauStatut"), 5)
	 WebUI.click(findTestObject("ElementsGeneriques/BarreOutils/AlertChangementStatut/T-NouveauStatut"))
	 WebUI.click(findTestObject("ElementsGeneriques/BarreOutils/AlertChangementStatut/T-Annule"))
	 while(WebUI.verifyElementPresent(findTestObject("ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK"), 3, FailureHandling.OPTIONAL)) {
	 WebUI.click(findTestObject("ElementsGeneriques/BarreOutils/AlertChangementStatut/B-OK"), FailureHandling.OPTIONAL)
	 }
	 }
	 WebUI.click(findTestObject('ElementsGeneriques/B-Retourner'))
	 }
	 }
	 bLoop = false
	 } catch(Exception ex) {}
	 }**/


	/**
	 * Crée un doublon pour la DS
	 * @param isMaster
	 * @param lastMaster
	 */
	@Keyword
	def remplirDoublonDS(String isMaster, String lastMaster) {
		WebUI.delay(1)
		if(isMaster.toUpperCase().equals("OUI")) {
			WebUI.check(findTestObject('A-DemandeService/P-DS/O-DemandesServices/ProblemesGeneraux/CB-IsTicketGlobal'))
		} else {
			WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/ProblemesGeneraux/B-TicketGlobal'))
			WebUI.waitForElementNotPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/ProblemesGeneraux/B-RechercherTicket'), 5)
			WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/ProblemesGeneraux/B-RechercherTicket'))

			//Recherche la DS master pour linker les deux DS ensemble
			gk.forceSetTextElement("A-DemandeService/P-DS/O-DemandesServices/ProblemesGeneraux/T-NumeroTicket", lastMaster)
			WebUI.sendKeys(findTestObject('A-DemandeService/P-DS/O-DemandesServices/ProblemesGeneraux/T-NumeroTicket'), Keys.chord(Keys.ENTER))
			WebUI.delay(1)
			WebUI.click(findTestObject('A-DemandeService/P-DS/O-DemandesServices/ProblemesGeneraux/L-PremierTicket'))
			WebUI.waitForElementNotPresent(findTestObject('A-DemandeService/P-DS/O-DemandesServices/ProblemesGeneraux/L-PremierTicket'), 5)
		}
	}
}