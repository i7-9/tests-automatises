package maximo
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.internal.JsonUtil as JsonUtil
import com.google.gson.Gson as Gson
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import com.kms.katalon.core.util.KeywordUtil
import java.util.regex.Pattern
import java.util.regex.Matcher

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.driver.DriverFactory
import java.util.ArrayList
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.webui.driver.DriverFactory as DF
import com.kms.katalon.core.logging.KeywordLogger
import javax.swing.Box;
import javax.swing.JFrame
import javax.swing.JLabel;
import javax.swing.JOptionPane
import javax.swing.JPanel;
import javax.swing.JTextField
import org.openqa.selenium.support.ui.WebDriverWait

public class ActifsKeys {
	private static GeneralKeys gk = new GeneralKeys()
	private static Geomatique gm = new Geomatique()
	private WebDriver driver = DriverFactory.getWebDriver()
	/**
	 * Acceder a l application Actifs
	 * @return 
	 */

	def accederAActifs() {
		if(GlobalVariable.site == 'DEV01') {
			gk.click('P-DEV-01-Accueil/Navbar/B-AllerA', null)
			gk.mouseOver('P-DEV-01-Accueil/Navbar/AllerA/Actifs/M-ActifsM', null)
			gk.mouseOver('P-DEV-01-Accueil/Navbar/AllerA/Actifs/M-Actifs', null)
			gk.click('P-DEV-01-Accueil/Navbar/AllerA/Actifs/M-Actifs', null)
		}
		else {
			gk.click('P-Accueil/Navbar/B-AllerA', null)
			gk.mouseOver('P-Accueil/Navbar/AllerA/Actifs/M-ActifsM', null)
			gk.mouseOver('P-Accueil/Navbar/AllerA/Actifs/M-Actifs', null)
			gk.click('P-Accueil/Navbar/AllerA/Actifs/M-Actifs', null)
		}
		WebUI.waitForPageLoad(10)
	}

	/**
	 *
	 * @param arrondissement: Cette variable recoit le responsable opérationnel du genre VDM-ARR
	 * @param type_actif: le type d'actif recoit le type d'actif pour pouvoir faire la distinction entre les différents emplacements
	 * @param vdm_actif_associer: Le VDM_actif_associer recoit le UUID de l'actif associé à l'actif à évaluer. Il peut recevoir un UUID ou rien
	 * @param fonction: cette variable recoit le type de fonction de l'actif. elle est utilisé seulement pour les vannes
	 * @param UUID: Elle recoit le UUID de l'actif
	 * @return un résultat boolean pour savoir si les données sont bonnes ou pas
	 */
	def Validation_emplacement(int territoire, String type_actif,String vdm_actif_associer,String fonction,String UUID) {
		boolean resultat_emplacement
		String emplacement
		String arrondissement
		//Modifier l'arrondissement pour prendre le territoire
		switch (territoire) {
			case 24:
				arrondissement="AC"
				break
			case 9:
				arrondissement="AJ"
				break
			case 27:
				arrondissement="CG"
				break
			case 6:
				arrondissement="BG"
				break
			case 17:
				arrondissement="LC"
				break
			case 18:
				arrondissement="LS"
				break
			case 23:
				arrondissement="MH"
				break
			case 16:
				arrondissement="MN"
				break
			case 5:
				arrondissement="OM"
				break
			case 13:
				arrondissement="FR"
				break
			case 22:
				arrondissement="PM"
				break
			case 19:
				arrondissement="PR"
				break
			case 25:
				arrondissement="RL"
				break
			case 15:
				arrondissement="LR"
				break
			case 21:
				arrondissement="SO"
				break
			case 14:
				arrondissement="LN"
				break
			case 12:
				arrondissement="VD"
				break
			case 20:
				arrondissement="VM"
				break
			case 26:
				arrondissement="VE"
				break}


		switch (type_actif){
			case 'AQU_RACCORD_P':
			//Tous les raccords d'aqueduc ont la même hiérarchie d'emplacement
			//if(vdm_actif_associer == '') {
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'RACAQ000' and LOCATION like ('"+arrondissement+"-AQU-RACC%')")

			//}
				break
			case 'AQU_ACCESSOIRE_P':
			//Accessoire d'aqueduc n'aurait pas de vdm actif associé
				if(vdm_actif_associer == '') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'ACCAQ000' and LOCATION like ('"+arrondissement+"-AQU-ACC%')")
				}
				//Accessoire de chambre d'aqueduc aura comme vdm actif associé la chambre correspondante
				else if(vdm_actif_associer != '') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'ACCAQ000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-ACC%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-CHAMBRE%')))")
				}

				break

			case 'AQU_VANNE_P':
				emplacement == '' + arrondissement + '-AQU-VANNE'
			//Vanne chambre doit avoir le UUID de la BI comme parent et comme fonction : ISOLEMENT DE BORNE INCENDIE
				if(vdm_actif_associer != '' && fonction=='BI') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'VAN000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-VANNE%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-BI%')))")
				}
				//Vanne chambre doit avoir le UUID de la chambre comme parent et comme fonction : INC, AR, CD, D, VDR, I, MP, RDP, RR, VA, V ou VRP
				else if(vdm_actif_associer != '' && (fonction=='INC' || fonction=='AR' || fonction=='CD' || fonction=='D'|| fonction=='VDR' || fonction=='I' || fonction=='MP' || fonction=='RDP' || fonction=='RR' || fonction=='VA' || fonction=='V' || fonction=='VRP')) {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'VAN000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-VANNE%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-CHAMBRE%')))")
				}
				//Vanne réseeau ne doit pas avoir de UUID actif associé et comme fonction : INC, AR, CD, D, VDR, I, MP, RDP, RR, VA, V ou VRP
				else if(vdm_actif_associer == '' && (fonction=='INC' || fonction=='AR' || fonction=='CD' || fonction=='D'|| fonction=='VDR' || fonction=='I' || fonction=='MP' || fonction=='RDP' || fonction=='RR' || fonction=='VA' || fonction=='V' || fonction=='VRP')) {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'VAN000' and LOCATION like ('"+emplacement+"%')")
				}
				//Vanne entrée de service doit avoir le UUID de la vanne d'entrée de service comme parent et comme fonction : SD, SDG, SF ou SG
				else if(vdm_actif_associer == '' && (fonction=='SD' || fonction=='SDG' || fonction=='SF' || fonction=='SG')) {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'VAN000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-VANNE%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-ES%')))")
				}
				if(resultat_emplacement == 'false') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'VAN000' and exists(select 1 from locations where location = asset.location and location like ('"+arrondissement+"-AQU-BIVI%'))")
				}

				break

			case 'AQU_CHAMBRE_S':
			//Toutes les chambres d'aqueduc ont la même hiérarchie d'emplacement
				if(vdm_actif_associer == '') {
					emplacement = arrondissement + '-AQU-CHAMBRE'
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'CHAQ000' and LOCATION like ('"+emplacement+"%')")

				}
				break

			case 'AQU_SEGMENT_L':
			//Segment de borne d'incendie aura un VDM_actif_associer
				if(vdm_actif_associer != '' && (fonction =="BD" || fonction=="BDG" || fonction=="BF" || fonction=="BG")) {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'REGAQ000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-VAN%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-ES%')))")
				}
				else if(vdm_actif_associer != '') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'SEGAQ000' and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and a2.location like ('"+arrondissement+"-AQU-BI%'))")
				}
				//Pour tout les autres types de segment d'aqueduc
				else {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'SEGAQ000' and exists(select 1 from locations where location = asset.location and location like ('"+arrondissement+"-AQU-SEG%'))")
				}
				if(resultat_emplacement == 'false') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'SEGAQ000' and exists(select 1 from locations where location = asset.location and location like ('"+arrondissement+"-AQU-BISEG%'))")
				}

				break

			case 'AQU_BORNEINCENDIE_P':
			//Pour toutes les bornes d'incendies on a la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'BI000' and exists(select 1 from locations where location = asset.location and location like ('"+arrondissement+"-AQU-BI%'))")

				break
			case 'AQU_REGARD_P':
			//Pour tout les regards d'aqueduc sont rattaché à une chambre
				if(vdm_actif_associer !='') {resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'REGAQ000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-REG%') and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-CHAMBRE%')))")
				}
				else {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'REGAQ000' and exists(select 1 from locations where location = asset.location and location like ('"+arrondissement+"-AQU-REG%'))")
				}

				break
			case 'EGO_BASSIN_P':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'BAS000' and LOCATION like ('"+arrondissement+"-EGO-BAS%')")

				break

			case 'EGO_RACCORD_P':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'RACEG000' and LOCATION like ('"+arrondissement+"-EGO-RACC%')")

				break

			case 'EGO_ACCESSOIRE_P':
			//Tous la même hiérarchie des emplacements
				if(vdm_actif_associer!='') {resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'ACCEG000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-EGO-CH%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-EGO-CH%')))")
				}
				else{resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'ACCEG000' and exists(select 1 from locations where location = asset.location and location like ('"+arrondissement+"-EGO-CH%'))")
				}

				break
			case 'EGO_SEGMENT_L':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'SEGEG000' and LOCATION like ('"+arrondissement+"-EGO-SEG%')")

				break

			case 'EGO_PUISARD_P':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'PUIEG000' and LOCATION like ('"+arrondissement+"-EGO-PUI%')")

				break

			case 'EGO_CHAMBRE_S':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'CHEG000' and LOCATION like ('"+arrondissement+"-EGO-CH%')")

				break

			case 'EGO_REGARD_P':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'REGEG000'  and LOCATION like ('"+arrondissement+"-EGO-CH%') and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and a2.location like ('"+arrondissement+"-EGO-CH%'))")
				if(resultat_emplacement == 'false') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'REGEG000' and exists(select 1 from locations where location = asset.location and location like ('"+arrondissement+"-EGO-REG%'))")
				}

				break
				break
		}
		if(resultat_emplacement == true) {
			return "true"
		}
		else {
			return "emplacement"
		}
	}

	/**
	 * 
	 * @param actifs_TP
	 * @param currentRow
	 * @return
	 */
	def valideActif(Map Actif_specss_TP, int currentRow) {
		gk.valideInput("A-Actifs/T-Actifs/T-Site", actifs_TP.getValue("SITEID", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-Statut", actifs_TP.getValue("STATUS", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-NoGeomatique", actifs_TP.getValue("VDM_INV_NO", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-NoGCAF", actifs_TP.getValue("VDM_INV_ID", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-Adresse", actifs_TP.getValue("VDM_ADRESSE", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-Juridiction", actifs_TP.getValue("VDM_JURIDICTION", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-Localisation", actifs_TP.getValue("VDM_LOCALISATION", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-ElevationTerrain", actifs_TP.getValue("VDM_ELEVATION_TERRAIN", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-TypeVoiePublic", actifs_TP.getValue("VDM_TYPE_VOIE_PUBLIC", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-TypeReseau", actifs_TP.getValue("VDM_TYPE_RESEAU", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-NomReservoir", actifs_TP.getValue("VDM_NOM_RESERVOIR", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-ResponsableOperationnel", actifs_TP.getValue("VDM_RESPONSABLE_OPERATIONNEL", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-Territoire", actifs_TP.getValue("VDM_TERRITOIRE", currentRow), false, "value")
		valideCheckBoxInput("A-Actifs/T-Actifs/C-IsRunning", actifs_TP.getValue("ISRUNNING", currentRow) == "EN SERVICE",
				actifs_TP.getValue("VDM_INV_NO", currentRow), "IsRunning")
		gk.valideInput("A-Actifs/T-Actifs/T-DateModification", actifs_TP.getValue("CHANGEDATE", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-DateInstallation", actifs_TP.getValue("INSTALLDATE", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-XCoord", actifs_TP.getValue("VDM_COORDONNEE_SPATIALE_X", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-YCoord", actifs_TP.getValue("VDM_COORDONNEE_SPATIALE_Y", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-DateAbandon", actifs_TP.getValue("VDM_DATE_ABANDON", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-DateFin", actifs_TP.getValue("VDM_DATE_FIN", currentRow), false, "value")
		gk.valideInput("A-Actifs/T-Actifs/T-EtatOperation", actifs_TP.getValue("VDM_ETAT_OPERATION", currentRow), false, "value")
	}


	/**
	 * 
	 * @param typeActif
	 * @return
	 */

	def valideModele(String typeActif) {
		gk.valideInput("A-Actifs/T-Actifs/T-Modele", getModelNumber(typeActif), false, "value")
	}


	def getModelNumber(String sTypeActif, TestData modeles_TP, String sGCAF) {

		for(int i=1; i<= modeles_TP.getRowNumbers(); i++) {
			println (sTypeActif  + " "+ sGCAF + " Compteur " + i)
			if(modeles_TP.getValue("Actifs", i).equalsIgnoreCase(sTypeActif)) {
				println modeles_TP.getValue("GCAF", i) + " == " + sGCAF
				if(modeles_TP.getValue("GCAF", i).equals(sGCAF)) {
					return modeles_TP.getValue("MAXIMO", i);
				}
			}
		}
		gk.logError("Modèle invalide!");
		return "MODELE " + sGCAF + " " + sTypeActif + "INVALIDE!"
	}


	/**
	 * 
	 * @param pathObject
	 * @param compare
	 * @param article
	 * @param champ
	 * @return
	 */

	def valideCheckBoxInput(String pathObject, Boolean compare, String article, String champ) {
		WebElement e =	DriverFactory.getWebDriver().findElement(By.id(findTestObject(pathObject).findProperty('id').getValue()))

		if(compare && WebUI.verifyElementChecked(findTestObject(pathObject), 0, FailureHandling.OPTIONAL)){
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(e))
		}else{
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(e))
			gk.logError("Erreur dans l'actif " + article + ": Le champ " + champ + " ne contient pas la valeur "+ compare)
		}
	}


	/**
	 * 
	 * @param nbSpecs
	 * @param allData
	 * @return
	 */

	def valideSpecs(int nbSpecs, List<List<String>> allData) {
		for(int i=0; i<nbSpecs; i++) {
			WebUI.delay(1)
			gk.setTextElement("A-Actifs/T-Specifications/T-Attribut", allData.get(i).get(4), null)
			gk.sendKeys("A-Actifs/T-Specifications/T-Attribut", Keys.chord(Keys.ENTER), null)
			WebUI.delay(2)

			if(allData.get(i).get(3) != "") {
				gk.valideInput("A-Actifs/T-Specifications/T-ValeurAlphaNum", allData.get(i).get(3), false, "value")
			} else if(allData.get(i).get(5) != "") {
				gk.valideInput("A-Actifs/T-Specifications/T-ValeurNum", allData.get(i).get(5), false, "value")
			} else if(allData.get(i).get(6) != "") {
				gk.valideInput("A-Actifs/T-Specifications/T-ValeurTable", allData.get(i).get(6), false, "value")
			}
		}
	}

	def valideinfoarrondissementvisuel() {
		//Validation visuel des infos d'arrondissement
	}

	/**
	 * Construit une requête SQL pour la recherche d'actif
	 * @param actifs_TP - Jeu de données des actifs
	 * @param allData - Tableau contenant les différents actifs (pour accélérer la recherche)
	 * @param currentRow - Row courante dans allData
	 * @param nbSpecs - Nombre de spécifications reliées au type d'actif
	 * @param sTypeActif - Type de l'actif que l'on essaie de trouver
	 */
	def construitRequete(Map Actif_specss_TP, List<List<String>> allData, int currentRow, int nbSpecs, String sTypeActif) {
		DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("M/d/yyyy")
		DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd")
		LocalDate parsedDate
		boolean bAnd = false
		String sRequete = "TEMPLATEID LIKE \\'%" + sTypeActif + "\\' AND " +
				"VDM_INV_NO LIKE \\'%" + actifs_TP.getValue("VDM_INV_NO", currentRow)+"\\' AND ASSETNUM IN (SELECT ASSETNUM FROM ASSETSPEC WHERE "

		//Parcours allData
		for(int i=nbSpecs*(currentRow-1); i<nbSpecs*(currentRow-1)+nbSpecs; i++) {
			if(allData.get(i).get(3) != "") {
				if(!bAnd) {
					bAnd = true
				} else {
					sRequete += " AND "
				}

				//Pour les diamètre d'orifice, le nom de colonne maximo diffère de celui de la BD
				if(allData.get(i).get(4) == "DIAMETRE_ORIFICE") {
					sRequete += " EXISTS(SELECT ASSETATTRID FROM ASSETSPEC WHERE ASSETATTRID LIKE \\'" + allData.get(i).get(4) + "_" + allData.get(i).get(7) + "\\'"
				} else {
					sRequete += " EXISTS(SELECT ASSETATTRID FROM ASSETSPEC WHERE ASSETATTRID LIKE \\'" + allData.get(i).get(4) + "\\'"
				}

				//Modifie le format de la date maximo pour qu'il corresponde à celui de la BD
				if(allData.get(i).get(4).substring(0, 4) == "DATE") {
					parsedDate = LocalDate.parse(allData.get(i).get(3), inputFormat)
					sRequete += " AND ALNVALUE LIKE \\'%" + outputFormat.format(parsedDate) + "\\') "
				} else {
					sRequete += " AND ALNVALUE LIKE \\'%" + allData.get(i).get(3).replace("'", "\\'\\'") + "\\') "
				}
			} else if(allData.get(i).get(5) != "") {
				if(!bAnd) {
					bAnd = true
				} else {
					sRequete += " AND "
				}

				sRequete += " EXISTS(SELECT ASSETATTRID FROM ASSETSPEC WHERE ASSETATTRID LIKE \\'" + allData.get(i).get(4) + "\\'"
				sRequete += " AND NUMVALUE LIKE \\'%" + allData.get(i).get(5).replace("'", "\\'") + "\\') "
			} else if(allData.get(i).get(6) != "") {
				if(!bAnd) {
					bAnd = true
				} else {
					sRequete += " AND "
				}

				sRequete += " EXISTS(SELECT ASSETATTRID FROM ASSETSPEC WHERE ASSETATTRID LIKE \\'" + allData.get(i).get(4) + "\\'"
				sRequete += " AND TABLEVALUE LIKE \\'%" + allData.get(i).get(6).replace("'", "\\'\\'") + "\\') "
			}
		}
		sRequete += ")"
	}


	/**
	 * Fonction qui permet gérer la liste des attributs et la liste des valeurs afin de fabriquer une requete pour les fiches d'actifs
	 * 
	 * @param bd, les données de l'actif
	 * @param noRow, la rangée dans la BD
	 * @param champIndesirable, les champs qu'on va pas utiliser daans la requête
	 * @param type, fiche actif ou specification
	 * @return requet, représente la requete
	 */
	def constuireRequeteFicheActif(TestData bd, int noRow, String [] champIndesirable){
		ArrayList<String> ArrayBD  = new ArrayList<String>()
		ArrayList<String> ArrayAttributs  = new ArrayList<String>()
		ArrayList<String> ArrayAttributsfinale  = new ArrayList<String>()
		ArrayList<String> ArrayMenu  = new ArrayList<String>()
		int i = 0
		int j = 0
		String requeteFiche = ""
		String valeur = ""

		// récupérer les valeurs de la bd sous format de list
		ArrayBD = bd.getAllData()
		while (j<ArrayBD.get(noRow).size()){
			boolean present = false
			int k = 0
			// si l'attributs ne fait pas parti des indésirables on l'ajoute à la liste des éléments de menu, et on récupère la valeur du champ
			while(k < champIndesirable.size()){
				if(ArrayBD.get(0).get(j)==champIndesirable[k]){
					present = true
				}
				k++
			}
			// si le champs n'est pas présent dans la liste des attributs indesirables on l'ajout a la liste de menu et sa valeur à la liste des valeurs
			if (present == false && ArrayBD.get(0).get(j) != ""){
				ArrayAttributs.add(ArrayBD.get(noRow).get(j))
				ArrayMenu.add(ArrayBD.get(0).get(j))
			}

			j++
		}

		// Gestion de l'ecriture de la requete pour les différents attributs
		while(i<ArrayAttributs.size()){
			valeur = ArrayAttributs[i]
			if (valeur == ""){
				valeur = " IS NULL"
			}
			valeur = gestionRequeteFiche(ArrayMenu.get(i), valeur)
			ArrayAttributsfinale.add(valeur)

			i++
		}

		// Contruire la requete
		requeteFiche = construireCW(ArrayMenu,ArrayAttributsfinale)

		return requeteFiche
	}


	/**
	 * Fonction qui permet de construire la requete pour la fiche des actifs
	 * @param arrayMenu, liste des attributs
	 * @param arrayAttributs, liste des valeurs des attributes
	 * @return requet, la requet finale pour la fiche actif
	 */
	def construireCW(ArrayList<String> arrayMenu, ArrayList<String> arrayAttributs){

		int i = 0
		String requet = ""
		while (i<arrayAttributs.size()){
			// si la requete ne contient rien au départ
			if (requet == ""){
				requet = arrayMenu[i]+" "+arrayAttributs[i]

				// Le cas ou il ya des éléments dans la requete
			}else {
				requet = requet + " and " + arrayMenu[i]+" "+arrayAttributs[i]
			}

			i++
		}

		return requet
	}


	/**
	 * Convertir la date en YYYY-MM-DD
	 * 
	 * @param date, date à convetir
	 * @return date, date convertie
	 */
	def convertirDate(String date){
		String [] listDate
		String annee
		String mois
		String jour

		try{
			// si le format est mm/dd/yyyy
			listDate = date.split("/")
			annee = listDate[2]
			mois = listDate[0]
			jour = listDate[1]
		}catch (Exception e){
			// si le format est mm-dd-yyyy
			listDate = date.split("-")
			annee = listDate[2]
			mois = listDate[0]
			jour = listDate[1]
		}
		// si le mois ou le jours est composé d'un seul chiffre on ajoute un zéro au début
		if (mois.length() == 1){
			mois = "0"+mois
		}
		if (jour.length()==1){
			jour = "0"+jour
		}
		String nouDate = ""+annee+"-"+mois+"-"+jour+""

		return nouDate
	}


	/**
	 * Fonction qui permet d'enlever une espace à la fin de la valeur s'il ya presence d'un
	 * 
	 * @param mot, représente la valeur dont l'espace sera enlevé
	 * 
	 * @return mot, la valeur sans esapace à la fin
	 */
	def enleverEspaceFin(String mot){
		String nMot
		nMot = mot.charAt(mot.length()-1)

		// tant que le dernier caractère est un espace on l'enlève
		while(nMot == " "){
			mot = mot.subSequence(0, mot.length()-1)
			nMot = mot.charAt(mot.length()-1)
		}

		return mot
	}


	/**
	 * Fonction qui permet de gerer les requete pour la fiche d'actif selon la valeur des différents attributs
	 * 
	 * @param valeurMenu, nom de l'attribut
	 * @param valeur, valeur de l'attribut
	 * @return valeur, la requert sql pour l'attribut en question
	 */
	def gestionRequeteFiche(String valeurMenu, String valeur){
		// mettre 1 si ISRUNNING est en service
		if(valeur == "EN SERVICE"){
			valeur = "=1"
		}else if (valeur == "HORS SERVICE"){
			valeur = "=0"
		}

		// mettre Like et enlever les "'" s'il sagit d'une adresse ou d'un nom de reservoir
		if ((valeurMenu == "VDM_ADRESSE" ||valeurMenu == "VDM_NOM_RESERVOIR") && valeur != " IS NULL"){
			valeur = valeur.replaceAll("'", "_")
			valeur = " LIKE '" +enleverEspaceFin(valeur) + "'"

			// gérer le cas des dates
		}else if ((valeurMenu=="CHANGEDATE"||valeurMenu=="INSTALLDATE") && valeur != " IS NULL"){
			valeur = "= to_date('" + convertirDate(valeur) +"', 'YYYY-MM-DD')"

			// enlever les virgules pour l'elevation de terrain
		}else if (valeurMenu=="VDM_ELEVATION_TERRAIN" && valeur != " IS NULL"){
			valeur = "= " + valeur.replaceAll(",", "")

			// mettre la valeur 0 pour les attributs PURCHASEPRICE et REPLACECOST
		}else if((valeurMenu == "PURCHASEPRICE" || valeurMenu == "REPLACECOST") && valeur == " IS NULL" ){
			valeur = "= 0"

			// mettre = valeur dans les autres cas
		}else if (valeurMenu!="VDM_ADRESSE" && valeurMenu !="INSTALLDATE" &&valeurMenu!="VDM_NOM_RESERVOIR" && valeur.charAt(0) != "=" && valeur != " IS NULL"){
			valeur = "='"+valeur+"'"
		}

		return valeur
	}


	/**
	 * Fonction qui permet de fabriquer une requete poour les specifications
	 *
	 * @param bd, les données de l'actif
	 * @param noRow, la rangée dans la BD
	 *
	 * @return requet, représente la requete
	 */
	def constuireRequeteSpecActif(TestData bd, int noRow){

		ArrayList<String> ArrayBD  = new ArrayList<String>()
		String valeur = ""
		String requet = ""
		// récupérer les valeurs de la bd sous format de liste
		ArrayBD = bd.getAllData()
		// convertir en format YYYY-MM-DD si c'est une date
		if((ArrayBD.get(noRow).get(4) == "DATE_CREEE" || ArrayBD.get(noRow).get(4) == "DATE_MODIFIEE") && ArrayBD.get(noRow).get(3) != ""){
			valeur = ArrayBD.get(0).get(3) + "='" + convertirDate(ArrayBD.get(noRow).get(3)) +"'"

			// enlever la virgule s'il s'agit du diamètre
		}else if((ArrayBD.get(noRow).get(4) == "DIAMETRE_MM" || ArrayBD.get(noRow).get(4) == "DIAMETRE_PO") && ArrayBD.get(noRow).get(6) != ""){
			String mesure = ArrayBD.get(noRow).get(6)
			valeur = ArrayBD.get(0).get(6) + " = " + mesure.replaceAll(",",".")

		}else if ((ArrayBD.get(noRow).get(4) == "PRECISION_DATE_INSTALL" || ArrayBD.get(noRow).get(4) == "TYPE_ACCESSOIRE_AQ"||ArrayBD.get(noRow).get(4) == "TYPE_CHAMBRE_EG") && ArrayBD.get(noRow).get(6) != ""){
			valeur = ArrayBD.get(noRow).get(6)
			valeur = ArrayBD.get(0).get(6) + " LIKE '" + valeur.replaceAll("'","_")+ "'"

		}else if((ArrayBD.get(noRow).get(4) == "DIAMETRE_ORIFICE") && ArrayBD.get(noRow).get(3) != "" && ArrayBD.get(noRow).get(3) != "NON APPLICABLE"&& ArrayBD.get(noRow).get(3) != "INCONNU"){
			String mesure = ArrayBD.get(noRow).get(3)
			valeur = ArrayBD.get(0).get(3) + " = " + mesure.replaceAll(",",".")

		}else if((ArrayBD.get(noRow).get(4) == "REMARQUE_GEOMATIQUE" || ArrayBD.get(noRow).get(4) == "TYPE_DRAIN") && ArrayBD.get(noRow).get(3) != ""){
			String mesure = ArrayBD.get(noRow).get(3)
			valeur = ArrayBD.get(0).get(3) + " LIKE '" + mesure.replaceAll("'","_")+ "'"

			// Gérer le type de valeur pour les autres attributs
		}else{
			if (ArrayBD.get(noRow).get(3)!=""){
				valeur = ArrayBD.get(0).get(3) + "= '" + ArrayBD.get(noRow).get(3)+"'"

			}else if(ArrayBD.get(noRow).get(5)!=""){

				valeur = ArrayBD.get(noRow).get(5)
				valeur = ArrayBD.get(0).get(5) + " = " + valeur.replaceAll(",",".")

			}else if(ArrayBD.get(noRow).get(6)!=""){
				valeur = ArrayBD.get(0).get(6) + "= '" + ArrayBD.get(noRow).get(6) +"'"

			}else if (ArrayBD.get(noRow).get(7)!=""){
				valeur = ArrayBD.get(0).get(7) + "= '" + ArrayBD.get(noRow).get(7) +"'"

			}
		}
		// si la valeur n'est pas null on construit la requete
		if (valeur!=""){
			requet += "and (ASSETATTRID = '" + ArrayBD.get(noRow).get(4) + "' and " + valeur +")"
		}

		return requet
	}


	/**
	 * Fonction qui permet d'imprimer la liste pour le rapport final
	 *
	 * @param listeInformation : la liste contenant les VDM_INV_NO des actifs
	 * @return
	 */
	def imprimerListeInfoRapport(ArrayList<String> listeInformation, String etat){
		int i = 0
		// Pour la liste d'actifs trouvés dans Maximo
		if (etat == "trouve"){
			gk.logInfo("Liste d'actifs retrouvés dans Maximo avec les bonnes valeurs : \n")
			// Pour la liste d'actifs non trouvés dans Maximo
		}else{
			gk.logInfo("Liste d'actifs non retrouvés dans Maximo ou contenant des erreurs : \n")
		}
		// Si la liste est vide
		if (listeInformation.size() == 0){
			gk.logInfo("                Aucun actifs")
			// Sinon imprimer la liste
		}else{
			while(i<listeInformation.size()){
				gk.logInfo("               "+listeInformation[i])
				i++
			}
		}
	}


	/**
	 * Fonction qui permet de choisir les paramètres pour construire les requetes en fonction des actifs et de rechercher l'actif
	 * 
	 * @param noGeo, numéro gééomatique de l'actif
	 * @param desc, la description de l'actif
	 * @param fonction, la fonction ou l'utilisation de l'actif
	 * 
	 * @return present, vrai si l'actif a été trouvé 
	 */
	def rechercherActif(String noGeo, String desc, String fonction, String emplActifAttendu){
		WebElement element
		String emplacement
		// Requete pour trouver l'actif
		String requete = "siteid = 'TP' and contains(description,'"+desc+"') > 0 and vdm_inv_no = '"+noGeo+"'"
		boolean present = false
		try{
			// Executer la requete
			gk.ouvrirClauseWhereExecuteRequete(requete)
			gk.click('Object Repository/A-Actifs/T-Actifs/Caracteristiques/T-Emplacement',null)
			emplacement = WebUI.getAttribute(findTestObject('Object Repository/A-Actifs/T-Actifs/Caracteristiques/T-Emplacement'), 'value')
			// Vérifier que l'actif possède le bon emplacement
			validationEmplActif(emplActifAttendu, emplacement, noGeo)
			gk.clicAfficheListe()
			WebUI.delay(1)

			// Constuire et executer les requetes pour vérifier l'hierarchie selon l'actif
			if (desc == "vanne"){
				present = construireRequeteHieraVanneAQU(fonction, emplacement, noGeo)
			}else if (desc == "segment d''aqueduc"){
				present = construireRequeteHieraSegmentAQU(fonction, emplacement, noGeo)
			}else if (desc == "accessoire d''aqueduc" || desc == "accessoire d''egout"){
				present = construireRequeteHieraAcc(fonction, emplacement, noGeo)
			}else if (desc == "raccord d''aqueduc"){
				present = construireRequeteHiera(emplacement, noGeo, "LC-AQU", "NON")
			}else if (desc == "borne d''incendie"){
				present = construireRequeteHiera(emplacement, noGeo, "LC-AQU-BI", "NON")
			}else if (desc == "chambre d''aqueduc"){
				present = construireRequeteHiera(emplacement, noGeo, "LC-AQU-VNCH", "NON")
			}else if (desc == "mm"){
				present = construireRequeteHiera(emplacement, noGeo, "LC-AQU-ES", "NON")
			}else if (desc == "regard d''aqueduc"){
				present = construireRequeteHiera(emplacement, noGeo, "LC-AQU-VNCH","OUI")
			}else if (desc == "bassin" || desc == "puisard" || desc == "raccord d''egout" || desc == "segment d''egout"){
				present = construireRequeteHiera(emplacement, noGeo, "LC-EGO","NON")
			}else if(desc == "chambre d''egout"){
				present = construireRequeteHiera(emplacement, noGeo, "LC-EGO-CH", "NON")
			}else if(desc == "regard d''egout"){
				present = construireRequeteHiera(emplacement, noGeo, "LC-EGO-CH", "OUI")
			}

			// Si l'article a été trouvé avec la bonne hierarchie on colorie le champs en vert
			if (present){
				gk.click('Object Repository/A-Actifs/T-Actifs/T-NoActif',null)
				element = driver.findElement(By.id('m4e28cd23-tb'))
				gk.logPassed("L'actif avec le numéro géomatique "+noGeo+" contient le bon emplacement parent")
				WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
				WebUI.delay(1)

				// Sinon on le colorie en rouge
			}else{
				gk.click('Object Repository/A-Actifs/T-RechercheActifNoGeo' ,null)
				element = driver.findElement(By.id('m6a7dfd2f_tfrow_[C:1]_txt-tb'))
				gk.logError("Erreur avec l'emplacement de l'actif avec le numéro géomatique "+noGeo+" il n'a pas le bon emplacement parent")
				WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
			}

		}catch (Exception e){}

		return present
	}


	/**
	 * Fonction pour valider l'emplacement de l'article, on met de la couleur valeur si c'est validé sinon on met de la couleur rouge
	 * 
	 * @param emplAttendu, emplacement attendu pour l'actif
	 * @param emplacement, emplacement de l'actif dans Maximo
	 * @param noGeo, numéro géomatique de l'actif
	 * 
	 * @return
	 */
	def validationEmplActif(String emplAttendu, String emplacement, String noGeo){
		WebElement element = driver.findElement(By.id('me8892a39-tb'))
		emplacement = emplacement.substring(0,emplAttendu.size())

		// Si l'actif possède le bon emplacement on colorie en vert
		if (emplacement == emplAttendu){
			gk.click('Object Repository/A-Actifs/T-Actifs/T-NoActif',null)
			gk.logPassed("L'actif avec le numéro géomatique "+noGeo+" contient le bon emplacement")
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(element))
			WebUI.delay(1)

			// Sinon on colorie en rouge
		}else{
			gk.click('Object Repository/A-Actifs/T-Actifs/T-NoActif',null)
			gk.logError("Erreur avec l'emplacement de l'actif avec le numéro géomatique "+noGeo+" il n'a pas le bon emplacement")
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(element))
			WebUI.delay(1)

		}
	}


	/**
	 * Fonction qui construit et execute la requete pour l'hierarchie de la vanne
	 * 
	 * @param fonction, fonction de la vanne
	 * @param emplacement, emplacement de la vanne
	 * @param noGeo, numéro géomatique de la vanne
	 * 
	 * @return present, vrai si l'actif a été trouvé 
	 */
	def construireRequeteHieraVanneAQU(String fonction, String emplacement,String noGeo){
		String emplParent=""
		boolean present = false
		try{
			String requete = "siteid = 'TP' and VDM_ACTIF_ASSOCIE is not null and vdm_inv_no = '" + noGeo +"' and location = '"+emplacement+"' and location in (select location from locations where location in(select location from LOCHIERARCHY where LOCHIERARCHY.PARENT like '"
			// si c'est une vanne d'isolement de borne
			if (fonction == "ISOLEMENT DE BORNE INCENDIE"){
				emplParent = "%LC-AQU-BI%"
				String requete3 = requete + emplParent+"'))"
				present = gk.ouvrirClauseWhereExecuteRequete(requete3 )
				// si c'est une vanne de chambre
			} else {
				// si la vanne est assocé à un actif
				emplParent = "%LC-AQU-CHAMBRE%"
				String requete1 = requete + emplParent +"'))"
				present = gk.ouvrirClauseWhereExecuteRequete(requete1)
				// si la vanne n'est pas assocé à un actif
				if(present == false){
					String requeteSansActifAss = "siteid = 'TP' and VDM_ACTIF_ASSOCIE is null and vdm_inv_no = '" + noGeo +"' and location = '"+emplacement+"' and location in (select location from locations where location in(select location from LOCHIERARCHY where LOCHIERARCHY.PARENT like '"
					emplParent = "LC-AQU-VNCH"
					String requete2 = requeteSansActifAss + emplParent+"'))"
					present = gk.ouvrirClauseWhereExecuteRequete(requete2)
				}
			}

		}catch (Exception e){}

		return present
	}


	/**
	 * Fonction qui construit et execute la requete pour l'hierarchie du segment
	 * 
	 * @param fonction, fonction du segment
	 * @param emplacement, emplacement du segment 
	 * @param noGeo, numéro géomatique du segment
	 * 
	 * @return present, vrai si l'actif a été trouvé 
	 */
	def construireRequeteHieraSegmentAQU(String fonction, String emplacement, String noGeo){
		String emplParent=""
		boolean present = false
		try{
			String requete = "siteid = 'TP' and VDM_ACTIF_ASSOCIE is not null and vdm_inv_no = '" + noGeo +"' and location = '"+emplacement+"' and location in (select location from locations where location in(select location from LOCHIERARCHY where LOCHIERARCHY.PARENT like '"
			// Segments borne d'incendie
			if (fonction == "BRANCHEMENT BORNE INCENDIE"){
				emplParent = "%LC-AQU-BI%"
				String requete1 = requete + emplParent +"'))"
				present = gk.ouvrirClauseWhereExecuteRequete(requete1)

				// Segments entrée de service
			}else if (fonction == "BRANCHEMENT SERVICE EAU" || fonction == "BRANCHEMENT SERVICE EAU ET GICLEUR" || fonction == "BRANCHEMENT FEU" || fonction == "BRANCHEMENT GICLEUR"){
				emplParent = "%LC-AQU-ES%"
				String requete2 = requete + emplParent+"'))"
				present = gk.ouvrirClauseWhereExecuteRequete(requete2)

				// Autres segments
			}else{
				String requeteSansActifAss = "siteid = 'TP' and VDM_ACTIF_ASSOCIE is null and vdm_inv_no = '" + noGeo +"' and location = '"+emplacement+"' and location in (select location from locations where location in(select location from LOCHIERARCHY where LOCHIERARCHY.PARENT like '"
				emplParent = "LC-AQU"
				String requete3 = requeteSansActifAss + emplParent+"'))"
				present = gk.ouvrirClauseWhereExecuteRequete(requete3)
			}

		}catch (Exception e){}

		return present
	}


	/**
	 * Fonction qui construit et execute la requete pour l'hierarchie de l'accessoire
	 *  
	 * @param noGeoChambre, numéro géomatique de la chambre associé à l'accessoire
	 * @param emplacement, emplacement de l'accessoire
	 * @param noGeo, numéro géomatique de l'accessoire
	 * 
	 * @return present, vrai si l'actif a été trouvé  
	 */
	def construireRequeteHieraAcc(String noGeoChambre, String emplacement, String noGeo){
		// requete avec un actif associé
		String requete = "siteid = 'TP' and VDM_ACTIF_ASSOCIE is not null and vdm_inv_no = '" + noGeo +"' and location = '"+emplacement+"' and location in (select location from locations where location in(select location from LOCHIERARCHY where LOCHIERARCHY.PARENT like '"
		// requete sans actif associé
		String requeteSansActifAss = "siteid = 'TP' and VDM_ACTIF_ASSOCIE is null and vdm_inv_no = '" + noGeo +"' and location = '"+emplacement+"' and location in (select location from locations where location in(select location from LOCHIERARCHY where LOCHIERARCHY.PARENT like '"
		String emplParent=""
		boolean present = false
		// Si l'accessoire n'est pas lié à une chambre
		if (noGeoChambre != ""){
			// Si c'est un accessoire d'égout
			if(emplacement.substring(0, 6)=="LC-EGO"){
				emplParent = "%LC-EGO-CHAMBRE%"
				// Si c'est un accessoire d'aqueduc
			}else{
				emplParent = "%LC-AQU-CHAMBRE%"
			}
			// contruire et executer la requete
			String requete1 = requete + emplParent+"'))"
			present = gk.ouvrirClauseWhereExecuteRequete(requete1)

			// Si l'accessoire est lié à une chambre
		}else{
			// Si c'est un accessoire d'égout
			if(emplacement.substring(0, 6)=="LC-EGO"){
				emplParent = "%LC-EGO-CH%"
				// Si c'est un accessoire d'aqueduc
			}else{
				emplParent = "%LC-AQU-VNCH%"
			}
			// contruire et executer la requete
			String requete2 = requeteSansActifAss + emplParent+"'))"
			present = gk.ouvrirClauseWhereExecuteRequete(requete2)
		}

		return present
	}


	/**
	 * Fonction qui permet de construire et d'executer une requete pour vérifier l'hierarchie de l'actif autre que les vannes, les segements et les accessoires
	 * 
	 * @param emplacement, emplacement de l'actif
	 * @param noGeo, numéro géomatique de l'actif 
	 * @param emplParent, emplacement parent de l'actif
	 * @param actifAss, est ce que l'actif est associé à un autre actif
	 * 
	 * @return present, vrai si l'actif a été trouvé 
	 */
	def construireRequeteHiera( String emplacement, String noGeo, String emplParent, String actifAss){
		String requete
		boolean present = false
		// Si associé à un autre actif
		if(actifAss == "OUI"){
			requete = "siteid = 'TP' and VDM_ACTIF_ASSOCIE is not null and vdm_inv_no = '" + noGeo +"' and location = '"+emplacement+"' and location in (select location from locations where location in(select location from LOCHIERARCHY where LOCHIERARCHY.PARENT like '"

			// Si n'est pas associé à un autre actif
		}else{
			requete = "siteid = 'TP' and VDM_ACTIF_ASSOCIE is null and vdm_inv_no = '" + noGeo +"' and location = '"+emplacement+"' and location in (select location from locations where location in(select location from LOCHIERARCHY where LOCHIERARCHY.PARENT like '%"

		}
		// Construire et executer la requete
		String requete1 = requete + emplParent+"%'))"
		present = gk.ouvrirClauseWhereExecuteRequete(requete1)

		return present
	}


	/** Valide les spécifications de l'actif de facon visuel dans le tableau des spécifications
	 * @param Actif, donne toutes les informations sur les actifs en format json
	 * @param currentRow, désigne l'actif à traiter
	 * @param Specsgeo
	 * @param Specsmaximo, ce sont le nom et le type de spécifications dans le tableau exemple:DATE_MODIFIEE de type DATE
	 * @return résultat booléan qui renvoi true si l'actif est pareil dans maximo et dans le journal
	 */
	def validespecsvisuel(Map Actif_specs,int currentRow,String [] Specsgeo,String [][] Specsmaximo) {
		String numvalue
		gk.click('A-Actifs/DEV-01/Section_specifications/a_Specifications', null)
		//aller aux specs

		gk.click('A-Actifs/DEV-01/Section_specifications/filtre attribut', null)
		int succes = 0
		boolean resultat_validation = true
		String spec_introuver=''

		for(int i=0; i<=Specsgeo.size()-1; i++) {
			if(Specsgeo[i] !=null) {
				Specsgeo[i]=Specsgeo[i].replaceAll(Pattern.quote("//"), Matcher.quoteReplacement("\\"))
			}
			WebUI.delay(1)
			gk.setTextElement("A-Actifs/T-Specifications/T-Attribut",Specsmaximo[i][0] , null)
			WebUI.sendKeys(findTestObject("A-Actifs/T-Specifications/T-Attribut"), Keys.chord(Keys.ENTER))
			WebUI.delay(1)

			if (Specsmaximo[i][1]=="DATE" && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {
				resultat_validation = gk.valideInput("A-Actifs/T-Specifications/T-ValeurAlphaNum", Specsgeo[i], false, "value")

			}
			else if (Specsmaximo[i][1]=="ALNNUM" && Specsgeo[i] != null &&  Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {
				String numvalue2 = Specsgeo[i].replace(".", "T")
				numvalue = numvalue2.split('T')[0]
				resultat_validation = gk.valideInput("A-Actifs/T-Specifications/T-ValeurAlphaNum", numvalue, false, "value")
			}
			else if (Specsgeo[i]=="-1.000"||Specsgeo[i]=="-999.000")  {
				numvalue = Specsgeo[i].replace(".000", ",0")
				resultat_validation = gk.valideInput("A-Actifs/T-Specifications/T-ValeurNum", numvalue, false, "value")
			}

			else if (Specsmaximo[i][0]=="ALTITUDE" && Specsgeo[i] != null && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA" && Specsgeo[i]!=-999.000 && Specsgeo[i]!=-1.000) {
				if(Specsgeo[i]=="0.000") {numvalue = "0,0"}
				else{numvalue = Specsgeo[i].replace(".", ",")}
				resultat_validation = gk.valideInput("A-Actifs/T-Specifications/T-ValeurNum", numvalue, false, "value")
			}
			else if (Specsmaximo[i][1]=="NUM" && Specsgeo[i] != null && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {

				numvalue = gk.Valeur_numerique(Specsgeo[i])
				resultat_validation = gk.valideInput("A-Actifs/T-Specifications/T-ValeurNum", numvalue, false, "value")
			}
			else if (Specsgeo[i]==null)  {
				String attribute = WebUI.getAttribute(findTestObject('A-Actifs/T-Specifications/T-ValeurAlphaNum'), 'value')//gk.valideInput("A-Actifs/T-Specifications/T-ValeurAlphaNum".isEmpty(),Specsgeo[i], false, "value")
				WebUI.verifyMatch(attribute, "", false)
				if(WebUI.verifyMatch(attribute, "", false)==true) {
					WebElement f
					f =	DF.getWebDriver().findElement(By.id('mc8d54871_tdrow_[C:5]_txt-tb[R:0]'))
					WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(f))
					WebUI.comment(Specsmaximo[i][0]+" contient la bonne valeure qui est : null")
					resultat_validation = true
				}
				else {resultat_validation = false}
			}
			else  {
				resultat_validation =gk.valideInput("A-Actifs/T-Specifications/T-ValeurAlphaNum", Specsgeo[i], false, "value")
			}
			if (resultat_validation==true) {succes++}
			else {spec_introuver += Specsmaximo[i][0]+" et "}
		}
		if(succes==Specsgeo.size()) {
			return true
		}
		else {
			return false
		}
	}
	/**
	 * 
	 * @param Actif_specs
	 * @param currentRow
	 * @return
	 */
	def valideinfoarrondissementvisuel(Map Actif_specs,int currentRow,String Type_Actif) {
		String[] nb_coordonne
		String [] Specsgeo = [
			Actif_specs[0].features[0].properties.DATEINSTALL,
			Actif_specs[0].features[0].geometry.coordinates[0],
			Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,
			Actif_specs[0].features[0].properties.NUMEROSECTEUR1,
			Actif_specs[0].features[0].geometry.coordinates[1],
			Actif_specs[0].features[0].properties.ELEVATION,
			Actif_specs[0].features[0].properties.CLASSIFICATION_RESEAU_R,
			Actif_specs[0].features[0].properties.RESPONSABLE_OPERATIONNEL_R,
			Actif_specs[0].features[0].properties.TERRITOIRE_R,
			Actif_specs[0].features[0].properties.PROPRIETAIRE_R,
			Actif_specs[0].features[0].properties.JURIDICTION_R,
			Actif_specs[0].features[0].properties.RESERVOIR_R,
			Actif_specs[0].features[0].properties.STATUT_POSITION_R,
			Actif_specs[0].features[0].properties.STATUTGEOMETRIQUE_R,
			Actif_specs[0].features[0].properties.DATEABANDON,
			Actif_specs[0].features[0].properties.HIERARCHIE_TRONCON_R
		]

		String [][] Specsmaximo = [
			["INSTALLDATE", "DATE"],
			[
				"VDM_COORDONNEE_SPATIALE_X",
				"NUM"
			],
			["VDM_ACTIF_ASSOCIE", "ALN"],
			[
				"VDM_NO_SECTEUR_REGULATION",
				"ALN"
			],
			[
				"VDM_COORDONNEE_SPATIALE_Y",
				"NUM"
			],
			[
				"VDM_ELEVATION_TERRAIN",
				"NUM"
			],
			[
				"VDM_CLASSIFICATION_RESEAU",
				"ALN"
			],
			[
				"VDM_RESPONSABLE_OPERATIONNEL",
				"ALN"
			],
			["VDM_TERRITOIRE", "ALNNUM"],
			["VDM_PROPRIETAIRE", "ALN"],
			["VDM_JURIDICTION", "ALN"],
			["VDM_NOM_RESERVOIR", "ALN"],
			[
				"VDM_STATUT_POSITION_ACTIF",
				"ALN"
			],
			[
				"VDM_STATUT_DESSIN_NUMERISATION",
				"ALN"
			],
			["VDM_DATE_ABANDON", "DATE"],
			[
				"VDM_HIERARCHIE_TRONCON",
				"ALN"
			]
		]
		String [] emplacement = [
			"A-Actifs/DEV-01/Info_arrondissement/Date dinstallation",
			"A-Actifs/DEV-01/Info_arrondissement/Coordonne spatiale X",
			"A-Actifs/DEV-01/Info_arrondissement/Actif associe",
			"A-Actifs/DEV-01/Info_arrondissement/Numero_secteur_regulation",
			"A-Actifs/DEV-01/Info_arrondissement/Coordonne spatiale Y",
			"A-Actifs/DEV-01/Info_arrondissement/Elevation terrain",
			"A-Actifs/DEV-01/Info_arrondissement/Classification rseau",
			"A-Actifs/DEV-01/Info_arrondissement/Responsable operationnel",
			"A-Actifs/DEV-01/Info_arrondissement/Territoire",
			"A-Actifs/DEV-01/Info_arrondissement/Proprietaire",
			"A-Actifs/DEV-01/Info_arrondissement/Juridiction",
			"A-Actifs/DEV-01/Info_arrondissement/Nom_reservoir",
			"A-Actifs/DEV-01/Info_arrondissement/Statut de position",
			"A-Actifs/DEV-01/Info_arrondissement/Statut de numerisation",
			"A-Actifs/DEV-01/Info_arrondissement/Date_abandon",
			"A-Actifs/DEV-01/Info_arrondissement/Hierarchie de troncon"
		]
		String COORDONNEE_SPATIALE
		int succes = 0
		int nb_spec_info = Specsgeo.size()
		int i = 0
		boolean resultat_validation = true
		String spec_introuver=''
		for(i=0; i <= Specsgeo.size()-1; i++) {
			if(Specsgeo[i] == null) {
				resultat_validation = gk.valideInput(emplacement[i], "", false, "value")
			}
			else if (Specsmaximo[i][1]=="DATE" && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {
				resultat_validation = gk.valideInput(emplacement[i], Specsgeo[i].split('T')[0], false, "value")

			}
			else if (Specsmaximo[i][1]=="ALNNUM"  &&  Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {
				String numvalue2 = Specsgeo[i].replace(".", "T")
				String numvalue = numvalue2.split('T')[0]
				resultat_validation = gk.valideInput(emplacement[i], numvalue, false, "value")
			}
			else if (Specsmaximo[i][0]=="VDM_COORDONNEE_SPATIALE_Y" || Specsmaximo[i][0]=="VDM_COORDONNEE_SPATIALE_X" ) {
				nb_coordonne = Specsgeo[i]
				if(nb_coordonne.size()==1) {
					COORDONNEE_SPATIALE = gk.formatnumber(Specsgeo[i])
					resultat_validation = gk.valideInputcoordonne(emplacement[i], COORDONNEE_SPATIALE.replace(",","."), false, "value")
				}
				else {resultat_validation = gk.valideInput(emplacement[i], "", false, "value")}
			}
			else if (Specsmaximo[i][1]=="NUM" && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {

				String numvalue = Specsgeo[i].replace(".", ",")
				resultat_validation = gk.valideInput(emplacement[i], numvalue, false, "value")
			}
			else  {
				resultat_validation =gk.valideInput(emplacement[i], Specsgeo[i], false, "value")
			}
			if (resultat_validation==true) {succes++}
			else {spec_introuver += Specsmaximo[i][0]+" et "}
		}
		if(Type_Actif == 'EGO_SEGMENT_L') {
			nb_spec_info += 1
			resultat_validation = gk.valideInput("A-Actifs/DEV-01/Info_arrondissement/Type de reseau", Actif_specs[0].features[0].properties.TYPERESEAU_R, false, "value")
			if (resultat_validation==true) {succes++}
			else {spec_introuver += Specsmaximo[i][0]+" et "}
		}

		if(succes==nb_spec_info) {
			return true
		}
		else {
			return false
		}
	}
	/**
	 * Valide les informations d'arrondissement de l'actif
	 * @param Actif, donne toutes les informations sur les actifs en format json
	 * @param currentRow, désigne l'actif à traiter
	 * @return résultat booléan qui renvoi true si l'actif est pareil dans maximo et dans le journal
	 */
	def genererSqlInformationsArrondissement(Map Actif_specs,int currentRow,String Type_Actif) {

		String coordonner
		String [] Specsgeo = [
			Actif_specs[0].features[0].properties.UUID,
			Actif_specs[0].features[0].properties.DATEINSTALL,
			Actif_specs[0].features[0].geometry.coordinates[0],
			Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,
			Actif_specs[0].features[0].properties.NUMEROSECTEUR1,
			Actif_specs[0].features[0].geometry.coordinates[1],
			Actif_specs[0].features[0].properties.ELEVATION,
			Actif_specs[0].features[0].properties.CLASSIFICATION_RESEAU_R,
			Actif_specs[0].features[0].properties.RESPONSABLE_OPERATIONNEL_R,
			Actif_specs[0].features[0].properties.TERRITOIRE_R,
			Actif_specs[0].features[0].properties.PROPRIETAIRE_R,
			Actif_specs[0].features[0].properties.JURIDICTION_R,
			Actif_specs[0].features[0].properties.RESERVOIR_R,
			Actif_specs[0].features[0].properties.STATUT_POSITION_R,
			Actif_specs[0].features[0].properties.STATUTGEOMETRIQUE_R,
			Actif_specs[0].features[0].properties.DATEABANDON,
			Actif_specs[0].features[0].properties.HIERARCHIE_TRONCON_R
		]
		String [][] Specsmaximo = [
			["UUID", "ALN"],
			["INSTALLDATE", "DATE"],
			[
				"VDM_COORDONNEE_SPATIALE_X",
				"NUM"
			],
			["VDM_ACTIF_ASSOCIE", "ALN"],
			[
				"VDM_NO_SECTEUR_REGULATION",
				"ALN"
			],
			[
				"VDM_COORDONNEE_SPATIALE_Y",
				"NUM"
			],
			[
				"VDM_ELEVATION_TERRAIN",
				"NUM"
			],
			[
				"VDM_CLASSIFICATION_RESEAU",
				"ALN"
			],
			[
				"VDM_RESPONSABLE_OPERATIONNEL",
				"ALN"
			],
			["VDM_TERRITOIRE", "ALNNUM"],
			["VDM_PROPRIETAIRE", "ALN"],
			["VDM_JURIDICTION", "ALN"],
			["VDM_NOM_RESERVOIR", "ALN"],
			[
				"VDM_STATUT_POSITION_ACTIF",
				"ALN"
			],
			[
				"VDM_STATUT_DESSIN_NUMERISATION",
				"ALN"
			],
			["VDM_DATE_ABANDON", "DATE"],
			[
				"VDM_HIERARCHIE_TRONCON",
				"ALN"
			]
		]
		String SQLRequest = ""
		String PSQL = ""
		for(int i=0; i<= Specsgeo.size()-1; i++) {
			if(Specsgeo[i] == null) {
				PSQL= Specsmaximo[i][0]+" is null "
			}
			else if (Specsmaximo[i][1]=="DATE" && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {
				PSQL=Specsmaximo[i][0]+"= ('" + Specsgeo[i].split("T")[0] + "')"
			}
			else if (Specsmaximo[i][1]=="ALNNUM" && Specsgeo[i]!="-1" && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {
				String numvalue = gk.Valeur_numerique(Specsgeo[i])
				PSQL= Specsmaximo[i][0]+"= ('" + numvalue + "') "
			}
			else if (Specsmaximo[i][0]=="VDM_COORDONNEE_SPATIALE_X" || Specsmaximo[i][0]=="VDM_COORDONNEE_SPATIALE_Y"){
				coordonner = Specsgeo[i]
				if(coordonner.size()==1) {
					PSQL = Specsmaximo[i][0]+"= ('" + coordonner + "')"
					PSQL=PSQL.replace(".", ",")
				}
				else{PSQL = Specsmaximo[i][0]+" is null "
				}
			}
			else if (Specsmaximo[i][1]=="NUM" && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {
				String numvalue = Specsgeo[i].replace(".", ",")
				PSQL= Specsmaximo[i][0]+"= ('" + numvalue + "')"

			}
			else  {
				PSQL = Specsmaximo[i][0]+"= ('" + Specsgeo[i] + "')"
			}
			if(i==0) {
				SQLRequest = "siteid='TP' and "+PSQL
			}

			else {
				PSQL = PSQL.replace(".", ",")
				SQLRequest += " and " + PSQL
			}
		}
		if(Type_Actif == 'EGO_SEGMENT_L') {
			SQLRequest += " and  VDM_TYPE_RESEAU = '"+Actif_specs[0].features[0].properties.TYPERESEAU_R+"'"
		}
		SQLRequest = SQLRequest.replace("= ('null')", "   is null ")

		return SQLRequest
	}

	/**
	 * 
	 * @param Actif_specs
	 * @param currentRow
	 * @param verification_visuel
	 * @param Type_Actif
	 * @return
	 */
	def genererSqlInformationsspecifications(Map Actif_specs,int currentRow, int verification_visuel, String Type_Actif) {
		String [] Specsgeo
		String [][] Specsmaximo
		String SQLRequest
		String PSQL = ""
		String numvalue
		boolean resultat
		switch (Type_Actif) {
			case 'AQU_RACCORD_P':
				Specsgeo = [
					Actif_specs[0].features[0].properties.ALTITUDE,
					Actif_specs[0].features[0].properties.COMPARTIMENT_R,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.ID_AQU_POINT,
					Actif_specs[0].features[0].properties.ID_POLE,
					Actif_specs[0].features[0].properties.MATERIAURACCORD_R,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.TYPERACCORD_R
				]
				Specsmaximo = [
					["ALTITUDE", "NUM"],
					["COMPARTIMENT", "ALN"],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "ALN"],
					["ID_POINT", "ALNNUM"],
					["ID_POLE", "ALNNUM"],
					["MATERIAU_RACCORD_AQ", "ALN"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					[
						"TYPE_RACCORD_AQ",
						"ALN"]
				]
				break
			case 'EGO_SEGMENT_L':
				Specsgeo = [
					Actif_specs[0].features[0].properties.BASSINVERSANT,
					Actif_specs[0].features[0].properties.CLASSESEGMENT_R,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DATEREHAB,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.DIAMETREHORIZONTAL_R,
					Actif_specs[0].features[0].properties.DIAMETREHORIZONTAL_R,
					Actif_specs[0].features[0].properties.DIAMETREVERTICAL_R,
					Actif_specs[0].features[0].properties.DIAMETREVERTICAL_R,
					Actif_specs[0].features[0].properties.FORMESEGMENT_R,
					Actif_specs[0].features[0].properties.ID_EGO_POINT_DEBUT,
					Actif_specs[0].features[0].properties.ID_EGO_POINT_FIN,
					Actif_specs[0].features[0].properties.MATERIAUSEGMENT_R,
					Actif_specs[0].features[0].properties.NO_CONDUITE,
					Actif_specs[0].features[0].properties.NOCONTRAT,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.NOM_USUEL,
					Actif_specs[0].features[0].properties.PENTEFOSSELATERAL1,
					Actif_specs[0].features[0].properties.PENTEFOSSELATERAL2,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.RADIERAMONT,
					Actif_specs[0].features[0].properties.RADIERAVAL,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.SENSECOULEMENT_R,
					Actif_specs[0].features[0].properties.TYPEECOULEMENT_R,
					Actif_specs[0].features[0].properties.TYPEREHAB_R,
					Actif_specs[0].features[0].properties.TYPESEGMENT_R
				]
				Specsmaximo = [
					["BASSIN_VERSANT", "ALN"],
					[
						"CLASSE_DE_RESISTANCE_EG",
						"ALN"
					],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					[
						"DATE_REHABILITATION",
						"DATE"
					],
					["DECOUPAGE", "ALN"],
					["DIAMETRE_H_MM", "ALNNUM"],
					["DIAMETRE_H_PO", "ALNNUM"],
					["DIAMETRE_V_MM", "ALNNUM"],
					["DIAMETRE_V_PO", "ALNNUM"],
					["FORME_CONDUITE", "ALN"],
					[
						"ID_EGO_POINT_DEBUT",
						"ALNNUM"
					],
					["ID_EGO_POINT_FIN", "ALNNUM"],
					["MATERIAU_SEGMENT_EG", "ALN"],
					["NO_CONDUITE_PI", "ALNNUM"],
					["NO_CONTRAT", "ALNNUM"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					["NOM_USUEL", "ALN"],
					[
						"PENTE_FOSSE_LATERAL_1",
						"ALNNUM"
					],
					[
						"PENTE_FOSSE_LATERAL_2",
						"ALNNUM"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					[
						"PROFONDEUR_RADIER_AMONT",
						"ALNNUM"
					],
					[
						"PROFONDEUR_RADIER_AVAL",
						"ALNNUM"
					],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					["SENS_ECOULEMENT", "ALN"],
					["TYPE_ECOULEMENT", "ALN"],
					[
						"TYPE_REHABILITATION_EG",
						"ALN"
					],
					[
						"TYPE_SEGMENT_EG",
						"ALN"]
				]
				break

			case 'EGO_PUISARD_P':
				Specsgeo = [
					Actif_specs[0].features[0].properties.ALTITUDE,
					Actif_specs[0].features[0].properties.BASSINVERSANT,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.DIMENSIONGRILLE_R,
					Actif_specs[0].features[0].properties.ID_EGO_POINT,
					Actif_specs[0].features[0].properties.ID_POLE,
					Actif_specs[0].features[0].properties.MATERIAUPUISARD_R,
					Actif_specs[0].features[0].properties.NUMEROPUISARD3,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.PROFONDEUR,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.VISIBLE_R
				]
				Specsmaximo = [
					["ALTITUDE", "NUM"],
					["BASSIN_VERSANT", "ALN"],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "ALN"],
					["DIMENSION_GRILLE", "ALNNUM"],
					["ID_POINT", "ALNNUM"],
					["ID_POLE", "ALNNUM"],
					["MATERIAU_PUISARD", "ALN"],
					[
						"NO_PUISARD_REMPLACE",
						"ALNNUM"
					],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PROFONDEUR", "NUM"],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					["VISIBLE", "ALN"]
				]
				break

			case 'EGO_CHAMBRE_S':
				Specsgeo = [
					Actif_specs[0].features[0].properties.BASSINVERSANT,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.FORMECHAMBRE_R,
					Actif_specs[0].features[0].properties.LARGEUR,
					Actif_specs[0].features[0].properties.LONGUEUR,
					Actif_specs[0].features[0].properties.MATERIAUCHAMBRE_R,
					Actif_specs[0].features[0].properties.NUMEROCHAMBRE1,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.PROFONDEUR,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.TYPECHAMBRE_R
				]
				Specsmaximo = [
					["BASSIN_VERSANT", "ALN"],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "DATE"],
					["FORME_CHAMBRE", "ALN"],
					["LARGEUR_CHAMBRE", "NUM"],
					["LONGUEUR_CHAMBRE", "NUM"],
					["MATERIAU_CHAMBRE_EG", "ALN"],
					["NO_CHAMBRE_DEEU", "ALN"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PROFONDEUR", "NUM"],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					[
						"TYPE_CHAMBRE_EG",
						"ALN"]
				]
				break

			case 'EGO_REGARD_P':
				Specsgeo = [
					Actif_specs[0].features[0].properties.ALTITUDE,
					Actif_specs[0].features[0].properties.BASSINVERSANT,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.DIMENSIONBASE_R,
					Actif_specs[0].features[0].properties.DIMENSIONCHEMINEE_R,
					Actif_specs[0].features[0].properties.DIMENSIONCOUVERCLE_R,
					Actif_specs[0].features[0].properties.ID_EGO_POINT,
					Actif_specs[0].features[0].properties.ID_POLE,
					Actif_specs[0].features[0].properties.MATERIAUREGARD_R,
					Actif_specs[0].features[0].properties.NO_CHAMBRE,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.ECHELON_R,
					Actif_specs[0].features[0].properties.PROFONDEUR,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.TYPEREGARD_R,
					Actif_specs[0].features[0].properties.VISIBLE_R
				]
				Specsmaximo = [
					["ALTITUDE", "NUM"],
					["BASSIN_VERSANT", "ALN"],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "ALN"],
					["DIMENSION_BASE_M", "ALNNUM"],
					[
						"DIMENSION_CHEMINEE_M",
						"ALNNUM"
					],
					[
						"DIMENSION_COUVERCLE_PO",
						"ALNNUM"
					],
					["ID_POINT", "ALNNUM"],
					["ID_POLE", "ALNNUM"],
					["MATERIAU_REGARD", "ALN"],
					[
						"NO_GEOMATIQUE_CHAMBRE_LIE",
						"ALNNUM"
					],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PRESENCE_ECHELON", "ALN"],
					["PROFONDEUR", "NUM"],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					["TYPE_REGARD_EG", "ALN"],
					["VISIBLE", "ALN"]
				]

				break

			case 'AQU_ACCESSOIRE_P':
				Specsgeo = [
					Actif_specs[0].features[0].properties.ALTITUDE,
					Actif_specs[0].features[0].properties.COMPARTIMENT_R,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.DIAMETRE_R,
					Actif_specs[0].features[0].properties.ID_AQU_POINT,
					Actif_specs[0].features[0].properties.ID_POLE,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.TYPEACCESSOIRE_R
				]
				Specsmaximo = [
					["ALTITUDE", "NUM"],
					["COMPARTIMENT", "ALN"],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "ALN"],
					["DIAMETRE_MM", "ALNNUM"],
					["ID_POINT", "ALNNUM"],
					["ID_POLE", "ALNNUM"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					[
						"TYPE_ACCESSOIRE_AQ",
						"ALN"]
				]

				break

			case 'EGO_ACCESSOIRE_P':
				Specsgeo = [
					Actif_specs[0].features[0].properties.ALTITUDE,
					Actif_specs[0].features[0].properties.COMPARTIMENT_R,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.ID_EGO_POINT,
					Actif_specs[0].features[0].properties.ID_POLE,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE
				]
				Specsmaximo = [
					["ALTITUDE", "NUM"],
					["COMPARTIMENT", "ALN"],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["ID_POINT", "ALNNUM"],
					["ID_POLE", "ALNNUM"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PROVENANCE_DONNEE", "ALN"],
					[
						"REMARQUE_GEOMATIQUE",
						"ALN"]
				]

				break

			case 'AQU_VANNE_P':
				Specsgeo = [
					Actif_specs[0].features[0].properties.ALTITUDE,
					Actif_specs[0].features[0].properties.COMPARTIMENT_R,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.DIAMETRE_R,
					Actif_specs[0].features[0].properties.FONCTIONVANNE_R,
					Actif_specs[0].features[0].properties.ID_AQU_POINT,
					Actif_specs[0].features[0].properties.ID_POLE,
					Actif_specs[0].features[0].properties.MECANISME_R,
					Actif_specs[0].features[0].properties.NUMEROCHAMBRE1,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.ORIENTATIONVANNE_R,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.PROFONDEUR,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.VISIBLE_R
				]
				Specsmaximo = [
					["ALTITUDE", "NUM"],
					["COMPARTIMENT", "ALN"],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "ALN"],
					["DIAMETRE_MM", "ALNNUM"],
					["FONCTION", "ALN"],
					["ID_POINT", "ALNNUM"],
					["ID_POLE", "ALNNUM"],
					["MECANISME_VANNE", "ALN"],
					[
						"NO_CHAMBRE_SUR_PLAN_CLE",
						"ALNNUM"
					],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					["ORIENTATION_VANNE", "ALN"],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PROFONDEUR", "NUM"],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					["VISIBLE", "ALN"]
				]

				break

			case 'AQU_CHAMBRE_S':
				Specsgeo = [
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.FORMECHAMBRE_R,
					Actif_specs[0].features[0].properties.LARGEUR,
					Actif_specs[0].features[0].properties.LONGUEUR,
					Actif_specs[0].features[0].properties.MATERIAUCHAMBRE_R,
					Actif_specs[0].features[0].properties.NUMEROCHAMBRE3,
					Actif_specs[0].features[0].properties.NUMEROCHAMBRE1,
					Actif_specs[0].features[0].properties.NUMEROCHAMBRE2,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.PROFONDEUR,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.TYPECHAMBRE_R
				]
				Specsmaximo = [
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "ALN"],
					["FORME_CHAMBRE", "ALN"],
					["LARGEUR_CHAMBRE", "NUM"],
					["LONGUEUR_CHAMBRE", "NUM"],
					["MATERIAU_CHAMBRE_AQ", "ALN"],
					[
						"NO_CHAMBRE_REGULATION",
						"ALN"
					],
					[
						"NO_CHAMBRE_SUR_PLAN_CLE",
						"ALN"
					],
					["NO_PLAN_CONTRAT_TEC", "ALN"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					["PROFONDEUR", "NUM"],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					["TYPE_CHAMBRE", "ALN"]
				]

				break

			case 'AQU_SEGMENT_L':
				Specsgeo = [
					Actif_specs[0].features[0].properties.CLASSESEGMENT_R,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DATEREHAB,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.DIAMETRE_R,
					Actif_specs[0].features[0].properties.ID_AQU_POINT_DEBUT,
					Actif_specs[0].features[0].properties.ID_AQU_POINT_FIN,
					Actif_specs[0].features[0].properties.MATERIAUSEGMENT_R,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.TYPEPROTECTION_R,
					Actif_specs[0].features[0].properties.TYPEREHAB_R,
					Actif_specs[0].features[0].properties.TYPESEGMENT_R
				]
				Specsmaximo = [
					[
						"CLASSE_DE_RESISTANCE_AQ",
						"ALN"
					],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					[
						"DATE_REHABILITATION",
						"DATE"
					],
					["DECOUPAGE", "ALN"],
					["DIAMETRE_MM", "ALNNUM"],
					["ID_POINT_DEBUT", "ALNNUM"],
					["ID_POINT_FIN", "ALNNUM"],
					["MATERIAU_SEGMENT_AQ", "ALN"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					["TYPE_PROTECTION", "ALN"],
					[
						"TYPE_REHABILITATION_AQ",
						"ALN"
					],
					[
						"TYPE_SEGMENT_AQ",
						"ALN"]
				]

				break

			case 'AQU_BORNEINCENDIE_P':
				Specsgeo = [
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.ID_AQU_POINT,
					Actif_specs[0].features[0].properties.ID_POLE,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.PROFONDEUR,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE
				]
				Specsmaximo = [
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "ALN"],
					["ID_POINT", "ALNNUM"],
					["ID_POLE", "ALNNUM"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PROFONDEUR", "NUM"],
					["PROVENANCE_DONNEE", "ALN"],
					[
						"REMARQUE_GEOMATIQUE",
						"ALN"]
				]

				break
			case 'EGO_RACCORD_P':
				Specsgeo = [
					Actif_specs[0].features[0].properties.ALTITUDE,
					Actif_specs[0].features[0].properties.BASSINVERSANT,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.ID_EGO_POINT,
					Actif_specs[0].features[0].properties.ID_POLE,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.TYPERACCORD_R
				]
				Specsmaximo = [
					["ALTITUDE", "NUM"],
					["BASSIN_VERSANT", "ALN"],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "ALN"],
					["ID_POINT", "ALNNUM"],
					["ID_POLE", "ALNNUM"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					[
						"TYPE_RACCORD_EG",
						"ALN"]
				]

				break
			case 'AQU_REGARD_P':
				Specsgeo = [
					Actif_specs[0].features[0].properties.ALTITUDE,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.ID_AQU_POINT,
					Actif_specs[0].features[0].properties.ID_POLE,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.TYPEREGARD_R
				]
				Specsmaximo = [
					["ALTITUDE", "NUM"],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "ALN"],
					["ID_POINT", "ALNNUM"],
					["ID_POLE", "ALNNUM"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					["TYPE_REGARD_AQ", "ALN"]
				]

				break
			case 'EGO_BASSINRETENTION_S':
				Specsgeo = [
					Actif_specs[0].features[0].properties.BASSINVERSANT,
					Actif_specs[0].features[0].properties.DATECREEE_M,
					Actif_specs[0].features[0].properties.DATEMODIFIEE_M,
					Actif_specs[0].features[0].properties.DECOUPAGE,
					Actif_specs[0].features[0].properties.LARGEUR,
					Actif_specs[0].features[0].properties.LONGUEUR,
					Actif_specs[0].features[0].properties.MATERIAUBASSIN_R,
					Actif_specs[0].features[0].properties.NOTRCGEOBASE,
					Actif_specs[0].features[0].properties.NOMFICHIERCREATION_M,
					Actif_specs[0].features[0].properties.DATEINSTALLPREC_R,
					Actif_specs[0].features[0].properties.PROFONDEUR,
					Actif_specs[0].features[0].properties.PROVENANCEDONNEE_R,
					Actif_specs[0].features[0].properties.REMARQUE,
					Actif_specs[0].features[0].properties.TYPEBASSIN_R
				]
				Specsmaximo = [
					["BASSIN_VERSANT", "ALN"],
					["DATE_CREEE", "DATE"],
					["DATE_MODIFIEE", "DATE"],
					["DECOUPAGE", "ALN"],
					["LARGEUR_BASSIN", "ALNNUM"],
					["LONGUEUR_BASSIN", "ALNNUM"],
					["MATERIAU_BASSIN", "ALN"],
					[
						"NO_TRONCON_GEOBASE",
						"ALNNUM"
					],
					[
						"NOM_FICHIER_CREATION",
						"ALN"
					],
					[
						"PRECISION_DATE_INSTALL",
						"ALN"
					],
					["PROFONDEUR", "NUM"],
					["PROVENANCE_DONNEE", "ALN"],
					["REMARQUE_GEOMATIQUE", "ALN"],
					["TYPE_BASSIN", "ALN"]
				]

				break
		}
		if(verification_visuel==0) {
			resultat = validespecsvisuel(Actif_specs,currentRow,Specsgeo,Specsmaximo)
			return resultat
		}
		else {
			for(int i=0; i<= Specsgeo.size()-1; i++) {
				//data value = Specsmaximo[i][0]
				//type = Specsmaximo[i][1]
				if (Specsgeo[i] == null) {
					PSQL= " exists (select 1 from assetspec where asset.assetnum = assetnum and assetattrid = '"+Specsmaximo[i][0]+"' and alnvalue is null)"
				}
				else if (Specsmaximo[i][1]=="DATE"  && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {//and exists (select 1 from assetspec asp where asset.assetnum = asp.assetnum and asp.assetattrid = 'DATE_CREEE' and  trunc(to_timestamp(asp.alnvalue, 'YYYY-MM-DD"T"HH24:MI:SS.ff3"Z"') ) is no
					PSQL= " exists (select 1 from assetspec where asset.assetnum = assetnum and assetattrid = '"+Specsmaximo[i][0]+"' and  trunc(to_timestamp(alnvalue, '"+'YYYY-MM-DD"T"HH24:MI:SS.ff3"Z"'+"') ) = trunc(to_date('"+Specsgeo[i].split('T')[0]+"','YYYY-MM-DD')))"
				}
				else if(Specsmaximo[i][0]=="REMARQUE_GEOMATIQUE"  ) {
					if(Specsgeo[i].contains("'")) {
						PSQL= " exists (select 1 from assetspec where asset.assetnum = assetnum and assetattrid = '"+Specsmaximo[i][0]+"' and alnvalue like '" + Specsgeo[i].replace("'", "_") + "')"
					}
				}
				else if (Specsmaximo[i][1]=="ALNNUM"  && Specsgeo[i]!="-1" && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {

					numvalue = gk.Valeur_numerique(Specsgeo[i])
					PSQL= " exists (select 1 from assetspec where asset.assetnum = assetnum and assetattrid = '"+Specsmaximo[i][0]+"' and alnvalue = '" + numvalue + "')"
				}
				/*else if (Specsmaximo[i][0]=="ALTITUDE" && Specsgeo[i] != null && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {
				 numvalue = Specsgeo[i].replace(".", ",")
				 ////String numvalue1 = numvalue2.split('T')[0]
				 ////int numvalue = numvalue1.toInteger()
				 PSQL= " exists (select 1 from assetspec where asset.assetnum = assetnum and assetattrid = '"+Specsmaximo[i][0]+"' and numvalue = '" + numvalue + "')"
				 }*/

				else if (Specsmaximo[i][1]=="NUM"  && Specsgeo[i]!="INC" && Specsgeo[i]!="NON APPLICABLE" && Specsgeo[i]!="NA") {

					numvalue = gk.Valeur_numerique(Specsgeo[i])
					PSQL= " exists (select 1 from assetspec where asset.assetnum = assetnum and assetattrid = '"+Specsmaximo[i][0]+"' and numvalue = '" + numvalue + "')"
				}
				else  {
					PSQL = " exists (select 1 from assetspec where asset.assetnum = assetnum and assetattrid = '"+Specsmaximo[i][0]+"' and alnvalue = '" + Specsgeo[i] + "')"
				}
				if(i==0) {
					SQLRequest ="siteid='TP' and uuid='"+Actif_specs[0].features[0].properties.UUID+"' and "+ PSQL
				}
				else {

					SQLRequest += " and" + PSQL
				}
			}
			SQLRequest = SQLRequest.replace("asset,assetnum", "asset.assetnum")


			return SQLRequest
		}
	}
}
