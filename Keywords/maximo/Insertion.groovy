package maximo


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.webui.driver.DriverFactory as DF
import java.util.ArrayList
import java.time.LocalDate
import com.kms.katalon.core.logging.KeywordLogger
import java.time.format.DateTimeFormatter
import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.util.internal.JsonUtil as JsonUtil
import com.google.gson.Gson as Gson
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import javax.swing.Box;
import javax.swing.JFrame
import javax.swing.JLabel;
import javax.swing.JOptionPane
import javax.swing.JPanel;
import javax.swing.JTextField

public class Insertion {
	//Transfert dans actif
	public GeneralKeys gk = new GeneralKeys()
	public WebDriver driver = DriverFactory.getWebDriver()
	public KeywordLogger log = new KeywordLogger()
	/**
	 * 
	 * @param arrondissement: Cette variable recoit le responsable opérationnel du genre VDM-ARR
	 * @param type_actif: le type d'actif recoit le type d'actif pour pouvoir faire la distinction entre les différents emplacements
	 * @param vdm_actif_associer: Le VDM_actif_associer recoit le UUID de l'actif associé à l'actif à évaluer. Il peut recevoir un UUID ou rien
	 * @param fonction: cette variable recoit le type de fonction de l'actif. elle est utilisé seulement pour les vannes
	 * @param UUID: Elle recoit le UUID de l'actif
	 * @return un résultat boolean pour savoir si les données sont bonnes ou pas
	 */
	def Validation_emplacement_AQU(String arrondissement, String type_actif,String vdm_actif_associer,String fonction,String UUID) {
		boolean resultat_emplacement
		String emplacement
		//Modifier l'arrondissement pour prendre le territoire
		arrondissement = arrondissement.split('-')[1]

		switch (type_actif){
			case 'AQU_RACCORD_P':
				println(vdm_actif_associer)
			//Tous les raccords d'aqueduc ont la même hiérarchie d'emplacement
			//if(vdm_actif_associer == '') {
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'RACAQ000' and LOCATION like ('"+arrondissement+"-AQU-RACC%')")
				return resultat_emplacement
			//}
				break
			case 'AQU_ACCESSOIRE_P':
			//Accessoire d'aqueduc n'aurait pas de vdm actif associé
				if(vdm_actif_associer == '') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'ACCAQ000' and LOCATION like ('"+emplacement+"%')")
				}
				//Accessoire de chambre d'aqueduc aura comme vdm actif associé la chambre correspondante
				else if(vdm_actif_associer != '') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'ACCAQ000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-ACC%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-CHAMBRE%')))")
				}
				return resultat_emplacement
				break

			case 'AQU_VANNE_P':
				emplacement == '' + arrondissement + '-AQU-VANNE'
			//Vanne chambre doit avoir le UUID de la BI comme parent et comme fonction : ISOLEMENT DE BORNE INCENDIE
				if(vdm_actif_associer != '' && fonction=='BI') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'VAN000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-VANNE%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-BI%')))")
				}
				//Vanne chambre doit avoir le UUID de la chambre comme parent et comme fonction : INC, AR, CD, D, VDR, I, MP, RDP, RR, VA, V ou VRP
				else if(vdm_actif_associer != '' && (fonction=='INC' || fonction=='AR' || fonction=='CD' || fonction=='D'|| fonction=='VDR' || fonction=='I' || fonction=='MP' || fonction=='RDP' || fonction=='RR' || fonction=='VA' || fonction=='V' || fonction=='VRP')) {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'VAN000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-VANNE%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-CHAMBRE%')))")
				}
				//Vanne réseeau ne doit pas avoir de UUID actif associé et comme fonction : INC, AR, CD, D, VDR, I, MP, RDP, RR, VA, V ou VRP
				else if(vdm_actif_associer == '' && (fonction=='INC' || fonction=='AR' || fonction=='CD' || fonction=='D'|| fonction=='VDR' || fonction=='I' || fonction=='MP' || fonction=='RDP' || fonction=='RR' || fonction=='VA' || fonction=='V' || fonction=='VRP')) {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'VAN000' and LOCATION like ('"+emplacement+"%')")
				}
				//Vanne entrée de service doit avoir le UUID de la vanne d'entrée de service comme parent et comme fonction : SD, SDG, SF ou SG
				else if(vdm_actif_associer == '' && (fonction=='SD' || fonction=='SDG' || fonction=='SF' || fonction=='SG')) {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'VAN000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-VANNE%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-ES%')))")
				}
				return resultat_emplacement
				break

			case 'AQU_CHAMBRE_S':
			//Toutes les chambres d'aqueduc ont la même hiérarchie d'emplacement
				if(vdm_actif_associer == '') {
					emplacement == '' + arrondissement + '-AQU-CHAMBRE'
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'CHAQ000' and LOCATION like ('"+emplacement+"%')")
					return resultat_emplacement
				}
				break

			case 'AQU_SEGMENT_L':
			//TODO: Manque un cas
			//Segment de borne d'incendie aura un VDM_actif_associer
				if(vdm_actif_associer != '') {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'SEGAQ000' and exists (select 1 from asset a2 where a2.uuid = "+vdm_actif_associer+" and a2.location like ('"+arrondissement+"-AQU-BI%'))")
				}
				//Pour tout les autres types de segment d'aqueduc
				else {
					resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'SEGAQ000' and exists(select 1 from locations where location = asset.location and location like ('"+arrondissement+"-AQU-SEG%'))")
				}
				return resultat_emplacement
				break

			case 'AQU_BORNEINCENDIE_P':
			//Pour toutes les bornes d'incendies on a la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'BIAQ000' and exists(select 1 from locations where location = asset.location and location like ('"+arrondissement+"-AQU-BI%'))")
				return resultat_emplacement
				break
			case 'AQU_REGARD_P':
			//Pour tout les regards d'aqueduc sont rattaché à une chambre
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'REGAQ000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-AQU-REG%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-AQU-CHAMBRE%')))")
				return resultat_emplacement
				break
				break
		}
	}
	/**
	 *
	 * @param arrondissement: Cette variable recoit le responsable opérationnel du genre VDM-ARR
	 * @param type_actif: le type d'actif recoit le type d'actif pour pouvoir faire la distinction entre les différents emplacements
	 * @param vdm_actif_associer: Le VDM_actif_associer recoit le UUID de l'actif associé à l'actif à évaluer. Il peut recevoir un UUID ou rien
	 * @param UUID: Elle recoit le UUID de l'actif
	 * @return un résultat boolean pour savoir si les données sont bonnes ou pas
	 */
	def Validation_emplacement_EGO(String arrondissement, String type_actif,String vdm_actif_associer,String UUID) {
		boolean resultat_emplacement
		arrondissement = arrondissement.split('-')[1]

		switch (type_actif) {
			case 'EGO_BASSIN_P':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'BAS000' and LOCATION like ('"+arrondissement+"-EGO-BAS%')")
				return resultat_emplacement
				break

			case 'EGO_RACCORD_P':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'RACEG000' and LOCATION like ('"+arrondissement+"-EGO-RACC%')")
				return resultat_emplacement
				break

			case 'EGO_ACCESSOIRE_P':
			//Tous la même hiérarchie des emplacements
				if(vdm_actif_associer!='') {resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'ACCEG000' and exists(select 1 from locations where location = asset.location and location like '"+arrondissement+"-EGO-CH%'and exists ( select 1 from lochierarchywhere location= locations.location and exists (select 1 from asset a2 where a2.uuid = '"+vdm_actif_associer+"' and lochierarchy.parent = a2.locationand a2.location like '"+arrondissement+"-EGO-CH%')))")
				}
				else{resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'ACCEG000' and exists(select 1 from locations where location = asset.location and location like ('"+arrondissement+"-EGO-CH%'))")
				}
				return resultat_emplacement
				break
			case 'EGO_SEGMENT_L':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'SEGEG000' and LOCATION like ('"+arrondissement+"-EGO-SEG%')")
				return resultat_emplacement
				break

			case 'EGO_PUISARD_P':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'PUIEG000' and LOCATION like ('"+arrondissement+"-EGO-PUI%')")
				return resultat_emplacement
				break

			case 'EGO_CHAMBRE_S':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'CHEG000' and LOCATION like ('"+arrondissement+"-EGO-CH%')")
				return resultat_emplacement
				break

			case 'EGO_REGARD_P':
			//Tous la même hiérarchie des emplacements
				resultat_emplacement = gk.ouvrirClauseWhereExecuteRequete("siteid = 'TP' and uuid = '"+UUID+"' and ITEMNUM = 'REGEG000'  and LOCATION like ('"+arrondissement+"-EGO-CH%') and exists (select 1 from asset a2 where a2.uuid = "+vdm_actif_associer+" and a2.location like ('"+arrondissement+"-EGO-CH%'))")
				return resultat_emplacement
				break
				break
		}
	}
}
