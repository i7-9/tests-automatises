package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class ClassificationsKeys {
	
	private static GeneralKeys gk = new GeneralKeys()

	/**
	 * Accéder a l'application Classifications
	 * @return
	 */
	@Keyword
	def accederAClassifications(){
		gk.clicBoutonAllerA()
		gk.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/Administration/M-AdministrationM'))
		gk.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/Administration/M-Organisations'))
		gk.mouseOver(findTestObject('P-Accueil/Navbar/AllerA/Administration/M-Classifications'))
		gk.click(findTestObject('P-Accueil/Navbar/AllerA/Administration/M-Classifications'))
		
		WebUI.waitForPageLoad(10)
	}
	
}
