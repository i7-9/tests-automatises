package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import javax.naming.Context
import javax.naming.InitialContext
import javax.swing.Box;
import javax.swing.JFrame
import javax.swing.JLabel;
import javax.swing.JOptionPane
import javax.swing.JPanel;
import javax.swing.JTextField
import oracle.jdbc.pool.OracleDataSource
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory as DF
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import internal.GlobalVariable
import java.sql.Connection
import java.sql.*
import java.sql.DriverManager
import java.sql.SQLException
import groovy.sql.*
import groovy.sql.Sql
import oracle.jdbc.*
import oracle.jdbc.pool.*
import groovy.json.JsonSlurper
import java.sql.Driver

public class GeneralKeys {


	private static KeywordLogger log = new KeywordLogger()
	private static WebDriver driver
	private static WebDriverBackedSelenium selenium

	private ArrayList<String> parametresDB = new ArrayList<String>()
	private ArrayList<String> resultasDB  = new ArrayList<String>()


	private static String connectionString = ""
	private static props = new Properties();
	private static oracleDriver = Class.forName('oracle.jdbc.OracleDriver').newInstance() as Driver
	private static Connection conn
	//private Sql sql

	/**
	 * Fonction d'ouvrir le navigateur
	 */
	@Keyword
	def openBrowser(){
		WebUI.comment('ouverture du navigateur')
		//Acceder a la page de connexion du site
		WebUI.openBrowser(selectSite(GlobalVariable.site))
		WebUI.waitForPageLoad(10)
		//Agrandit la fenetre
		WebUI.maximizeWindow()
	}


	/**
	 * Permet de se connecter � Maximo
	 */
	@Keyword
	def connexion(){
		WebUI.comment('Connexion à Maximo')
		//Connexion avec l utilisateur du test en cours et informations sur le test en cours
		setTextElement('P-Login/T-Username', GlobalVariable.username, null)
		WebUI.setMaskedText(findTestObject('P-Login/T-Passwd'), GlobalVariable.mdp)
		click('P-Login/B-Login', null)
		WebUI.waitForPageLoad(10)
	}

	def static getConnexion() {
		if(conn == null|| conn.isClosed()){
			conn = oracleDriver.connect(connectionString, props)
		}
		return conn
	}




	/*
	 * @param numvalue1
	 * @return
	 */
	def Valeur_numerique(String numvalue1) {
		numvalue1 = numvalue1.replace(".","T")
		String numvalue2 = numvalue1.split('T')[1]
		String numvalue = numvalue1.split('T')[0]
		numvalue2 = numvalue2.replace("000","")
		numvalue2 = numvalue2.replace("00", "")
		if(numvalue2 != "") {
			numvalue = numvalue + '.' + numvalue2
		}
		return numvalue
	}



	/**
	 * Fonction qui permet d'afficher une boite de dialogue qui va permettre à l'utilisateur d'entrer une variable
	 * 
	 * @param titre, titre de la fenêtre de dialogue
	 * @param action, action qui doit être faite par l'utilisateur
	 * @return userInput, la variable entrée par l'utilisateur
	 */
	def inputJFrame(String titre, String action){
		// Création du JFram
		JFrame frame = new JFrame(titre)
		frame.requestFocus()
		// Enregistrement du input
		String userInput = JOptionPane.showInputDialog(frame, action)

		return userInput
	}


	/**
	 * Fonction qui permet d'afficher une boite de dialogue avec de multiples champs qui doivent être remplis pas l'utilisateur
	 * 
	 * @param titresChamps, liste contenant les titres des champs 
	 * @return inputValues, Liste contenant les valeurs entrées par l'utilisateur
	 */
	def multipleInputJPanel(ArrayList<String> titresChamps){
		// Listes pour les JTextField et les valeurs
		ArrayList<String> listJTextField = new ArrayList<String>()
		ArrayList<String> inputValues = new ArrayList<String>()
		int i = 0
		// Création du JPanel
		JPanel myPanel = new JPanel()
		// Parcourir la liste des titres de champs et pour chaque titre créer un JTextField et l'ajouter au JPanel
		while (i<titresChamps.size()){
			JTextField jTextField = new JTextField(5)
			listJTextField.add(jTextField)
			myPanel.add(new JLabel(titresChamps.get(i)));
			myPanel.add(listJTextField.get(i));
			myPanel.add(Box.createHorizontalStrut(15));
			i++
		}
		// Récupérer le résultat quand l'utilisateur appuie sur OK, cancel
		int result = JOptionPane.showConfirmDialog(null, myPanel,"Remplir les champs suivants", JOptionPane.OK_CANCEL_OPTION);

		// Si l'utilisateur appuie sur ok on récupère les valeurs entrées par l'utilisateur et on l'ajoute à la liste de valeur
		if (result == JOptionPane.OK_OPTION) {
			int j = 0
			while(j<listJTextField.size()){
				JTextField jTextField = listJTextField.get(j)
				String valeur = jTextField .getText()
				inputValues.add(valeur)
				j++
			}
		}
		return inputValues
	}


	/**
	 * Fonction permettant de se deconnecter de Maximo
	 */
	@Keyword
	def deconnexion(){
		//Deconnexion de l'utilisateur en cours
		click('P-Accueil/Navbar/B-Logout', null)
		fermerFenetreIntempestive()
	}


	/**
	 * Permet de simuler un clic sur le bouton Home
	 */
	@Keyword
	def clicBoutonHome(){
		waitForElementPresent('P-Accueil/Navbar/B-Home', 5, null)
		click('P-Accueil/Navbar/B-Home', null)
	}


	/**
	 * Permet de simuler un clic sur le bouton aller a
	 */
	@Keyword
	def clicBoutonAllerA(){
		if(GlobalVariable.site == 'DEV01') {
			click('P-DEV-01-Accueil/Navbar/B-AllerA', null)
		}else {
			click('P-Accueil/Navbar/B-AllerA', null)
		}
	}


	/**
	 * Permet de positionner le curseur sur un element du menu aller a
	 * @param menuPath
	 */
	@Keyword
	def mouseOverMenuInAllerA(String menuPath){
		mouseOver('P-Accueil/Navbar/AllerA/'+menuPath, null)
	}


	/**
	 * Permet de cliquer sur un element du menu aller a
	 * @param menuPath
	 */
	@Keyword
	def mouseClicMenuInAllerA(String menuPath){
		click('P-Accueil/Navbar/AllerA/'+menuPath, null)
	}


	/**
	 * Permet de creer un nouvel objet simuler un clic sur le bouton  plus jaune
	 */
	@Keyword
	def clicCreationNouvelObject(){
		click('ElementsGeneriques/BarreOutils/B-Nouveau', null)
	}


	/**
	 * Permet d'enregistrer l'ajout d'un objet
	 */
	@Keyword
	def clicEnregistrerObject(){
		WebUI.waitForElementClickable(findTestObject('ElementsGeneriques/BarreOutils/B-Enregistrer'), 3)
		click('ElementsGeneriques/BarreOutils/B-Enregistrer', null)
		WebUI.delay(1)
	}


	/**
	 * Permet de fermer la fenetre intempestive : Voulez-vous sauvegarder ...
	 */
	@Keyword
	public def fermerFenetreIntempestive(){
		click('P-Accueil/Navbar/AllerA/Alert-Error/B-Alert-Non', FailureHandling.OPTIONAL)
	}


	/**
	 * Permet de fermer les fen�tres d'erreur SQL
	 */
	@Keyword
	public def fermerErreursSQL(){
		click('P-Accueil/Navbar/AllerA/Alert-Error/B-Alert_Fermer', FailureHandling.OPTIONAL)
		click('ElementsGeneriques/ClauseWhere/B-OKnotFound', FailureHandling.OPTIONAL)
	}


	/**
	 * Utiliser Keylogger pour un logError
	 * @param message
	 */
	@Keyword
	def logError(String message){
		log.logError(message)
	}


	/**
	 * Utiliser Keylogger pour un logInfo
	 * @param message
	 */
	@Keyword
	def logInfo(String message){
		log.logInfo(message)
	}


	/**
	 * Utiliser Keylogger pour un logFailed
	 * @param message
	 */
	@Keyword
	def logFailed(String message){
		log.logFailed(message)
	}


	/**
	 * Utiliser Keylogger pour un logWarning
	 * @param message
	 */
	@Keyword
	def logWarning(String message){
		log.logWarning(message)
	}


	/**
	 * Utiliser Keylogger pour un logPassed
	 * @param message
	 */
	@Keyword
	def logPassed(String message){
		log.logPassed(message)
	}


	/**
	 * Retourner le Web driver
	 */
	@Keyword
	def getWebDriver(){
		return driver
	}


	/**
	 * Retourner le WebDriverBackedSelenium
	 */
	@Keyword
	def getWebDriverBackedSelenium(){
		return selenium
	}


	/**
	 * Rechercher les elements sur la page By
	 * @param by
	 */
	@Keyword
	def findElementsBy(By by){
		return driver.findElements(by)
	}


	/**
	 * Rechercher un element sur la page By
	 * @param by
	 */
	@Keyword
	def findOneElementBy(By by){
		return driver.findElement(by)
	}


	/**
	 * Mettre un valeur texte a un objet Katalon
	 * @param objectPath - Chemin de l'objet
	 * @param value - Valeur � �crire
	 */
	@Keyword
	def setTextElement(String objectPath, String value, FailureHandling fh){
		if(fh == null) {
			fh = FailureHandling.STOP_ON_FAILURE
		}
		WebUI.waitForElementVisible(findTestObject(objectPath), 2)
		WebUI.setText(findTestObject(objectPath), value, fh)
	}


	/**
	 * Mettre un valeur texte a un objet Katalon
	 * @param objectPath - Chemin de l'objet
	 * @param value - Valeur � �crire
	 */
	@Keyword
	def forceSetTextElement(String objectPath, String value){
		//Loop tant qu'il y a un crash ou que la valeur de l'objet n'est pas la bonne
		while(WebUI.getAttribute(findTestObject(objectPath), "value", FailureHandling.OPTIONAL) != value) {

			try{
				WebUI.delay(1)
				setTextElement(objectPath, value, FailureHandling.OPTIONAL)

			}catch(Exception ex) {
				WebUI.delay(1)
				logError("Le texte s'est effac� dans le champ: " + objectPath)
			}
		}
	}


	/**
	 * Permet d'ouvrir le clause where et d'indiquer une requete
	 * @param String SQLRequest - Requete SQL a taper dans la zone de recherche
	 * @return boolean si la requete a fonctionnee ou non
	 */
	@Keyword
	def ouvrirClauseWhereExecuteRequete(String SQLRequest){
		WebElement e
		if(GlobalVariable.site == 'DEV01') {
			click('Object Repository/A-Actifs/DEV-01/liste',null)
		}
		else {
			click('Object Repository/ElementsGeneriques/B-ZoneRecherchePlus',null)
		}

		click('Object Repository/ElementsGeneriques/B-ZoneRecherchePlus', null)
		click('Object Repository/ElementsGeneriques/B-ClauseWhere', null)

		click('Object Repository/ElementsGeneriques/ClauseWhere/TA-ClauseWhere', null)
		e =	DF.getWebDriver().findElement(By.id("m8366b731-ta"))

		//�crit la requ�te SQL dans le javascript de l'objet (setText ne fonctionne pas pour un TextArea)
		//WebUI.executeJavaScript("arguments[0].value = '"+ SQLRequest +"';", Arrays.asList(e))
		WebUI.setText(findTestObject('Object Repository/ElementsGeneriques/ClauseWhere/TA-ClauseWhere'),SQLRequest)
		click("ElementsGeneriques/ClauseWhere/TA-ClauseWhere", null)
		sendKeys("ElementsGeneriques/ClauseWhere/TA-ClauseWhere", [Keys.CONTROL, "a"], null)
		WebUI.delay(5)
		//Executer la recherche la recherche
		click('ElementsGeneriques/ClauseWhere/B-Rechercher', null)

		//waitForNotPresent('ElementsGeneriques/ClauseWhere/B-Rechercher', 45, FailureHandling.CONTINUE_ON_FAILURE)
		//V�rifie si la requ�te a �t� ex�cut�e avec succ�s ou non


		if(waitForNotPresent('ElementsGeneriques/ClauseWhere/F-Clause_Where', 60, FailureHandling.OPTIONAL)){
			if(!waitForNotPresent('ElementsGeneriques/ClauseWhere/Alert', 2, FailureHandling.CONTINUE_ON_FAILURE)){
				fermerErreursSQL()
				log.logError("Erreur SQL dans le requete : " + SQLRequest)
				return false}
			else if(!waitForNotPresent('ElementsGeneriques/ClauseWhere/B-OKnotFound',2, FailureHandling.CONTINUE_ON_FAILURE)){
				fermerErreursSQL()
				log.logError("La requete suivante : " + SQLRequest + " n'a pas de resultat")
				return false
			}
			else {
				log.logInfo("Execution avec succes de la requete : " + SQLRequest)
				return true}
		} /*else{
		 waitForElementPresent('ElementsGeneriques/B-AffichageListe', 15, FailureHandling.OPTIONAL)
		 log.logInfo("Execution avec succes de la requete : " + SQLRequest)
		 return true
		 }*/

	}


	/**
	 * Simuler un clic sur le bouton affichage liste
	 */
	@Keyword
	def clicAfficheListe(){
		click('ElementsGeneriques/B-AffichageListe', null)
	}


	/**
	 * Simuler une recherche rapide dans la barre du haut
	 * @param String request - Recherche � effectuer
	 * @return boolean correspondant au succ�s de la recherche
	 */
	@Keyword
	def rechercheRapide(String request){
		setTextElement('ElementsGeneriques/T-RechercheRapide', request, null)
		sendKeys('ElementsGeneriques/T-RechercheRapide', [Keys.ENTER], null)

		if(waitForElementPresent('ElementsGeneriques/ClauseWhere/Alert-notFound', 2, FailureHandling.OPTIONAL)){
			click('ElementsGeneriques/ClauseWhere/B-OKnotFound', null)
			log.logError("L'objet : "+request +" n'a pu etre trouv�")
			return false
		}else{
			log.logInfo("L'objet : "+request +" existe")
			return true
		}
	}


	/**
	 * Permet de simuler le fait d'appuyer sur le bouton de cr�ation d'un nouvel objet qq soit la page ou on se trouve
	 */
	@Keyword
	def creerNouvelElement(){
		click('ElementsGeneriques/BarreOutils/B-Nouveau', null)
	}
	/**
	 * Valide la valeur d'un objet � celle attendue. Change la couleur du champ en fonction du r�sultat (vert pour succ�s, rouge pour erreur)
	 * @param pathObject - Objet dont on veut v�rifier la valeur
	 * @param compare - String contenant la valeur attendue
	 */
	@Keyword
	def valideInputcoordonne(String sObjet, String sCompare, boolean bById, String sAttribute){
		WebElement e

		//Si l'�l�ment est trouv� par ID ou par Path
		if(bById) {
			e =	DF.getWebDriver().findElement(By.id(sObjet))
		} else {
			waitForElementPresent(sObjet, 5, null)
			e = DF.getWebDriver().findElement(By.id(findTestObject(sObjet).findPropertyValue("id")))
		}
		WebUI.delay(1)
		String coordonnestr = e.getAttribute(sAttribute).replace(",", ".")
		coordonnestr.replaceAll(" ","")
		String coordonne = coordonnestr
		println(coordonne)
		//Si le texte de l'�l�ment contient la bonne valeur, colore l'�l�ment en vert sinon en rouge
		if(sCompare == "") {
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(e))
			logPassed("L'élément contient la bonne valeure qui est : null")
			return true

		}else if(!e.getAttribute(sAttribute).isEmpty() && WebUI.verifyMatch(coordonne, "^" + sCompare + (sAttribute=='innerHTML'?'\\S*$':'$'), true, FailureHandling.CONTINUE_ON_FAILURE)){

			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(e))
			logPassed("L'élément contient la bonne valeure qui est :" + sCompare)
			return true

		}else{
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(e))
			//logError("Erreur avec la valeur: "+ sCompare)
			return false
		}
	}

	/**
	 * Valide la valeur d'un objet � celle attendue. Change la couleur du champ en fonction du r�sultat (vert pour succ�s, rouge pour erreur)
	 * @param pathObject - Objet dont on veut v�rifier la valeur
	 * @param compare - String contenant la valeur attendue
	 */
	@Keyword
	def valideInput(String sObjet, String sCompare, boolean bById, String sAttribute){
		WebElement e

		//Si l'�l�ment est trouv� par ID ou par Path
		if(bById) {
			e =	DF.getWebDriver().findElement(By.id(sObjet))
		} else {
			waitForElementPresent(sObjet, 5, null)
			e = DF.getWebDriver().findElement(By.id(findTestObject(sObjet).findPropertyValue("id")))
		}
		println(e)
		WebUI.delay(1)
		//Si le texte de l'�l�ment contient la bonne valeur, colore l'�l�ment en vert sinon en rouge
		if(sCompare == "") {
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(e))
			logPassed("L'élément contient la bonne valeure qui est : null")
			return true

		}else if(!e.getAttribute(sAttribute).isEmpty() && WebUI.verifyMatch(e.getAttribute(sAttribute), "^" + sCompare + (sAttribute=='innerHTML'?'\\S*$':'$'), true, FailureHandling.CONTINUE_ON_FAILURE)){
			println(e.getAttribute(sAttribute))
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'green';", Arrays.asList(e))
			logPassed("L'élément contient la bonne valeure qui est :" + sCompare)
			return true

		}else{
			WebUI.executeJavaScript("arguments[0].style.backgroundColor = 'red';", Arrays.asList(e))
			//logError("Erreur avec la valeur: "+ sCompare)
			return false
		}
	}


	/**
	 * Actualise la page en appuyant sur le bouton Actualiser
	 */
	@Keyword
	def refreshPage(){
		click("ElementsGeneriques/BarreOutils/B-Actualiser", null)
	}


	/**
	 * G�n�re un rapport sous la forme d'un PDF
	 */
	@Keyword
	def generePDF(){
		click("ElementsGeneriques/BarreOutils/B-GenerePDF", null)
	}


	/**
	 * G�n�re un rapport dans un fichier Excel
	 */
	@Keyword
	def genereExcel(){
		click("ElementsGeneriques/BarreOutils/B-GenereExcel", null)
	}


	/**
	 * Obtient propri�t� sur l'objet courant (Utilis� pour bugfix de changement de statut d'intervention
	 */
	@Keyword
	def obtientPropriete(){
		click("ElementsGeneriques/BarreOutils/B-GetPropriete", null)
	}


	/**
	 * Clique sur un �l�ment
	 * @param objectPath Chemin de l'�l�ment
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 */
	@Keyword
	def click(String objectPath, FailureHandling fh){
		boolean bLoop = true

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				WebUI.click(findTestObject(objectPath), fh)
				bLoop = false
			}catch(Exception ex){}
		}
	}


	/**
	 * Clique sur un �l�ment
	 * @param objectPath Chemin de l'�l�ment
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 */
	@Keyword
	def clickObject(TestObject testObject, FailureHandling fh){
		boolean bLoop = true

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				WebUI.click(testObject, fh)
				bLoop = false
			}catch(Exception ex){}
		}
	}


	/**
	 * Attend qu'un �l�ment soit pr�sent dans la fen�tre
	 * @param objectPath Chemin de l'�l�ment
	 * @param delay Temps d'attente maximal pour que l'�l�ment soit pr�sent
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 * @return bool�en disant si l'�l�ment est apparu dans le d�lais pr�vu ou non
	 */
	@Keyword
	def waitForElementPresent(String objectPath, int delay, FailureHandling fh) {
		boolean bLoop = true
		boolean bReturn

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				bReturn = WebUI.waitForElementPresent(findTestObject(objectPath), delay, fh)
				bLoop = false
			}catch(Exception ex){}
		}

		return bReturn
	}


	/**
	 * Coche une case/bouton radio
	 * @param objectPath Chemin de l'�l�ment
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 */
	@Keyword
	def check(String objectPath, FailureHandling fh) {
		boolean bLoop = true

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				WebUI.check(findTestObject(objectPath))
				bLoop = false
			}catch(Exception ex){}
		}
	}


	/**
	 * Appuie sur des touches du clavier
	 * @param objectPath Chemin de l'�l�ment
	 * @param keys Touches � appuyer (1 ou 2)
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 */
	@Keyword
	def sendKeys(String objectPath, ArrayList keys, FailureHandling fh) {
		boolean bLoop = true

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				if(keys.size() == 1)
					WebUI.sendKeys(findTestObject(objectPath), Keys.chord(keys.get(0)), fh)
				else
					WebUI.sendKeys(findTestObject(objectPath), Keys.chord(keys.get(0), keys.get(1)), fh)
				bLoop = false
			}catch(Exception ex){}
		}
	}


	/**
	 * Attends qu'un �l�ment ait une certaine valeur
	 * @param objectPath Chemin de l'�l�ment
	 * @param text Valeur attendue
	 * @param delay D�lais d'attente maximal
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 * @return Retourne si l'�l�ment a la valeur souhait�e
	 */
	@Keyword
	def waitForValue(String objectPath, String text, int delay, FailureHandling fh) {
		boolean bLoop = true
		boolean bReturn

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				bReturn = WebUI.waitForElementAttributeValue(findTestObject(objectPath), 'value', text, delay)
				bLoop = false
			}catch(Exception ex){}
		}

		return bReturn
	}


	/**
	 * Attend qu'un �l�ment ne soit plus pr�sent
	 * @param objectPath Chemin de l'�l�ment
	 * @param delay D�lais d'attente maximal
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 * @return true si l'�l�ment n'est plus pr�sent
	 */
	@Keyword
	def waitForNotPresent(String objectPath, int delay, FailureHandling fh) {
		boolean bLoop = true
		boolean bReturn

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				bReturn = WebUI.waitForElementNotPresent(findTestObject(objectPath), delay)
				bLoop = false
			}catch(Exception ex){}
		}

		return bReturn
	}


	/**
	 * D�place la souris au dessus d'un �l�ment
	 * @param objectPath Chemin de l'�l�ment
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 */
	@Keyword
	def mouseOver(String objectPath, FailureHandling fh) {
		boolean bLoop = true

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				WebUI.mouseOver(findTestObject(objectPath))
				bLoop = false
			}catch(Exception ex){}
		}
	}


	/**
	 * Attend qu'un �l�ment devienne visible
	 * @param objectPath Chemin de l'�l�ment
	 * @param delay D�lais d'attente maximal
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 * @return true si l'�l�ment est devenu visible
	 */
	@Keyword
	def waitForVisible(String objectPath, int delay, FailureHandling fh) {
		boolean bLoop = true
		boolean bReturn

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				bReturn = WebUI.waitForElementVisible(findTestObject(objectPath), delay, fh)
				bLoop = false
			}catch(Exception ex){}
		}

		return bReturn
	}


	/**
	 * V�rifie que l'�l�ment ait une certaine valeur
	 * @param objectPath Chemin de l'�l�ment
	 * @param delay D�lais d'attente maximal
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 * @return true si l'�l�ment contient la bonne valeur
	 */
	@Keyword
	def verifyElementValue(String objectPath, String name, String value, int delay, FailureHandling fh) {
		boolean bLoop = true
		boolean bReturn

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				bReturn = WebUI.verifyElementAttributeValue(findTestObject(objectPath),name, value, delay, fh)
				bLoop = false
			}catch(Exception ex){}
		}
		return bReturn
	}


	/**
	 * V�rifie que l'�l�ment soit pr�sent
	 * @param objectPath Chemin de l'�l�ment
	 * @param delay D�lais d'attente maximal
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 * @return true si l'�l�ment est pr�sent
	 */
	@Keyword
	def verifyElementPresent(String objectPath, int delay, FailureHandling fh) {
		boolean bLoop = true
		boolean bReturn
		if(fh == null) {
			fh = FailureHandling.STOP_ON_FAILURE
		}
		while(bLoop) {
			try {
				bReturn = WebUI.verifyElementPresent(findTestObject(objectPath), delay, fh)
				bLoop = false
			}catch(Exception ex){}
		}
		return bReturn
	}


	/**
	 * V�rifie que l'�l�ment ne soit pas pr�sent
	 * @param objectPath Chemin de l'�l�ment
	 * @param delay D�lais d'attente maximal
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 * @return true si l'�l�ment n'est pas pr�sent
	 */
	@Keyword
	def verifyElementNotPresent(String objectPath, int delay, FailureHandling fh) {
		boolean bLoop = true
		boolean bReturn

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				bReturn = WebUI.verifyElementNotPresent(findTestObject(objectPath), delay, fh)
				bLoop = false
			}catch(Exception ex){}
		}
		return bReturn
	}


	/**
	 * Attend que l'�l�ment soit cliquable
	 * @param objectPath Chemin de l'�l�ment
	 * @param delay D�lais d'attente maximal
	 * @param fh Objet FailureHandling contenant l'action � effectuer si la commande cause une erreur
	 * @return true si l'�l�ment est cliquable
	 */
	@Keyword
	def waitForClickable(String objectPath, int delay, FailureHandling fh) {
		boolean bLoop = true
		boolean bReturn

		if(fh == null)
			fh = FailureHandling.STOP_ON_FAILURE

		while(bLoop) {
			try {
				bReturn = WebUI.waitForElementClickable(findTestObject(objectPath), delay, fh)
				bLoop = false
			}catch(Exception ex){}
		}
		return bReturn
	}


	/**
	 * Cette fonction permet de convertir un WebElement en un TestObject en utilisant le id de l'objet
	 * @param id, représente le id de l'objet
	 * @return
	 */
	@Keyword
	def convertWebElement (String id){
		// On crée un XPath en utilisant le id
		String xpath = '//*[@id="' + id  + '"]'
		// Créer un objet avec le XPath
		TestObject object = new TestObject("objectName")
		object.addProperty("xpath", ConditionType.EQUALS, xpath)
		return object
	}


	/**
	 * Fonction qui permet de reouvrir Maximo avec une code U différent
	 * @param codeU, code U de l'utilisateur désiré
	 *
	 * @return
	 */
	def reouvrirMaximo(String codeU){
		//Se déconnecter
		click('Object Repository/P-Accueil/Navbar/B-Logout',null)
		//Connexion avec l utilisateur du test en cours et informations sur le test en cours
		setTextElement('P-Login/T-Username', codeU, null)
		WebUI.setMaskedText(findTestObject('P-Login/T-Passwd'), "maximo")
		WebUI.delay(1)
		click('P-Login/B-Login', null)
		WebUI.waitForPageLoad(5)
	}


	/**
	 * Fonction qui permet de choisir l'url approprié 
	 * 
	 * @param site
	 * @return site, url
	 */
	@Keyword
	def selectSite(String site) {
		switch(site.toUpperCase()) {
			case 'PROD':
				return 'http://seis-mxprd/maximo/webclient/login/login.jsp'
				break
			case 'ACC04':
				return 'http://seis-mxacc04/maximo/webclient/login/login.jsp'
				break
			case 'TST01':
				return 'http://seis-mxtst01.step-mtl.qc.ca/maximo/webclient/login/login.jsp'
				break
			case 'TST03':
				return 'http://seis-mxtst03.step-mtl.qc.ca/maximo/webclient/login/login.jsp'
				break
			case 'DEV03':
				return 'http://seis-mxdev03/maximo/webclient/login/login.jsp'
				break
			case 'DEV04':
				return 'http://seis-mxdev04/maximo/webclient/login/login.jsp'
				break
			case 'DEV06':
				return 'http://seis-mxdev06/maximo/webclient/login/login.jsp'
				break
			case 'TST06':
				return 'http://seis-mxtst06.step-mtl.qc.ca/maximo/webclient/login/login.jsp'
				break
			case 'ACC06':
				return 'http://seis-mxacc06.step-mtl.qc.ca/maximo/webclient/login/login.jsp'
				break
			case 'INT':
				return 'http://seis-mxint.step-mtl.qc.ca:9082/maximo/ui/'
				break
			case 'DEV01':
				return 'http://seis-mxdev01.step-mtl.qc.ca/maximo/webclient/login/login.jsp'
				break
			default:
				return site
				break
		}
	}
}
