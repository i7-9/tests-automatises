package maximo

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.regex.Pattern
import java.util.regex.Matcher

import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.webui.driver.DriverFactory as DF
import java.util.ArrayList
import java.time.LocalDate
import com.kms.katalon.core.logging.KeywordLogger
import java.time.format.DateTimeFormatter
import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.util.internal.JsonUtil as JsonUtil
import com.google.gson.Gson as Gson
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import javax.swing.Box;
import javax.swing.JFrame
import javax.swing.JLabel;
import javax.swing.JOptionPane
import javax.swing.JPanel;
import javax.swing.JTextField

import internal.GlobalVariable as GlobalVariable

//TODO: pour chaque ajout on doit vérifier que c'est la derniere transaction de l'actif si un autre existe on verifie seulement l'emplacement
public class Geomatique {
	private GeneralKeys gk = new GeneralKeys()
	private ActifsKeys ak = new ActifsKeys()
	private WebDriver driver = DriverFactory.getWebDriver()
	private KeywordLogger log = new KeywordLogger()
	/**
	 * Permet de valider si l'actif fait partie d'un arrondissement intégré à Maximo
	 * @param arrondissement
	 * @return
	 */
	def estArrondissementMaximo(double arrondissement) {
		//TODO : @mathieu,savoie : stp faire en sorte d'avoir des variables pour les arrondissements concernés par la validation
		boolean isInMaximo = false
		if(arrondissement == 14 || arrondissement == 17 || arrondissement == 20 || arrondissement ==16) {
			isInMaximo = true
		}
		return isInMaximo
	}
	/**
	 * 
	 * @param Actif 
	 * @param currentRow
	 * @param Type_Actif
	 * @return
	 */
	def insertion(Map Actif_specs,int currentRow, String Type_Actif,int verification_visuel) {


		String resultat_actif //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement
		String SQLRequest //Recoit la demande SQL à envoyer dans Maximo
		boolean resultat_clause //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement
		def territoire


		resultat_clause = true
		//Vérification des informations d'arrondissement pour l'actif

		if(currentRow==1) {
			SQLRequest = "siteid = 'TP' and uuid = '"+Actif_specs[0].features[0].properties.UUID+"'"
			resultat_clause =gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
			if(resultat_clause == true) {resultat_clause = ak.valideinfoarrondissementvisuel(Actif_specs,currentRow, Type_Actif)}
		}
		else {
			SQLRequest = ak.genererSqlInformationsArrondissement(Actif_specs,currentRow,Type_Actif)
			resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
		}
		if(resultat_clause == false) {resultat_actif = "ASSET"}
		//Si le résultat de la clause where est true on peut donc valider les specifications
		if(resultat_clause == true) {

			SQLRequest = ak.genererSqlInformationsspecifications(Actif_specs,currentRow,verification_visuel,Type_Actif)
			ak.accederAActifs()
			if(SQLRequest !='true' && SQLRequest!='false') {
				resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
			}
			else if(SQLRequest=='false') {
				resultat_clause = false
			}
			if(Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE==null) {
				Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE=''
			}
			territoire = Actif_specs[0].features[0].properties.TERRITOIRE_R as int
			if(Type_Actif=='AQU_VANNE_P') {
				resultat_clause = ak.Validation_emplacement(territoire,Type_Actif,Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,Actif_specs[0].features[0].properties.FONCTIONVANNE_R,Actif_specs[0].features[0].properties.UUID)
			}
			else if(Type_Actif == '') {
				resultat_clause = ak.Validation_emplacement(territoire,Type_Actif,Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,Actif_specs[0].features[0].properties.TYPESEGMENT_R,Actif_specs[0].features[0].properties.UUID)
			}
			else {
				resultat_clause = ak.Validation_emplacement(territoire,Type_Actif,Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,"",Actif_specs[0].features[0].properties.UUID)
			}

		}
		return resultat_actif
	}
	/**
	 * 
	 * @param Actif
	 * @param currentRow
	 * @param Type_Actif
	 * @param verification_visuel
	 * @return
	 */
	def modification_attribut(Map Actif_specs,int currentRow, String Type_Actif,int verification_visuel, String champmodifies){
		/*Recoit le résultat de la validation des spécifications de l'actif*/boolean resultat_clause
		String resultat_actif //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement
		/*Recoit la demande SQL à envoyer dans Maximo*/String SQLRequest
		def territoire

		resultat_clause = true
		//Vérification des informations d'arrondissement pour l'actif

		if(currentRow==1) {
			SQLRequest = "siteid = 'TP' and uuid = '"+Actif_specs[0].features[0].properties.UUID+"'"
			resultat_clause =gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
			if(resultat_clause == true) {resultat_clause = ak.valideinfoarrondissementvisuel(Actif_specs,currentRow, Type_Actif)}
		}
		else {
			SQLRequest = ak.genererSqlInformationsArrondissement(Actif_specs,currentRow,Type_Actif)
			resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
		}
		if(resultat_clause == false) {resultat_actif = "ASSET"}
		//Si le résultat de la clause where est true on peut donc valider les specifications
		if(resultat_clause == true) {
			SQLRequest = ak.genererSqlInformationsspecifications(Actif_specs,currentRow,verification_visuel,Type_Actif)
			ak.accederAActifs()
			if(SQLRequest !='true' && SQLRequest!='false') {
				resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
				if(resultat_clause == false) {resultat_actif = "ASSET_SPEC"}
			}
			else if(SQLRequest=='false') {
				resultat_actif = "ASSET_SPEC"
			}
		}
		if(champmodifies.contains('VDM_ACTIF_ASSOCIE')==true) {
			if(Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE==null) {
				Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE=''
			}
			territoire = Actif_specs[0].features[0].properties.TERRITOIRE_R as int
			if(Type_Actif=='AQU_VANNE_P') {
				resultat_clause = ak.Validation_emplacement(territoire,Type_Actif,Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,Actif_specs[0].features[0].properties.FONCTIONVANNE_R,Actif_specs[0].features[0].properties.UUID)
			}
			else if(Type_Actif == '') {
				resultat_clause = ak.Validation_emplacement(territoire,Type_Actif,Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,Actif_specs[0].features[0].properties.TYPESEGMENT_R,Actif_specs[0].features[0].properties.UUID)
			}
			else {
				resultat_clause = ak.Validation_emplacement(territoire,Type_Actif,Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,"",Actif_specs[0].features[0].properties.UUID)
			}
		}
		return resultat_actif
	}
	/**
	 * 
	 * @param Actif_specs
	 * @param currentRow
	 * @param Type_Actif
	 * @return
	 */ 
	def Remplacement(Map Actif_specs,int currentRow, String Type_Actif) {

		String resultat_actif //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement
		/*Recoit la demande SQL à envoyer dans Maximo*/String SQLRequest
		/*Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement*/boolean resultat_clause
		/*Variable temporaire pour l'extraction du geoJson*/String geojson
		def territoire


		resultat_clause = true

		//un actif remplacer va avoir un ancien Id sur lequel il va etre placer(changement de borne donc ajout retrait) Doit être le même emplacement que l'ancien actif)
		//Vérification des informations d'arrondissement pour l'actif
		SQLRequest = ak.genererSqlInformationsArrondissement(Actif_specs,currentRow,Type_Actif)
		resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)

		if(resultat_clause == false) {resultat_actif = "ASSET"}

		//Vérification des specs de l'actif
		if(resultat_clause == true) {

			SQLRequest = ak.genererSqlInformationsspecifications(Actif_specs,currentRow,1,Type_Actif)
			ak.accederAActifs()
			resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)

			if(resultat_clause == false) {
				resultat_actif = "ASSET_SPEC"
			}
		}
		//Vérification de l'emplacement
		if(resultat_clause == true) {if(Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE==null) {Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE=''}

			territoire = Actif_specs[0].features[0].properties.TERRITOIRE_R as int
			resultat_clause = ak.Validation_emplacement(territoire,Type_Actif,Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,"",Actif_specs[0].features[0].properties.UUID)
			if(resultat_clause == false) {
				resultat_actif = "Emplacement"
			}
		}
		return resultat_actif
	}
	/**
	 * 
	 * @param Actif
	 * @param currentRow
	 * @param Type_Actif
	 * @return
	 */
	def agregation(Map Actif_specs,int currentRow, String Type_Actif) {

		String resultat_actif //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement
		/*Recoit la demande SQL à envoyer dans Maximo*/String SQLRequest
		/*Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement*/boolean resultat_clause
		/*Permet de récupérer le numéro de l'actif avant le changement pour l'aggrégation et le scindement*/String Actif_avant
		/*Numéro de ligne de l'actif supprimé par aggrégation ou scindement*/int Num_actif_avant

		def territoire

		resultat_clause = true
		//Actif_avant = Actif_specs.getObjectValue('ID_AVANT',currentRow)
		Num_actif_avant=currentRow+1
		//plusieurs qui sont mis en un(s'assurer que l'id avant est supprimer)
		//Vérification des informations d'arrondissement pour l'actif
		SQLRequest = ak.genererSqlInformationsArrondissement(Actif_specs,currentRow,Type_Actif)
		resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
		if(resultat_clause == false) {resultat_actif = "ASSET"}
		//Vérification des specs de l'actif
		if(resultat_clause == true) {

			SQLRequest = ak.genererSqlInformationsspecifications(Actif_specs,currentRow,1,Type_Actif)
			ak.accederAActifs()
			resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)

			if(resultat_clause == false) {
				resultat_actif = "ASSET_SPEC"
			}
		}
		//Vérification de l'emplacement
		if(resultat_clause == true) {if(Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE==null) {Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE=''}

			territoire = Actif_specs[0].features[0].properties.TERRITOIRE_R as int
			resultat_clause = ak.Validation_emplacement(territoire,Type_Actif,Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,"",Actif_specs[0].features[0].properties.UUID)
			if(resultat_clause == false) {
				resultat_actif = "Emplacement"
			}
		}
		//Valider la suppression de l'actif avant
		/*while (Actif_avant != Actif_specs.getObjectValue('ID_AVANT',Num_actif_avant) && Num_actif_avant != currentRow) {
		 if(Actif_avant != Actif_specs.getObjectValue('ID_AVANT',Num_actif_avant) && Num_actif_avant != currentRow) {Num_actif_avant++}
		 else if(Num_actif_avant == Actif_specs.getObjectValue('ID_AVANT',Num_actif_avant)) {
		 resultat_clause = gk.ouvrirClauseWhereExecuteRequete("uuid = '"+ Actif_specs.getObjectValue('UUID', Num_actif_avant) +"' and siteid = 'TP' and STATUS = 'A' and VDM_ETAT_OPERATION = 'HU' and VDM_DATE_ABANDON is not null")
		 }
		 else if(Num_actif_avant == Actif_specs.getRowNumbers()) {Num_actif_avant=1}
		 else if(Num_actif_avant == currentRow) {return "false"}
		 }*/
		return resultat_actif
	}
	/**
	 * 
	 * @param Actif
	 * @param currentRow
	 * @param Type_Actif
	 * @return
	 */
	def scindement(Map Actif_specs,int currentRow, String Type_Actif) {

		String resultat_actif //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement
		/*Recoit la demande SQL à envoyer dans Maximo*/String SQLRequest
		/*Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement*/boolean resultat_clause
		/*Permet de récupérer le numéro de l'actif avant le changement pour l'aggrégation et le scindement*/String Actif_avant
		/*Numéro de ligne de l'actif supprimé par aggrégation ou scindement*/int Num_actif_avant
		def territoire
		//int maxRows =

		resultat_clause = true
		//une conduite se divise en plusieur, vérifier que l'ancien est supprimer avec l'id avant
		//possible que la suppression soit mauvaise en suppression mais soit bon en scindement
		//Vérification des informations d'arrondissement pour l'actif
		SQLRequest = ak.genererSqlInformationsArrondissement(Actif_specs,currentRow,Type_Actif)
		resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
		if(resultat_clause == false) {resultat_actif = "ASSET"}

		//Vérification des specs de l'actif
		if(resultat_clause == true) {

			SQLRequest = ak.genererSqlInformationsspecifications(Actif_specs,currentRow,1,Type_Actif)
			ak.accederAActifs()
			resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)

			if(resultat_clause == false) {
				resultat_actif = "ASSET_SPEC"
			}
		}
		//Vérification de l'emplacement
		if(resultat_clause == true) {
			if(Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE==null) {
				Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE=''}

			territoire = Actif_specs[0].features[0].properties.TERRITOIRE_R as int
			resultat_clause = ak.Validation_emplacement(territoire,Type_Actif,Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,"",Actif_specs[0].features[0].properties.UUID)
			if(resultat_clause == false) {
				resultat_actif = "Emplacement"
			}
		}
		return resultat_actif
	}
	/**
	 * 
	 * @param Actif
	 * @param currentRow
	 * @param Type_Actif
	 * @return
	 */
	def inversion(Map Actif_specs,int currentRow,String Type_Actif) {


		String resultat_actif //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement
		/*Recoit la demande SQL à envoyer dans Maximo*/String SQLRequest
		/*Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement*/boolean resultat_clause

		resultat_clause = true
		//Vérification des informations d'arrondissement pour l'actif
		SQLRequest = ak.genererSqlInformationsArrondissement(Actif_specs,currentRow,Type_Actif)
		resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
		if(resultat_clause == false) {resultat_actif = "ASSET"}

		//Vérification des specs de l'actif
		if(resultat_clause == true) {

			SQLRequest = ak.genererSqlInformationsspecifications(Actif_specs,currentRow,1,Type_Actif)
			ak.accederAActifs()
			resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)

			if(resultat_clause == false) {
				resultat_actif = "ASSET_SPEC"
			}
		}
		return resultat_actif
	}
	/**
	 * 
	 * @param Actif
	 * @param currentRow
	 * @param Type_Actif
	 * @return
	 */
	def deplacement(Map Actif_specs,int currentRow, String Type_Actif){
		//TODO:faire sur qu'on verifie

		String resultat_actif //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement
		/*Recoit la demande SQL à envoyer dans Maximo*/String SQLRequest
		/*Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement*/boolean resultat_clause
		/*Variable temporaire pour l'extraction du geoJson*/String geojson
		def territoire // variable contenant le numéro du territoire de l'acitf

		resultat_clause = true
		//Vérification des informations d'arrondissement pour l'actif
		SQLRequest = ak.genererSqlInformationsArrondissement(Actif_specs,currentRow,Type_Actif)
		resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
		if(resultat_clause == false) {resultat_actif = "ASSET"}

		//Vérification des specs de l'actif
		if(resultat_clause == true) {

			SQLRequest = ak.genererSqlInformationsspecifications(Actif_specs,currentRow,1,Type_Actif)
			ak.accederAActifs()
			resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)

			if(resultat_clause == false) {
				resultat_actif = "ASSET_SPEC"
			}
		}
		//Vérification de l'emplacement
		if(resultat_clause == true) {
			if(Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE==null) {
				Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE=''}

			territoire = Actif_specs[0].features[0].properties.TERRITOIRE_R as int
			resultat_clause = ak.Validation_emplacement(territoire,Type_Actif,Actif_specs[0].features[0].properties.VDM_ACTIF_ASSOCIE,"",Actif_specs[0].features[0].properties.UUID)
			if(resultat_clause == false) {
				resultat_actif = "Emplacement"
			}
		}

		return resultat_actif
	}
	/**
	 * 
	 * @param Actif
	 * @param currentRow
	 * @param Type_Actif
	 * @return
	 */
	def rebranchement(Map Actif_specs,int currentRow, String Type_Actif){

		String resultat_actif="" //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement
		/*Recoit la demande SQL à envoyer dans Maximo*/String SQLRequest
		/*Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement*/boolean resultat_clause
		resultat_clause = true
		//Vérification des informations d'arrondissement pour l'actif
		SQLRequest = ak.genererSqlInformationsArrondissement(Actif_specs,currentRow,Type_Actif)
		resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
		if(resultat_clause == false) {resultat_actif = "ASSET"}

		//Vérification des specs de l'actif
		if(resultat_clause == true) {

			SQLRequest = ak.genererSqlInformationsspecifications(Actif_specs,currentRow,1,Type_Actif)
			ak.accederAActifs()
			resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)

			if(resultat_clause == false) {
				resultat_actif = "ASSET_SPEC"
			}
		}
		return resultat_actif
	}
	/**
	 * 
	 * @param Actif
	 * @param currentRow
	 * @param Type_Actif
	 * @return
	 */
	def ajustements(Map Actif_specs,int currentRow, String Type_Actif){


		/*Recoit la demande SQL à envoyer dans Maximo*/String SQLRequest
		/*Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement*/boolean resultat_clause
		String resultat_actif //Recoit le résultat de la clause SQL pour la validation des informations de l'arrondissement

		resultat_clause = true
		//Vérification des informations d'arrondissement pour l'actif
		SQLRequest = ak.genererSqlInformationsArrondissement(Actif_specs,currentRow,Type_Actif)
		resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)
		if(resultat_clause == false) {resultat_actif = "ASSET"}

		//Vérification des specs de l'actif
		if(resultat_clause == true) {

			SQLRequest = ak.genererSqlInformationsspecifications(Actif_specs,currentRow,1,Type_Actif)
			ak.accederAActifs()
			resultat_clause = gk.ouvrirClauseWhereExecuteRequete(SQLRequest)

			if(resultat_clause == false) {
				resultat_actif = "ASSET_SPEC"
			}
		}

		return resultat_actif
	}


	def convertXmlToJson(xml) {
		// Parse it
		def parsed = new XmlParser().parseText( xml )

		// Convert it to a Map containing a List of Maps
		def jsonObject = [ ATTRIBUTS: parsed.ATTRIBUT.collect {
				[ (it.@nom) : it.text() ]
			}]

		// And dump it as Json
		def json = new groovy.json.JsonBuilder( jsonObject )

		return json
	}
}
