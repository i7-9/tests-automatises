package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.By
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

public class VisualAssignment {
	private GeneralKeys gk = new GeneralKeys()


	/**
	 * Acceder a l'application Visual Assignment
	 * 
	 */
	@Keyword
	def accederAVS() {
		gk.click('P-Accueil/Navbar/B-AllerA', null)
		gk.mouseOver('P-Accueil/Navbar/AllerA/Interventions/M-InterventionsM', null)
		gk.waitForElementPresent('P-Accueil/Navbar/AllerA/Interventions/M-VisualAssignment', 2, null)
		gk.mouseOver('P-Accueil/Navbar/AllerA/Interventions/M-VisualAssignment', null)
		gk.click('P-Accueil/Navbar/AllerA/Interventions/M-VisualAssignment', null)
	}


	/**
	 * Fonction qui permet de chercher une intervention dans Visual Assignment dnas la section des intervention ordonnancées
	 * 
	 * @param noIntervention, le numéro de l'intervention désiré
	 * 
	 * @return
	 */
	def rechercheOrd(String noIntervention, String noGO){
		// appuyer sur le filtre
		gk.click('Object Repository/A-VisualAssignment/P-Ordonnance/B-Filtre',null)
		// Si on n'a pas de gamme d'opération on recherche le numéro de l'intervention dans le BT
		if (noGO == ""){
			gk.click('Object Repository/A-VisualAssignment/P-Ordonnance/T-RechercheBT',null)
			gk.forceSetTextElement('Object Repository/A-VisualAssignment/P-Ordonnance/T-RechercheBT', noIntervention)
			WebUI.sendKeys(findTestObject('Object Repository/A-VisualAssignment/P-Ordonnance/T-RechercheBT'), Keys.chord(Keys.ENTER))
			// Sinon on le recherche dans parent
		}else {
			// entrer le numéro d'intervention et appuyer sur Entrer
			gk.click('Object Repository/A-VisualAssignment/P-Ordonnance/T-RechercheParent',null)
			gk.forceSetTextElement('Object Repository/A-VisualAssignment/P-Ordonnance/T-RechercheParent', noIntervention)
			WebUI.sendKeys(findTestObject('Object Repository/A-VisualAssignment/P-Ordonnance/T-RechercheParent'), Keys.chord(Keys.ENTER))
		}
	}


	/**
	 * Fonction qui permet de retourner une liste qui contient le numéro d'assignation de la première tâche et de la dernière tâche
	 * 
	 * @param nbActivite, le nombre d'activité dans l'intervention  qui représente le nombre de tâche
	 * 
	 * @return minMaxAssID, le numéro d'assignation de la première tâche et de la dernière tâche
	 */
	def retournerListAssigementID(String noGo){
		WebUI.delay(1)
		String assgID
		// Création d'une liste regroupant tout element web qui ont comme class "WorkOrder"
		List<WebElement> workOrder = gk.findElementsBy(By.className("WorkOrder"))
		// Liste qui contient le numéro d'assignation de toutes les tâches
		ArrayList<String> listAssID = new ArrayList<String>()
		// Liste qui contient le numéro d'assignation de la première et la dernière tache
		ArrayList<String> minMaxAssID = new ArrayList<String>()
		WebElement currentWebOrder
		String id
		int i = 0
		// Remplir la liste de numéros d'assignation pour toutes les tâches
		while (i<workOrder.size()){
			currentWebOrder = workOrder.get(i)
			id = currentWebOrder.getAttribute("id")
			WebUI
			String [] listId = id.split("|")
			if (noGo == ""){
				assgID = ""+id.substring(10)
			}else{
				assgID = ""+id.substring(8)
			}
			listAssID.add(assgID)
			i++
		}
		return listAssID
	}


	/**
	 * Fonction qui gère l'assignation de la main d'oeuvre aux différentes tâches
	 * 
	 * @param assgID
	 * @return
	 */
	def assigner(ArrayList<String> assgID){
		boolean loop = true
		String idAssg
		boolean click
		TestObject objectAssg
		// représente le numéro de l'assignation de la première tâche
		int min = Integer.parseInt(assgID.get(0))
		int i = 0
		int m = min-1
		WebUI.delay(1)

		while (loop){
			// Essayer d'assigner la tâche
			try{
				idAssg = m.toString()
				try{
					// On crée une testObject de l'icone de l'assignation et on clique dessus
					objectAssg = gk.convertWebElement("IMG_assignment-"+m)
					WebUI.delay(1)
					click = afficherMenuAssg(objectAssg)
					m++
				}catch (Exception a){
					// Si on trouve pas l'icone de la tache on clique sur l'icone suivante (La parente) et on revient pour cliquer sur la précédente
					m++
					try{
						objectAssg = gk.convertWebElement("IMG_assignment-"+m)
						click = afficherMenuAssg(objectAssg)
						if(click == true){
							m = m-1
						}
					}catch (Exception b){
						m++
						// Si on trouve pas l'icone de la tache on clique sur l'icone suivante (La parente) et on revient pour cliquer sur la précédente
						objectAssg = gk.convertWebElement("IMG_assignment-"+m)
						click = afficherMenuAssg(objectAssg)
						if(click == true){
							m = m-2
						}
					}
				}
			}catch (Exception s){
				loop = false
			}
		}
	}


	/**
	 * fonction qui permet de cliquer sur les icones des tâches qui n'ont pas encore été assignées 
	 * 
	 * @param object représente l'objet sur lequel on veut cliquer
	 * @return click, représente un boolean qui sera égal à true si on a fait un right click sinon à false
	 */
	def afficherMenuAssg(TestObject object){
		boolean click = false
		boolean loop = true
		// Src de l'objet pour determiner si la tâche a été assignée ou non
		String src = WebUI.getAttribute(object, "src")
		String [] listSrc = src.split("/")
		// Si la tâche n'a pas encore été assignée on l'assigne en choissisant une main d'oeuvre
		if (listSrc[(listSrc.size()-1)] == "unassigned.gif" || listSrc[(listSrc.size()-1)] == "unassigned_multiple.gif"){
			// On répète le precessus tant que la tâche n'a pas été assigée
			while ((listSrc[(listSrc.size()-1)] == "unassigned.gif" || listSrc[(listSrc.size()-1)] == "unassigned_multiple.gif")|| loop == true){
				try{
					WebUI.delay(1)
					WebUI.click(object)
					// cliquer sur le bouton affecter à
					gk.click('Object Repository/A-VisualAssignment/P-Ordonnance/F-MenuCliqueDroit/B-AffecterA',null)
					WebUI.delay(1)
					//Selectionner une main d'oeuvre
					selectionnerMainOeuvre()
					// Src de l'objet pour determiner si la tâche a été assignée ou non
					src = WebUI.getAttribute(object, "src")
					listSrc = src.split("/")
					click = true
					loop = false
				}catch (Exception e){}
			}
		}
		return click
	}


	/**
	 * fonction qui permet selectionner la main d'oeuvre qui sera assignée à la tâche
	 * 
	 * @return
	 */
	def selectionnerMainOeuvre(){
		// On choisi une main d'oeuvre parmi la liste de main d'oeuvre qui sont en vert
		List<WebElement> greenCell = gk.findElementsBy(By.className("GreenCell"))
		int maindOeuvre= Math.floor(Math.random() * ((10-1) - 1 + 0)) + 0
		greenCell.get(maindOeuvre).click()
	}


	/**
	 * Fonction qui permet de récupéré la date de debut planifie dans les tâches 
	 * 
	 * @return dateDebut, correspond à la date planifiée de la tâche
	 */
	def recupererDateDebutPlanifie(){
		// Cliquer sur l'onglet Plan
		gk.click('A-SuiviInterventions/P-Int/Tabs/Plans', null)
		// Aller àâche et afficher le details de la tâche
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/B-AfficherDetails',null)
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/T-DateDebutPlan',null)
		// Enregistrer la date de debut
		String dateDebut = WebUI.getAttribute(findTestObject('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/T-DateDebutPlan'),"value")
		gk.click('Object Repository/A-SuiviInterventions/P-Int/Plans Tab/Tache/B-AfficherDetails',null)
		return dateDebut
	}


	/**
	 * Fontion qui permet d'envoyer la date de debut planifié 
	 * 
	 * @param date représente la date de début planifié
	 * @param path l'adresse de la case pour la date planifiée
	 * @return
	 */
	def envoyerDebutPLan(String date, String path){
		// Aller dans l'onglet intervention
		gk.click('A-SuiviInterventions/P-Int/Tabs/Intervention',null)
		// Insérer la date planifiée et appuyer sut TAB
		gk.click(path,null)
		gk.forceSetTextElement(path, date)
		WebUI.sendKeys(findTestObject(path), Keys.chord(Keys.TAB))
		WebUI.delay(3)
	}
}
