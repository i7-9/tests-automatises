package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.By
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

public class VisualScheduler {
	private GeneralKeys gk = new GeneralKeys()
	private int dayToGo = 0
	private int heuresTotales = 0
	private int minutesTotales = 0


	/**
	 * Accder à l'application Visual Scheduler
	 */
	@Keyword
	def accederAVS() {
		gk.click('P-Accueil/Navbar/B-AllerA', null)
		gk.mouseOver('P-Accueil/Navbar/AllerA/Interventions/M-InterventionsM', null)
		gk.waitForElementPresent('P-Accueil/Navbar/AllerA/Interventions/M-VisualScheduler', 2, null)
		gk.mouseOver('P-Accueil/Navbar/AllerA/Interventions/M-VisualScheduler', null)
		gk.click('P-Accueil/Navbar/AllerA/Interventions/M-VisualScheduler', null)
	}


	/**
	 * Drag les tâche de la GO pour les assigner
	 * @param arrayTaches - Liste des tâches de la GO
	 * @param arrayDurees - Liste des durées des tâches de la GO
	 */
	@Keyword
	def assigneTaches(ArrayList<String> arrayTaches, ArrayList<String> arrayDurees) {
		Actions act
		WebDriver driver = DriverFactory.getWebDriver()

		//Affiche les textboxs de filtre
		gk.click("A-VisualScheduler/P-Plan/NonPlanifie/Img-Filtre", null)
		WebUI.delay(1)
		gk.click("A-VisualScheduler/P-Plan/Planifie/Img-Filtre", null)

		//Assigne les différentes tâches
		for(int i=0; i<arrayTaches.size(); i++) {
			//Filtres les éléments
			gk.forceSetTextElement("A-VisualScheduler/P-Plan/Planifie/T-BT", arrayTaches.get(i))
			gk.sendKeys("A-VisualScheduler/P-Plan/Planifie/T-BT", [Keys.ENTER], null)
			gk.forceSetTextElement("A-VisualScheduler/P-Plan/NonPlanifie/T-BT", arrayTaches.get(i))
			gk.sendKeys("A-VisualScheduler/P-Plan/NonPlanifie/T-BT", [Keys.ENTER], null)

			WebUI.delay(2)

			//Vérifie que la tâche peut être assignée, si oui l'assigne, si non augmente la journée d'assignation courante
			if(verifieHeure(arrayDurees.get(i))) {
				//Crée affectation
				act = new Actions(driver)
				act.dragAndDrop(
						driver.findElement(By.id('IMG_WO|'+ arrayTaches.get(i) +'|TP_Head13')),
						driver.findElement(By.id('unschedgrid_filtercol_'+dayToGo))
						).build().perform()
			} else {
				dayToGo++
				heuresTotales = 0
				minutesTotales = 0
			}
		}
	}

	/**
	 * Vérifie si la tâche peut être assignée
	 * @param duree - Duree de la tâche qu'on veut assigner
	 */
	@Keyword
	def verifieHeure(String duree) {
		WebDriver driver = DriverFactory.getWebDriver()
		WebElement elementTotal = driver.findElement(By.id('null|PREPAE|null-' + dayToGo))
		List<WebElement> listTotal = elementTotal.findElements(By.tagName("div"))
		WebElement innerDivTotal = listTotal.get(0)
		int totalHeure = 0
		int totalMin = 0
		int dureeHeure = 0
		int dureeMin = 0

		//Si la case de temps total pour la journée est vide, on ne peut assigner de tâche cette journée là
		if(innerDivTotal.getAttribute("innerHTML") == "") {
			return false
		}

		totalHeure = Integer.parseInt(innerDivTotal.getAttribute("innerHTML").split(":")[0])
		totalMin = Integer.parseInt(innerDivTotal.getAttribute("innerHTML").split(":")[1])
		dureeHeure = Integer.parseInt(duree.split(":")[0])
		dureeMin = Integer.parseInt(duree.split(":")[1])

		//Vérifie s'il y a un incrément d'heure dû aux minutes
		if(minutesTotales + dureeMin >= 60) {
			minutesTotales = minutesTotales + dureeMin - 60
			heuresTotales++
		} else {
			minutesTotales += dureeMin
		}

		//Additionne l'heure de la durée de la tâche
		heuresTotales += dureeHeure

		//Renvoie true si la tâche entre à l'horaire, false si la tâche ne rentre pas
		if(minutesTotales > totalMin) {
			return totalHeure - heuresTotales - 1 >= 0
		} else {
			return totalHeure - heuresTotales >= 0
		}
	}


	/**
	 * Fonction qui permet d'ouvrir le plan de travail dans Visual scheduler pour une certaine intervention 
	 * 
	 * @param noIntervention représente le numéro de l'intervention désirée
	 * @return
	 */
	def ouvrirPlanTravail(String noIntervention){
		gk.click('Object Repository/A-VisualScheduler/T-noIntervention', null)
		WebUI.sendKeys(findTestObject('Object Repository/A-VisualScheduler/T-noIntervention'), Keys.chord(Keys.ENTER))
		gk.click('Object Repository/A-VisualScheduler/T-ResultatRecherche', null)
		gk.click('Object Repository/A-VisualScheduler/O-PlanDeTravail/B-PlanDeTravail',null)
	}


	/** 
	 * Fonction qui permer de rechercher une intervvention dans les interventions non ordonnancées
	 * 
	 * @param noIntervention
	 * @return
	 */
	def rechercheNonOrd(String noIntervention, String noGO){
		// appuyer sur le filtre
		gk.click('Object Repository/A-VisualScheduler/O-PlanDeTravail/P-Non-ordonnance/B-Filtre',null)
		if (noGO == ""){
			// entrer le numéro d'intervention dans le champs parent et appuyer sur la touche entrer
			gk.click('Object Repository/A-VisualScheduler/O-PlanDeTravail/P-Non-ordonnance/T-BT',null)
			gk.forceSetTextElement('Object Repository/A-VisualScheduler/O-PlanDeTravail/P-Non-ordonnance/T-BT', noIntervention)
			WebUI.sendKeys(findTestObject('Object Repository/A-VisualScheduler/O-PlanDeTravail/P-Non-ordonnance/T-BT'), Keys.chord(Keys.ENTER))
		}else {
			// entrer le numéro d'intervention dans le champs parent et appuyer sur la touche entrer
			gk.click('Object Repository/A-VisualScheduler/O-PlanDeTravail/P-Non-ordonnance/T-Parent',null)
			gk.forceSetTextElement('Object Repository/A-VisualScheduler/O-PlanDeTravail/P-Non-ordonnance/T-Parent', noIntervention)
			WebUI.sendKeys(findTestObject('Object Repository/A-VisualScheduler/O-PlanDeTravail/P-Non-ordonnance/T-Parent'), Keys.chord(Keys.ENTER))
		}

	}


	/**
	 * Fonction qui permet d'ordonnancer l'intervention en la glissant dans la date appropriée
	 * @param noIntervention
	 * @param arrayNoActivite
	 * @return
	 */
	def ordonnancerIntervention(ArrayList<String> arrayNoActivite, String noIntervention){
		boolean loop = true
		boolean loop2
		int i = 0
		TestObject tache
		// Choisir une position aléatoire de la 3eme semaine
		int pos = Math.floor(Math.random() * ((11-1) - 8 + 1)) + 8
		// Ordonnancer toutes les tâches reliées à l'intervention
		while (loop){
			try{
				WebUI.delay(3)
				if (arrayNoActivite.size > 0){
					tache = gk.convertWebElement("IMG_WO|"+arrayNoActivite.get(i)+"|TP_Head12")
				}else{
					tache = gk.convertWebElement("IMG_WO|"+noIntervention+"|TP_Head12")
				}
				TestObject posFinale = gk.convertWebElement("unschedgrid_filtercol_"+pos)
				loop2 = true
				// Déplacer l'icone pour l'ordonnancement
				WebUI.dragAndDropToObject(tache, posFinale)
				// S'il ya une message d'erreur on clique sur fermer, on attend 200 secondes, on actualise on réssaye d'ordonnancer la tâche
				while(WebUI.waitForElementPresent(findTestObject('Object Repository/MS-MessageSystem/B-Fermer'), 1)){
					gk.click('Object Repository/MS-MessageSystem/B-Fermer',null)
					WebUI.delay(200)
					gk.click('Object Repository/A-VisualScheduler/B-Actualiser',null)
					WebUI.delay(15)
					WebUI.dragAndDropToObject(tache, posFinale)
				}
				i++
			}catch (Exception e){

				loop = false
			}

		}
	}
}