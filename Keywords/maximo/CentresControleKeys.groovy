package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class CentresControleKeys {
	GeneralKeys gk = new GeneralKeys()
	/**
	 * Cliquer sur une application favorite
	 * @param afValue
	 */
	@Keyword
	def clicApplicationFavorite(def afValue){
		if(gk.verifyElementPresent('P-Accueil/CentreControle/ApplicationsFavorites/'+ afValue, 3, FailureHandling.OPTIONAL)){
			gk.click(findTestObject('P-Accueil/CentreControle/ApplicationsFavorites/'+ afValue), null)
		}
	}

	/**
	 * Cliquer sur une insertion rapide
	 * @param irValue
	 */
	@Keyword
	def clicInsertionRapide(def irValue){
		if(gk.verifyElementPresent('P-Accueil/CentreControle/InsertionsRapides/'+ irValue, 3, FailureHandling.OPTIONAL)){
			gk.click('P-Accueil/CentreControle/InsertionsRapides/'+ irValue, null)
		}
	}

	/**
	 * Rafraichir le centre de controle
	 */
	@Keyword
	def clicRefreshCentreControle(){
		WebUI.comment('START OF METHOD')
		if(gk.verifyElementPresent('P-Accueil/CentreControle/B-RafraichirCC', 3, FailureHandling.OPTIONAL)){
			WebUI.comment('IN IF')
			gk.click('P-Accueil/CentreControle/B-RafraichirCC', null)
			WebUI.comment('END OF IF')
		}
		WebUI.comment('END OF METHOD')
	}

	/**
	 * Valider la mise a jour du centre de controle
	 */
	@Keyword
	def confirmAlertRefreshCentreControle(){
		WebUI.comment('START OF METHOD')

		if(gk.verifyElementPresent('P-Accueil/CentreControle/Alert/B-Oui', 3, FailureHandling.OPTIONAL)){
			WebUI.comment('IN IF')
			gk.click('P-Accueil/CentreControle/Alert/B-Oui', null)
			WebUI.comment('AFTER CLICK')
		}
		WebUI.delay(8)
		WebUI.comment('END OF METHOD')
	}

	/**
	 * Refuser la mise a jour du centre de controle
	 */
	@Keyword
	def refuseAlertRefreshCentreControle(){
		WebUI.delay(2)
		if(gk.verifyElementPresent('P-Accueil/CentreControle/Alert/B-Non', 3, FailureHandling.OPTIONAL)){
			gk.click('P-Accueil/CentreControle/Alert/B-Non', null)
		}
		WebUI.delay(8)
	}

	/**
	 * Annuler la mise a jour du centre de controle
	 */
	@Keyword
	def cancelAlertRefreshCentreControle(){
		WebUI.delay(2)
		if(gk.verifyElementPresent('P-Accueil/CentreControle/Alert/B-Annuler', 3, FailureHandling.OPTIONAL)){
			gk.click('P-Accueil/CentreControle/Alert/B-Annuler', null)
		}
		WebUI.delay(8)
	}

	/**
	 * Valide les insertions rapides?
	 * @param datasFile
	 */
	@Keyword
	def validerInsertionsRapides(TestData datasFile){
		'Validation du contenu du portlet : insertion rapide'
	}
}
