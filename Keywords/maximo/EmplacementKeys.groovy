package maximo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class EmplacementKeys {
	/**
	 * Navigue à l'emplacement parent d'un emplacement
	 */
	@Keyword
	def accederEmplacementParent() {
		boolean bLoop = true

		while(bLoop) {
			try {
				WebUI.click(findTestObject("A-Emplacement/Emplacement/B-Parent"))
				WebUI.waitForElementPresent(findTestObject("A-Emplacement/Emplacement/L-LinkEmplacement"), 5)
				WebUI.click(findTestObject("A-Emplacement/Emplacement/L-LinkEmplacement"))
				bLoop = false
			} catch(Exception ex) {}
		}
	}
}
